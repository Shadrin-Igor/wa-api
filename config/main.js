const env = process.env.NODE_ENV;

module.exports = {
  'secret': 'devdacticIsAwesome',
  'baseUrl': 'http://localhost:3004', // 30000
  'publicUrl': env === 'development' ? 'http://localhost:4200' : 'http://web-applications.uz', // 30000
  'fileUrl': env === 'development' ? 'http://localhost:3004' : 'http://web-applications.uz:3004', // 30000
  'publicFolder': 'public/',
  'frontUrls': ['localhost:4200', 'localhost:4201', 'web-applications.uz'],
  "port" : 3004,
};
