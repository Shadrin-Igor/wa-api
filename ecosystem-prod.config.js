module.exports = {
  apps: [{
    'name': 'wa-api-prod',
    'script': './bin/www',
    'watch': false,
    "ignore_watch" : ["uploads", "public/files", "node_modules"],
    /*    'out_file': 'combined.log',
        'error_file': 'combined.log',*/
    'env': {
      'NODE_ENV': 'development',
      'UPLOAD_URL': '/var/www/www-root/web-applications/api/uploads/'
    },
    'env_production': {
      'NODE_ENV': 'production'
    },
  }]
};
