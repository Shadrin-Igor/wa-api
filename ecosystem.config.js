module.exports = {
  apps: [{
    'name': 'wa-api-dev',
    'script': './bin/www',
    'watch': true,
    "ignore_watch" : ["uploads", "public/files", "node_modules", ".idea", ".git"],
    /*    'out_file': 'combined.log',
        'error_file': 'combined.log',*/
    'env': {
      'NODE_ENV': 'development',
      'UPLOAD_URL': '/var/www/www-root/web-applications/api/uploads/'
    },
    'env_production': {
      'NODE_ENV': 'production'
    },
  }]
};
