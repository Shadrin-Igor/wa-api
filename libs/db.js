let fs = require('fs');
let path = require('path');
let Sequelize = require('sequelize');
let {connectDB} = require('api-shared');
console.log('NODE_ENV', process.env.NODE_ENV);
const env = process.env.NODE_ENV;
let dataBase, userName, password, host;

if (env === 'production') {
  host = 'localhost';
  dataBase = 'web-applications';
  userName = 'admin-webapp';
  password = 'nV9kB8bG0exH3p';
} else {
/*  dataBase = 'travelblog';
  userName = 'tbadmin';
  password = '123qwe';*/
  host = '91.240.84.122';
  dataBase = 'web-applications';
  userName = 'admin-webapp';
  password = 'nV9kB8bG0exH3p';
}

const db = connectDB(dataBase, userName, password, host);

fs.readdirSync('./models').forEach((file) => {
  let model = db.sequelize['import'](path.join('../models', file));
  db[model.name] = model;
});

Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

module.exports = db;
