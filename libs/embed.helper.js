let queue = require('queue');
let _ = require('underscore');
let {BaseEmbedHelper} = require('api-shared');

class EmbedHelper extends BaseEmbedHelper {
    getCategory(db, categoryIds, params) {
        const attributes = ['id', ...params.fields];
        console.log('params.fields', params);
        const q = queue({concurrency: 1});
        let categories;
        return new Promise((resolve, reject) => {
            q.push(next => {
                db.Categories.findAll({
                    where: {id: categoryIds},
                    attributes,
                    raw: true
                })
                    .then(categoriesRes => {
                        categories = categoriesRes;
                        next();
                    })
                    .catch((error) => {
                        next(error)
                    });
            });

            q.push(next => {
                db.Categories.findAll({
                    where: {id: categoryIds},
                    attributes,
                    raw: true
                })
                    .then(categoriesRes => {
                        categories = categoriesRes;
                        next();
                    })
                    .catch((error) => {
                        next(error)
                    });
            });

            q.start((error) => {
                if (error) reject(error);
                else resolve(categories);
            });
        });
    }
}

module.exports = new EmbedHelper();
