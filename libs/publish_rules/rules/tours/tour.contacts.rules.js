let globalConstants = require('../../../../constants');
let constants = require('../../constants');

module.exports = {
  list: {
    contacts: {
      info_phone: {
        rule: [constants.NOT_NULL],
        require: true
      },
      info_email: {
        rule: [constants.NOT_NULL],
        require: true
      },
      info_contacts: {
        rule: [constants.NOT_NULL],
      }
    },
  }
};