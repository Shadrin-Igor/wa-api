let globalConstants = require('../../../../constants');
let constants = require('../../constants');

module.exports = {
  list: {
    about: {
      description: {
        rule: [
          { rule: constants.NOT_NULL, require: true },
          { rule: constants.MIN_LENGTH, options: { value: 200 }, require: true }
        ]
      },
      info_hotel_link: {
        rule: [constants.NOT_NULL]
      }
    }
  }
};