let Sequelize = require('sequelize');
let {helper} = require('api-shared');

module.exports = (sequelize, DataTypes) => {

    let Images = sequelize.define('Images', {
            title: {
                type: Sequelize.STRING,
                allowNull: false,
                // unique: true
            },
            image: {
                type: Sequelize.STRING,
                // allowNull: false
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            description_full: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            slug: {
                type: Sequelize.STRING,
            },
            is_public: {
                type: Sequelize.BOOLEAN
            },
            user_id: {
                type: Sequelize.INTEGER
            },
            category_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            views: {
                type: Sequelize.INTEGER
            },
            meta_title: {
                type: Sequelize.STRING
            },
            meta_description: {
                type: Sequelize.TEXT
            },
            meta_keyword: {
                type: Sequelize.TEXT
            }
        }
    );

    Images.beforeValidate((data) => {
        if (!data.slug) {
            data.slug = helper.getSlug(data.title);
        }
    });

    return Images;
};
