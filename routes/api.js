let express = require('express'),
  router = express.Router();

let {checkAccess,
  checkToken,
  errorsRoute,
  authRoute,
  filesRoute,
  dateRoute,
  hashTagsRoute,
  categoriesRoute,
  tagLinksRoute,
  commentsRoute,
  emotionsRoute,
  viewsRoute
} = require('api-shared');

let images = require('./api/images');

router.get('/', (req, res) => {
  res.status(404).json({'error': 'Invalid URL'});
});

router.get('/unauthorized', (req, res) => {
  console.log('----unauthorized');
  res.status(401).json({'error': 'unauthorized'});
});

module.exports = []
  .concat(router)
  .concat(checkToken)
  .concat(checkAccess)
  .concat(errorsRoute)
  .concat(authRoute)
  .concat(images)
  .concat(hashTagsRoute)
  .concat(categoriesRoute)
  .concat(tagLinksRoute)
  .concat(commentsRoute)
  .concat(emotionsRoute)
  .concat(viewsRoute)
  .concat(filesRoute)
  .concat(dateRoute);
