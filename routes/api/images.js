let express = require('express'),
    router = express.Router(),
    queue = require('queue'),
    _ = require('underscore');
let Sequelize = require('sequelize');
let embedHelper = require('../../libs/embed.helper');


let {
    fieldTypes,
    validateTypes,
    helper,
    validatorHelper,
    relationsHelper,
    viewHelper,
    checkJwtToken
} = require('api-shared');


const moduleKey = 'images';
const EMPTY_TAGS = 'EMPTY_TAGS';

router.get(`/${moduleKey}`, (req, res) => {
    const db = req.app.get('db');
    const tagId = req.query.tag_id || '';
    const q = queue({concurrency: 1});
    let data;
    let iDs = [];

    if (tagId) {
        q.push(next => {
            db.Tag_links.findAndCountAll({
                    where: {tag_id: tagId, product_type: 'image'},
                    attributes: ['product_id'],
                    raw: true,
                }
            )
                .then(tagData => {
                    if (tagData.count) {
                        iDs = tagData.rows.map(item => item.product_id);
                    }
                    next();
                })
                .catch(error => {
                    next(error);
                })
        });
    }

    q.push(next => {
        getImagesList(req, null, iDs)
            .then(dataReq => {
                data = dataReq;
                next();
            })
            .catch(error => {
                next(error);
            })
    });

    q.start((error) => {
        if (!error) {
            res.send({status: 200, ...data});
        } else {
            res.status(500).send({
                status: 500,
                error: {
                    error: JSON.stringify(error)
                }
            });
        }
    });
});

router.get(`/${moduleKey}/panel`, checkJwtToken, (req, res) => {
    const q = queue({concurrency: 1});
    let data;
    let iDs = [];

    q.push(next => {
        getImagesList(req, {showAll: true}, iDs)
            .then(dataReq => {
                data = dataReq;
                next();
            })
            .catch(error => {
                console.log('error', error);
                next(error);
            })
    });

    q.start((error) => {
        if (!error) {
            res.send({status: 200, ...data});
        } else {
            res.status(500).send({
                status: 500,
                error: {
                    error: JSON.stringify(error)
                }
            });
        }
    });
});

router.get(`/${moduleKey}/:slug`, (req, res) => {
    const slug = req.params.slug;
    const db = req.app.get('db');

    getImage(req, slug)
        .then(data => {
            res.send({
                status: 200,
                data
            });
        })
        .catch(error => {
            res.status(422)
                .send({status: 422, error: {list: helper.normaliseSequelizeErrors(error.errors)}});
        });

});

router.get(`/${moduleKey}/panel/:id`, (req, res) => {
    const id = req.params.id;

    getImage(req, null, id, true)
        .then(data => {
            res.send({
                status: 200,
                data
            });
        })
        .catch(error => {
            res.status(422)
                .send({status: 422, error: {list: helper.normaliseSequelizeErrors(error.errors)}});
        });

});

router.get(`/${moduleKey}/:id/next-images`, (req, res) => {
    const imageId = req.params.id;
    const db = req.app.get('db');
    const q = queue({concurrency: 1});
    const Op = Sequelize.Op;
    const fields = helper.parseFields(req.query.fields);
    let prevImage;
    let nextImage;
    const attributes = ['id', 'category_id', ...fields.fields];
    const images = [];

    q.push(next => {
        db.Images.findOne({
            where: {
                id: {[Op.gt]: imageId},
                is_public: true
            },
            attributes,
            raw: true,
            limit: 1
        })
            .then((image) => {
                prevImage = image;
                if (image) {
                    images.push(image);
                }
                next();
            })
            .catch((error) => {
                next(error)
            });
    });

    q.push(next => {
        db.Images.findOne({
            where: {
                id: {[Op.lt]: imageId},
                is_public: true
            },
            raw: true,
            attributes,
            limit: 1
        })
            .then((image) => {
                nextImage = image;
                if (image) {
                    images.push(image);
                }
                next();
            })
            .catch((error) => {
                next(error)
            });
    });

    q.push(next => {
        getEmbedChecking(db, fields, [...images])
            .then(newImages => {
                next();
            })
            .catch(error => {
                next(error);
            })
    });

    q.start((error) => {
        if (!error) {
            res.send({
                status: 200,
                data: {
                    prevImage,
                    nextImage
                }
            });
        } else {
            res.send({status: 500, error: {list: helper.normaliseSequelizeErrors(error.errors)}});
        }
    });
});

router.post(`/${moduleKey}`, checkJwtToken, (req, res) => {
    const db = req.app.get('db');
    const data = req.body;
    data.user_id = req.user.id;

    const rules = getRules();
    const errors = validatorHelper.validateData(db, data, rules);
    if (!errors.length) {
        db.Images.create(data, {raw: true})
            .then(newImageData => {
                relationsHelper.saveTagList(db, 'image', newImageData.id, data['tags']);
                res.send({status: 200, data: {id: newImageData.id}});
            })
            .catch(error => {
                res.status(400).send({
                    status: 400,
                    error: {list: helper.normaliseSequelizeErrors(error.errors)}
                });
            });
    } else {
        res.status(400).send({status: 400, error: errors});
    }
});

router.delete(`/${moduleKey}/:id`, checkJwtToken, (req, res) => {
    const db = req.app.get('db');
    const id = req.params.id;
    const embed = req.query.embed || '';
    const q = queue({concurrency: 1});
    let error;
    let data = {deleteId: id};

    q.push((next) => {
        db.Images.destroy({
            where: {
                id: id,
                user_id: req.user.id
            }
        })
            .then(() => {
                // @TODO Нужно сделать каскадное удаление связанных услуг и страниц вместе с их файлами
                next();
            })
            .catch((deleteError) => {
                error = deleteError;
                next();
            });
    });

    if (embed) {
        const embeds = helper.parseEmbed(embed);
        if (embeds['images']) {
            q.push((next) => {
                // helper.parseEmbed();
                getImagesList(req, embeds['images'].params)
                    .then((listImages) => {
                        data['images'] = listImages;
                        next();
                    })
                    .catch((imagesError) => {
                        error = imagesError;
                        next();
                    })
            });
        }
    }

    q.start(() => {
        if (error) res.status(200).send({status: 500, error});
        else res.send({status: 200, data});
    });

});

router.put(`/${moduleKey}/:id`, checkJwtToken, (req, res) => {
    const rules = getRules();
    const data = req.body;
    const db = req.app.get('db');
    const imageId = req.params.id;
    let responseData = {};
    let errorData = {};

    const userId = req.user.id;
    const userRoleId = req.user.role_id;
    const where = {id: imageId};
    if (userRoleId !== 1) {
        where['user_id'] = userId;
    }

    let q = queue({concurrency: 1});

    // Check and upload images from text
    q.push(next => {
        console.log('1');
        validatorHelper.validateData(db, data, rules)
            .then(error => {
                if (!error || !error.length) {
                    const saveData = validatorHelper.getSaveData(data, rules);

                    db.Images.update(saveData, {where})
                        .then(response2 => {
                            console.log('response2', response2);
                            // relationsHelper.saveTagList(db, 'image', imageId, data['tags']);
                            responseData = response2;
                            next();
                        })
                        .catch(error => {
                            errorData = helper.normaliseSequelizeErrors(error.errors);
                            next();
                        });
                } else {
                    errorData = error;
                    next();
                }
            })
            .catch(error => {
                console.log('_________________catch', error);
                errorData = error;
                next();
            });
    });
    /*
      q.push(next => {
        console.log('2');
        validatorHelper.validateData(db, data, rules)
          .then(error => {
            if (!error || !error.length) {
              console.log('2-1');
              const saveData = validatorHelper.getSaveData(data, rules);
              db.Images.update(saveData, {where})
                .then(response2 => {
                  console.log('2-2');
                  relationsHelper.saveTagList(db, 'image', imageId, data['tags']);
                  responseData = response2;
                  next();
                })
                .catch(error => {
                  console.log('2-3');
                  errorData = helper.normaliseSequelizeErrors(error.errors);
                  next();
                });
            } else {
              console.log('2-4');
              errorData = error;
              next();
            }
          });
      });*/

    q.push(next => {
        if (errorData && (Object.keys(errorData).length || errorData.length)) {
            next();
        }

        if (data.tags && data.id) {
            embedHelper.updateTagsByText(db, data.tags, 'image', data.id)
                .then(() => {
                    next();
                })
                .catch(error => {
                    console.log('______error', error);
                    errorData = {error}
                    next();
                });

        } else {
            errorData = {error: EMPTY_TAGS}
        }

    });

    q.start(() => {
        if (errorData && (Object.keys(errorData).length || errorData.length)) {
            res.status(422).send({status: 422, error: {...errorData}});
        } else {
            res.send({status: 200, data: responseData});
        }
    });
});

getImagesList = (req, params = null, iDs = []) => {
    return new Promise((solved, reject) => {
        const db = req.app.get('db');
        const data = req.body;
        const fields = helper.parseFields(req.query.fields);
        const where = req.query.where || '';
        const sort = helper.parseSort(req.query.sort);
        let perPage = +req.query.per_page || 10;
        let activePage = +req.query.page || 1;
        let slug;

        if (params) {
            if (params.page) {
                activePage = params.page;
            }
            if (params.perPage) {
                perPage = params.perPage;
            }
            if (params.slug) {
                slug = params.slug;
            }
        }

        const offset = perPage * (activePage - 1);
        if (req.user) {
            data.user_id = req.user.id;
        }

        const options = {
            limit: perPage,
            offset,
            raw: true,
            order: sort ? sort : [['id', 'DESC']],
            attributes: ['id', 'category_id', ...fields.fields],
        };

        if (!params || !params.showAll) {
            options['where'] = {is_public: true};
        }

        const whereParams = helper.parseWhere(where);
        if (whereParams) {
            options['where'] = {...options['where'], ...whereParams};
        }

        if (iDs.length) {
            options['where'] = {...options['where'], id: iDs};
        }

        if (slug && !parseInt(slug) && slug !== 'panel') {
            options['where'] = {
                ...options['where'],
                slug
            };
        }

        if (params && params.id && parseInt(params.id)) {
            options['where'] = {
                ...options['where'],
                id: parseInt(params.id)
            };
        }

        let images = {};
        const q = queue({concurrency: 1});
        q.push(next => {
            db.Images.findAndCountAll(options)
                .then(data => {
                    images = {
                        data: data.rows,
                        meta: {
                            totalCount: data.count,
                            perPage: perPage,
                            page: activePage
                        }
                    };
                    next();
                })
                .catch(error => {
                    console.log('error', error);
                });
        });

        if (fields) {
            q.push(next => {


                getEmbedChecking(db, fields, [...images.data])
                    .then(newImages => {
                        next();
                    })
                    .catch(error => {
                        next(error);
                    })
            });
        }

        q.start((error) => {
            if (error) {
                console.log('error', error);
            }
            solved(images);
        });
    });
};

getImage = (req, slug, id, forPanel = false) => {
    const db = req.app.get('db');
    return new Promise((solve, reject) => {
        const q = queue({concurrency: 1});
        let image;

        q.push(next => {

            getImagesList(req, {id, slug, showAll: forPanel})
                .then((result) => {
                    if (result && result.data.length) {
                        image = result.data[0];
                    }
                    next();
                })
                .catch(error => {
                    next(error);
                })
        });

        q.push(next => {
            let ip = req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress;

            if (image && image.id) {
                if (ip === '::1') {
                    ip = '127:0:0:1'
                }
                viewHelper.updateView(db, image.id, 'image', ip)
                    .then(() => {
                        next();
                    })
                    .catch((error) => {
                        next(error);
                    })
            } else {
                next();
            }
        });

        q.start(error => {
            if (error) {
                reject(error);
            } else {
                solve(image ? image : {})
            }
        });
    });

}

getEmbedChecking = (db, fields, images) => {
    const q = queue({concurrency: 1});
    return new Promise((solved, reject) => {

        if (fields) {
            if (fields.files) {
                q.push(next => {
                    if (images) {
                        const imageIds = images.map(item => item.id);
                        embedHelper._filesEmbed(db, images, 'image', fields.files)
                            .then(changedItems => {
                                images = changedItems;
                                next();
                            })
                            .catch(error => {
                                reject(error);
                            });
                    } else {
                        next();
                    }
                });

            }

            if (fields.tags) {
                q.push(next => {
                    // const imageIds = images.map(item => item.id);
                    embedHelper._tagsEmbed(db, images, 'image', fields.tags)
                        .then(newGlobalItems => {
                            images = newGlobalItems;
                            next();
                        })
                        .catch(error => {
                            reject(error);
                        });
                });
            }

            if (fields.category) {
                q.push(next => {
                    const categoryIds = images.map(item => item.category_id);
                    embedHelper.getCategory(db,
                        categoryIds,
                        fields.category)
                        .then(categories => {
                            const groupItems = _.groupBy(categories, item => item.id);
                            images.forEach(image => {
                                if (groupItems[image.category_id]) {
                                    image['category'] = groupItems[image.category_id][0];
                                } else {
                                    image['category'] = {};
                                }
                            });
                            next();
                        })
                        .catch(error => {
                            reject(error);
                        });
                });
            }

            if (fields.views) {
                q.push(next => {
                    embedHelper._viewsEmbed(
                        db,
                        images,
                        'image',
                        fields.views
                    )
                        .then(newGlobalItems => {
                            images = newGlobalItems;
                            next();
                        })
                        .catch(error => {
                            reject(error);
                        });
                });
            }

            if (fields.comments) {
                q.push(next => {
                    embedHelper._commentsEmbed(
                        db,
                        images,
                        'image',
                        fields.comments
                    )
                        .then(newGlobalItems => {
                            images = newGlobalItems;
                            next();
                        })
                        .catch(error => {
                            reject(error);
                        });
                });
            }

            if (fields.emotions) {
                q.push(next => {
                    embedHelper._emotionsEmbed(
                        db,
                        images,
                        'image',
                        fields.emotions
                    )
                        .then(newGlobalItems => {
                            images = newGlobalItems;
                            next();
                        })
                        .catch(error => {
                            reject(error);
                        });
                });
            }

            q.start(() => {
                solved(images);
            });
        } else {
            solved(images);
        }
    });
};

getRules = (data, section = '') => {
    const rules = [
        {
            field: 'title',
            tableField: 'title',
            type: fieldTypes.STRING,
            rules: [validateTypes.REQUIRE],
        },
        {
            field: 'description',
            tableField: 'description',
            type: fieldTypes.STRING,
            rules: [validateTypes.REQUIRE],
        },
        {
            field: 'description_full',
            tableField: 'description_full',
            type: fieldTypes.STRING,
            rules: [validateTypes.REQUIRE],
        },
        {
            field: 'category_id',
            tableField: 'category_id',
            type: fieldTypes.INTEGER,
            rules: [validateTypes.REQUIRE],
        },
        {
            field: 'tags',
            type: fieldTypes.TAGS_TEXT,
            rules: [validateTypes.REQUIRE],
        },
        {
            field: 'meta_title',
            type: fieldTypes.STRING,
            tableField: 'meta_title',
        },
        {
            field: 'meta_keyword',
            type: fieldTypes.STRING,
            tableField: 'meta_keyword',
        },
        {
            field: 'meta_description',
            type: fieldTypes.STRING,
            tableField: 'meta_description',
        },
        {
            field: 'is_public',
            type: fieldTypes.BOOLEAN,
            tableField: 'is_public',
        }
    ];

    return rules;
};

module.exports = router;
