--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE IF EXISTS ONLY public."Roles_rights" DROP CONSTRAINT IF EXISTS users_rights_users_roles__fk;
DROP INDEX IF EXISTS public.fki_country_id_fk;
DROP INDEX IF EXISTS public.fki_comments_parent_id;
DROP INDEX IF EXISTS public.fki_comment_user_id;
DROP INDEX IF EXISTS public.fki_category_link_category_id;
DROP INDEX IF EXISTS public.comment_product_index;
DROP INDEX IF EXISTS public.categories_index_slug;
ALTER TABLE IF EXISTS ONLY public."Views_stats" DROP CONSTRAINT IF EXISTS views_stat_uniq;
ALTER TABLE IF EXISTS ONLY public."Views_stats" DROP CONSTRAINT IF EXISTS views_stat_id_pk;
ALTER TABLE IF EXISTS ONLY public."Views" DROP CONSTRAINT IF EXISTS views_pkey;
ALTER TABLE IF EXISTS ONLY public."Users_roles" DROP CONSTRAINT IF EXISTS users_roles_pk;
ALTER TABLE IF EXISTS ONLY public."Users" DROP CONSTRAINT IF EXISTS pk_users_id;
ALTER TABLE IF EXISTS ONLY public."Roles_rights" DROP CONSTRAINT IF EXISTS pk_u_r_id_2;
ALTER TABLE IF EXISTS ONLY public."Users_rights" DROP CONSTRAINT IF EXISTS pk_u_r_id;
ALTER TABLE IF EXISTS ONLY public."Images" DROP CONSTRAINT IF EXISTS pk_tours_id;
ALTER TABLE IF EXISTS ONLY public."Rights_blocks" DROP CONSTRAINT IF EXISTS pk_r_b_id;
ALTER TABLE IF EXISTS ONLY public."Access_tokens" DROP CONSTRAINT IF EXISTS pk_id;
ALTER TABLE IF EXISTS ONLY public."Сities" DROP CONSTRAINT IF EXISTS pk_cities_id;
ALTER TABLE IF EXISTS ONLY public."Countries" DROP CONSTRAINT IF EXISTS pk_c_id;
ALTER TABLE IF EXISTS ONLY public."Hash_tags" DROP CONSTRAINT IF EXISTS hash_tags_slug_uniq;
ALTER TABLE IF EXISTS ONLY public."Hash_tags" DROP CONSTRAINT IF EXISTS hash_tags_name;
ALTER TABLE IF EXISTS ONLY public."Firms" DROP CONSTRAINT IF EXISTS firms_pk;
ALTER TABLE IF EXISTS ONLY public."Categories" DROP CONSTRAINT IF EXISTS categories_slug;
ALTER TABLE IF EXISTS ONLY public."Categories" DROP CONSTRAINT IF EXISTS categories_name;
ALTER TABLE IF EXISTS ONLY public."Tag_links" DROP CONSTRAINT IF EXISTS "Tag_links_pkey";
ALTER TABLE IF EXISTS ONLY public."Images" DROP CONSTRAINT IF EXISTS "I_u_slug";
ALTER TABLE IF EXISTS ONLY public."Hash_tags" DROP CONSTRAINT IF EXISTS "Hash_tags_pkey";
ALTER TABLE IF EXISTS ONLY public."Files" DROP CONSTRAINT IF EXISTS "Files_pkey";
ALTER TABLE IF EXISTS ONLY public."Emotions" DROP CONSTRAINT IF EXISTS "Emotions_pkey";
ALTER TABLE IF EXISTS ONLY public."Comments" DROP CONSTRAINT IF EXISTS "Comments_pkey";
ALTER TABLE IF EXISTS ONLY public."Category_links" DROP CONSTRAINT IF EXISTS "Category_links_pkey";
ALTER TABLE IF EXISTS ONLY public."Categories" DROP CONSTRAINT IF EXISTS "Categories_pkey";
ALTER TABLE IF EXISTS public."Users_roles" ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS public."Firms" ALTER COLUMN id DROP DEFAULT;
DROP TABLE IF EXISTS public."Сities";
DROP SEQUENCE IF EXISTS public.messages_id_seq;
DROP SEQUENCE IF EXISTS public.firms_id_seq1;
DROP SEQUENCE IF EXISTS public.firms_id_seq;
DROP SEQUENCE IF EXISTS public.firm_user_services_id_seq;
DROP SEQUENCE IF EXISTS public.firm_user_pages_id_seq;
DROP SEQUENCE IF EXISTS public.firm_services_id_seq;
DROP SEQUENCE IF EXISTS public.firm_pages_id_seq;
DROP SEQUENCE IF EXISTS public.firm_moderations_id_seq;
DROP SEQUENCE IF EXISTS public.cities_id_seq;
DROP TABLE IF EXISTS public."Views_stats";
DROP SEQUENCE IF EXISTS public.views_stat_id;
DROP TABLE IF EXISTS public."Views";
DROP SEQUENCE IF EXISTS public.views_id_seq;
DROP SEQUENCE IF EXISTS public."Users_roles_id_seq";
DROP TABLE IF EXISTS public."Users_roles";
DROP TABLE IF EXISTS public."Users_rights";
DROP TABLE IF EXISTS public."Users";
DROP SEQUENCE IF EXISTS public.users_id_seq;
DROP TABLE IF EXISTS public."Tag_links";
DROP SEQUENCE IF EXISTS public.tag_links_id_seq;
DROP TABLE IF EXISTS public."Roles_rights";
DROP SEQUENCE IF EXISTS public.users_rights_id_seq;
DROP TABLE IF EXISTS public."Rights_blocks";
DROP SEQUENCE IF EXISTS public.rights_blocks_id_seq;
DROP TABLE IF EXISTS public."Images";
DROP SEQUENCE IF EXISTS public.tours_id_seq;
DROP TABLE IF EXISTS public."Hash_tags";
DROP SEQUENCE IF EXISTS public.hash_tag_id_seq;
DROP TABLE IF EXISTS public."Firms";
DROP TABLE IF EXISTS public."Files";
DROP SEQUENCE IF EXISTS public.files_id_seq;
DROP TABLE IF EXISTS public."Emotions";
DROP SEQUENCE IF EXISTS public.emotions_id_seq;
DROP TABLE IF EXISTS public."Countries";
DROP SEQUENCE IF EXISTS public.countries_id_seq;
DROP TABLE IF EXISTS public."Comments";
DROP SEQUENCE IF EXISTS public.comment_id_seq;
DROP TABLE IF EXISTS public."Category_links";
DROP TABLE IF EXISTS public."Categories";
DROP SEQUENCE IF EXISTS public.categories_id;
DROP TABLE IF EXISTS public."Access_tokens";
DROP SEQUENCE IF EXISTS public.access_token_id_seq;
DROP TYPE IF EXISTS public.product_type;
--
-- Name: product_type; Type: TYPE; Schema: public; Owner: travel_blogg_user
--

CREATE TYPE public.product_type AS ENUM (
    'image'
);


ALTER TYPE public.product_type OWNER TO travel_blogg_user;

--
-- Name: access_token_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.access_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.access_token_id_seq OWNER TO travel_blogg_user;

SET default_tablespace = '';

--
-- Name: Access_tokens; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Access_tokens" (
    id integer DEFAULT nextval('public.access_token_id_seq'::regclass) NOT NULL,
    user_id integer,
    ip character varying(15) NOT NULL,
    token character varying(255) NOT NULL,
    exp_date integer,
    "createdAt" date,
    "updatedAt" date,
    refresh_token text
);


ALTER TABLE public."Access_tokens" OWNER TO travel_blogg_user;

--
-- Name: categories_id; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.categories_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id OWNER TO travel_blogg_user;

--
-- Name: Categories; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Categories" (
    id integer DEFAULT nextval('public.categories_id'::regclass) NOT NULL,
    "createdAt" date NOT NULL,
    "updatedAt" date,
    name character varying(150) NOT NULL,
    "position" integer,
    public boolean DEFAULT false,
    slug character varying(150),
    description text,
    image character varying(255)
);


ALTER TABLE public."Categories" OWNER TO travel_blogg_user;

--
-- Name: Category_links; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Category_links" (
    id integer NOT NULL,
    "updatedAt" date,
    "createdAt" date,
    product_id integer NOT NULL,
    category_id integer NOT NULL,
    product_type public.product_type NOT NULL
);


ALTER TABLE public."Category_links" OWNER TO travel_blogg_user;

--
-- Name: comment_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comment_id_seq OWNER TO travel_blogg_user;

--
-- Name: Comments; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Comments" (
    id integer DEFAULT nextval('public.comment_id_seq'::regclass) NOT NULL,
    "updatedAt" date,
    "createdAt" date,
    user_id integer NOT NULL,
    title character varying(150) NOT NULL,
    description text NOT NULL,
    product_id integer NOT NULL,
    product_type public.product_type NOT NULL,
    parent_id integer
);


ALTER TABLE public."Comments" OWNER TO travel_blogg_user;

--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.countries_id_seq OWNER TO travel_blogg_user;

--
-- Name: Countries; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Countries" (
    id integer DEFAULT nextval('public.countries_id_seq'::regclass) NOT NULL,
    name character varying(150) NOT NULL,
    "createdAt" date,
    "updatedAt" date
);


ALTER TABLE public."Countries" OWNER TO travel_blogg_user;

--
-- Name: emotions_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.emotions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emotions_id_seq OWNER TO travel_blogg_user;

--
-- Name: Emotions; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Emotions" (
    id integer DEFAULT nextval('public.emotions_id_seq'::regclass) NOT NULL,
    "updatedAt" date,
    "createdAt" date,
    product_id integer NOT NULL,
    product_type public.product_type NOT NULL,
    ip character varying NOT NULL,
    emotion integer NOT NULL
);


ALTER TABLE public."Emotions" OWNER TO travel_blogg_user;

--
-- Name: COLUMN "Emotions".emotion; Type: COMMENT; Schema: public; Owner: travel_blogg_user
--

COMMENT ON COLUMN public."Emotions".emotion IS '''like: 1
dislike: 0''';


--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.files_id_seq OWNER TO travel_blogg_user;

--
-- Name: Files; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Files" (
    id integer DEFAULT nextval('public.files_id_seq'::regclass) NOT NULL,
    folder character varying(150) NOT NULL,
    is_used boolean,
    section character varying(150) NOT NULL,
    item_id integer NOT NULL,
    user_id integer NOT NULL,
    type character varying(50),
    main boolean,
    "order" integer,
    title character varying,
    file character varying(255) NOT NULL,
    "createdAt" date,
    "updatedAt" date
);


ALTER TABLE public."Files" OWNER TO travel_blogg_user;

--
-- Name: Firms; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Firms" (
    id integer NOT NULL,
    name character varying DEFAULT 255 NOT NULL,
    country_id integer,
    description text,
    legal_name character varying DEFAULT 255,
    email character varying DEFAULT 150 NOT NULL
);


ALTER TABLE public."Firms" OWNER TO travel_blogg_user;

--
-- Name: TABLE "Firms"; Type: COMMENT; Schema: public; Owner: travel_blogg_user
--

COMMENT ON TABLE public."Firms" IS 'Фирмы';


--
-- Name: hash_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.hash_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hash_tag_id_seq OWNER TO travel_blogg_user;

--
-- Name: Hash_tags; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Hash_tags" (
    id integer DEFAULT nextval('public.hash_tag_id_seq'::regclass) NOT NULL,
    "createdAt" date,
    "updatedAt" date,
    name character varying(150) NOT NULL,
    slug character varying(150),
    description text,
    image character varying
);


ALTER TABLE public."Hash_tags" OWNER TO travel_blogg_user;

--
-- Name: tours_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.tours_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tours_id_seq OWNER TO travel_blogg_user;

--
-- Name: Images; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Images" (
    id integer DEFAULT nextval('public.tours_id_seq'::regclass) NOT NULL,
    title character varying(255) NOT NULL,
    user_id integer NOT NULL,
    country_id integer,
    city_id integer,
    image character varying(255),
    slug character varying(255),
    "createdAt" date,
    "updatedAt" date,
    description text,
    is_public boolean DEFAULT false,
    category_id integer NOT NULL,
    views integer DEFAULT 0,
    meta_title character varying(250),
    meta_description text,
    meta_keyword text,
    description_full text
);


ALTER TABLE public."Images" OWNER TO travel_blogg_user;

--
-- Name: rights_blocks_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.rights_blocks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rights_blocks_id_seq OWNER TO travel_blogg_user;

--
-- Name: Rights_blocks; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Rights_blocks" (
    id integer DEFAULT nextval('public.rights_blocks_id_seq'::regclass) NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255),
    "createdAt" date,
    "updatedAt" date
);


ALTER TABLE public."Rights_blocks" OWNER TO travel_blogg_user;

--
-- Name: users_rights_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.users_rights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_rights_id_seq OWNER TO travel_blogg_user;

--
-- Name: Roles_rights; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Roles_rights" (
    id integer DEFAULT nextval('public.users_rights_id_seq'::regclass) NOT NULL,
    "create" boolean DEFAULT false,
    read boolean DEFAULT false,
    update_action boolean DEFAULT false,
    delete_action boolean DEFAULT false,
    "createdAt" date,
    "updatedAt" date,
    right_id integer,
    role_id integer NOT NULL
);


ALTER TABLE public."Roles_rights" OWNER TO travel_blogg_user;

--
-- Name: tag_links_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.tag_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_links_id_seq OWNER TO travel_blogg_user;

--
-- Name: Tag_links; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Tag_links" (
    id integer DEFAULT nextval('public.tag_links_id_seq'::regclass) NOT NULL,
    tag_id integer NOT NULL,
    product_id integer NOT NULL,
    "createdAt" date NOT NULL,
    "updatedAt" date,
    product_type public.product_type
);


ALTER TABLE public."Tag_links" OWNER TO travel_blogg_user;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO travel_blogg_user;

--
-- Name: Users; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Users" (
    id integer DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    last_name character varying(150),
    avatar character varying(150),
    sex integer,
    password character varying(255) NOT NULL,
    salt character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    role_id integer,
    "createdAt" date,
    "updatedAt" date,
    name character varying(150)
);


ALTER TABLE public."Users" OWNER TO travel_blogg_user;

--
-- Name: Users_rights; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Users_rights" (
    id integer DEFAULT nextval('public.users_rights_id_seq'::regclass) NOT NULL,
    user_id integer NOT NULL,
    "create" boolean DEFAULT false,
    read boolean DEFAULT false,
    update_action boolean DEFAULT false,
    delete_action boolean DEFAULT false,
    "createdAt" date,
    "updatedAt" date,
    right_id integer
);


ALTER TABLE public."Users_rights" OWNER TO travel_blogg_user;

--
-- Name: Users_roles; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Users_roles" (
    id integer NOT NULL,
    name character varying,
    is_admin boolean DEFAULT false,
    "createdAt" date,
    "updatedAt" date
);


ALTER TABLE public."Users_roles" OWNER TO travel_blogg_user;

--
-- Name: Users_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public."Users_roles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Users_roles_id_seq" OWNER TO travel_blogg_user;

--
-- Name: Users_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: travel_blogg_user
--

ALTER SEQUENCE public."Users_roles_id_seq" OWNED BY public."Users_roles".id;


--
-- Name: views_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.views_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.views_id_seq OWNER TO travel_blogg_user;

--
-- Name: Views; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Views" (
    id integer DEFAULT nextval('public.views_id_seq'::regclass) NOT NULL,
    "updatedAt" date,
    "createdAt" date,
    product_id integer NOT NULL,
    product_type public.product_type NOT NULL,
    ip character varying(50) NOT NULL,
    date date
);


ALTER TABLE public."Views" OWNER TO travel_blogg_user;

--
-- Name: views_stat_id; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.views_stat_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.views_stat_id OWNER TO travel_blogg_user;

--
-- Name: Views_stats; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Views_stats" (
    id integer DEFAULT nextval('public.views_stat_id'::regclass) NOT NULL,
    product_id integer,
    product_type public.product_type NOT NULL,
    views integer,
    "updatedAt" date,
    "createdAt" date
);


ALTER TABLE public."Views_stats" OWNER TO travel_blogg_user;

--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cities_id_seq OWNER TO travel_blogg_user;

--
-- Name: firm_moderations_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.firm_moderations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.firm_moderations_id_seq OWNER TO travel_blogg_user;

--
-- Name: firm_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.firm_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.firm_pages_id_seq OWNER TO travel_blogg_user;

--
-- Name: firm_services_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.firm_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.firm_services_id_seq OWNER TO travel_blogg_user;

--
-- Name: firm_user_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.firm_user_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.firm_user_pages_id_seq OWNER TO travel_blogg_user;

--
-- Name: firm_user_services_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.firm_user_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.firm_user_services_id_seq OWNER TO travel_blogg_user;

--
-- Name: firms_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.firms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.firms_id_seq OWNER TO travel_blogg_user;

--
-- Name: firms_id_seq1; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.firms_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.firms_id_seq1 OWNER TO travel_blogg_user;

--
-- Name: firms_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: travel_blogg_user
--

ALTER SEQUENCE public.firms_id_seq1 OWNED BY public."Firms".id;


--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: travel_blogg_user
--

CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_id_seq OWNER TO travel_blogg_user;

--
-- Name: Сities; Type: TABLE; Schema: public; Owner: travel_blogg_user
--

CREATE TABLE public."Сities" (
    id integer DEFAULT nextval('public.cities_id_seq'::regclass) NOT NULL,
    name character varying(150) NOT NULL,
    country_id integer NOT NULL,
    "createdAt" date,
    "updatedAt" date
);


ALTER TABLE public."Сities" OWNER TO travel_blogg_user;

--
-- Name: Firms id; Type: DEFAULT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Firms" ALTER COLUMN id SET DEFAULT nextval('public.firms_id_seq1'::regclass);


--
-- Name: Users_roles id; Type: DEFAULT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Users_roles" ALTER COLUMN id SET DEFAULT nextval('public."Users_roles_id_seq"'::regclass);


--
-- Data for Name: Access_tokens; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Access_tokens" (id, user_id, ip, token, exp_date, "createdAt", "updatedAt", refresh_token) VALUES (62, 1, '82.215.79.74', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZXhwIjoxNjA5MjYyNzE4fQ.ix_HRwBF670phHkIgAzU5qFCTpeawcHMoQFK18fO_ZI', 1610703446, '2020-12-29', '2021-01-15', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJrZXkiOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuZXlKcFpDSTZNU3dpWlhod0lqb3hOakE1TWpZeU56RTRmUS5peF9IUndCRjY3MHBoSGtJZ0F6VTVxRkNUcGVhd2NITW9RRksxOGZPX1pJLVJlZnJlc2hUb2tlbi0xNjA5MjUxOTE4In0.WMvKs3As2mFa3sGv8JH8SzoLIwZCdhn2_tzb5HTiQDE');
INSERT INTO public."Access_tokens" (id, user_id, ip, token, exp_date, "createdAt", "updatedAt", refresh_token) VALUES (59, 1, '80.80.221.95', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZXhwIjoxNjA4NTY2MDI0fQ.Rbas742F8Fd52c2P0FvodD4gNc3YyPJphbrmmLS8Ug8', 1610631854, '2020-12-21', '2021-01-14', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJrZXkiOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuZXlKcFpDSTZNU3dpWlhod0lqb3hOakE0TlRZMk1ESTBmUS5SYmFzNzQyRjhGZDUyYzJQMEZ2b2RENGdOYzNZeVBKcGhicm1tTFM4VWc4LVJlZnJlc2hUb2tlbi0xNjA4NTU1MjI0In0.mrywUq-f9bvFx8dE6mOfkg1m0a4uipojK7Fcl4qHCww');
INSERT INTO public."Access_tokens" (id, user_id, ip, token, exp_date, "createdAt", "updatedAt", refresh_token) VALUES (61, 1, '185.163.26.79', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZXhwIjoxNjA4ODg3NTc3fQ.LJaq_ghiXNQvb83wYSr0-_4RHiEXs2wTa5I0jslLrW0', 1608887682, '2020-12-21', '2020-12-25', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJrZXkiOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuZXlKcFpDSTZNU3dpWlhod0lqb3hOakE0T0RnM05UYzNmUS5MSmFxX2doaVhOUXZiODN3WVNyMC1fNFJIaUVYczJ3VGE1STBqc2xMclcwLVJlZnJlc2hUb2tlbi0xNjA4ODc2Nzc3In0.2-1aGBXfddMG5OQps39CF38p48mwgxeOyaK13t_4iqQ');
INSERT INTO public."Access_tokens" (id, user_id, ip, token, exp_date, "createdAt", "updatedAt", refresh_token) VALUES (58, 1, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZXhwIjoxNjA5MTUwODYwfQ.7JeSRfo7qTXgn7f54VefP1mB0WB0vC6qbCKPL92Jen4', 1609161240, '2020-07-20', '2020-12-28', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJrZXkiOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuZXlKcFpDSTZNU3dpWlhod0lqb3hOakE1TVRVd09EWXdmUS43SmVTUmZvN3FUWGduN2Y1NFZlZlAxbUIwV0IwdkM2cWJDS1BMOTJKZW40LVJlZnJlc2hUb2tlbi0xNjA5MTQwMDYwIn0.3LNtXv_O9u3nDm8HphCgHSf83xeBDw1iYHA06S3GZZY');


--
-- Data for Name: Categories; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Categories" (id, "createdAt", "updatedAt", name, "position", public, slug, description, image) VALUES (2, '2019-10-02', '2019-10-02', 'Курорты', NULL, true, 'resorts', '2- Изобрел данные лучи 1895 году немецкий ученый Рентген: во время работы с катодолучевой трубкой он обнаружил эффект флуоресценции платино-цианистого бария. Тогда и произошло описание таких лучей и их удивительной способности проникать сквозь ткани организма. Лучи стали называться икс-лучами (х-лучи). Позже в России их стали именовать ', 'images/banner/home-banner.jpg');
INSERT INTO public."Categories" (id, "createdAt", "updatedAt", name, "position", public, slug, description, image) VALUES (6, '2019-08-02', NULL, 'Заманчивые туры', NULL, true, 'tours', NULL, NULL);
INSERT INTO public."Categories" (id, "createdAt", "updatedAt", name, "position", public, slug, description, image) VALUES (3, '2019-10-02', '2019-10-02', 'Страны для отдыха', NULL, true, 'country', '3-Изобрел данные лучи 1895 году немецкий ученый Рентген: во время работы с катодолучевой трубкой он обнаружил эффект флуоресценции платино-цианистого бария. Тогда и произошло описание таких лучей и их удивительной способности проникать сквозь ткани организма. Лучи стали называться икс-лучами (х-лучи). Позже в России их стали именовать ', 'images/banner/banner2.jpg');
INSERT INTO public."Categories" (id, "createdAt", "updatedAt", name, "position", public, slug, description, image) VALUES (9, '2019-08-02', NULL, 'Новости туризма', NULL, true, 'tours-news', NULL, NULL);
INSERT INTO public."Categories" (id, "createdAt", "updatedAt", name, "position", public, slug, description, image) VALUES (1, '2019-08-02', '2019-08-02', 'Города', NULL, true, 'cities', 'Изобрел данные лучи 1895 году немецкий ученый Рентген: во время работы с катодолучевой трубкой он обнаружил эффект флуоресценции платино-цианистого бария. Тогда и произошло описание таких лучей и их удивительной способности проникать сквозь ткани организма. Лучи стали называться икс-лучами (х-лучи). Позже в России их стали именовать ', 'images/banner/banner.jpg');
INSERT INTO public."Categories" (id, "createdAt", "updatedAt", name, "position", public, slug, description, image) VALUES (4, '2019-10-02', '2019-10-02', 'Привлекательные пляжи', NULL, true, 'beaches', '4 - Изобрел данные лучи 1895 году немецкий ученый Рентген: во время работы с катодолучевой трубкой он обнаружил эффект флуоресценции платино-цианистого бария. Тогда и произошло описание таких лучей и их удивительной способности проникать сквозь ткани организма. Лучи стали называться икс-лучами (х-лучи). Позже в России их стали именовать ', 'images/banner/home-banner.jpg');
INSERT INTO public."Categories" (id, "createdAt", "updatedAt", name, "position", public, slug, description, image) VALUES (5, '2019-10-02', '2019-10-02', 'Лучшие отели мира', NULL, true, 'hotels', '5- Изобрел данные лучи 1895 году немецкий ученый Рентген: во время работы с катодолучевой трубкой он обнаружил эффект флуоресценции платино-цианистого бария. Тогда и произошло описание таких лучей и их удивительной способности проникать сквозь ткани организма. Лучи стали называться икс-лучами (х-лучи). Позже в России их стали именовать ', 'images/banner/banner.jpg');
INSERT INTO public."Categories" (id, "createdAt", "updatedAt", name, "position", public, slug, description, image) VALUES (10, '2019-10-02', '2019-10-02', 'Туризм в условиях пандемии', NULL, true, 'tourism-covid', '5- Изобрел данные лучи 1895 году немецкий ученый Рентген: во время работы с катодолучевой трубкой он обнаружил эффект флуоресценции платино-цианистого бария. Тогда и произошло описание таких лучей и их удивительной способности проникать сквозь ткани организма. Лучи стали называться икс-лучами (х-лучи). Позже в России их стали именовать ', 'images/banner/banner.jpg');


--
-- Data for Name: Category_links; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--



--
-- Data for Name: Comments; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Comments" (id, "updatedAt", "createdAt", user_id, title, description, product_id, product_type, parent_id) VALUES (2, '2019-08-10', '2019-08-10', 1, 'test comment', 'comment-descripion', 4, 'image', NULL);
INSERT INTO public."Comments" (id, "updatedAt", "createdAt", user_id, title, description, product_id, product_type, parent_id) VALUES (3, '2019-08-10', '2019-08-10', 1, 'test comment 2', 'comment-descripion 2', 4, 'image', NULL);
INSERT INTO public."Comments" (id, "updatedAt", "createdAt", user_id, title, description, product_id, product_type, parent_id) VALUES (4, '2019-08-10', '2019-08-10', 1, 'test comment 3', 'comment-descripion 3', 4, 'image', NULL);
INSERT INTO public."Comments" (id, "updatedAt", "createdAt", user_id, title, description, product_id, product_type, parent_id) VALUES (5, '2019-08-10', '2019-08-10', 1, 'test comment 4', 'comment-descripion 4', 4, 'image', NULL);


--
-- Data for Name: Countries; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Countries" (id, name, "createdAt", "updatedAt") VALUES (1, 'Узбекистан', NULL, NULL);
INSERT INTO public."Countries" (id, name, "createdAt", "updatedAt") VALUES (2, 'Россия', NULL, NULL);
INSERT INTO public."Countries" (id, name, "createdAt", "updatedAt") VALUES (3, 'Малайзия', NULL, NULL);


--
-- Data for Name: Emotions; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Emotions" (id, "updatedAt", "createdAt", product_id, product_type, ip, emotion) VALUES (2, '2019-08-10', '2019-08-10', 4, 'image', '127:0:0:1', 1);
INSERT INTO public."Emotions" (id, "updatedAt", "createdAt", product_id, product_type, ip, emotion) VALUES (3, '2019-08-10', '2019-08-10', 4, 'image', '127:0:0:2', 1);
INSERT INTO public."Emotions" (id, "updatedAt", "createdAt", product_id, product_type, ip, emotion) VALUES (4, '2019-08-10', '2019-08-10', 4, 'image', '127:0:0:3', 1);
INSERT INTO public."Emotions" (id, "updatedAt", "createdAt", product_id, product_type, ip, emotion) VALUES (5, '2019-08-10', '2019-08-10', 4, 'image', '127:0:0:4', 0);
INSERT INTO public."Emotions" (id, "updatedAt", "createdAt", product_id, product_type, ip, emotion) VALUES (6, '2019-08-10', '2019-08-10', 4, 'image', '127:0:0:5', 0);


--
-- Data for Name: Files; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (1, 'files/image/4/', NULL, 'image', 4, 1, NULL, NULL, NULL, NULL, '1559592535_5785.jpeg', '2019-06-03', '2019-06-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (2, 'files/image/4/', NULL, 'image', 4, 1, NULL, NULL, NULL, NULL, '1560286113_4222.jpeg', '2019-06-11', '2019-06-11');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (3, 'files/image/4/', NULL, 'image', 4, 1, NULL, NULL, NULL, NULL, '1560286113_8208.jpeg', '2019-06-11', '2019-06-11');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (4, 'files/image/4/', NULL, 'image', 4, 1, NULL, NULL, NULL, NULL, '1560286113_4894.jpg', '2019-06-11', '2019-06-11');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (39, 'files/image/11/', NULL, 'image', 11, 1, NULL, NULL, NULL, NULL, '1573996329_1163.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (40, 'files/image/7/', NULL, 'image', 7, 1, NULL, NULL, NULL, NULL, '1573996435_9417.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (41, 'files/image/7/', NULL, 'image', 7, 1, NULL, NULL, NULL, NULL, '1573996435_8294.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (42, 'files/image/7/', NULL, 'image', 7, 1, NULL, NULL, NULL, NULL, '1573996435_6119.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (43, 'files/image/7/', NULL, 'image', 7, 1, NULL, NULL, NULL, NULL, '1573996436_9097.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (44, 'files/image/7/', NULL, 'image', 7, 1, NULL, NULL, NULL, NULL, '1573996436_7361.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (45, 'files/image/7/', NULL, 'image', 7, 1, NULL, NULL, NULL, NULL, '1573996437_3225.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (25, 'files/image/7/', NULL, 'image', 7, 1, NULL, NULL, NULL, NULL, '1573995681_8881.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (24, 'files/image/7/', NULL, 'image', 7, 1, NULL, true, NULL, NULL, '1573995680_6269.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (26, 'files/image/8/', NULL, 'image', 8, 1, NULL, NULL, NULL, NULL, '1573996010_8595.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (27, 'files/image/9/', NULL, 'image', 9, 1, NULL, NULL, NULL, NULL, '1573996031_9521.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (28, 'files/image/10/', NULL, 'image', 10, 1, NULL, NULL, NULL, NULL, '1573996047_2664.jpg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (29, 'files/image/12/', NULL, 'image', 12, 1, NULL, NULL, NULL, NULL, '1573996067_3812.jpeg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (30, 'files/image/13/', NULL, 'image', 13, 1, NULL, NULL, NULL, NULL, '1573996084_2253.jpeg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (31, 'files/image/14/', NULL, 'image', 14, 1, NULL, NULL, NULL, NULL, '1573996106_7911.jpeg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (32, 'files/image/15/', NULL, 'image', 15, 1, NULL, NULL, NULL, NULL, '1573996122_4963.jpeg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (33, 'files/image/16/', NULL, 'image', 16, 1, NULL, NULL, NULL, NULL, '1573996138_7946.jpeg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (34, 'files/image/17/', NULL, 'image', 17, 1, NULL, NULL, NULL, NULL, '1573996157_4281.jpeg', '2019-11-17', '2019-11-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (738, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 3, NULL, '1609653753_7316.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (735, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 4, NULL, '1609653750_8448.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (740, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 7, NULL, '1609653755_3006.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (736, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 8, NULL, '1609653751_3646.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (734, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 9, NULL, '1609653749_9615.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (743, 'files/image/63/', NULL, 'image', 63, 1, NULL, true, 0, NULL, '1609753545_8176.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (741, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 1, NULL, '1609653755_3447.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (739, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 2, NULL, '1609653753_7963.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (737, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 5, NULL, '1609653751_8876.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (742, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, 6, NULL, '1609653756_7942.jpg', '2021-01-03', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (65, 'files/image/19/', NULL, 'image', 19, 1, 'editor', NULL, NULL, NULL, '1589834740_5743.jpg', '2020-05-18', '2020-05-18');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (66, 'files/image/19/', NULL, 'image', 19, 1, 'editor', NULL, NULL, NULL, '1589834785_2912.jpeg', '2020-05-18', '2020-05-18');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (67, 'files/image/18/', NULL, 'image', 18, 1, 'editor', NULL, NULL, NULL, '1589834851_2173.jpg', '2020-05-18', '2020-05-18');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (68, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1592980991_8839.jpeg', '2020-06-24', '2020-06-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (69, 'files/image/35/', NULL, 'image', 35, 1, 'editor', NULL, NULL, NULL, '1593892747_4043.jpg', '2020-07-04', '2020-07-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (70, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594421327_1567.jpeg', '2020-07-10', '2020-07-10');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (791, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 6, NULL, '1610607543_1399.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (74, 'files/image/34/', NULL, 'image', 34, 1, 'editor', NULL, NULL, NULL, '1594584506_7723.jpg', '2020-07-12', '2020-07-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (75, 'files/image/34/', NULL, 'image', 34, 1, 'editor', NULL, NULL, NULL, '1594584698_4039.jpg', '2020-07-12', '2020-07-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (76, 'files/image/34/', NULL, 'image', 34, 1, 'editor', NULL, NULL, NULL, '1594584730_9765.jpg', '2020-07-12', '2020-07-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (77, 'files/image/34/', NULL, 'image', 34, 1, 'editor', NULL, NULL, NULL, '1594584894_5214.jpg', '2020-07-12', '2020-07-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (78, 'files/image/34/', NULL, 'image', 34, 1, 'editor', NULL, NULL, NULL, '1594585032_2390.jpg', '2020-07-12', '2020-07-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (79, 'files/image/34/', NULL, 'image', 34, 1, 'editor', NULL, NULL, NULL, '1594585090_7979.jpg', '2020-07-12', '2020-07-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (80, 'files/image/34/', NULL, 'image', 34, 1, 'editor', NULL, NULL, NULL, '1594585129_7464.png', '2020-07-12', '2020-07-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (81, 'files/image/34/', NULL, 'image', 34, 1, 'editor', NULL, NULL, NULL, '1594585307_5637.jpg', '2020-07-12', '2020-07-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (798, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 13, NULL, '1610607552_2341.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (811, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, NULL, NULL, '1610611700_3891.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (813, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, NULL, NULL, '1610611701_5721.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (822, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614460_4750.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (89, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594741954_2738.jpg', '2020-07-14', '2020-07-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (90, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594785796_7046.jpg', '2020-07-15', '2020-07-15');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (91, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594785826_7703.jpg', '2020-07-15', '2020-07-15');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (92, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594785875_4905.jpg', '2020-07-15', '2020-07-15');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (93, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594786050_1480.jpg', '2020-07-15', '2020-07-15');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (94, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594786116_3253.jpg', '2020-07-15', '2020-07-15');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (99, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594958400_7296.jpg', '2020-07-17', '2020-07-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (100, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1594958435_5177.jpg', '2020-07-17', '2020-07-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (103, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595771259_2744.jpg', '2020-07-26', '2020-07-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (111, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595772164_5999.jpg', '2020-07-26', '2020-07-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (112, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595772420_5836.jpg', '2020-07-26', '2020-07-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (113, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595772464_1884.jpg', '2020-07-26', '2020-07-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (114, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595772578_6330.png', '2020-07-26', '2020-07-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (122, 'files/image/36/', NULL, 'image', 36, 1, NULL, true, NULL, NULL, '1595825918_5278.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (125, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595826411_6485.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (126, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595826438_2518.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (123, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595826319_7765.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (124, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595826355_9056.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (127, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595826464_9313.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (128, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595826483_6792.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (129, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595826641_7930.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (130, 'files/image/36/', NULL, 'image', 36, 1, 'editor', NULL, NULL, NULL, '1595826773_9440.jpg', '2020-07-27', '2020-07-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (744, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 1, NULL, '1609753644_2665.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (784, 'files/image/61/', NULL, 'image', 61, 1, NULL, NULL, NULL, NULL, '1610549422_1550.jpg', '2021-01-13', '2021-01-13');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (796, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 11, NULL, '1610607549_1224.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (823, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614498_6998.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (831, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614883_1402.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (835, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610615538_3603.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (158, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595952632_1881.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (159, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595952900_3950.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (160, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595952973_8551.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (161, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595953110_9488.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (162, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595953250_3979.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (163, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595953427_3202.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (164, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595953502_7061.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (165, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595953632_6506.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (166, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595953760_6292.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (167, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595953863_6402.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (168, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595953925_6788.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (169, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595954000_5155.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (170, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1595954089_7387.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (171, 'files/image/45/', NULL, 'image', 45, 1, NULL, true, NULL, NULL, '1595954338_9365.jpg', '2020-07-28', '2020-07-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (172, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596033171_2872.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (173, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596033657_7891.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (174, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596033829_9797.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (175, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596033861_7844.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (176, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596033917_4002.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (177, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034033_9857.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (178, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034170_9396.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (179, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034489_7659.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (180, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034587_7771.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (181, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034616_4585.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (182, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034692_1157.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (183, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034776_5598.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (184, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034866_4432.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (185, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596034930_9753.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (186, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035000_2474.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (187, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035125_8900.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (188, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035421_2857.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (189, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035512_2806.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (190, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035645_9271.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (191, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035756_6272.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (192, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035823_8606.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (193, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035897_1013.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (194, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596035993_4456.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (195, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596036097_3229.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (196, 'files/image/46/', NULL, 'image', 46, 1, NULL, true, NULL, NULL, '1596036281_7697.jpg', '2020-07-29', '2020-07-29');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (197, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596122975_2882.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (198, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596123019_2943.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (199, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596123055_2836.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (200, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596123079_6840.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (201, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596123100_5971.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (202, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596123119_6880.JPG', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (203, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596123145_9451.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (204, 'files/image/47/', NULL, 'image', 47, 1, NULL, true, NULL, NULL, '1596123333_4343.jfif', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (205, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596124395_2764.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (206, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596124424_9443.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (207, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596124556_9783.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (208, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596124712_7219.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (209, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596124870_6606.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (210, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596124917_2174.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (211, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596124950_2553.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (212, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596125016_7789.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (213, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596125060_2343.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (214, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596125080_4904.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (215, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596125113_8862.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (216, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596125268_9810.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (217, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596125357_5727.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (218, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1596125499_9099.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (219, 'files/image/48/', NULL, 'image', 48, 1, NULL, true, NULL, NULL, '1596125591_8721.jpg', '2020-07-30', '2020-07-30');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (220, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201177_9943.jfif', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (221, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201197_4397.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (839, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610615656_7621.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (852, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616229_1126.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (853, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616269_1716.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (870, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610617904_8757.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (875, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618091_7537.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (222, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201210_3668.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (226, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201260_6369.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (228, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201289_4958.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (746, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 3, NULL, '1609753652_1421.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (747, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 4, NULL, '1609753653_4548.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (748, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 5, NULL, '1609753653_1049.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (749, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 6, NULL, '1609753654_2630.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (750, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 8, NULL, '1609753656_9134.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (752, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 9, NULL, '1609753659_2139.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (753, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 10, NULL, '1609753660_6772.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (754, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 11, NULL, '1609753660_6925.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (755, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 12, NULL, '1609753661_6094.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (756, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 13, NULL, '1609753662_3686.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (757, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 14, NULL, '1609753663_8186.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (758, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 15, NULL, '1609753663_2627.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (785, 'files/image/70/', NULL, 'image', 70, 1, NULL, true, 0, NULL, '1610607529_3628.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (786, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 1, NULL, '1610607534_3716.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (787, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 2, NULL, '1610607535_7871.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (789, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 4, NULL, '1610607538_9022.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (802, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, NULL, NULL, '1610611310_7179.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (821, 'files/image/67/', NULL, 'image', 67, 1, NULL, true, NULL, NULL, '1610614404_6566.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (824, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614552_3620.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (825, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614597_4163.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (826, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614638_5608.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (827, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614682_5119.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (834, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610615504_3417.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (840, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610615698_7393.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (842, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610615756_3625.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (859, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616523_4289.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (861, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616605_4569.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (891, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619047_9102.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (903, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619320_8719.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (910, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619783_9552.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (914, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610620046_2514.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (917, 'files/image/20/', NULL, 'image', 20, 1, NULL, NULL, NULL, NULL, '1610692604_3276.jpg', '2021-01-15', '2021-01-15');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (223, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201221_2244.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (224, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201235_8892.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (225, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201250_2750.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (227, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201270_2137.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (229, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201317_1335.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (230, 'files/image/45/', NULL, 'image', 45, 1, NULL, NULL, NULL, NULL, '1596201325_2284.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (231, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207008_8247.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (232, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207009_3108.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (233, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207009_4141.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (234, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207010_2214.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (235, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207010_7124.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (236, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207011_8880.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (237, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207011_4513.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (238, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207012_2107.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (239, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207013_5278.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (240, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207014_6685.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (241, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207014_3073.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (242, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207015_9120.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (243, 'files/image/48/', NULL, 'image', 48, 1, NULL, NULL, NULL, NULL, '1596207015_2497.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (244, 'files/image/47/', NULL, 'image', 47, 1, NULL, NULL, NULL, NULL, '1596207219_9152.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (245, 'files/image/47/', NULL, 'image', 47, 1, NULL, NULL, NULL, NULL, '1596207220_2849.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (246, 'files/image/47/', NULL, 'image', 47, 1, NULL, NULL, NULL, NULL, '1596207220_2392.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (247, 'files/image/47/', NULL, 'image', 47, 1, NULL, NULL, NULL, NULL, '1596207221_8905.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (248, 'files/image/47/', NULL, 'image', 47, 1, NULL, NULL, NULL, NULL, '1596207222_3704.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (249, 'files/image/47/', NULL, 'image', 47, 1, NULL, NULL, NULL, NULL, '1596207223_1218.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (250, 'files/image/47/', NULL, 'image', 47, 1, NULL, NULL, NULL, NULL, '1596207223_6692.JPG', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (251, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207479_5364.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (252, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207479_8029.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (253, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207480_4087.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (254, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207481_8239.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (255, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207481_4440.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (256, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207482_1681.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (257, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207483_1704.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (258, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207484_2779.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (259, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207484_3885.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (260, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207485_6238.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (261, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207485_1277.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (262, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207486_1491.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (263, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207486_5354.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (264, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207487_4403.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (265, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207488_4996.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (266, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207489_1413.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (267, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207489_3148.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (268, 'files/image/46/', NULL, 'image', 46, 1, NULL, NULL, NULL, NULL, '1596207490_8608.jpg', '2020-07-31', '2020-07-31');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (270, 'files/image/49/', NULL, 'image', 49, 1, NULL, true, NULL, NULL, '1596708269_7031.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (272, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708434_2429.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (273, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708463_9432.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (274, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708491_3744.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (275, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708519_2277.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (276, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708613_6491.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (277, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708645_8077.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (278, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708666_9913.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (279, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708691_5528.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (280, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708715_5836.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (281, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596708741_1780.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (282, 'files/image/49/', NULL, 'image', 49, 1, 'editor', NULL, NULL, NULL, '1596709446_2938.jpg', '2020-08-06', '2020-08-06');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (283, 'files/image/50/', NULL, 'image', 50, 1, NULL, true, NULL, NULL, '1598353317_5513.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (284, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598354246_9583.jpeg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (285, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598354300_4534.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (286, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598354745_7691.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (287, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598354904_7841.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (288, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598354932_6215.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (289, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598355010_9484.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (290, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598355132_7573.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (291, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598356141_2100.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (292, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598356189_4221.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (293, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598356373_7840.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (294, 'files/image/50/', NULL, 'image', 50, 1, 'editor', NULL, NULL, NULL, '1598356515_2299.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (295, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356695_6483.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (296, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356695_7756.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (297, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356696_8207.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (298, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356697_7964.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (299, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356697_8373.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (300, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356698_5390.jpeg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (767, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860595_7428.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (768, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860597_5107.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (769, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860597_9644.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (770, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860598_8935.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (771, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860599_6779.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (301, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356699_7115.jfif', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (302, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356699_1551.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (303, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356700_7700.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (304, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356701_5272.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (305, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356701_3689.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (306, 'files/image/50/', NULL, 'image', 50, 1, NULL, NULL, NULL, NULL, '1598356702_2259.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (307, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598357925_5522.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (308, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598358013_9195.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (309, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598358041_4682.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (310, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598358430_6997.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (311, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598358463_5004.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (312, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598362765_5258.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (313, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598362879_7292.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (314, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598362904_9405.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (315, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598363082_4332.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (316, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598363113_7020.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (317, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598363136_4658.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (318, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598363367_5223.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (319, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598363410_4615.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (320, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598363480_4280.jpg', '2020-08-25', '2020-08-25');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (321, 'files/image/52/', NULL, 'image', 52, 1, NULL, true, NULL, NULL, '1598413230_6148.jfif', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (322, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413303_4585.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (323, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413404_5955.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (324, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413425_2494.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (325, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413566_8339.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (326, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413592_2183.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (327, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413626_9048.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (328, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413774_3568.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (329, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413802_1558.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (330, 'files/image/52/', NULL, 'image', 52, 1, 'editor', NULL, NULL, NULL, '1598413825_2947.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (331, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413884_3184.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (332, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413885_1436.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (333, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413886_2877.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (334, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413886_8208.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (335, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413887_4461.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (336, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413887_4594.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (337, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413888_1760.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (338, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413889_7443.jfif', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (339, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413890_2676.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (340, 'files/image/52/', NULL, 'image', 52, 1, NULL, NULL, NULL, NULL, '1598413890_8959.jpg', '2020-08-26', '2020-08-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (341, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598522405_2760.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (342, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598522543_4393.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (343, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598522663_1836.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (344, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598522693_7421.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (345, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598522753_8363.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (346, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1598522781_9036.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (347, 'files/image/53/', NULL, 'image', 53, 1, NULL, true, NULL, NULL, '1598522898_1597.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (348, 'files/image/53/', NULL, 'image', 53, 1, NULL, NULL, NULL, NULL, '1598522938_3047.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (349, 'files/image/53/', NULL, 'image', 53, 1, NULL, NULL, NULL, NULL, '1598522940_4849.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (350, 'files/image/53/', NULL, 'image', 53, 1, NULL, NULL, NULL, NULL, '1598522941_6676.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (351, 'files/image/53/', NULL, 'image', 53, 1, NULL, NULL, NULL, NULL, '1598522945_2250.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (352, 'files/image/53/', NULL, 'image', 53, 1, NULL, NULL, NULL, NULL, '1598522946_4129.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (353, 'files/image/53/', NULL, 'image', 53, 1, NULL, NULL, NULL, NULL, '1598522948_1756.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (354, 'files/image/53/', NULL, 'image', 53, 1, 'editor', NULL, NULL, NULL, '1598524524_9305.jpeg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (355, 'files/image/53/', NULL, 'image', 53, 1, 'editor', NULL, NULL, NULL, '1598524546_1003.jpg', '2020-08-27', '2020-08-27');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (356, 'files/image/54/', NULL, 'image', 54, 1, NULL, true, NULL, NULL, '1599145961_4839.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (358, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146060_3519.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (359, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146091_4367.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (361, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146342_5778.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (362, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146377_4511.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (363, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146421_8244.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (364, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146461_8154.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (365, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146488_7310.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (366, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146526_4076.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (367, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146668_4692.JPG', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (368, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146743_5352.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (369, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146762_6652.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (370, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599146982_6301.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (371, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147018_1365.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (372, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147044_2696.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (373, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147061_1050.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (374, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147103_8415.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (375, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147137_7935.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (376, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147177_1373.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (377, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147267_4877.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (772, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860600_2135.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (773, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860602_4131.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (378, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147306_3597.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (379, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147347_1632.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (380, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147380_6599.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (381, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147409_5117.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (382, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147439_1951.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (383, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147481_7612.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (384, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147516_7002.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (385, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147545_5708.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (386, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599147667_8109.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (387, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147730_3662.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (388, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147731_2837.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (389, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147732_3648.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (390, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147733_1290.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (391, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147734_7540.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (392, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147738_5620.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (393, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147739_7379.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (394, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147740_5766.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (395, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147741_1050.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (396, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147742_1816.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (397, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147744_2191.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (398, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147744_2802.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (399, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147746_7228.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (400, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147746_1081.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (401, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147747_4038.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (402, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147748_4889.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (403, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147749_3242.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (404, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147751_6089.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (405, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147753_7093.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (406, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147754_1655.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (407, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147755_3417.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (408, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147756_5195.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (409, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147757_9081.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (410, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147758_5493.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (411, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147760_4564.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (412, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147761_6178.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (413, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147762_7618.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (414, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599147764_7953.jpg', '2020-09-03', '2020-09-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (415, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599490406_3517.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (416, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599490472_7242.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (417, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599490514_2296.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (418, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599490588_8681.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (419, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599490781_3083.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (420, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599490874_2492.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (421, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599490917_4868.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (422, 'files/image/54/', NULL, 'image', 54, 1, 'editor', NULL, NULL, NULL, '1599490947_5485.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (423, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599491072_9110.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (424, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599491072_7940.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (425, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599491073_1574.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (426, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599491074_9275.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (427, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599491075_2511.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (428, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599491075_7004.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (429, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599491076_6155.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (430, 'files/image/54/', NULL, 'image', 54, 1, NULL, NULL, NULL, NULL, '1599491077_5977.jpg', '2020-09-07', '2020-09-07');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (431, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652065_2747.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (432, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652101_1031.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (433, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652122_5421.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (434, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652156_4777.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (435, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652191_8509.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (436, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652270_8954.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (437, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652398_1098.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (438, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652593_5812.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (439, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652616_9350.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (440, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652640_8072.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (441, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652672_8301.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (442, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652693_6299.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (443, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652713_2776.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (444, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652731_6545.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (445, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652751_2984.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (446, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652769_4086.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (447, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1599652791_5093.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (448, 'files/image/55/', NULL, 'image', 55, 1, NULL, true, NULL, NULL, '1599652904_5699.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (449, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652929_7314.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (450, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652929_7878.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (451, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652930_4544.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (452, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652931_8525.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (453, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652931_3430.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (454, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652932_5435.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (455, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652933_1109.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (456, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652934_4978.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (459, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652935_1633.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (462, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652938_4004.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (464, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652939_5513.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (763, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 7, NULL, '1609754642_4879.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (774, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860603_1891.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (775, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860604_2253.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (776, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860605_7958.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (777, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860606_4962.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (778, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860606_2221.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (779, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860607_7368.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (780, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860608_2471.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (781, 'files/image/66/', NULL, 'image', 66, 1, NULL, NULL, NULL, NULL, '1609860609_4911.jpg', '2021-01-05', '2021-01-05');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (788, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 3, NULL, '1610607536_4539.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (795, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 10, NULL, '1610607548_2781.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (797, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 12, NULL, '1610607550_5383.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (803, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, NULL, NULL, '1610611644_7022.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (812, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, NULL, NULL, '1610611700_9358.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (828, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614752_3839.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (832, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614926_2257.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (848, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616074_4643.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (850, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616162_2210.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (855, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616347_4941.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (857, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616420_8880.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (858, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616473_3771.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (860, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616565_9709.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (869, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610617850_2687.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (872, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610617983_8796.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (877, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618145_8394.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (878, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618199_7404.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (880, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618374_5177.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (882, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618473_4442.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (886, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618606_9775.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (889, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618710_6030.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (893, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619110_7629.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (911, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619822_8087.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (915, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610620114_1429.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (457, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652934_9112.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (458, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652935_5620.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (460, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652936_5677.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (461, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652937_8333.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (463, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652938_2026.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (465, 'files/image/55/', NULL, 'image', 55, 1, NULL, NULL, NULL, NULL, '1599652940_3127.jpg', '2020-09-09', '2020-09-09');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (466, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600065168_1795.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (467, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600097559_5434.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (468, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600097595_8535.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (469, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600097634_4378.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (470, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600097669_3173.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (471, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600097702_4297.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (472, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600097920_8682.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (473, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600097960_4199.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (474, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098004_1781.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (475, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098036_5880.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (476, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098063_2682.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (477, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098090_5982.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (478, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098122_2307.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (479, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098181_2779.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (480, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098214_7599.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (481, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098247_3775.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (482, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098278_8201.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (483, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098308_4704.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (484, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098340_4174.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (485, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098368_7262.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (486, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600098392_6856.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (487, 'files/image/56/', NULL, 'image', 56, 1, NULL, true, NULL, NULL, '1600098448_6735.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (488, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098473_9732.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (489, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098474_8937.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (490, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098475_1283.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (491, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098475_8828.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (492, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098476_3960.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (493, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098477_9066.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (494, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098478_5414.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (495, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098478_4882.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (496, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098479_3509.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (497, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098481_1581.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (498, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098483_7001.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (499, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098483_8143.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (500, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098484_9015.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (501, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098485_5068.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (502, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098485_1897.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (503, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098486_5036.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (504, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098486_7267.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (505, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098487_8945.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (506, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098488_1220.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (507, 'files/image/56/', NULL, 'image', 56, 1, NULL, NULL, NULL, NULL, '1600098489_1215.jpg', '2020-09-14', '2020-09-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (508, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339708_6789.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (509, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339727_9084.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (510, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339746_9702.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (511, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339787_8313.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (512, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339814_8641.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (513, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339828_6256.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (514, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339843_6615.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (515, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339857_4251.jpeg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (516, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339871_4032.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (517, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339886_4503.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (518, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339902_3064.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (519, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339922_8896.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (520, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339941_6113.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (521, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339958_2785.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (522, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339976_9120.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (523, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600339992_2731.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (524, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340007_9589.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (525, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340024_3541.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (526, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340041_8081.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (527, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340056_2015.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (528, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340074_1024.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (529, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340088_5018.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (530, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340101_3368.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (531, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340116_8861.JPG', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (532, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340128_7173.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (533, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340278_9017.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (534, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340293_4043.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (535, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340538_2311.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (536, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340550_5134.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (537, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1600340565_7576.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (538, 'files/image/57/', NULL, 'image', 57, 1, NULL, true, NULL, NULL, '1600340590_5638.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (539, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340605_3001.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (540, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340605_7215.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (541, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340606_9572.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (542, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340607_3275.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (543, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340608_6835.jpeg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (544, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340608_6581.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (545, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340609_9257.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (546, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340610_7877.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (547, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340611_6598.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (548, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340611_6714.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (549, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340612_6369.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (550, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340613_6172.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (551, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340614_2007.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (552, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340614_6564.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (553, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340615_5515.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (554, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340616_2708.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (555, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340616_6035.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (556, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340617_4342.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (557, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340618_1944.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (558, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340618_1170.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (559, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340620_5125.JPG', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (560, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340621_6285.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (561, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340621_6718.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (562, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340622_6237.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (563, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340623_4716.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (564, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340623_3289.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (565, 'files/image/57/', NULL, 'image', 57, 1, NULL, NULL, NULL, NULL, '1600340624_7471.jpg', '2020-09-17', '2020-09-17');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (566, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608555225_7647.jpg', '2020-12-21', '2020-12-21');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (567, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608555233_8918.jpg', '2020-12-21', '2020-12-21');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (568, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608555256_4586.jpg', '2020-12-21', '2020-12-21');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (569, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608555406_9850.jpg', '2020-12-21', '2020-12-21');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (570, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608555434_2686.jpg', '2020-12-21', '2020-12-21');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (571, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608555567_6505.jpg', '2020-12-21', '2020-12-21');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (572, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608563381_4977.jpg', '2020-12-21', '2020-12-21');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (575, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608615344_6785.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (576, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608615733_7129.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (577, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608615766_4410.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (578, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608615795_5509.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (579, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608615917_6345.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (580, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608615984_2323.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (581, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616017_6653.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (582, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616037_9702.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (583, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616307_3489.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (584, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616327_7307.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (585, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616344_4956.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (586, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616568_5295.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (587, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616593_3514.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (588, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616619_8443.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (589, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608616642_8584.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (599, 'files/image/58/', NULL, 'image', 58, 1, NULL, true, NULL, NULL, '1608617500_6796.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (607, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608647836_9520.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (600, 'files/image/58/', NULL, 'image', 58, 1, NULL, NULL, NULL, NULL, '1608617501_4919.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (601, 'files/image/58/', NULL, 'image', 58, 1, NULL, NULL, NULL, NULL, '1608617502_5711.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (602, 'files/image/58/', NULL, 'image', 58, 1, NULL, NULL, NULL, NULL, '1608617502_8997.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (603, 'files/image/58/', NULL, 'image', 58, 1, NULL, NULL, NULL, NULL, '1608617503_3886.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (608, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608647862_7022.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (604, 'files/image/58/', NULL, 'image', 58, 1, NULL, NULL, NULL, NULL, '1608617504_2606.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (609, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608647887_1292.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (605, 'files/image/58/', NULL, 'image', 58, 1, NULL, NULL, NULL, NULL, '1608617505_5898.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (606, 'files/image/58/', NULL, 'image', 58, 1, NULL, NULL, NULL, NULL, '1608617505_9347.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (610, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608647951_7956.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (611, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608647974_9511.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (612, 'files/image/59/', NULL, 'image', 59, 1, NULL, true, NULL, NULL, '1608648013_1097.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (613, 'files/image/59/', NULL, 'image', 59, 1, NULL, NULL, NULL, NULL, '1608648014_9526.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (614, 'files/image/59/', NULL, 'image', 59, 1, NULL, NULL, NULL, NULL, '1608648014_9971.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (615, 'files/image/59/', NULL, 'image', 59, 1, NULL, NULL, NULL, NULL, '1608648015_2460.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (616, 'files/image/59/', NULL, 'image', 59, 1, NULL, NULL, NULL, NULL, '1608648015_8591.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (617, 'files/image/59/', NULL, 'image', 59, 1, NULL, NULL, NULL, NULL, '1608648016_3128.jpg', '2020-12-22', '2020-12-22');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (618, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608741959_4193.jpg', '2020-12-23', '2020-12-23');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (619, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608742348_5805.jpg', '2020-12-23', '2020-12-23');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (620, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608742366_1226.jpg', '2020-12-23', '2020-12-23');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (621, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608742386_3635.jpg', '2020-12-23', '2020-12-23');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (622, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608742410_8364.jpg', '2020-12-23', '2020-12-23');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (623, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608742447_6244.jpg', '2020-12-23', '2020-12-23');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (624, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608742696_4490.jpg', '2020-12-23', '2020-12-23');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (723, 'files/image/61/', NULL, 'image', 61, 1, NULL, false, 1, NULL, '1609357179_9148.jpg', '2020-12-30', '2021-01-12');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (657, 'files/image/62/', NULL, 'image', 62, 1, NULL, true, NULL, NULL, '1608827099_6160.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (790, 'files/image/70/', NULL, 'image', 70, 1, NULL, NULL, 5, NULL, '1610607540_1994.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (881, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618419_4667.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (883, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618519_1498.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (730, 'files/image/20/', NULL, 'image', 20, 1, NULL, true, 0, NULL, '1609586458_9716.jpg', '2021-01-02', '2021-01-03');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (761, 'files/image/63/', NULL, 'image', 63, 1, NULL, NULL, 2, NULL, '1609754131_8147.jpg', '2021-01-04', '2021-01-04');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (782, 'files/image/68/', NULL, 'image', 68, 1, NULL, true, NULL, NULL, '1610081321_9327.jpg', '2021-01-08', '2021-01-08');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (888, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618668_3096.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (829, 'files/image/67/', NULL, 'image', 67, 1, NULL, NULL, NULL, NULL, '1610614794_4598.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (833, 'files/image/69/', NULL, 'image', 69, 1, NULL, true, NULL, NULL, '1610615463_1708.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (836, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610615577_5357.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (847, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616035_2964.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (849, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616116_9490.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (864, 'files/image/69/', NULL, 'image', 69, 1, NULL, NULL, NULL, NULL, '1610616768_8887.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (868, 'files/image/64/', NULL, 'image', 64, 1, NULL, true, NULL, NULL, '1610617820_7426.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (871, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610617944_3247.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (873, 'files/image/64/', NULL, 'image', 64, 1, NULL, NULL, NULL, NULL, '1610618033_1014.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (890, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619016_1925.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (705, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165207_8000.jpg', '2020-12-28', '2020-12-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (643, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826558_1349.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (644, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826577_3587.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (645, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826620_9148.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (646, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826655_1980.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (647, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826687_4877.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (648, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826713_6911.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (649, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826737_8371.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (650, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826801_4591.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (651, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826829_5539.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (652, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826854_9246.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (653, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826874_9349.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (654, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608826893_5555.JPG', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (655, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608827034_1821.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (656, 'files/image/0/', NULL, 'image', 0, 1, 'editor', NULL, NULL, NULL, '1608827052_6150.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (658, 'files/image/62/', NULL, 'image', 62, 1, NULL, NULL, NULL, NULL, '1608827099_8486.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (659, 'files/image/62/', NULL, 'image', 62, 1, NULL, NULL, NULL, NULL, '1608827100_9669.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (660, 'files/image/62/', NULL, 'image', 62, 1, NULL, NULL, NULL, NULL, '1608827100_3288.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (898, 'files/image/61/', NULL, 'image', 61, 1, NULL, NULL, NULL, NULL, '1610619179_1088.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (900, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619204_9351.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (901, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619242_8542.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (902, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619277_6565.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (904, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619364_7486.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (912, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619867_5960.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (913, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610619909_8191.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (670, 'files/image/62/', NULL, 'image', 62, 1, NULL, NULL, NULL, NULL, '1608827107_8983.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (671, 'files/image/62/', NULL, 'image', 62, 1, NULL, NULL, NULL, NULL, '1608827108_2878.jpg', '2020-12-24', '2020-12-24');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (916, 'files/image/68/', NULL, 'image', 68, 1, NULL, NULL, NULL, NULL, '1610620159_1326.jpg', '2021-01-14', '2021-01-14');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (706, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165224_6090.jpg', '2020-12-28', '2020-12-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (707, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165251_7870.jpg', '2020-12-28', '2020-12-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (708, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165271_1173.jpg', '2020-12-28', '2020-12-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (709, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165303_5587.jpg', '2020-12-28', '2020-12-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (710, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165325_4638.jpg', '2020-12-28', '2020-12-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (711, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165413_3006.jpg', '2020-12-28', '2020-12-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (684, 'files/image/60/', NULL, 'image', 60, 1, NULL, true, NULL, NULL, '1608963378_4568.jpg', '2020-12-26', '2020-12-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (685, 'files/image/60/', NULL, 'image', 60, 1, NULL, NULL, NULL, NULL, '1608963415_7799.jpg', '2020-12-26', '2020-12-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (686, 'files/image/60/', NULL, 'image', 60, 1, NULL, NULL, NULL, NULL, '1608963416_1511.jpg', '2020-12-26', '2020-12-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (687, 'files/image/60/', NULL, 'image', 60, 1, NULL, NULL, NULL, NULL, '1608963416_1019.jpg', '2020-12-26', '2020-12-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (688, 'files/image/60/', NULL, 'image', 60, 1, NULL, NULL, NULL, NULL, '1608963417_2895.jpg', '2020-12-26', '2020-12-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (689, 'files/image/60/', NULL, 'image', 60, 1, NULL, NULL, NULL, NULL, '1608963417_7574.jpg', '2020-12-26', '2020-12-26');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (712, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165449_4571.jpg', '2020-12-28', '2020-12-28');
INSERT INTO public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") VALUES (713, 'files/image/62/', NULL, 'image', 62, 1, 'editor', NULL, NULL, NULL, '1609165480_4655.jpg', '2020-12-28', '2020-12-28');


--
-- Data for Name: Firms; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--



--
-- Data for Name: Hash_tags; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (3, '2019-08-02', '2019-08-02', 'Яблоки', 'apples', '3 - Вильгельм Конрад Рентген родился 17 марта 1845 г. в пограничной с Голландией области Германии, в городе Ленепе. Он получил техническое образование в Цюрихе в той самой Высшей технической школе (политехникуме), в которой позже учился Эйнштейн. Увлечение физикой заставило его после окончания школы в 1866 г. продолжить физическое образование. Защитив в 1868 г. диссертацию на степень доктора философии, он работал ассистентом на кафедре физики сначала в Цюрихе, потом в Гисене, а затем в Страсбурге (1874-1879) у Кундта.', 'images/banner/home-banner.jpg');
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (2, '2019-08-02', '2019-08-02', 'Бананы', 'bananas', '2 - Вильгельм Конрад Рентген родился 17 марта 1845 г. в пограничной с Голландией области Германии, в городе Ленепе. Он получил техническое образование в Цюрихе в той самой Высшей технической школе (политехникуме), в которой позже учился Эйнштейн. Увлечение физикой заставило его после окончания школы в 1866 г. продолжить физическое образование. Защитив в 1868 г. диссертацию на степень доктора философии, он работал ассистентом на кафедре физики сначала в Цюрихе, потом в Гисене, а затем в Страсбурге (1874-1879) у Кундта.
', 'images/banner/banner-2.jpg');
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (4, '2019-09-10', '2019-09-10', 'Груши', 'pears', '4- Вильгельм Конрад Рентген родился 17 марта 1845 г. в пограничной с Голландией области Германии, в городе Ленепе. Он получил техническое образование в Цюрихе в той самой Высшей технической школе (политехникуме), в которой позже учился Эйнштейн. Увлечение физикой заставило его после окончания школы в 1866 г. продолжить физическое образование. Защитив в 1868 г. диссертацию на степень доктора философии, он работал ассистентом на кафедре физики сначала в Цюрихе, потом в Гисене, а затем в Страсбурге (1874-1879) у Кундта.', 'images/banner/home-banner.jpg');
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (5, '2019-09-10', '2019-09-10', 'Персики', 'peaches', '5 - Вильгельм Конрад Рентген родился 17 марта 1845 г. в пограничной с Голландией области Германии, в городе Ленепе. Он получил техническое образование в Цюрихе в той самой Высшей технической школе (политехникуме), в которой позже учился Эйнштейн. Увлечение физикой заставило его после окончания школы в 1866 г. продолжить физическое образование. Защитив в 1868 г. диссертацию на степень доктора философии, он работал ассистентом на кафедре физики сначала в Цюрихе, потом в Гисене, а затем в Страсбурге (1874-1879) у Кундта.', 'images/banner/banner.jpg');
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (6, '2019-09-10', '2019-09-10', 'Виноград', 'grape', '6 - Вильгельм Конрад Рентген родился 17 марта 1845 г. в пограничной с Голландией области Германии, в городе Ленепе. Он получил техническое образование в Цюрихе в той самой Высшей технической школе (политехникуме), в которой позже учился Эйнштейн. Увлечение физикой заставило его после окончания школы в 1866 г. продолжить физическое образование. Защитив в 1868 г. диссертацию на степень доктора философии, он работал ассистентом на кафедре физики сначала в Цюрихе, потом в Гисене, а затем в Страсбурге (1874-1879) у Кундта.', 'images/banner/banner.jpg');
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (7, '2019-09-10', '2019-09-10', 'Дыни', 'melons', '7- Вильгельм Конрад Рентген родился 17 марта 1845 г. в пограничной с Голландией области Германии, в городе Ленепе. Он получил техническое образование в Цюрихе в той самой Высшей технической школе (политехникуме), в которой позже учился Эйнштейн. Увлечение физикой заставило его после окончания школы в 1866 г. продолжить физическое образование. Защитив в 1868 г. диссертацию на степень доктора философии, он работал ассистентом на кафедре.', 'images/banner/home-banner.jpg');
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (27, '2021-01-02', '2021-01-02', 'курорт', 'kurort', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (28, '2021-01-02', '2021-01-02', 'сезон', 'sezon', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (29, '2021-01-02', '2021-01-02', 'склон', 'sklon', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (30, '2021-01-02', '2021-01-02', 'горнолыжный', 'gornolyzhnyi', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (31, '2021-01-02', '2021-01-02', 'трасса', 'trassa', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (32, '2021-01-02', '2021-01-02', 'горнолыжные курорты', 'gornolyzhnye-kurorty', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (33, '2021-01-02', '2021-01-02', 'казахстан', 'kazahstan', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (34, '2021-01-02', '2021-01-02', 'горный', 'gornyi', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (35, '2021-01-02', '2021-01-02', 'covid-19', 'covid-19', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (36, '2021-01-02', '2021-01-02', 'турция', 'turciya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (37, '2021-01-02', '2021-01-02', 'рейс', 'reis', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (38, '2021-01-02', '2021-01-02', 'россия', 'rossiya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (39, '2021-01-02', '2021-01-02', 'виза', 'viza', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (40, '2021-01-02', '2021-01-02', 'мальдивы', 'maldivy', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (41, '2021-01-02', '2021-01-02', 'отель', 'otel', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (42, '2021-01-02', '2021-01-02', 'турист', 'turist', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (43, '2021-01-02', '2021-01-02', 'раждан россии', 'razhdan-rossii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (44, '2021-01-02', '2021-01-02', 'куба', 'kuba', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (48, '2021-01-02', '2021-01-02', 'танзания', 'tanzaniya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (45, '2021-01-02', '2021-01-02', 'карантин', 'karantin', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (46, '2021-01-02', '2021-01-02', 'оаэ', 'oae', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (47, '2021-01-02', '2021-01-02', 'паспортном контроле', 'pasportnom-kontrole', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (49, '2021-01-02', '2021-01-02', 'отдых', 'otdyh', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (50, '2021-01-02', '2021-01-02', 'туристический', 'turisticheskii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (51, '2021-01-02', '2021-01-02', 'аэропорт', 'aeroport', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (52, '2021-01-02', '2021-01-02', 'отдых в турции', 'otdyh-v-turcii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (53, '2021-01-02', '2021-01-02', 'пандемия', 'pandemiya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (54, '2021-01-02', '2021-01-02', 'посольство', 'posolstvo', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (55, '2021-01-02', '2021-01-02', 'египет', 'egipet', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (56, '2021-01-02', '2021-01-02', 'египетский', 'egipetskii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (57, '2021-01-02', '2021-01-02', 'отдых в египете', 'otdyh-v-egipete', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (58, '2021-01-02', '2021-01-02', 'египтянин', 'egiptyanin', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (59, '2021-01-02', '2021-01-02', 'поездка', 'poezdka', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (60, '2021-01-02', '2021-01-02', 'пляж', 'plyazh', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (61, '2021-01-02', '2021-01-02', 'зима', 'zima', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (62, '2021-01-02', '2021-01-02', 'море', 'more', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (63, '2021-01-02', '2021-01-02', 'россиянин', 'rossiyanin', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (64, '2021-01-02', '2021-01-02', 'температура воздуха', 'temperatura-vozduha', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (65, '2021-01-02', '2021-01-02', 'италия', 'italiya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (66, '2021-01-02', '2021-01-02', 'hotel', 'hotel', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (67, '2021-01-02', '2021-01-02', 'отдых в италии', 'otdyh-v-italii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (68, '2021-01-02', '2021-01-02', 'стоимость одной ночи', 'stoimost-odnoi-nochi', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (69, '2021-01-02', '2021-01-02', 'билет', 'bilet', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (70, '2021-01-02', '2021-01-02', 'итальянский', 'italyanskii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (71, '2021-01-02', '2021-01-02', 'приехавших на отдых в италию', 'priehavshih-na-otdyh-v-italiyu', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (72, '2021-01-02', '2021-01-02', 'абхазия', 'abhaziya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (73, '2021-01-02', '2021-01-02', 'отдых в абхазии', 'otdyh-v-abhazii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (74, '2021-01-02', '2021-01-02', 'двухместный номер', 'dvuhmestnyi-nomer', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (75, '2021-01-02', '2021-01-02', 'апартамент', 'apartament', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (76, '2021-01-02', '2021-01-02', 'узбекистан', 'uzbekistan', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (77, '2021-01-02', '2021-01-02', 'отдых узбекистан', 'otdyh-uzbekistan', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (78, '2021-01-02', '2021-01-02', 'отдых в узбекистане', 'otdyh-v-uzbekistane', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (79, '2021-01-02', '2021-01-02', 'ташкент', 'tashkent', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (80, '2021-01-02', '2021-01-02', 'бухара', 'buhara', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (81, '2021-01-02', '2021-01-02', 'самарканд', 'samarkand', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (82, '2021-01-02', '2021-01-02', 'таиланд', 'tailand', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (83, '2021-01-02', '2021-01-02', 'гостиница', 'gostinica', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (84, '2021-01-02', '2021-01-02', 'пхукет', 'phuket', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (85, '2021-01-02', '2021-01-02', 'airway', 'airway', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (86, '2021-01-02', '2021-01-02', 'израиль', 'izrail', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (87, '2021-01-02', '2021-01-02', 'хорватия', 'horvatiya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (88, '2021-01-02', '2021-01-02', 'загреб', 'zagreb', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (89, '2021-01-02', '2021-01-02', 'остров', 'ostrov', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (90, '2021-01-02', '2021-01-02', 'отдыхающий', 'otdyhayushchii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (91, '2021-01-02', '2021-01-02', 'хорватский', 'horvatskii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (92, '2021-01-02', '2021-01-02', 'египе', 'egipe', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (93, '2021-01-02', '2021-01-02', 'шарм-эль-шейх', 'sharm-el-sheih', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (94, '2021-01-02', '2021-01-02', 'греция', 'greciya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (95, '2021-01-02', '2021-01-02', 'греческий', 'grecheskii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (96, '2021-01-02', '2021-01-02', 'афина', 'afina', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (97, '2021-01-02', '2021-01-02', 'тур', 'tur', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (98, '2021-01-02', '2021-01-02', 'туры в турцию', 'tury-v-turciyu', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (99, '2021-01-02', '2021-01-02', 'турецкий', 'tureckii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (100, '2021-01-02', '2021-01-02', 'анталия', 'antaliya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (101, '2021-01-02', '2021-01-02', 'кипр', 'kipr', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (102, '2021-01-02', '2021-01-02', 'болгария', 'bolgariya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (103, '2021-01-02', '2021-01-02', 'франция', 'franciya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (104, '2021-01-02', '2021-01-02', 'кипрский', 'kiprskii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (105, '2021-01-02', '2021-01-02', 'пересадкой москве', 'peresadkoi-moskve', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (106, '2021-01-03', '2021-01-03', 'стоимость', 'stoimost', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (107, '2021-01-03', '2021-01-03', '3 турист', '3-turist', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (108, '2021-01-03', '2021-01-03', 'цена', 'cena', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (109, '2021-01-03', '2021-01-03', 'отдых на пляже', 'otdyh-na-plyazhe', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (110, '2021-01-03', '2021-01-03', 'лето', 'leto', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (119, '2021-01-03', '2021-01-03', 'граждан россии', 'grazhdan-rossii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (121, '2021-01-04', '2021-01-04', 'термальный', 'termalnyi', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (122, '2021-01-04', '2021-01-04', 'источник', 'istochnik', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (123, '2021-01-04', '2021-01-04', 'термального источника', 'termalnogo-istochnika', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (124, '2021-01-04', '2021-01-04', 'горнолыжных курортов', 'gornolyzhnyh-kurortov', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (125, '2021-01-05', '2021-01-05', 'лыжник', 'lyzhnik', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (126, '2021-01-05', '2021-01-05', 'горнолыжный курорт', 'gornolyzhnyi-kurort', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (127, '2021-01-05', '2021-01-05', 'шамони', 'shamoni', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (128, '2021-01-05', '2021-01-05', '123', '123', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (129, '2021-01-08', '2021-01-08', 'канатный', 'kanatnyi', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (130, '2021-01-08', '2021-01-08', 'комплекс', 'kompleks', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (131, '2021-01-08', '2021-01-08', 'подъемник', 'podemnik', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (132, '2021-01-08', '2021-01-08', 'канатной дорогой', 'kanatnoi-dorogoi', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (133, '2021-01-08', '2021-01-08', 'остиница', 'ostinica', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (134, '2021-01-08', '2021-01-08', 'кукисвумчорр', 'kukisvumchorr', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (135, '2021-01-12', '2021-01-12', 'австрия', 'avstriya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (136, '2021-01-12', '2021-01-12', 'леха', 'leha', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (137, '2021-01-12', '2021-01-12', 'австрийский', 'avstriiskii', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (138, '2021-01-12', '2021-01-12', 'горнолыжных курортах', 'gornolyzhnyh-kurortah', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (139, '2021-01-12', '2021-01-12', 'майрхофен', 'mairhofen', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (140, '2021-01-12', '2021-01-12', 'шладминг', 'shladming', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (141, '2021-01-12', '2021-01-12', 'бад-гаштайн', 'bad-gashtain', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (142, '2021-01-12', '2021-01-12', 'tag1', 'tag1', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (143, '2021-01-13', '2021-01-13', 'tag 2', 'tag-2', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (144, '2021-01-14', '2021-01-14', 'amirsoy', 'amirsoy', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (145, '2021-01-14', '2021-01-14', 'курорт amirsoy resort', 'kurort-amirsoy-resort', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (146, '2021-01-14', '2021-01-14', 'oscar lane oscar', 'oscar-lane-oscar', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (147, '2021-01-14', '2021-01-14', 'resort', 'resort', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (148, '2021-01-14', '2021-01-14', 'отдыхающая', 'otdyhayushchaya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (149, '2021-01-14', '2021-01-14', 'lane oscar lane', 'lane-oscar-lane', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (150, '2021-01-14', '2021-01-14', 'гриндевальд', 'grindevald', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (151, '2021-01-14', '2021-01-14', 'швейцария', 'shveicariya', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (152, '2021-01-14', '2021-01-14', 'горнолыжник', 'gornolyzhnik', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (153, '2021-01-14', '2021-01-14', 'кран-монтан', 'kran-montan', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (154, '2021-01-14', '2021-01-14', 'санкт-мориц', 'sankt-moric', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (155, '2021-01-14', '2021-01-14', 'лейкербард', 'leikerbard', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (156, '2021-01-14', '2021-01-14', 'цермат', 'cermat', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (157, '2021-01-14', '2021-01-14', 'катание', 'katanie', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (158, '2021-01-14', '2021-01-14', 'арк', 'ark', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (159, '2021-01-14', '2021-01-14', 'европа', 'evropa', NULL, NULL);
INSERT INTO public."Hash_tags" (id, "createdAt", "updatedAt", name, slug, description, image) VALUES (160, '2021-01-14', '2021-01-14', 'зимнего отдыха', 'zimnego-otdyha', NULL, NULL);


--
-- Data for Name: Images; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (64, 'Самые популярные горнолыжные курорты Франции', 1, NULL, NULL, NULL, 'samye-populyarnye-v-mire-gornolyzhnye-kurorty-francii', '2021-01-05', '2021-01-14', 'Этой статьей мы решили начать серию публикаций, посвященную горнолыжным курортам разных стран. В них мы постараемся разместить возможно большее количество информации о самых популярных местах зимнего отдыха в каждой стране мира. Начнем с Франции.', true, 2, 0, NULL, NULL, NULL, '<p>В нашем обзоре мы не будем рассказывать о дорогостоящих и богемных курортах, ведь о них, итак, все знают. Наше повествование о популярных местах зимнего отдыха, предоставляющих высокий уровень услуг по вполне приемлемым ценам, что позволяет отдыхать на них не только толстосумам.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610617850_2687.jpg" /></p>

<h2>Арк 1950</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610617904_8757.jpg" /></p>

<p>Этот горнолыжный курорт, являющийся адекватной альтернативой обожаемым богемой Куршавелю и Мерибелю, входит в тройку лучших во Франции. Уютный базовый поселок расположен так удобно, что до него без труда добираются из Гренобля и Лиона, Шамбери и Женевы. Здесь имеются пункты проката машин, для которой выделяется место на паркинге.</p>

<p>По Арка 1950 не ездят машины, что соответствует принятой властями этого городка концепции, но свободно передвигаются пешеходы, сноубордисты и лыжники. Последними практикуется &laquo;катание от дверей&raquo; &mdash; когда лыжня начинается прямо от дверей апартаментов.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610617944_3247.jpg" /></p>

<p>Курорт считается самым молодым из четырех Лез Арков. Его построили 10 лет назад и по сей день он признается лучшим местом доступа в Парадиски (лыжный рай) &mdash; зону катания, занимающую по размерам второе место в мировом рейтинге. Размах, с которым построен этот комплекс не может никого оставить равнодушным: лыжников обслуживает 144 подъемника, курсирующих по десяткам трасс, протяженностью свыше 425 километров.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610617983_8796.jpg" /></p>

<p>На местных склонах имеются маршруты как для профессионалов, так и для новичков. Желающим получить острые ощущения предложат испытать свои силы в скиджоринге (когда лыжника тянет лошадь) и скоростных гонках на санках, а для любителей прогулок на природе предоставляются снегоходы. Увидеть одновременно Швейцарию, Францию и Италию можно во время двадцатиминутного полета на вертолете. Здесь найдется где покататься любителям бобслея и хафпайпа.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618033_1014.jpg" /></p>

<h2>Шамони</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618091_7537.jpg" /></p>

<p>Обосновали этот горнолыжный курорт еще 200 лет назад. Его отличительная черта &mdash; высокая посещаемость не только зимой, но и летом. В теплое время года в окрестностях занимаются хайкингом и гольфом, а также ездят на горных велосипедах и совершают пешие прогулки, любуясь живописными окрестностями.</p>

<p>Шамони расположился в долине у подножья Монблана (самая высокая вершина в Западной Европе) на высоте 1200 м над уровнем моря. В этом патриархе отрасли, имеющем идеальную репутацию, закладывались все ныне действующие каноны отдыха на горнолыжных курортах. На отдых сюда приезжает как респектабельная публика, так и молодежь, тяготеющая к экстриму.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618145_8394.jpg" /></p>

<p>Большая часть лыжных трасс проложена на высоте, превышающей отметку в 2000 м. До вершины Эгюй-дю-Миди (3842 м) прямо из центра Шамони доставит фуникулер, считающийся самым высоким в Европе. Отсюда любителям экстремальных спусков открывается вид на знаменитую Белую долину &mdash; спуск, проложенный через ледники на протяжении двадцати двух километров. 85 трасс обслуживается шести десятью подъемниками.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618199_7404.jpg" /></p>

<p>Шамони &mdash; несколько деревень, объединенных в единый организм. Начинающим лыжникам лучше отправиться в район Ле-Тур, пологие склоны которого по силам одолеть даже тем, у кого совсем нет опыта. Для семейных отдыхающих лучше подойдет район Лез-Уш с его плавными трассами, проложенными среди лесных массивов. Любителям сложных маршрутов рекомендуется отправиться в район Гран-Монте, где их начало находится на высоте 3 300 м, а окончание &mdash; на 1235м.</p>

<p>За один день пользования ски-пас взрослому отдыхающему придется заплатить от &euro;54, а за неделю &mdash; &euro;306. Эксплуатация полного комплекта снаряжения на протяжении одного дня обойдется в &euro;100. За &euro;20 можно на полдня отдать ребенка в детский клуб или детсад.</p>

<h2>Межев</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618374_5177.jpg" /></p>

<p>Как гласит легенда, инициаторами образования зоны зимнего отдыха выступило семейство Ротшильд, по мнению которых, это живописное место Французских Альп как нельзя лучше подходило для размещения элитного места отдыха для богемы. Но время берет свое и сегодня свои двери гостям открывают не только роскошные шале а-ля le Chalet Du Mont D&#39;Arbois, но и вполне демократичные жилища, входящие в комплекс Loges Blanches.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618419_4667.jpg" style="height:763px; width:1200px" /></p>

<p><em>Chalet Du Mont D&#39;Arbois</em></p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618473_4442.jpg" /></p>

<p><em>Loges Blanches</em></p>

<p>Межев, по праву считающийся самым живописным горнолыжным курортом с мировой известностью, находится в Верхней Савойе. Для катания он разбит на 3 области: Мон-д&#39;Арбуа, Рошбрюн-Кот, Ле Жай. Перемещение к последней возможно на конной повозке. В каждой из зон идеального условия для начинающих лыжников и любителей семейного отдыха, ведь максимальная высота трасс не превышает 2350 м, а до их начала можно на подъемнике прямо с центральной площади деревни.</p>

<p>Но это вовсе не означает, что здесь нечего делать опытным лыжникам и любителям экстрима. В их распоряжении такие трассы, как Росри и Черная принцесса. Для фрирайдеров имеется спуск с горы Жоли и тот, который носит название Кот 2000. Начинающим покорителям горных склонов предлагается склон Мандарин, отличающийся повышенной комфортностью.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618519_1498.jpg" /></p>

<p>Доехать до Межева лучше всего с аэропорта Женевы. Летом эта местность наводняется гольфистами, которых привлекают местные гольф-поля, считающиеся эталонными. По этой причине здесь часто проводятся соревнования среди профессионалов. Кроме того, летом сюда едут любители хайкинга.<br />
Проживание в отелях Ле-Фер-а-Шеваль, Монблан и большом количестве шале-полупансионах стоит от &euro;100 за сутки на одного человека.</p>

<h2>Ле-Дез-Альп</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618606_9775.jpg" /></p>

<p>50 лет назад на двух альпийских пастбищах сформировался горнолыжный курорт, по территории которого проложено порядка двухсот двадцати километров трасс. 36% из них предназначены для начинающих, 44% &mdash; для лыжников среднего уровня, а 20% покорятся только опытным спортсменам. Верхняя граница катания находится на высоте 3600 м над уровнем моря, а нижняя &mdash; на 2300 м. Сноубордисты помимо доступа к склонам общего пользования имеют возможность поупражняться в хаф-пайп и сноупарке.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618668_3096.jpg" /></p>

<p>Лыжники с достаточно высоким уровнем подготовки приезжают кататься даже летом в зону, оборудованную на леднике Мон-де-Лан, расположившегося в непосредственной близости от вершины главной горы. Это самый большой в Европе ледник, используемый для круглогодичного катания.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/64/1610618710_6030.jpg" /></p>

<p>Взрослые могут 2 дня пользоваться ски-пас за &euro;100, а если оплатить за неделю вперед, то получится не более &euro;250. Разбежка цен за проживание от девяноста до ста евро в сутки.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (63, 'На зимний отдых в Италию', 1, NULL, NULL, NULL, 'na-zimnii-otdyh-v-italiyu', '2021-01-04', '2021-01-04', 'Хочется верить словам классика о том, что ничто не вечно под луной и еще до окончания зимы улучшится обстановка с коронавирусом в мире. А когда это произойдет, надо будет срочно искать куда поехать отдыхать, пока снег не начал таять. Мы предлагаем в качестве одного из вариантов Италию.', true, 3, 0, NULL, NULL, NULL, '<p>Даже зимой любой желающий найдет в Италии места, которые стоит посетить. В это время года существенно снижаются стоимость проезда и жилья. Кроме того, нет такого большого наплыва туристов, как летом и это хотя именно на зиму приходится пик театрального сезона, а также время массовых распродаж. Но главное притяжение для туристов &mdash; термальные источники и горнолыжные курорты.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753644_2665.jpg" /></p>

<h2>Термальные источники</h2>

<p>В Италии насчитывается более четырехсот термальных источников, к которым приезжают люди с имеющие проблемы с опорно-двигательной системой, гинекологией, кожным покрытием, а также целым рядом других болезней. На местных курортах созданы все условия, чтобы туристы не только поправляли здоровье, но и возможность расслабиться.</p>

<h3>Термы Бормио</h3>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609754131_8147.jpg" /></p>

<p>Чтобы посетить этот чрезвычайно популярный горнолыжный курорт, следует поехать в регион под названием Ломбардия. В трех СПА-комплексах, расположенных на его территории, туристы имеют доступ к термальным источникам, работающие на протяжении круглого года.</p>

<p>Лечебные ванны в источниках, функционирующих в отеле QC Terme Hotel Bagni Vecchi, принимали еще древние римляне, которые и оборудовали купальни, предназначенные для умывания, принятия сауны и грязевых ванн. Солидный возраст здравницы создает некоторые неудобства: чтобы попасть из одной удалённой части бань в другую приходится выходить на открытый воздух.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753652_1421.jpg" /></p>

<p>В новом комплексе QC Terme Grand Hotel Bagni Nuovi предлагается довольно широкий спектр оздоравливающих процедур.&nbsp; Именно этой части Бромио отдыхающие чаще всего отдают предпочтение.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753653_4548.jpg" /></p>

<p>Для желающих одновременно совместить экзотику старины и с современными технологиями, прямо в городе свои двери распахнул отель Palace Hotel Wellness &amp; Beauty. Здесь посетителям предлагается на выбор множество процедур, ванны с гидромассажем, большой крытый бассейн.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753653_1049.jpg" /></p>

<p>Расположен Бормио на высоте 1225 м над уровнем моря в Альпах, а потому у туристов помимо купания в термах есть возможность отправиться в туристические походы, позволяющие насладиться чистейшим горным воздухом, или на склон для спуска на горных лыжах очень хорошего качества.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753654_2630.jpg" /></p>

<h3>Монсуммано Терме</h3>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609754642_4879.jpg" /></p>

<p>Уникальность этого курорта заключается в том, что термальными водами заполняются пещеры, образованные природой. Благодаря этому тандему, создается особый микроклимат, наделенный способностью исцеляющее воздействовать на больных астмой и бронхитом. Еще сюда приезжают, чтобы лечить суставы, сердечно-сосудистую систему, многие виды болезни кожи, неправильный обмен веществ в организме.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753656_9134.jpg" /></p>

<p>Grotta Giusti предоставляет своим посетителям возможность бесплатного посещения термального источника в гроте Джусти. В некоторые из его номеров налажена подача термальной лечебной воды для принятия ванн, не выходя из апартаментов.</p>

<h3>Фьюджи</h3>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753659_2139.jpg" /></p>

<p>В восьмидесяти километрах от столицы Италии расположился еще один знаменитый за пределами этой страны термальный курорт. Вода из источников в этой местности не пахнет и не имеет вкуса, зато богата содержанием фульвиковой и гумусовой&nbsp;кислот. Она способна очистить организм от шлаков и камней в различных внутренних органах человека, а также облегчить страдания мучащимся от нарушений артериального давления, подагры, диатеза.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753660_6772.jpg" /></p>

<p>Время, свободное от оздоровительных процедур, отдыхающие, как правило, посвящают неспешным прогулкам по улочкам средневековой части Города. Здесь расположено множество старинных церквей и монастырей, открытых для посещения.</p>

<h3>Абано-Терме</h3>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753660_6925.jpg" /></p>

<p>В сорока километрах от Венеции на территории природного парка Эуганейские холмы расположился известный далеко за пределами Италии термальный курорт Абоно-Терме. Местная вода, насыщенная солями йода и брома, не имеет аналогов в мире.</p>

<h2>Горы</h2>

<p>Одним из востребованных у итальянцев и их гостей зимним развлечением является катание на лыжах. Что во многом обусловлено большим числом горнолыжных курортов в этой стране. У подножья Альп раскинулся курорт Valle d&rsquo;Aosta, объединяющий 25 как значительных (Cervinia, Courmayeur, La Thuile, Pilu, Monte Rosa), так и мелких зон отдыха.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753661_6094.jpg" /></p>

<p>Особенность Валле Д&rsquo;Аоста в том, что он расположен на месте соприкосновения итальянской, французской и швейцарской территорий, а потому начав спуск в Италии, закончить его можно во Франции или Швейцарии. Правда, при этом требуется наличие при себе документа с шенгенской визой, иначе логическим концом такого катания станет депортация.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753662_3686.jpg" /></p>

<p>Италия поражает не только количеством горнолыжных курортов, но и их разнообразием, на склонах которых лыжнику с любым уровнем подготовки есть место для отдыха. В Ломбардии это Valtellina, Livigno, Bormio и множество других. А на территории автономной области Трентино-Альто-Адидже, которую еще называют Dolomiti Superski, расположились Val di Fassa и Val di Fiemme.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753663_8186.jpg" /></p>

<p>Помимо скоростных спусков, практически на каждом из перечисленных курортов отдыхающим предоставляется возможность покататься на санках и коньках, а также заняться лыжным альпинизмом. Каждому желающему обязательно предложат купить единый абонемент, позволяющий пользоваться трассами всего района.</p>

<h2>Погода</h2>

<p>Протяженность Италии с юга на север составляет 1170 км. Рельеф местности в разных регионах претерпевает существенные изменения. Если на севере климат считается умеренно континентальным, то на юге и в центральной части он средиземноморский. Выпадение основной массы осадков приходится на ноябрь &mdash; декабрь.</p>

<p>Средняя дневная температура в декабре:</p>

<ul>
	<li>Юг &mdash; от +13оС до +14 оС днем, которые ночью снижаются до +4оС-+5оС.</li>
	<li>Север &mdash; дневные показатели не превышают плюс четырех-пяти градусов, при ночных минус пяти.</li>
</ul>

<p>Январь считается самым холодным месяцем. Если в Риме температура днем еще поднимается до плюс десяти градусов, то в Вероне и Милане этот показатель не превышает четырех, при устойчивых заморозках ночью.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/63/1609753663_2627.jpg" /></p>

<p>Февраль на севере снежный, а на юге дождливый. В этом месяце самое теплое место в стране &mdash; Палермо, где воздух днем прогревается до +15оС, а ночью не опускается ниже +11оС.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (69, 'Швейцария: эталонные трассы на горнолыжных курортах ', 1, NULL, NULL, NULL, 'shveicariya-etalonnye-trassy-na-gornolyzhnyh-kurortah-', '2021-01-08', '2021-01-14', 'Швейцарские Альпы заслуженно называют страной горных лыж. Уровень сервиса на швейцарских курортах вполне оправдывает стоимость предоставляемых услуг. Опытные спортсмены особо отмечают подготовку трасс на швейцарских курортах на эталонном уровне. Мы расскажем о самых популярных среди них.', true, 2, 0, NULL, NULL, NULL, '<p>На каком бы курорте Швейцарии вы ни находились, можете быть уверены, что не будете стоять в очереди на подъемники. Обусловлено это современностью их конструкции и хорошей пропускной способностью. Высокий уровень комфорта для отдыхающих &mdash; характерная черта швейцарских курортов.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610615504_3417.jpg" /></p>

<h2>Церматт</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610615538_3603.jpg" style="height:789px; width:1184px" /></p>

<p>Для размещения курорта, внесенного в рейтинг Best of the Alps и ставшего одним из символов Швейцарии, было выбрано место у подножья горы, которая называется&nbsp;Маттерхорн.&nbsp; В Цермат закрыт въезд для автомобилей с двигателями внутреннего сгорания, а для перевозки туристов используются электромобили, либо запряженные лошадьми стилизованные под старину кареты.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610615577_5357.jpg" /></p>

<p>Место выбрано таким образом, что оно абсолютно недоступно для ветров, и это позволяет сохранять снежное покрытие практически круглый год. Для строительства выбрали участок земли на отметке 1620,0 м. Суммарная протяженность всех трасс &mdash; 360,0 км. Крутизна склонов Цермат требует наличия достаточно высокого уровня подготовки у лыжника.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610615656_7621.jpg" /></p>

<p>50,0 км маршрутов, условно называемых &laquo;черными&raquo; предназначены для хорошо подготовленных горнолыжников. Они проложены в Schwarzsee, ниже Stockhorn и в Trift. &laquo;Красные&raquo; трассы (150,0 км) по силам средне подготовленным лыжникам. Найти их можно в Trockener Steg, Klein Matterhorn, Sunegga, Riffelberg. Начинающим и малоопытным лучше выбрать &laquo;синие&raquo; спуски, благо их также много &mdash; 110,0 км.</p>

<p>Для подъема отдыхающих в Церматт используется 35 подъемников, а для обеспечения их досуга имеется более сотни ресторанов, дискотек, баров или достойная альтернатива &mdash; спортивный комплекс. Осмотреть живописные окрестности с высоты птичьего полета можно с вертолета.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610615698_7393.jpg" /></p>

<p>Для того чтобы узнать, что и сколько стоит на курорте Цермат достаточно перейти по <a href="https://www.zermatt.ch/ru">ссылке</a>.</p>

<h2>Санкт-Мориц</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610615756_3625.jpg" /></p>

<p>Горнолыжники начали осваивать эту местность еще в далеком 1864 году. Сегодня его характерной особенностью признано наличие значительного количества отелей 5*, что делает его привлекательным для любителей отдохнуть предельно комфортно, поучаствовать в светских тусовках и кушать в роскошных ресторанах. Завсегдатаи утверждают, что воздух Санкт-Мориц можно сравнить с &laquo;сухим шампанским&raquo;.</p>

<p>Здесь два раза проходили Олимпийские зимние игры, а в настоящее время постоянно организуются спортивные состязания. 1850,0 м &mdash; отметка на которой расположилось поражающее своей красотой&nbsp;озеро Лей-да-Сан-Муреццан, а на его берегах раскинулся Санкт-Мориц. Склоны, практически всегда залитые светом яркого солнца, эксплуатируются не выше 3303,0 м.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616035_2964.jpg" /></p>

<p>Протяженность девяти маршрутов черной масти составляет 35,0 км. Длина шестидесяти одной трассы красной категории не превышает двухсот сорока пяти километров. На 70,0 км протянулись 18 спусков, квалифицированных как синие. Санкт-Мориц нельзя назвать удачным местом для лыжников, решивших освоить азы спусков по горным склонам, так как их рельеф здесь довольно сложен.</p>

<p>Территория курорта часто становится местом проведения необычных и даже экзотических мероприятий. На ледовой поверхности проводятся матчи по гольфу, поло и крикету, а также лошадиные бега. Она же используется для проведения парада автомобилей.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616074_4643.jpg" /></p>

<p><a href="https://www.stmoritz.com/en/">Здесь</a> можно получить подробную информацию о ценовой политике курорта.</p>

<h2>Гриндевальд</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616116_9490.jpg" /></p>

<p>Этот лыжный курорт в перечне наиболее живописных горнолыжных курортов не только Швейцарии, но и Европы. Рядом с ним расположены деревня Interlaken и гора Jungfrau, на которую проложена самая высокогорная железная дорога мира. Гриндевальду принадлежит звание лучшего фешенебельного курорта Европы и он, естественно, внесен в рейтинг Best of the Alps.</p>

<p>В связи с ограниченным количеством роскошных отелей, многие отдыхающие отдают предпочтение апартаментам или шале. Получение Гриндевальдом статуса горнолыжного курорта пришлось на 1947 год и с тех пор он принимает спортсменов и гостей круглогодично. Наивысшая точка - 2950,0 м.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616162_2210.jpg" /></p>

<p>Общая протяженность трасс равна двухсот тринадцати километрам. У двадцати восьми из них статус черных, у ста двадцати &mdash; красных, а у шестидесяти пяти &mdash; синих. В сноупарке любят кататься сноубордисты.</p>

<p>Самым сложным склоном считается Лауберхон-Ренштрекке, а звание наиболее протяженного закрепилось затем что идет от Лауберхорна до Гриндевальда. С гор можно спуститься не только на лыжах, но и на санках. Имеется площадка для катания на коньках и игры в керлинг. Экстремалям предлагается полет на параплане.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616229_1126.jpg" /></p>

<p>Мы специально ничего говорим о ценах в Гриндевальде, так как всю информации легко получить, кликнув <a href="https://сюдаgrindelwald.net/">сюда</a>.</p>

<h2>Кран-Монтана</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616269_1716.jpg" /></p>

<p>В любое время года спуститься с заснеженной горы можно, приехав на этот горный курорт, где основной язык общения &mdash; французский. На месте образования Кран-Монтана располагались две небольшого размера деревушки, которые превратились в спортивную арену международного масштаба, с такими отличительными чертами, как здоровый климат, чистейший воздух и практически постоянно сияющее солнце.</p>

<p>В Кран-Монтана все признаки современного города прекрасно сочетается с размеренной деревенской идиллией, а неспешный отдых с активными занятиями зимними видами спортом. Причем отдыхающим предлагается заняться не только спусками с горы, но и сквошем, теннисом, плаванием, условия для которых созданы в фитнес-центре. Пешие прогулки можно сочетать с катанием на лошадях и воздушных шарах.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616347_4941.jpg" /></p>

<p>Кран-Монтана &mdash; наиболее благоприятное место для освоения спуска с гор на лыжах, так как здесь имеются спуски любого уровня сложности. Среди спортсменов, имеющих среднюю подготовленность, особым спросом пользуется трасса под названием Piste Nationale. Для доставки горнолыжников на стартовую позицию непрерывно функционируют 24 канатных подъемника.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616420_8880.jpg" /></p>

<p>Если вам интересны подробности по ценам, то вы их без труда получите в полном объеме, осуществив переход по <a href="https://www.crans-montana.ch/">ссылке</a>. &nbsp;</p>

<h2>Лейкербад</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616473_3771.jpg" /></p>

<p>В приведенном нами перечне это единственный швейцарский курорт, имеющий термальные источники. Лейкербад пользуется у европейцев неизменной популярностью как центр талассотерапии. Люди едут сюда семьями, чтобы полюбоваться живописностью неповторимых ландшафтов, поправить здоровье и спуститься с горы на лыжах. Терминальные источники оформлены в виде комплекса, включающего в себя СПА-центр и бассейны, сауны и римско-ирландские бани, а также гидромассаж.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616523_4289.jpg" /></p>

<p>Лейкербард подойдет для не имеющих опыта горнолыжников, не очень уверенных в своих силах. К их услугам многочисленные школы катания как на лыжах, так и на сноубордах. Для закрепления полученных навыков новички пользуются тремя &laquo;зелеными&raquo; трассами. Суммарная длина спусков всех категорий составляет 60,0 км. Обслуживаются они семнадцатью подъемниками.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616565_9709.jpg" /></p>

<p>На территории Лейкербард имеется детсад, в который принимаются дети, достигшие возраста семи лет. Лейкербард &mdash; оптимальное место в Швейцарии для семейного отдыха. Всем желающим, помимо спуска с гор предлагают занятия акробатикой, поиграть в теннис, полетать на параплане. Информация о том, сколько все это удовольствие стоит размещена на <a href="https://leukerbad.ch/">сайте</a>.</p>

<h2>Энгельберг</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616605_4569.jpg" /></p>

<p>Название местности Швейцарии где издавна собирались люди, чтобы кататься на лыжах с немецкого переводится как Гора ангелов. От Энельберга до Люцерна не более тридцати пяти километров, а это центральная часть государства. Максимально высокая точка на отметке 3333,0 м. 82,0 км спусков, предлагаемых отдыхающим, располагаются на перепаде высот от 1000,0 м до 3028,0 м.</p>

<p>Протяженность самой длиной горнолыжной трассы, которая называется Jochstok-Samnieplatz, составляет 12 км, а перепад высот на ней полтора километра. Всего для катания оборудовано 28 лыжных трасс, 60% из которых имеют красную категорию. Подъем лыжников осуществляется двадцатью пятью подъемниками.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/69/1610616768_8887.jpg" /></p>

<p>Вечерний досуг обеспечивается полусотней ресторанов, дискотек, баров, кемпингов, кинотеатров. Перейдите <a href="https://www.titlis.ch/de">сюда</a>, и вы узнаете все, что касается стоимости каждой услуги.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (57, 'Зима на пороге — самое время готовить пляжные принадлежности', 1, NULL, NULL, NULL, 'zima-na-poroge--samoe-vremya-gotovit-plyazhnye-prinadlezhnosti', '2020-09-17', '2021-01-03', 'Зима и солнце - день чудесный. Похоже, что классик писал эти строки совсем не про Россию, ведь все именно так и есть на пляжах некоторых стран, вкоторые россияне предпочитают убегать от снега и морозов. Мы предлогаем поговорить о том, где зимой солнце и ласковое море.', true, 3, 0, NULL, NULL, NULL, '<p>Возможно, кто-то усомнится в адекватности автора, написавшего такой заголовок, но смею вас заверить, что это не соответствует действительности. Тогда как совместить такие, казалось бы, совершенно противоположные понятия, как зима и пляж? Очень просто &mdash; когда в России стоят трескучие морозы и метет метель, в другом полушарии Земли жарко.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339787_8313.jpg" /></p>

<p>&nbsp;</p>

<p>А что может быть приятней, чем лежа под ярким солнцем на берегу теплого моря любоваться красотами русской зимы, присланными друзьями через интернет? О странах, расположенных на этой благословенной половине нашей планеты и преимуществах зимнего отдыха в них и пойдет речь.&nbsp;</p>

<h2>Египет</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339814_8641.jpg" /></p>

<p>&nbsp;</p>

<p>Эта страна стабильно занимает лидирующие позиции в списке мест, выбираемых россиянами для отдыха в любое время года. Но зимой палящие +45&deg;C опускаются до вполне приемлемых +25&deg;C, притом, что температура воды держится на уровне +23&deg;C. Для того чтобы из суровой зимы перенестись в теплое лето достаточно четырех часов.</p>

<p>Зимой это направление совмещает красивейшее чистое море, белоснежные очень чистые пляжи, возможность осмотра большого количества достопримечательностей с доступностью цен. Список лучших для отдыха зимой мест выглядит так.</p>

<h3>Шарм-эль-Шейх</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339828_6256.jpg" /></p>

<p>&nbsp;</p>

<p>По факту является самым популярным и престижным курортом, гарантирующем уровень сервиса на европейском уровне. Это любимое место для поклонников дайвинга, совершающих погружения среди рифов и кораллов, чтобы понаблюдать за морскими обитателями, водящихся здесь в большом количестве. Однако это же делает непростым вход в море с берега. Чтобы решить эту проблему, каждый отель для своих постояльцев устанавливает специальные понтоны.&nbsp;&nbsp;</p>

<p>В зимние месяцы температура днем не поднимается выше 20-22&ordm;С, опускаясь в ночные часы до 16-18&ordm;С. Вода на пляжах прогревается до 23&ordm;С.</p>

<h3>Хургада</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339843_6615.jpg" /></p>

<p>&nbsp;</p>

<p>Мягкий климат, вкупе с развитой инфраструктурой и доступностью цен делает этот курорт отличным местом отдыха для семейных пар с детьми. Днем воздух прогревается до 20-22&ordm;С, ночью охлаждаясь до 15-17&ordm;С, при этом показатель температуры воды держится на отметке 23&ordm;С.</p>

<h3>Эль Гуна</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339857_4251.jpeg" /></p>

<p>&nbsp;</p>

<p>Отличительная черта этого нового курорта &mdash; небольшое количество туристов из России, что положительно влияет на уровень цен. Благодаря большому количеству каналов он очень похож на Венецию. Здесь днем несколько прохладней, чем в предыдущих двух случаях &mdash; 19-21&ordm;С, соответственно и вода чуть прохладней &mdash;. Ночная температура в пределах 15-17&ordm;С.</p>

<h3>Дахаб&nbsp;</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339871_4032.jpg" /></p>

<p>&nbsp;</p>

<p>Идеальное место отдыха для дайверов, серферов и любителей отдыхать в уединении. Здесь нет развитой инфраструктуры и туристам не надоедают желающие подзаработать аниматоры. Температура окружающего воздуха днем (20-22&ordm;С) также располагает к получению наслаждения от одиночества, а ночью (15-17&ordm;С) позволяет полноценно выспаться, не пользуясь кондиционерами, что позволяет насладиться чистым морским воздухом из открытого окна. А утро начнется с погружения в комфортные 22&ordm;С прибрежной воды.</p>

<p>Стоимость отдыха на перечисленных курортах зимой, как правило, не превышает 40 000 рублей, если пара приедет на неделю и выбирает вариант &laquo;все включено&raquo;.</p>

<h2>ОАЭ</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339886_4503.jpg" /></p>

<p>&nbsp;</p>

<p>Посреди жаркой пустыни раскинулось это государство, для возведения которого использовались стекло и металл. Поэтому в летние месяцы окружающий воздух раскаляется +50&deg;C, а порой и выше. Далеко не каждый россиянин способен перенести такую температуру. Другое дело зима, когда температура окружающей среды снижается до вполне приемлемых +27&deg;C, а вода прогревается до +23&deg;C.</p>

<p>Это наилучшее время для получения ровного загара без опасения сжечь кожный покров до болезненного состояния. Комфортно также летать на дельтапланах и отдавать должное многочисленным экскурсионным развлечениям. В этом современном государстве, сказочном халифате, рядом с вековыми арабскими устоями, традициями и восточными дворцами комфортно себя чувствуют роскошные виллы, современные торговые центры и дорогие машины.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339902_3064.jpg" /></p>

<h3>Дубай</h3>

<p>Это город самых высоких небоскребов и дорогих отелей. Лучшего шопинга чем здесь не найдешь во всей стране. Посещение Дубая будет неполноценным, если не увидеть такие удивительнейшие сооружения, выполненные человеческими руками, как Пальма Джумейра</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339922_8896.jpg" /></p>

<p>&nbsp;</p>

<p>и Архипелаг Мир.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339941_6113.jpg" /></p>

<p>&nbsp;</p>

<p>Дневная температура в это время года не превышает 25&ordm;С, что позволяет купаться (вода нагревается до 21&ordm;С) и загорать без опасенья обгореть или перегреться. Комфорт ночного отдыха обусловлен температурой воздуха от 19-20&ordm;С.</p>

<h3>Абу-Даби</h3>

<p>В этом городе старинные достопримечательности органично сочетаются с современными парками, аквариумами, зоопарком, базарами. Побывать в Абу-Даби и не увидеть аквариума в Аль-Айне, тематического парка Ferrari World, Дворец шейха, рынки, торгующие коврами и верблюдами &mdash; непростительная ошибка.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339958_2785.jpg" /></p>

<p>&nbsp;</p>

<p>После посещения достопримечательностей, когда температура воздуха 26&ordm;С, рекомендуется обязательно посетить один из четырех пляжей Абу-Даби, считающихся самыми чистыми в мире, чтобы окунуться в теплую (21&ordm;С) воду. Полноценному отдыху после наполненного событиями и впечатлениями дня поможет комфортная ночная температура окружающего воздуха &mdash; 19-21&ordm;С.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339976_9120.jpg" /></p>

<h3>Шарджа</h3>

<p>Это место для тех, кому больше по душе спокойно отдохнуть на природе или пляже в семейном кругу с маленькими детьми. В Шардже, культурной столице страны, практически полностью отсутствуют ночные клубы, зато есть где разгуляться и порезвиться детишкам, тем более что этому благоприятствует температура воздуха в это время года &mdash; 23-25&ordm;С.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600339992_2731.jpg" /></p>

<p>&nbsp;</p>

<p>После развлекательной части, безусловно, требуется окунуться в прибрежные воды одного из трех бесплатных пляжей. Для любителей диких пляжей добро пожаловать на Аль Корниш или Аль Мунтазах. Если кому-то не хочется выезжать за черту города, то к его услугам самое популярное у горожан место отдыха &mdash; пляж Шарджи Аль Хан.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340007_9589.jpg" /></p>

<p>&nbsp;</p>

<p>Чтобы вдвоем отдохнуть в ОАЭ на протяжении одной недели, лучше всего приобрести тур за 50&nbsp;000 рублей.</p>

<h2>Шри-Ланка</h2>

<p>В Шри-Ланке нет отелей, поражающих своими размерами и устройством, не проводятся шумные дискотеки и вечеринки, а потому сюда едут туристы, желающие полноценно отдохнуть от шума и суеты больших городов. Эта, небольшая по размерам страна, буквально нашпигована курортами, на которых очень комфортно отдыхать зимой. В это время воздух прогревается здесь до 27&deg;С, нагревая воду пляжей +26&deg;С.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340024_3541.jpg" /></p>

<p>&nbsp;</p>

<p>Для разнообразия пляжного отдыха туристам предоставляется множество экскурсий. Вам с удовольствием покажут чайные плантации, дождевой лес Синхараджа, черепаховые фермы, буддийские храмы, расположенные в пещерном комплексе и Канди &mdash; королевский город.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340041_8081.jpg" /></p>

<p>&nbsp;</p>

<p>Перелет в это райское место займет от 9 до 12 часов, а визу выдадут прямо в аэропорту. Одну ночевка в трехзвездочном отеле обойдется в 500 рублей, а за обед в недорогом ресторане придется отдать всего 265 рублей.</p>

<h2>Таиланд</h2>

<p>В этой стране лучше всего отдыхать с октября по февраль. Самым популярным среди российских туристов считается курорт Паттайя, что обусловлено наличием очень хорошо развитой инфраструктуры. Многие заведения имеют в штате работника, говорящего на русском языке.&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340056_2015.jpg" /></p>

<p>&nbsp;</p>

<p>Отличительная черта Пхукета, славящегося белоснежными пляжами и чистой водой, &mdash; отличные условия жизни. Здесь найдут занятия по вкусу и семейные пары с детьми, и молодые тусовщики, и любители активного отдыха. Детям будет интересно посмотреть аквариум (стоимость билета около $5), и посетить парк развлечений FantaSea, в котором показывают шоу слонов ($55). А для молодежи каждый вечер организовываются сумасшедшие вечеринки.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340074_1024.jpg" /></p>

<p>&nbsp;</p>

<p>Количество пляжей этого курорта впечатляет, но расположены они на приличном расстоянии друг от друга. Их разнообразие позволяет найти оптимальный вариант и для любителей погружений с аквалангом, и для тех, кто предпочитает расслабиться под солнышком, лежа на шезлонге.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340088_5018.jpg" /></p>

<p>&nbsp;</p>

<p>Средняя дневная температура колеблется в пределах 29-32&ordm;С, что позволяет воде прогреваться до 28&ordm;С. Ночью становится немного прохладней &mdash; 24-28&ordm;С. Оптимальный вариант для пары, собравшийся на недельный отдых в Таиланд &mdash; купить тур за 60&nbsp;000 рублей.</p>

<h2>Вьетнам</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340101_3368.jpg" /></p>

<p>&nbsp;</p>

<p>Это направление относительно недавно начало интересовать россиян. В основном сюда едут те, кому нравится общение с дикой природой и отдых на пляжах, не перенаселенных отдыхающими. К неоспоримым преимуществам отдыха во Вьетнаме, конечно же, надо отнести дешевизну отдыха, вкусную еду, доброжелательное отношение людей, высокий уровень сервиса в отелях и хорошие пляжи.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340116_8861.JPG" /></p>

<p>&nbsp;</p>

<p>Для любителей пляжного отдыха лучше всего ехать в Дананг, сюда не стоит направлять свои стопы серфингистам и семьям с детьми. Последние, вне всяких сомнений, получат море удовольствий и от посещения самого большого в стране аквапарка. Все это при тридцатиградусной жаре и температуре воды около 23&deg;С.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340128_7173.jpg" /></p>

<p>&nbsp;</p>

<p>Чтобы долететь до аэропорта Хошимин, достаточно девяти, а иногда тринадцати часов. Стоимость билета начинается с 28&nbsp;500 рублей. Виза для россиян не нужна. За одну ночь в отеле плата составит 700 рублей, при условии, что у него не больше трех звезд. А вкусный и сытный обед обойдется в 420 рублей.</p>

<h2>&nbsp;</h2>

<h2>Мальдивы</h2>

<p>Полное отсутствие суеты и райские пляжи &mdash; именно так можно коротко охарактеризовать этот поистине райский уголок, раскинувшийся на двух тысячах островов, из которых только 200 обитаемы. Размер этих островов настолько мал, что на каждом из них располагается только один курорт с единственной гостиницей, самыми чистыми в мире пляжами, белым песком, пальмами, коралловыми рифами и роскошными бунгало.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340278_9017.jpg" /></p>

<p>&nbsp;</p>

<p>Туристам, которые не представляют отдых без клубных вечеринок, дискотек и осмотра достопримечательностей, ехать сюда не стоит, так как всего этого здесь нет. Зато есть простор для дайвинга, путешествий на катерах к необитаемым островам, пляжи с температурой воздуха 30-32&deg;С, лазурное море, прогретое до 28&deg;С и тихие ночи, когда из окна потягивает свежий воздух, охлаждающийся до 28&deg;С.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340293_4043.jpg" /></p>

<p>&nbsp;</p>

<p>Стоимость тура на двух туристов начинается со 140&nbsp;000 рублей.</p>

<h2>&nbsp;</h2>

<h2>Бразилия</h2>

<p>К таким неотъемлемым атрибутам Бразилии как карнавалы и футбол, мы бы еще добавили зимний отдых на пляже Копакабана. Если лететь сюда в январе или феврале, то билет обойдется в 70&nbsp;000 рублей на одного в обе стороны. Его стоимость поднимется до 120&nbsp;000 рублей для желающих встретить Новый год на знаменитом пляже.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340538_2311.jpg" /></p>

<p>&nbsp;</p>

<p>Зато это зрелище останется в памяти на всю жизнь. В новогоднюю ночь на берег моря выходят все жители Рио-де-Жанейро, одетые в белые наряды. Когда часы начинают отбивать полночь, они бросают в набегающие волны цветы и любуются фейерверками. А в Лагуне Родриго де Фрейтас туристам предоставляется возможность насладиться видом самой большой в мире новогодней елки на плаву. Ее высота равна 85 метрам.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340550_5134.jpg" /></p>

<p>&nbsp;</p>

<p>Однако празднование Нового года отнюдь не отменяет возможности купаться и загорать, ведь температура воздуха в это время достигает 29&deg;С. Вода прогревается примерно до такого же уровня. Кроме того, туристам предлагают сесть в мини-поезд и за $48 доехать до знаменитой статуи Христа на горе Корковадо и вернуться.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600340565_7576.jpg" /></p>

<p>&nbsp;</p>

<p>Формат нашей статьи не позволяет описать все места пригодные для зимнего отдыха россиян, пожелавших на время забыть о снеге и морозах под лучами теплого солнца в ласковых морских водах. Мы выбрали только те варианты, о которых нашли в сети наибольшее количество положительных отзывов. &nbsp;&nbsp;&nbsp;</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (20, 'Отпуск на райских островах Мальдив', 1, NULL, NULL, NULL, 'zxcvzxcvzxcv10', '2019-06-07', '2021-01-15', 'Существует ли Рай на земле? Не знаем, но на островах он точно есть, особенно если они Мальдивские. Отдых здесь пролетает как одно мгновение и запоминается на всю жизнь. Но чтобы это было именно так и никак иначе необходимо знать об некоторых нюансах, с которыми мы вас с удовольствием ознакомим.', true, 3, 10, 'Отпуск на райских островах Мальдив', 'Отпуск на райских островах Мальдив', 'отель, отдых, цена, мальдивы, турист, остров, стоимость', '<p>Мальдивам принадлежит более двух тысяч остров, основная масса которых считается необитаемой. Некоторое количество занимают местные жители, а 80 открыты для туризма и отдыха. Сообщение между ними поддерживается с помощью гидроплана или водного транспорта.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653755_3447.jpg" /></p>

<p>Чтобы отпуск запомнился только положительными моментами необходимо предварительно ознакомиться с особенностями пребывания в стране. Информации в интернете очень много, но она вся разрозненная, а потому приходится тратить много времени, чтобы найти ответы на интересующие вопросы. Мы сделали выжимку, собрав все, о чем надо знать в одной статье. Как нам кажется, это должно вам помочь.</p>

<h2>Что надо подготовить, для поездки на Мальдивы</h2>

<p>У отдыхающего обязательно наличие загранпаспорта со считываемой машиной зоной (MRZ). Это требование распространяется и на детей. Срок действия документа должен быть не менее полугода с момента въезда на Мальдивы. Виза россиянам не нужна, если они не собираются пробыть на островах более трех месяцев.</p>

<p>Кстати, такая продолжительность безвизовой поездки доступна только россиянам, гражданам Непала, Пакистана, Индии, Бангладеш. Для туристов из других стран срок не превышает тридцати дней. В любом случае въехавшим в страну без визы не разрешено наниматься на работу. Для получения полной информации по этому вопросу достаточно зайти на официальный сайт иммиграционной службы Мальдив:<a href="http://www.immigration.gov.mv"> www.immigration.gov.mv</a>.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1610692604_3276.jpg" /></p>

<p>Следующее требование &mdash; наличие обязательной для каждого туриста суммы. Получается она путем элементарного умножения $150 на количество дней проживания. Практически везде принимается оплата в долларах и евро, а потому потребности в приобретении местной валюты не возникает. В отелях и ресторанах принимается расчет основными кредитными картами (VISA, MASTER CARD, DINERS), но при этом учитывайте, что снимаются высокие комиссионные &mdash; до 4,5 %</p>

<p><strong>Полезная информация!</strong> Долларовые купюры, должны быть выпущены после 2000 года, иначе их не примут к оплате.</p>

<p>У каждого прибывшего проверяется наличие обратного авиабилета, как гарантии того, что он в установленный срок покинет гостеприимную страну. Необязательно, но желательно иметь медицинскую страховку.</p>

<p>Она покроет расходы в том случае если отдыхающего придется доставлять в одну из больниц, которые есть только на материке и их услуги стоят недешево. За один прием к терапевту придется заплатить &euro;60, одна сдача анализов обойдется в &euro;30, а рентген &mdash; &euro;40. Если потребуется срочное хирургическое вмешательство, то за операцию заплатите &euro;1400.</p>

<p>Стоимость страховки на одного человека, планирующего отдыхать 10 дней от $19 до $24. За двоих придется заплатить $40 &divide; $50. Любителям подводного плавания предоставляется возможность оформить индивидуальную страховку в дайвинг-центре, имеющимся на каждом острове. За две недели погружений плата составляет $10 на одного дайвера.</p>

<p>Еще в самолете бортпроводники раздают всем пассажирам миграционные карты на английском языке. Она состоит из двух частей: на прибытие и убытие. Одна половина оставляется у представителя паспортного контроля, а вторую необходимо сохранить и предъявить при выезде.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653753_7963.jpg" /></p>

<h2>Ограничения</h2>

<p>В страну нельзя ввозить предметы, перечисленные в списке:</p>

<ul>
	<li>&nbsp;Любой вид алкогольных напитков.</li>
	<li>Наркотические вещества.</li>
	<li>Свинину (страна мусульманская).</li>
	<li>Взрывчатые вещества.</li>
	<li>Видеокассеты.</li>
	<li>Порнографические материалы.</li>
	<li>Огнестрельное оружие и то, что предназначено для подводной охоты.</li>
	<li>Химикаты.</li>
</ul>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653753_7316.jpg" /></p>

<p>Если их обнаружат в багаже при таможенном осмотре, то будут оформлять изъятие.</p>

<p>Имеется целый ряд ограничений и по ходу пребывания в стране. Туристам запрещено:</p>

<ul>
	<li>Заниматься рыбной ловлей в акватории островов. Прерогатива организации рыбалок отдана отелям, которые предлагают поохотиться как на обычную, так и на крупную рыбу. Стоимость, в зависимости от этого, колеблется от $60 до $300.</li>
	<li>Рвать или ломать кораллы (мертвые и живые) недалеко от берега. Доставать из моря и повреждать раковины. За нарушение этого запрета взимается штраф $500.</li>
	<li>По населенным пунктам ходить в купальных костюмах, а на пляже купаться и загорать топлес. Нудизм вообще запрещен законом и наказуем штрафом в $1000.</li>
	<li>Оставлять в неположенном месте любой вид мусора.</li>
	<li>Распивать в общественных местах алкогольные напитки.</li>
</ul>

<p>Не следует забывать, что Мальдивы &mdash; мусульманская страна, а потому женщинам рекомендуется появляться в общественных местах с закрытыми плечами и в юбках ниже колен. Такой же длины рекомендуется носить шорты мужчинам.</p>

<h2>Как добраться до островов, и сколько это стоит</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653750_8448.jpg" /></p>

<p>Для покупки дешевых билетов лучше всего воспользоваться поисковыми сервисами. В таких из них, как Яндекс.Авиабилеты (<a href="http://yandex.aero-bilety.ru/">yandex.aero-bilety.ru</a>), OneTwoTrip (<a href="https://www.onetwotrip.com">onetwotrip.com</a>), Aviasales (<a href="http://yabs.yandex.ru/count/F3x048ILHXS509S2CSf285u00000E0H00aW2OBm8Q09mhFoHaWQ00RZNc5Y80SBZmD0na07q-PwjBfW1pEwM-ogu0Ohgb_0Om06Y0TW1akIR5E01kAZK4-W1O8W2_Rg50Q02w8pa0TW20l02oBwbomRu0l3gyU2XnhZLvG600woaL8W3yzBHf0oO0_pGEwW32h031BW4_m7e1BMCBVW4aumUY0MJZ1wG1ONpaKEW1Pcw3AW5cReCi0MPkWou1TN6v1l01P_NyKF81S2OZ1pW1GNm1G6O1eBGhFCEe0Q-eG6e1hwX0Sa6zWZjCqQT82VH1fqsPnLzmV8xsGO0001W00000Aa75P3q-SKeu_wm1u20c2pG1mBW1v0DyGVF8SKFMvmBp_W70e081D08W8A3WSI0W8Q80U08j8PoW0e1mGe00000003mFzWA0k0AW8bwgWiGw1OHP1MG003jpoV3Fvm50DaB5P3q-SKeu_xe2vEC7l0B2eWCihlUlW7e32FW3GE93kNG8jlvVl0_a0x0X3sO3lQHj9_ZgAUG9w0Em8Gzi0u1s0u2eG_bq2BR-NxmFzaFTQLlyREbxp_W3m604EdLsn2G49QyykNcyO3LTfeG2H400000003mFyWG3FWG0e0H0eWH0P0H0Q4H00000000y3-e4S24FR0H0VeH6Gq000005G00000T000002K00000BG0000284W6G4W6f4auzLAiHTLC_iHAloyCWNsRhF-aIrCw8bFx6aydm4WA84m6G4sIO4mIe4-E7hwhlvE3p5S0J____________0TeJ2WW0400O0200A03W4zN6v1kW5DN6v1ke58NpaKF850JG5F___________m7O5B3JtTq5w1G4?etext=2202.NMJbI9CJUnF4Vdzg-XHTuuUA1U32h2OJXEQbtzS5lIqe9S4sWLX6fJLf_91RgXmOjku75cvgSCPN0ROOv_PJBGR4em1vamFqbWR6aXd0em8.836fbfbf83aeafa7cf562563dd08f55b7abd9b3d&amp;from=yandex.ru%3Bsearch%26%23x2F%3B%3Bweb%3B%3B0%3B">aviasales.ru</a>) предоставляется возможность создания уведомления о выгодных предложениях по продаже авиабилетов на нужную дату.</p>

<p>На Мальдивы выполняются прямые рейсы Аэрофлота, но их стоимость в пределах 40 тыс. рублей. Если согласится на пересадку в аэропорту какой-либо арабской страны, то поездка обойдется 28 тыс. &divide; 35 тыс. рублей.</p>

<p>От аэропорта до выбранного острова туристы добираются и за несколько минут, и за несколько часов. Это зависит от места расположения желанного отдыха. Соответственно и стоит это от нескольких американских долларов до значительной суммы. Самым выгодным средством передвижения до островов, расположенных близко является общественный паром, проезд на котором стоит $2 &divide; $10.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653751_8876.jpg" /></p>

<p>За доставку с ветерком на скоростном небольшом катере придется отдать за одного человека от $30 -$100. Стоимость находится в прямой зависимости от того сколько пассажиров, и как далеко их надо везти. Соответственно выгодно, когда набирается полный катер.</p>

<p>Если отель расположен на одном из далеко расположенных островов, то добраться до него из аэропорта Мале можно только на гидросамолете. Удовольствие это не из дешевых: $250 &divide; $700 заплатит каждый пассажир. Колебание в этом случае также обусловлено удаленностью конечной точки и количеством желающих туда добраться.</p>

<p>Туристу, которому захотелось посетить другие острова, помимо того, где расположен его отель, следует приготовиться к дополнительным расходам. В качестве примера возьмем стоимость поездок с острова под названием Маафуши:</p>

<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Резорт &ndash; цель посещения</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Проезд за одного туриста</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Высадка на остров</strong></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Фихалхохи&nbsp;</p>
			</td>
			<td style="vertical-align:top">
			<p>$45</p>
			</td>
			<td style="vertical-align:top">
			<p>$24</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Фан Айленд</p>
			</td>
			<td style="vertical-align:top">
			<p>$45</p>
			</td>
			<td style="vertical-align:top">
			<p>$30</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Бияду</p>
			</td>
			<td style="vertical-align:top">
			<p>$20</p>
			</td>
			<td style="vertical-align:top">
			<p>$30</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Рихивели</p>
			</td>
			<td style="vertical-align:top">
			<p>$45</p>
			</td>
			<td style="vertical-align:top">
			<p>$60</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Деревня Эмбуду</p>
			</td>
			<td style="vertical-align:top">
			<p>$45</p>
			</td>
			<td style="vertical-align:top">
			<p>$60</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Кандума&nbsp;</p>
			</td>
			<td style="vertical-align:top">
			<p>$25</p>
			</td>
			<td style="vertical-align:top">
			<p>$150</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Анантара&nbsp;</p>
			</td>
			<td style="vertical-align:top">
			<p>$25</p>
			</td>
			<td style="vertical-align:top">
			<p>$178</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Адаран Престиж Ваду&nbsp;</p>
			</td>
			<td colspan="2" style="vertical-align:top">
			<p>$100</p>
			</td>
		</tr>
	</tbody>
</table>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653756_7942.jpg" /></p>

<h2>Отели Мальдивских островов</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653755_3006.jpg" style="height:789px; width:1185px" /></p>

<p>На любом отдельном островке, открытом для посещения туристами имеется по одному отелю. Учитывая отсутствие какой-либо конкуренции, цены здесь явно завышенные. По крайней мере так утверждают россияне, отдыхавшие там. Стоимость только некоторых продуктов и услуг выглядит так:</p>

<ul>
	<li>Бутылка питьевой воды &mdash; от $3 до $5.</li>
	<li>За такое же количество пива просят $9</li>
	<li>Коктейль, в зависимости от компонентов, &mdash; от $9 до $25</li>
	<li>Пол-литра вина &mdash; $45.</li>
	<li>Водка, джин, виски &mdash; до $150 за одну бутылку.</li>
	<li>Экскурсионные поездки или походы &mdash; $125 на одного человека.</li>
	<li>Снорклинг (плавание под водой в маске) &mdash; $50.</li>
	<li>SPA-услуги начинаются со $100.</li>
</ul>

<p>Спиртные напитки продают только на территории отелей и в резортах. Соответственно и распивать их можно только там.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653751_3646.jpg" />&nbsp;</p>

<p>Стоимость и уровень обитания в мальдивских отелях, как и везде, находится в прямой зависимости от количества звезд на его фасаде. Скажем ночь в Portia Hotel &amp; Spa (3 звезды) стоит 5 421 руб. а в Ostrov Hotel (3 звезды) стоит 9 733 руб. Цены в пятизвездочных отелях выглядят так:</p>

<ul>
	<li>Kihaa Maldives &mdash; 20 826 руб.</li>
	<li>Nika Island Resort &amp; Spa, Maldives &mdash; 21 845 руб.</li>
	<li>Angsana Ihuru &mdash; 35 017 руб.</li>
	<li>Huvafen Fushi &mdash; 63 942 руб.</li>
	<li>Ayada Maldives &mdash; 66 777 руб.</li>
</ul>

<p>Данные взяты нами на сайте<a href="https://www.booking.com"> booking.com</a>, так как именно у них нам понравился интерфейс и количество альтернативных предложений. В каждом случае речь идет о номере на два человека.</p>

<p><strong>Это интересно!</strong> За сутки блаженства в Royal Residence (королевской резиденции) достаточно заплатить полтора миллиона рублей. А ночь на вилле, носящей название Мурака, расположенной под толщей воды, оценивается в $50 тыс. и забронировать ее можно не меньше чем на четверо суток.</p>

<p>Анализ найденной в интернете информации позволили нам разбить все имеющиеся предложения на две группы:</p>

<p>Отдых для не очень богатых туристов:</p>

<ul>
	<li>самое доступное жилье, которое существует на Мальдивах, обойдется в $60 за одни сутки. Это гетхаусы, на раздельно расположенных островах Тодду или Гули, Расду или Маафуши, а также Хулхумале;</li>
	<li>средней принято считать разбежку от $85 до $100. Столько стоит номер в отеле с тремя или четырьмя звездами, построенном на обитаемом острове;</li>
	<li>за апартаменты в четырехзвездочном отеле с собственной инфраструктурой, просят от $150 до $350;</li>
	<li>до пятисот долларов доходят цены номеров в отеле, отмеченном пятью звездами, с проживанием по схеме &laquo;все включено&raquo;.</li>
</ul>

<p>Отдых для состоятельных людей:</p>

<ul>
	<li>в изысканном отеле (5*) за сутки придется выложить от $750 до $1500;</li>
	<li>проживание на семейной вилле обойдется от $1500 до $3000.</li>
</ul>

<p>А о том, как живут богатые люди, мы уже писали несколько выше.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/20/1609653749_9615.jpg" /></p>

<h2>Советы бывалых: на чем можно сэкономить, отдыхая на Мальдивах</h2>

<p>Один из вопросов, чаще других встречающийся на просторах интернета: как найти предложения, позволяющие отдохнуть на Мальдивах по доступной цене. И это законное желание, но при этом никто не спрашивает на чем можно сэкономить. Мы посетили большое число форумов туристов и сделали подборку рекомендаций от опытных туристов.</p>

<p>Оформили мы их в виде советов от бывалых:</p>

<p>Совет № 1. Начинать готовиться к отпуску надо заранее. В этом случае у вас будет время выбрать самый выгодный и недорогой тур, да и билеты на самолет, приобретенные заблаговременно, обойдутся дешевле. Мы, например, покупаем их здесь:<a href="https://www.aviasales.uz/"> aviasales.ru</a>. На этом некоторым удается, как они утверждают, сэкономить до 60% от общей стоимости отпуска, оплачиваемого непосредственно перед отъездом.</p>

<p>Совет № 2. Если поехать на Мальдивы в период с мая по октябрь (так называемый низкий сезон), то за отдых реально заплатить вдвое меньше чем в пиковый период. Обусловлено это наличием муссонов в это время года, однако отдых на пляжном от этого сильно не страдает, ведь на этой широте тепло всегда.</p>

<p>Совет No 3. Ощутимо дешевле, если сравнивать с резоте, провести отпуск на острове-деревне. Объясняется это тем, что наличие нескольких гестхаузов, магазинов, лавок и кафе создает конкуренцию и заставляет снижать цены. А в случае с резотом все принадлежит отелю, а потому он устанавливает свои правила. Наилучшие, как нам кажется, варианты на этом ресурсе:<a href="http://paikea.ru"> paikea.ru</a>.</p>

<p>Совет No 4. На экскурсии лучше всего отправляться, скооперировавшись с другими постояльцами. Если осматривать достопримечательности группой, то это обходится дешевле, чем индивидуальные походы.</p>

<p>Совет No 5. Для того чтобы позагорать на пляжах дорогих резот совсем не обязательно там жить. Достаточно оплатить трансфер и входной сбор и наслаждаться идеальными условиями. Так некоторые объезжают все близлежащие острова и не разориться при этом.</p>

<p>Совет No 6. Не стоит покупать большое количество сувениров в лавках. Во-первых, продаются они по завышенным ценам. Во-вторых, практически все изделия подобного плана изготовлены в Китае или Индии. Изделие с местным колоритом и по удобоваримой цене лучше покупать на островах-деревнях, где живут умельцы их изготавливающие.</p>

<p>Совет No 7. Лучше отказаться от дополнительных услуг и мини-баров в отелях, заказов завтрака в постель и ужина в номер. Это при расчете непременно впишут в ваш счет. Помните, что нет ничего зазорного в том, что вы узнаете сколько стоит та или иная услуга. Обслуживающий персонал доброжелателен и всегда скажет правду.&nbsp;</p>

<p>Вот, пожалуй, и вся интересная информация, которую нам удалось раздобыть для вас на туристических сайтах. Надеемся, что она вам пригодилась или пригодится в ближайшее время. Теплого солнца вам и ласкового моря!</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (70, 'Австрия: страна горнолыжных курортов', 1, NULL, NULL, NULL, 'avstriya-strana-gornolyzhnyh-kurortov', '2021-01-11', '2021-01-14', 'В любом мировом рейтинге лучших горнолыжных курортов львиная доля принадлежит Австрии. И это легко объяснимо, ведь их в этой стране насчитывается аж 432. Чтобы помочь сориентироваться в этом море предложений, особенно тем, кто едет впервые, мы и написали эту статью.', true, 2, 0, NULL, NULL, NULL, '<p>Львиная доля Австрии покрыта отрогами Альп. У жителей этой страны зимние виды спорта пользуются большой популярностью, чем и обусловлено наличие такого количества горнолыжных центров. Особенность австрийского подхода к развитию этой отрасли в том, что они предпочитают вкладывать деньги в развитие традиционных курортных поселков небольшого размера.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607534_3716.jpg" /></p>

<h2>Шладминг</h2>

<p>Ранее известный как шахтерский город, Шладминг стабильно находится в списке элиты горнолыжных курортов. В этом четырех пиковом районе имеются 190 км отлично спланированных склонов, которые предназначены не только для лыжников, профессионально занимающихся этим видом спорта, но и новичков, а также семейного отдыха.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607535_7871.jpg" /></p>

<p>Эти спуски используются для проведения чемпионатов мирового уровня по сноуборду и лыжам. По мнению профессионалов, они одни из самых лучших в мире. В Шладминг по соседству с черными трассами, которые опытные лыжники называют &laquo;бриллиантовыми&raquo;, расположились &laquo;семейные&raquo; склоны, рассчитанные на детей.</p>

<p>Доля легких (зеленых) маршрутов составляет порядка 59 км из суммарной длины спусков, тогда как 110 км расценивается как имеющие средний уровень сложности. Для оборудования двенадцати маршрутов использована поверхность ледника Дахштайн Глетчер, где также выделили значительную зону для тех, кто любит прокатиться по целине.</p>

<p>Самыми любимыми у лыжников стали склоны под названием Хохвурцен, а также Планай. Именно здесь проложили наиболее сложный во всем регионе маршрут черной категории, которая используется при проведении этапов мирового Кубка, а также ночного слалома. Кроме того, имеется санный маршрут протяженностью 7 км. Тем, кто предпочитает беговые лыжи, безусловно, понравится стадион Шладминг, на котором организовано ночное катание.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607536_4539.jpg" /></p>

<p>Ски-пасс на одного взрослого человека обойдется в &euro;53,5 при условии, что он не является студентом, так как в этом случае получится сэкономить, заплатив всего &euro;40,0. Дешевле всего кататься детям &mdash; &euro;27,0. На день взять в прокат комплект горнолыжный либо для сноуборда можно за &euro;32,0 для взрослых и &euro;16,0 &mdash; для детей. За аренду санок придется заплатить &euro;7,0. Цены до середины декабря и с начала марта снижаются на 8%. Для получения подробной информации достаточно перейти <a href="https://www.schladming-dachstein.at/de/Schladming">по ссылке</a>, правда, доступна она только для тех, кто владеет английским или немецким языками.</p>

<h2>Майрхофен</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607538_9022.jpg" /></p>

<p>Больше всего русскоязычных туристов можно встретить в негласной столице долины Циллерталь &mdash; Майрхофене. Здесь шутят, что русский стал одним из официальных языков в этой местности, ведь на нем говорит практически весь обслуживающий персонал. Самый крутой во всей Австрии спуск с символическим названием Харакири с углом наклона 78% тоже находится на этих склонах.</p>

<p>Выделенные для фрирайдеров 300 км маркированных трасс позволяют не проезжать по одной и той же траектории дважды. Место для катания детей выбрано таким образом, что высота на нем меняется от 490 до 610 м, что позволяет избежать горной болезни. Для почитателей сноуборда на территории Майрхофен, входящий в число лучших в Австрии, сноупарк &mdash; Burton Park Mayrhofen.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607540_1994.jpg" /></p>

<p>Притом, что суммарная протяженность трасс равна 137 км, новичкам выделили 40 км. Для обслуживания зоны катания, занимающей склоны от 629 до 2&nbsp;490 м, используется пятьдесят семь подъемников. Самая продолжительная лыжная магистраль растянулась на 10 км, а для отдающих предпочтение санкам &mdash; на 7,6 км.</p>

<p>Стоимость однодневного разрешения на пользование услугами подъемника:</p>

<ul>
	<li>для взрослого &mdash; &euro;55,0;</li>
	<li>для молодежи &mdash; &euro;43,0;</li>
	<li>для детей &mdash; &euro;26,0.</li>
</ul>

<p>За аренду на день полного горнолыжного комплекта придется заплатить:</p>

<ul>
	<li>взрослому &mdash; &euro;34,0;</li>
	<li>ребенку &mdash; &euro;17,0.</li>
</ul>

<p>Полный объем информации по услугам, оказываемых на Майрхофен, можно получить <a href="https://mayrhofen.ru/">здесь</a>.</p>

<h2>Зёльден</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607543_1399.jpg" /></p>

<p>Очередной пользующийся повышенной популярностью у россиян, которые меж собой называют его австрийской Ибицей, курорт Зёльден. На нем расположилось одновременно 3 вершины, высота которых превышает 3000 м. На каждой из них оборудованы смотровые обзорные площадки, позволяющие любоваться великолепными видами. В Зёльдене имеется 2 сноупарка, спортивный центр под названием Freizeit Arena S&ouml;lden, хафпайп и термы Aqua Dom.</p>

<p>На склонах проложено 146,5 км спусков, длина самого затяжного из них равна 16 км, из которых 4 освещены. С помощью тридцати четырех подъемников лыжники поднимаются до стартовой позиции 26км маршрутов категории черных либо красных, а также к 69,8 км маршрутов, предназначенных для новичков.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607661_6749.jpg" /></p>

<p>Стоимость услуг приведена в таблице:</p>

<table cellspacing="0" style="border-collapse:collapse">
	<tbody>
		<tr>
			<td colspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:1px solid black; vertical-align:top; width:623px">
			<p><strong>Ски-пасс</strong></p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:311px">
			<p>Возрастная категория</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:312px">
			<p>Цена (&euro;)</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:311px">
			<p>Взрослые</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:312px">
			<p>55,5</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:311px">
			<p>Пожилые</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:312px">
			<p>47,5</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:311px">
			<p>Студенты</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:312px">
			<p>44,5</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:311px">
			<p>Дети</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:312px">
			<p>30,0</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Однодневная аренда комплекта сноубордиста или горнолыжника изменяется от тридцати трех до пятидесяти восьми евро, главный, учитываемый при этом фактор &mdash; возраст. Все подробности о стоимости проживания, питания и развлечений вы получите, кликнув <a href="https://www.soelden.com/winter.html">сюда</a>.&nbsp;</p>

<h2>Санкт-Антон-ам-Арльберг</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607545_3145.jpg" /></p>

<p>В Санкт-Антон-ам-Арльберг зарождалась австрийская лыжная школа и он является членом клуба Best of the Alps. Маршрутизировано 305 км из них 180 категории freeride. Новичкам предоставляется 130 км склонов, средне подготовленным лыжникам &mdash; 123 км, а профессионалам &mdash; 51 км. Всех их готовы доставить на стартовую позицию 90 подъемников.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607546_5017.jpg" /></p>

<p>Чтобы взрослый человек мог кататься целый день, ему надо купить ски-пасс за &euro;54,5. Для пожилых и молодежи сделана скидка, а потому им это удовольствие обойдется в &euro;49,5. Дешевле всего стоит спуск по склонам детей &mdash; &euro;32,5. Желающим взять в аренду снаряжение, следует приготовить от сорока шести до шестидесяти восьми евро, в зависимости от выбранной модели. Столько же запросят за комплект сноубордиста.</p>

<p>Вечером полноценно отдохнуть помогут сауны вкупе с джакузи, а также порядка восьмидесяти ресторанов и кафе. Чтобы узнать, что и сколько стоит достаточно навести курсор на это <a href="https://www.stantonamarlberg.com/ru/summer">слово</a> и выполнить клик.</p>

<h2>Лех</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607548_2781.jpg" /></p>

<p>Курорт от всех остальных отличается фешенебельностью и высокими ценами. Отдых в Лех считается гламурным, очень дорогим и престижным. Снежный покров на 139 км трасс изумительного качества позволяет получить удовольствие вне зависимости от уровня сложности спуска. К услугам сноубордистов внушительных размеров фан-парк, а любителей беговых лыж приглашают на лыжню, протянувшуюся на 33 км.</p>

<p>На австрийских горнолыжных курортах не принято использовать хели-ски для доставки горнолыжников до спусков, но для Леха сделано исключение. А потому здесь желающих доставляют до места старта на вертолете.</p>

<p>Порядка одной третей всех трасс Лех относятся к зеленой категории. На долю сложных маршрутов выпадает 24% зоны катания, а для вне трассовых спусков выделено 200 км склонов.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607549_1224.jpg" /></p>

<p>Узнать сколько состоятельные люди готовы платить за услуги, предоставляемые на Лех, можно выполнив <a href="https://www.lechzuers.com/de">переход</a>.</p>

<h2>Бад-Гаштайн</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610607550_5383.jpg" /></p>

<p>На расстоянии 105 км от Зальцбурга расположился еще один престижный элитный курорт &mdash; Бад-Гаштайн. Здесь расположено самое старое в Австрии горное казино и все вокруг дышит пафосностью и дороговизной, чем и обусловлено то, что его еще называют горным Монте Карло.</p>

<p>Бад-Гаштайн нельзя назвать местом, где новички могут осваивать азы горнолыжного искусства. Основная масса здешних спусков относятся к категории синих или красных, предназначенных для достаточно опытных лыжников. Благодаря высокому уровню качества инфраструктуры, здесь проводятся этапы Кубка мира. Подходящие для себя трассы найдут хафпайперы и бордеркроссеры.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/70/1610611310_7179.jpg" /></p>

<p>Цены на ски-пасс за день следующие:</p>

<ul>
	<li>&euro;41,5 &mdash; за взрослого (низкий сезон) и &euro;43,5 в высокий;</li>
	<li>&euro;22,5 &mdash; за детей (до шести лет посадка в подъемник бесплатная).</li>
</ul>

<p>Цены на проживание, питание и развлечения узнайте, зайдя на на сайт.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (56, 'Отдых в Италии — эмоции на всю жизнь', 1, NULL, NULL, NULL, 'otdyh-v-italii--emocii-na-vsyu-zhizn', '2020-09-14', '2021-01-03', 'Италия - страна, посещение которой запомнится на всю жизнь. И это действительно так, ведь не зря главным девизом страны считается "dolce vita", что переводится, как сладкая жизнь. И она становится таковой для каждого, кто приехал на отдых в Италию.', true, 3, 0, NULL, NULL, NULL, '<p>Популярность эмоциональной и темпераментной Италии среди туристов остается на высоком уровне уже на протяжении многих лет. Здесь отдых на прекрасных пляжах чередуется с посещением ресторанов и кафе, для наслаждения любимыми всем миром итальянскими блюдами. А посещение достопримечательностей разбавляется большим разнообразием развлечений.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600097559_5434.jpg" /></p>

<h2>Как лучше добираться до Италии</h2>

<p>Приехать на отдых в Италию можно несколькими способами, у каждого из которых имеются свои положительные и отрицательные стороны. Их выбор находится в прямой зависимости от предпочтений едущего в отпуск и его материальных возможностей. Подробно остановимся на каждом из них.</p>

<h3>Самолет</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600097595_8535.jpg" /></p>

<p>&nbsp;</p>

<p>Безусловно, самолет является самым простым вариантом передвижения для тех, кто решил поехать на отдых в Италию, тем более что в этой стране функционирует 24 аэропорта, принимающие воздушные лайнеры со всего мира. Основная масса рейсов для россиян выполняется из Москвы. Проживающим в других городах России придется делать пересадку в столице страны.</p>

<p>За то, чтобы добраться до Рима, воспользовавшись услугами Аэрофлота, приготовьтесь заплатить от 16&nbsp;241 рубля до 25&nbsp;000 рублей. Зато на дорогу уйдет всего 4 часа. Если время не играет большой роли, то есть вариант полететь самолетом компании Wizz всего 7 326 рублей, но в этом случае предусмотрена пересадка в Будапеште, на что уйдет дополнительных 18 часов, при непосредственном времени полета, не превышающем 4,5 часа.</p>

<p>Если вдруг у вас появилось желание посмотреть Афины, то можно за 8&nbsp;000 рублей купить билет на рейс компании Aegean, которым предусмотрены две посадки &mdash; в Салониках и Афинах. В последнем городе время между прилетом и отправлением составляет 8 часов. Еще есть вариант долететь до Венеции за 3,5 часа и потратив для этого 15&nbsp;000 рублей.</p>

<h3>Поезд</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600097634_4378.jpg" /></p>

<p>&nbsp;</p>

<p>Одним из самых романтических способов передвижения, по мнению многих, является поезд. Он подходит тем, кто едет на отдых в Италию, не имея ограничений по времени и хочет посмотреть на другие европейские страны из окна вагона. Для таких туристов каждый день от перрона Белорусского ж. д. вокзала в Москве отходит поезд на Ниццу, который проходит через Геную, Милан, Верону. Время отправки &mdash; 11:18, а продолжительность поездки до одного из перечисленных городов от 36 до 40 часов.</p>

<p>Непосредственное влияние на стоимость билета оказывает тип вагона, в котором турист желает ехать на отдых в Италию. Самый бюджетный &mdash; вагон 2 класса за одно койко-место в нем придется заплатить <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a> 211 в один конец. Проезд в вагоне 1 класса стоит <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a> 320, а за удобства класса люкс плата равна <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a> 752.</p>

<p>Имеется еще ряд особенностей, касающихся приобретения билетов и их предварительного заказа. Во-первых, нет возможности оформить билеты раньше, чем за 60 суток до дня отправления. Во-вторых, за проезд детей, не достигших возраста 12 лет, предоставляется скидка в 50%. В-третьих, скидка также положена, если выкупается полностью все купе, или билеты приобретаются в одну сторону людьми, не достигшими возраста 26 лет и перешагнувшими планку в 60 лет. В этих случаях экономия иногда составляет до 30%.</p>

<h2>Что можно ввозить в Италию, а что нельзя</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600097669_3173.jpg" /></p>

<p>&nbsp;</p>

<p>В основе таможенных правил, применяемых к туристам, решившим отдохнуть в Италии, лежат законы ЕС. Мы ознакомились с этими документами и готовы поделится полученной информацией с вами.</p>

<p>Что разрешено для ввоза в Италию, а что нет</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600097702_4297.jpg" /></p>

<p>&nbsp;</p>

<p>При таможенном досмотре тем, кто приехал на отдых в Италию, не позволят ввезти:</p>

<ul>
	<li>Вещества, относящиеся к разряду психотропных, наркотических, токсичных, пожароопасных, взрывоопасных.</li>
	<li>Оружие любых категорий и отдельные его части. Речь идет об огнестрельном, холодном, химическом, бактериологическом, ядерном вооружении.</li>
	<li>Боеприпасы любых видов и амуницию. Это ограничение распространяется и на металлоискатели, с помощью которых некоторые туристы пытаются найти монетки и украшения на пляжах.</li>
	<li>Подделки, в том числе контрафактные товары, фальшивые деньги, &laquo;пиратская продукция&raquo;.</li>
	<li>Эротические видео- и печатные материалы.</li>
	<li>Информационную продукцию, содержание которой направлено на разжигание расовой, религиозной, национальной вражды.</li>
	<li>Животных или растения, внесенные в Красную книгу.</li>
	<li>Семена всех видов и свежий картофель.</li>
	<li>Мясо и мясо содержащие продукты, в том числе и бутерброды или пирожки.</li>
	<li>Молоко и молочные продукты.</li>
</ul>

<h3>Что ввозить можно, но в ограниченном количестве</h3>

<p>Имеются ограничения, которые не запрещают ввоз той или иной продукции, но ограничивают ее количество:</p>

<ul>
	<li>Алкогольные напитки крепостью свыше 22% &mdash; не более 1 л, или 2 л продукции, чья крепость не превышает 22%.</li>
	<li>В придачу к высокоградусной спиртной продукции разрешается ввезти 4 л вина, а также 16 л пива.</li>
	<li>Сигареты &mdash; не более 10 пачек (200 шт.), или 250 грамм табака, или 50 сигар, или 100 сигарилл.</li>
	<li>Рыбу и морепродукты &mdash; не больше 20 кг на человека.</li>
	<li>Любая продукция пчеловодства и куриные яйца&mdash; 2 кг на туриста.</li>
</ul>

<h2>Валюта</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600097920_8682.jpg" /></p>

<p>&nbsp;</p>

<p>На территории Италии с 2002 года имеет хождение евро. В декларации при перенесении границы, следует обязательно указывать количество европейской валюты, превышающей не декларируемый минимум в &euro;10&nbsp;000. За нарушение означенного правила предусмотрено наложение штрафа в &euro;300.</p>

<p>Произвести обмен рублей в Италии &mdash; дело не простое, а потому следует либо заблаговременно купить евро до отъезда, либо брать с собой доллары или фунты. Можно конечно произвести ченч по приезде в аэропорт, но требуется учитывать, что курс там, как правило, несколько ниже чем в банках.</p>

<p>Финансовые учреждения работают с 8:30 до 16. Единовременно позволяется купить не более &euro;500, предъявив при этом паспорт. В Италии имеются отделения Банка Интеза и ЮниКредит Банка, а потому пользоваться их картами очень удобно, так как банкоматы встречаются довольно часто.</p>

<p>Практически во всех значительных торговых точках крупных городов принимаются карты. Какие именно становится понятно после рассмотрения наклеек на дверях ресторанов или кафе. Как правило, это Diners Сlub, American Express, Master card, VISA. Согласно действующему в стране законодательству, здесь нельзя совершать за наличный расчет покупки, стоимость которых превышает $1000. &nbsp;</p>

<h2>На каких курортах Италии следует обязательно побывать</h2>

<p>Италия &mdash; страна, в которой имеется все, что требуется для полноценного отдыха: целых пять морей, омывающих берега этого государства, знаменитая на весь мир еда, высочайший уровень сервиса. Для души имеется большое количество памятников культуры с просто фантастической архитектурой. Девиз страны &mdash; dolce vita (сладкая жизнь) и этим все сказано.</p>

<h3>Итальянская Ривьера, Лигурийское побережье</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600097960_4199.jpg" /></p>

<p>&nbsp;</p>

<p>Здесь любят отдыхать те, кому по вкусу богемный, спокойный отдых, а приверженцы активного отдыха предпочитают сюда не приезжать. На протяжении всего побережья к ярко-синему морю спускаются черные скалы. На нем расположилось несколько небольших городков &mdash; любимые места Ги де Мопассана и Хемингуэя. Заплатив <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a>4 каждый желающий устраивает себе мини-тур на поезде, чтобы полюбоваться живописными строениями этих населенных пунктов.</p>

<p>Итальянская Ривьера имеет статус элитного курорта, а потому цены в кафе и ресторанах довольно внушительные. Ужин на одного человека в среднем обойдется в <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a>50, но за эти деньги клиента накормят очень вкусно и сытно.</p>

<p>Чтобы составить представление о стоимости номеров в отелях, приведем цены в некоторых из них:</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Название отеля</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Стоимость одной ночи &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>(рублей за одну ночь)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel Medusa</p>
			</td>
			<td style="vertical-align:top">
			<p>4 799</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel San Giuseppe</p>
			</td>
			<td style="vertical-align:top">
			<p>7 390</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>San Pietro Palace Hotel</p>
			</td>
			<td style="vertical-align:top">
			<p>13 390</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<h3>Таормина, Сицилия</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098004_1781.jpg" /></p>

<p>&nbsp;</p>

<p>Сицилия, конечно же, является местом концентрации dolce vita. А город Таормина и вовсе называют раем для отдыхающих, особенно если они сторонники совмещения пляжного отдыха с экскурсионным. Лучшим на побережье считается пляж Isola Bella. За право на протяжении суток купаться здесь придется заплатить от <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a>10 до <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a>15 в зависимости от сезона. В эту стоимость входят аренда лежака и зонтика.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098036_5880.jpg" /></p>

<p>&nbsp;</p>

<p>Вкусно пообедать здесь можно за <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a>50, но есть и более экономный вариант &mdash; купить продукты и устроить пикник в парке Villa Comunale, вход в который бесплатный. А вот за посещение древнегреческого амфитеатра, возведенного в III веке до нашей эры, потребуют плату <a href="https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B5%D0%B2%D1%80%D0%BE">&euro;</a>10 за каждого туриста.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098063_2682.jpg" /></p>

<p>&nbsp;</p>

<p>Цены за номер на двоих в местных отелях выглядят так:</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Название отеля</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Стоимость одной ночи &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>(рублей за одну ночь)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Cohen Hostel</p>
			</td>
			<td style="vertical-align:top">
			<p>5 790</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel Bel Soggiorno</p>
			</td>
			<td style="vertical-align:top">
			<p>9 490</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>La Malandrina Apartments &amp; Suites</p>
			</td>
			<td style="vertical-align:top">
			<p>16 990</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<h3>Остров Капри, Неаполь</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098090_5982.jpg" /></p>

<p>&nbsp;</p>

<p>Сюда любил приезжать Максим Горький. В память об этом итальянцы поставили ему памятник в городе Сорренто, расположенном недалеко. Несмотря на то что по всему побережью близко к морю выходят скалы, здесь есть очень хорошие пляжи для приехавших на отдых в Италию. Один из них Marina Piccola, расположенный недалеко от знаменитых скал Фаральони, являющихся символом острова и поражающих туристов своими размерами. За вход на пляж и лежак взимается плата &euro;20.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098122_2307.jpg" /></p>

<p>&nbsp;</p>

<p>Капри считается элитным курортом, что, конечно же, нашло свое отражение в ценах. Так если в другом месте за капучино просят &euro;1,5, то здесь выпить любимый напиток не удастся дешевле чем за &euro;5. И такое существенное отличие буквально во всех ценах.</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Название отеля</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Стоимость одной ночи &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>(рублей за одну ночь)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Bed and Breakfast Casa Mariella</p>
			</td>
			<td style="vertical-align:top">
			<p>4 699</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Santa Chiara Boutique Hotel</p>
			</td>
			<td style="vertical-align:top">
			<p>13 390</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Grand Hotel Vesuvio</p>
			</td>
			<td style="vertical-align:top">
			<p>21 190</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<h3>Римини</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098181_2779.jpg" /></p>

<p>&nbsp;</p>

<p>Если Рим &mdash; столица Италии, то Римини &mdash; центр ночной жизни, так как его облюбовала молодежь для своих тусовок. Одним из привлекающих их аспектов является дешевизна, по сравнению с остальной Европой, жизни здесь. Так скажем, чтобы пообедать, заказав какой-нибудь напиток, вполне хватит &euro;20.</p>

<p>Коктейль в баре стоит &euro;8, а потанцевать в одном из лучших в Италии клубов обойдется от &euro;15 до &euro;35. Цена варьируется в зависимости от дня недели и уровня проводимого мероприятия. Купаться молодежь предпочитает на пляже Bagno 38 Egisto, где за &euro;21 получаешь право на вход, лежак и зонтик.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098214_7599.jpg" /></p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Название отеля</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Стоимость одной ночи &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>(рублей за одну ночь)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Residence Hotel Le Viole</p>
			</td>
			<td style="vertical-align:top">
			<p>4 699</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel Aurea</p>
			</td>
			<td style="vertical-align:top">
			<p>5 290</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Villa Lidia</p>
			</td>
			<td style="vertical-align:top">
			<p>14 990</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<h3>Лидо-ди-Езоло</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098247_3775.jpg" /></p>

<p>&nbsp;</p>

<p>Для тех, кто собирается на отдых в Италию с детьми, нет лучше места чем Лидо-ди-Езоло. В этой стране очень любят детей, позволяя им все: сидеть со взрослыми за одним столом во время ужина, посещать любые магазины, шуметь и радоваться жизни. И при этом никто не попросит урезонить разыгравшихся малышей.</p>

<p>Здесь много развлечений для детей, и они вполне доступны по цене. Так для посещения океанариума SEA LIFE надо за &euro;13 купить билет для ребенка и &euro;17 отдать за взрослого. Использование каждого аттракциона в луна-парке New Jesolandia стоит &euro;1. Чтобы накормить обедом уставшего от развлечений бамбини, достаточно &euro;15.&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098278_8201.jpg" /></p>

<p>&nbsp;</p>

<p>Доступны здесь и цены за одну ночь, проведенную в отеле:</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Название отеля</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Стоимость одной ночи &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>(рублей за одну ночь)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel Bellaria</p>
			</td>
			<td style="vertical-align:top">
			<p>9 090</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel Gardenia</p>
			</td>
			<td style="vertical-align:top">
			<p>7 390</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel Vienna</p>
			</td>
			<td style="vertical-align:top">
			<p>7 390</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<h3>Милано-Мариттима</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098308_4704.jpg" /></p>

<p>&nbsp;</p>

<p>В это место едут отдыхать те семьи, которым скучно ограничиваться только пляжем. Его считают одним из самых активных среди итальянских курортов. Если вы желаете научиться ходить под парусом, &mdash; пожалуйста, при условии готовности заплатить за недельные курсы &euro;170. Для любителей животных предоставляется возможность в течение часа насладиться катанием на лошади, потратив при этом &euro;35.</p>

<p>Протяженность чистейших песчаных пляжей Милано-Мариттима порядка десяти километров. У каждого из них имеется свой номер и практически на каждом за возможность искупаться и позагорать придется платить.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098340_4174.jpg" /></p>

<p>&nbsp;</p>

<p>За отдых в местных отелях придется заплатить примерно столько:</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Название отеля</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Стоимость одной ночи &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>(рублей за одну ночь)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel City Beach Resort</p>
			</td>
			<td style="vertical-align:top">
			<p>6 890</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Residence Ola</p>
			</td>
			<td style="vertical-align:top">
			<p>8 890</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Hotel Palace</p>
			</td>
			<td style="vertical-align:top">
			<p>18 390</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<h2>Где отдохнуть в Италии не слишком много расходуя</h2>

<p>Каждому хочется съездить на отдых в Италию, но не каждый может себе позволить провести отпуск на топовых курортах. Для этой категории туристов наши следующие предложения.</p>

<h3>Бриндизи, Апулия</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098368_7262.jpg" /></p>

<p>&nbsp;</p>

<p>Сюда стоит поехать уже потому, что здесь родина сыра буррата. А если серьезно, то приехавших на отдых в Италию сюда привлекает возможность расслабленно и спокойно отдохнуть на море. Дело в том, что расположен он на самом каблуке географического итальянского сапога и так далеко не каждому хочется ехать, хотя здесь расположено большое количество архитектурных чудес, и прекрасных пляжей.</p>

<p>Один из них &mdash; Lido Onda Blu с водой удивительной голубизны. За вход попросят заплатить &euro;21, но в эту плату входят плата за вход, парковку, зонтик и 2 лежака. Прямо на пляже можно за 6&euro; перекусить салатом из выбранных самим ингредиентов. А за полноценный обед турист заплатит всего 11 &euro; + &euro;1,2 &mdash; за чашку капучино. На сытый желудок есть возможность прогуляться по Археологическому музею за &euro;5.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1600098392_6856.jpg" /></p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Название отеля</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Стоимость одной ночи &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>(рублей за одну ночь)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Malvasia Bed and Breakfast</p>
			</td>
			<td style="vertical-align:top">
			<p>4 399</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Santo Stefano Luxury Rooms</p>
			</td>
			<td style="vertical-align:top">
			<p>8 790</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Villa Luminosa</p>
			</td>
			<td style="vertical-align:top">
			<p>14 990</p>
			</td>
		</tr>
	</tbody>
</table>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (45, 'ТОП-10 лучших зарубежных мест для отдыха в 2020 году', 1, NULL, NULL, NULL, 'top-10-luchshih-zarubezhnyh-mest-dlya-otdyha-v-2020-godu', '2020-07-28', '2021-01-03', 'Каждому россиянину после напряженного рабочего года хочется полноценно и красиво отдохнуть. Где и как это можно сделать? Мы сделали специально для вас подборку самых популярных мест отдыха у жителей России.', true, 3, 0, NULL, 'Отпуск ', 'море, страна, отдых, виза, пляж, берег, курорт', '<h1>&nbsp;</h1>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595952632_1881.jpg" /></p>

<p>&nbsp;</p>

<p>Наступил 2020 год, а значит пришло время задуматься, как и где отдохнуть летом. При этом место желательно выбрать так, чтобы там в полной мере получилось восполнить нехватку солнечного тепла в организме, истощившегося на протяжении длинной зимы. Мы проанализировали большое количество информации, относящейся к туризму и отдыху, чтобы предложить вам 10 самых лучших мест за границей для полноценного проведения отпуска.</p>

<h2>Турция&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595952900_3950.jpg" /></p>

<p>&nbsp;</p>

<p>Возглавляет составленный нами рейтинг солнечная Анталия &mdash; туристский маршрут, неизменно пользующийся популярностью у отдыхающих разных национальностей. Для ее посещения не требуется визы, а теплое, ласковое море дополняется достопримечательностями, возраст которых перевалил за 2000 лет. Торговые центры, ремесленные лавки и пестрые базары приглашают заняться увлекательным шопингом. Многочисленные пляжи, 459 из которых отмечены Голубым флагом, позволяют найти место и для молодежной тусовки, и для спокойного отдыха узким семейным кругом. Поездка на срок меньше 60 дней не требует оформления визы.</p>

<h2>Греция</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953110_9488.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Что в мечтах представляется россиянину, собирающемуся полноценно отдохнуть? Завтрак на балкончике уютного отеля, с которого открывается великолепный вид на гладь моря? Величественные руины храма, много лет назад воздвигнутого древними зодчими высоко в горах, и сегодня оставляющие яркое впечатление? Золотой песок и ласковое море? Если перефразировать слова киногероя, то уверенно кажем, что все это есть в Греции.</p>

<p>Получить удовольствие от общения с древним искусством можно в величественном афинском Акрополе, помнящем времена до нашей эры? Кому интересно увидеть разноцветные пляжи надо обязательно побывать на островах Санторини, ведь здесь они черные или красные, белоснежные и золотистые. Утверждают, что стать мудрее можно, если поехать в Салоники и потереть палец Аристотеля. Побывав в Греции один раз, вы непременно включите эту в список постоянных туристских маршрутов.</p>

<h2>Мальдивы</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953250_3979.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Если спросить некоторое количество людей, зачем ехать на Мальдивы, то полученные ответы могут сильно отличаться. Серфер скажет, что здесь идеальная волна. Дайверы расскажут о плавании с гигантскими черепахами среди причудливых и поражающих красотой кораллов. Влюбленные вспомнят о незабываемых впечатлениях медового месяца, проведенного здесь. Любители экзотики опишут, как жили в подводном отеле, а люди неравнодушные к рекордам &mdash; пересекли экватора у атолла Адду. А суть этих ответов одна &mdash; туристам и отдыхающим обязательно найдется чем заняться на Мальдивах, а потому туда стоит ехать. Тем более что между Россией и Мальдивами принят безвизовый режим.</p>

<h2>Кипр</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953427_3202.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Посетить Кипр &mdash; получить возможность окунуться в античные времена, увидеть своими глазами муфлона, стать обладателем бронзового загара, донырнуть до затонувшего корабля и стать моложе на несколько лет. Здесь, чтобы съесть апельсин не надо идти на рынок, достаточно протянуть руку и сорвать его с одного из деревьев, растущих вокруг. Туриста или отдыхающего обязательно угостят королевским вином, а доброжелательность местных жителей позволит завести много интересных знакомств.</p>

<p>Берег Фигового дерева отлично подходит для отдыха с детьми. На Нисси-Бич в Айя-Напа вольготно любящей потусоваться молодежи, а романтические встречи вдвоем лучше проводить в Голубой лагуне у мыса Греко. Ходят слухи, что для сохранения любви навечно влюбленным надо хотя бы раз искупаться рядом со скалой Афродиты, расположенной недалеко от города Пафос.</p>

<h2>Египет</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953502_7061.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Страна таинственных пирамид и бескрайних пустынь, с хорошо организованными туристскими маршрутами, позволяющими прикоснуться к древней культуре. Посещение трех великих пирамид Гизы, успешно противостоящим разрушительному действию времени уже делает поездку незабываемой. А если еще отдохнуть в городе-курорте Шарм-эш-Шейх, и окунуться в глубины Красного моря, поражающие своей красотой, то такой отдых запомнится надолго. Россиянам виза оформляется в аэропорту прилета в течение нескольких минут.</p>

<h2>Таиланд</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953632_6506.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Таиланд понравится тем, кому по душе жаркое солнце и ласковое море. Отдых в этой стране вечного лета запоминается просторными чистыми пляжами, экзотической растительностью и доброжелательностью аборигенов. Здесь можно найти отели на любой вкус и возможности, но чаще встречаются заведения категории 2 и 3 звезды. Если ваша туристическая поездка не продлится больше 30 дней, то оформлять визу не надо. Жемчужиной тайских курортов считается Пхукет, который славится количеством пляжей, расположенных, в окаймлении удивительной природы, на берегу чистого моря.</p>

<h2>Хорватия</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953760_6292.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Берега Хорватии омываются Адриатическим морем. Эта юго-восточная европейская страна собрала в себе все положительное из стран Европы. Хорватские отели отличаются гостеприимством и ценами, способными приятно удивить. Для въезда можно воспользоваться болгарской визой. Начало купального сезона приходится на середину мая, а конец &mdash; на октябрь. Пляжи чаще покрыты галькой или песком, но есть и такие, где берег покрыт бетоном или скалами.</p>

<h2>Израиль</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953925_6788.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Израиль многолик. Он может быть шумным, строгим, спокойным, гостеприимным и в то же время осторожным. История этой страны удивительна. Здесь расположены святыни трех религий: христианской, исламской, иудейской. Страна интересна для туристов и отдыхающих святынями и целебными свойствами Мертвого моря. Виза россиянам оформляется служащими аэропорта, при условии, что они не рассчитывают находиться в стране больше 90 дней. Особенность расположения страны такова, что летом купальный сезон открыт в Средиземном море, а зимой желающие купаются в Красном.</p>

<h2>Болгария</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595954000_5155.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>На восточной части Балканского полуострова расположилась Болгария, курорты которой были очень популярны среди россиян в постсоветское время. Своими берегами она выходит к Черному морю. Практически на любом курорте страны можно отдыхать с детьми, так песчаные берега их пляжей полого уходят в воду. Купаться можно с конца мая до середины сентября. Для поездки надо оформлять паспорт туриста.</p>

<h2>Франция</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595954089_7387.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Закончим наш обзор изысканным, дорогим и роскошным курортом на Лазурном берегу &mdash; Ниццей. Уже в июле прилегающие к нему воды прогреваются до температуры комфортной для купания. В это же время город становится столицей джаза, придающей ему некоторую раскрепощенность. Истинное лицо курорта становится видно, если совершить променад по набережной, окруженной помпезными зданиями. И она, эта помпезность, поневоле передается людям, гулящим по ней. Здесь каждый почувствует себя истинным аристократом, пусть и на время отпуска.</p>

<p>Приведенная нами информация, конечно же, не является исчерпывающей. Если вы нашли полезный для себя материал &mdash; мы очень рады. В дальнейших публикациях на нашем сайте вы сможете получить большое количество полезной для полноценного, качественного и комфортного отдыха информации.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (46, 'Турция — Мекка для туристов', 1, NULL, NULL, NULL, 'turciya--mekka-dlya-turistov', '2020-07-29', '2021-01-03', 'У большинства россиян отдых в Турции ассоциируется с турами "все включено". Однако это далеко не все причины, по которым стоит поехать в эту страну. И мы постараемся убедить вас в этом. ', true, 3, 0, NULL, 'Турция, пляж, турист, город, отдых', NULL, '<p>Турция &mdash; место отдыха, стабильно пользующихся у россиян, отправляющихся в отпуск. Но даже опытным туристам невредно получить некоторое количество полезной информации.</p>

<p>&nbsp;<img src="http://travel-blogg.ru:3003/files/image/0/1596033657_7891.jpg" /></p>

<p>&nbsp;</p>

<p>Собираясь купить тур в Турцию, нелишним будет ознакомиться с информацией об этой стране. Это поможет не попасть в неприятную ситуацию и провести отдых приятно, избежав негативных сюрпризов.</p>

<p>Уникальность географического расположения страны объясняет ее популярность у туристов. Помимо выходов к трем морям: Черному, Эгейскому и Средиземному, она еще имеет внутреннее море &mdash; Мраморное. Правда, туристом оно неинтересно в связи с интенсивностью судоходства по нему.</p>

<p>Турция является светской страной, так как государство и религия разделены. Потому не стоит заблуждаться, увидев большое количество минаретов, с которых по утрам раздается зычный призыв муэдзинов на молитву. Разного рода ограничения касаются только самих верующих, а туристам дозволяется ходить по городу и пляжу в привычных для них нарядах. То есть на девушку, носящую мини-юбку, или юношу в коротких шортах никто не будет показывать пальцем.</p>

<p>Турки терпимо относятся к алкоголю. Практически в каждом магазине на территории туристической зоны торгуют вином и пивом. Местные жители почти не употребляют алкогольные напитки, но не создают дискомфорта туристам и отдыхающим, желающим их употребить.</p>

<p>Валюта у Турции своя, называется она турецкой лирой. Однако в курортных зонах законом не запрещено хождения доллара и евро. Поэтому за весь отпуск отдыхающий может ни разу не увидеть национальные деньги.</p>

<h2>Сколько стоит полноценно отдохнуть в Турции</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596033829_9797.jpg" /></p>

<p>&nbsp;</p>

<p>Начнем с того, что за визу платить не надо, так как между Турцией и Россией безвизовый режим. Полная стоимость отдыха помимо цены авиабилетов и туристического тура включает текущие расходы, без которых не обходится ни одна поездка. Этот показатель незначителен, если отдыхающий проживает в отелях All Inclusive. Тогда он оплачивает только экскурсии, непредвиденные расходы.</p>

<p>Цена основной массы однодневных экскурсий не превышает $ 60. За семь дней чаще всего, проводится две экскурсии, то есть на одного человека чтобы обеспечить достойный отдых в Турции достаточно $ 120. Именно по такой цене в прошедшем году предлагалось поучаствовать в однодневной поездке в Храм святого Николая, Памуккале, аквапарки Долу Су или Троя.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596033917_4002.jpg" /></p>

<p>&nbsp;</p>

<p>Самыми популярными подарками, привозимыми из Турции, принято считать восточные сладости. Если их приобретать в супермаркете, то стоят они недорого, на рынках и сувенирных магазинах цены существенно выше. За упаковку халвы весом 500 г просят от $ 1,5, если для изготовления использовался кунжут, до $ 3, когда в качестве сырья выступают фисташки.</p>

<p>З00 г лукума оцениваются в $ 2,5, а двухкилограммовая упаковка &mdash; $ 5. Стоимость варьируется в зависимости от того сколько и каких использовалось орехов при изготовлении. $ 10 стоит простая турка для кофе, но лавки предлагают приобрести и экземпляры с красивой чеканкой, если не жалко $ 100. Столько же придется выложить за шикарно оформленный кальян.</p>

<p>Если не приобретать шикарных сувениров, то для туристов, проживающих по турпутевке All Inclusive, хватит $ 200. Из них от $ 25 до $ 75 уйдет на покупку подарочных сладостей, $ 25, чтобы купить местную SIM-карту, а $ 100 &mdash; на всякий случай.</p>

<p>Планируя туры в Турцию, в которые не включено питание, необходимо предусмотреть соответствующую графу в списке расходов. При этом учтите, что на разных курортах цены могут сильно отличаться. Мы приводим средние показатели. Если на завтрак заказать яичницу, сосиску и кофе, то это обойдется $ 3 &divide; $ 4. За полноценный обед или ужин в кафе придется отдать $ 8 &divide; $ 12, а в среднего уровне ресторане $ 10 &divide; $ 25.</p>

<p>Произведя несложные подсчеты, получим, что питание в недорогих кафе обойдется в $ 20 на человека ежедневно. Любителям ресторанной еды придется запланировать $ 30 &divide; $ 50 на каждого отдыхающего. Но если он курящий, то расходы увеличатся в разы, так как сигареты в Турции очень дорогие. Предсказываемый практическим опытом вариант &mdash; взять с собой запас из дома.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596034033_9857.jpg" /></p>

<p>&nbsp;</p>

<h2>Как правильно выбрать тур</h2>

<p>Приняв решение купить тур в Турцию, каждый турист в первую очередь руководствуеся своими предпочтениями. Если на первом месте стоит активный отдых на пляже, то ему подойдут варианты на период с мая по октябрь. Любителям экскурсий лучше выбрать февраль, март, апрель. Для шопинга оптимально ехать с января по апрель, так как в это время самые дешевые авиабилеты.</p>

<p>На ресурсе под названием Центр Путешествий выложено много предложений по горящим турам в Турцию, базовая стоимость которых начинается с 7000 рублей на одного взрослого человека. В эту сумму входит медицинская страховка, действующая всю поездку, и цена авиабилетов, но нет питания. Выбираете путевку на 500 рублей дороже и вас уже накормят завтраком. Чтобы получить подробную информацию по этому вопросу, рекомендуем пройти на сайт по ссылке <a href="https://entertravel.ru/tours/turkey/87">entertravel.ru</a>.</p>

<p>Согласно рейтингу, отображающему итоги 2019 года, лучше продавались туры в Турцию туристическим оператором Anex Tour (anextour.com). Похуже показатели у Coral Travel (<a href="https://www.coral.ru/">coral.ru</a>). Между этим компаниями, в которые обращается большинство россиян, желающих отдохнуть в Турции, идет непрерывное соревнование за звание лидера. Третье место занимает Пегас Туристик (<a href="https://pegast.ru/">pegast.ru</a>).</p>

<p>Названные компании предлагают выгодные условия туристам и гарантировано выполняют взятые на себя обязательства.</p>

<h2>Места, которые обязательно надо увидеть</h2>

<p>Для многих туристов Турция ассоциируется исключительно с пляжами, системой &laquo;Все включено&raquo; и поразительными порциями в отелях. И это притом, что история этой страны формировалась под влиянием нескольких могущественных цивилизаций.</p>

<h3>Стамбул</h3>

<h3>&nbsp;<img src="http://travel-blogg.ru:3003/files/image/0/1596034489_7659.jpg" />&nbsp;&nbsp;</h3>

<p>В этом городе впечатляющее количество достопримечательностей. В Старом городе надо обязательно посетить Гранд Базар &mdash; кладезь турецкого колорита, в котором можно найти все, что душе угодно. Окунувшись один раз в атмосферу этого азиатского рынка ее уже не забудешь. Местные торговцы обидятся, если вы не станете с ним торговаться и с удовольствием уступят в цене, когда это делается с азартом.</p>

<p>Что пятизвездочные отели неглавная ценность Турции понимаешь, посетив дворцы Топкапы (XV век) и Долмабахче, растянувшийся на 600 метров по берегу пролива Босфор, собор Святой Софии (VI веке н. э.) и Голубую мечеть (начало XVII столетия). Несколько месяцев понадобилось строителям для возведения в XV веке крепости Румелихисар, а ее красота до сих пор радует глаза плывущих по проливу.</p>

<h3><strong>Курорт </strong>Памуккале</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596034587_7771.jpg" /><br />
<br />
Всемирно известный геотермальный курорт расположен на расстоянии 250 км от Анталии. Его источники богатые окисью кальция и сероводородом оказывают благотворное влияние на организм человека. За правильностью дозировок следят врачи. В отелях расположенный в непосредственной близости двухместный номер на одну ночь предлагают не меньше чем за $ 50, но и не больше чем за $ 58. Печень, в котрый собраны отели Турции, предлагающих подобные варианты выглядит так: <a href="https://planetofhotels.com/turciya/pamukkale/hotel-tripolis-hotel">Tripolis-Hotel</a>, <a href="https://planetofhotels.com/turciya/pamukkale/hotel-hal-tur-hotel">Hal-Tur-Hotel</a>, <a href="https://planetofhotels.com/turciya/pamukkale/hotel-alida-hotel">Alida-Hotel</a>, Hotel-Goreme, Ayapam-Hotel, <a href="https://planetofhotels.com/turciya/pamukkale/hotel-yildizhan-hotel">Yildizhan-Hotel</a>.</p>

<h3>Национальный парк Гереме</h3>

<h3>&nbsp;</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596034866_4432.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>В центральной части Турции расположился Национальный парк Гереме вокруг одноименного поселения. Его площадь составляет 300 квадратных километров, и он включен в список Всемирного наследия Юнеско. С апреля по октябрь парк работает с восьми часов утра до семи вечера. Все остальное время, экскурсии прекращаются на полчаса раньше. Однодневный тур в небольшой группе обойдется в $ 49,93. За вход в музей под открытым небом без группы надо заплатить $ 31,95, а осмотр Темной церкви обойдется в $ 10,65.</p>

<p>Для предпочитающих любоваться красотами Национального парка Гереме с высоты птичьего полета, предлагаем обратиться к компании, которая организовывает воздушные путешествия, в корзине монгольфьера. В одну группу набирается 25 человек, билет на каждого из которых стоит &euro; 217. Чтобы узнать нюансы, достаточно перейти по ссылке <a href="https://experience.tripster.ru/experience/18451/?utm_campaign=affiliates&amp;utm_medium=widget&amp;utm_source=travelpayouts&amp;utm_content=Cappadocia">experience.tripster.ru</a>.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596034776_5598.jpg" /></p>

<p>&nbsp;</p>

<h3>Эфес</h3>

<p>Этот город &mdash; рай для любителей бродить по старинным строениям. В Древней Греции об Эфесе складывалось неисчислимое количество легенд и мифов. Здесь расположен храм Артемиды, входящий в семерку чудес света.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596034930_9753.jpg" /></p>

<p>&nbsp;</p>

<p>Но знаменит город не только этим грандиозным строением. Ведь это музей под открытым небом, окруженный холмами и горами. Побывать в Турции и не увидеть библиотеку Цельсия или хорошо сохранившийся античный театр &mdash; непростительная ошибка.</p>

<p>&nbsp;<img src="http://travel-blogg.ru:3003/files/image/0/1596035000_2474.jpg" /></p>

<h2>Развлечения</h2>

<p>Духовная пища, вне всякого сомнения, нужна человеку, но о развлечениях забывать тоже нельзя, благо в этой стране с этим нет проблем. Мы остановимся на тех, которые пользуются спросом у россиян, купивших туры в Турцию. О путешествии на воздушных шарах мы уже говорили, но ведь есть еще и парапланы, на которых каждому желающему предоставляется возможность ощутить себя птицей заплатив $ 125. Правда, ощущение свободного полета несколько снижается присутствием инструктора.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596035125_8900.jpg" /></p>

<p>&nbsp;</p>

<p>Отдав от $ 40 до $ 45, отправляйтесь в сафари на квадроциклах. Маршрут по вашему желанию&nbsp;прокладывают среди лесных деревьев, по горным ущельям, песчано-галечным плато, на которых раскинулись турецкие деревеньки. Их жители обязательно предложат вам свежие фрукты, шашлык или жареную рыбу, не бесплатно, конечно.</p>

<p>Заядлым рыболовам предлагается заплатить $ 60 - $ 80 и заняться своим любимым развлечением. При этом вам предложат на выбор два варианта: морской и речной. В первом случае к месту ужения доставляют на быстроходных катерах, а во втором &mdash; вывозят на форелевое хозяйство или на речку Алара. В любом случае рыбака обеспечивают снастями и насадками. Завершается рыбалка пикником, на котором вам приготовят только что выловленную рыбу.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596035421_2857.jpg" /></p>

<p>&nbsp;</p>

<p>За $ 30 вам предложат экскурсию по интригующим название &laquo;Турецкая ночь&raquo;. По сути своей, это концертная программа, проходящая в зале древнего замка или ресторане стилизованного под старину. Вокруг сцены для отдыхающих поставлены столики с угощениями, как правило, бесплатными, а на сцене происходит действо. Это могут быть и завораживающие своим танцем дервиши, и исполняющие танец живота восточные красавицы. Напитки придется оплачивать дополнительно.</p>

<p>Хорошим завершающим штрихом, позволяющим смыть прах старины, пыль сафари и брызги морских волн станет посещение Хаммам &mdash; турецких бань. Сразу надо оговориться, что они существенно отличаются от русского аналога. Зачастую мужчин и женщин моют в общем зале и занимаются этим специальные люди &mdash; хамамщики.</p>

<p>При этом здесь действует ряд ограничений: никому нельзя заходить в моечный зал обнаженным, так же, как и вульгарно себя вести. При стопроцентной влажности температура воздуха тут не поднимается выше 60оС, и она помогает расслабиться для принятия мыльного массажа, пилинга, обертывания водорослями и нанесения разнообразных масок. Стоит это удовольствие от $ 20 до $ 30, если забронировать место через интернет. Гид возьмет с вас не менее $ 50, а если пойти в баню самостоятельно, то хватит $ 10.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596035512_2806.jpg" /></p>

<p>&nbsp;</p>

<h2>Где лучше купаться и загорать</h2>

<p>Предлагаемый вам рейтинг мест для купания составлен на основании отзывов российских туристов, и выглядит он таким образом:</p>

<p>&nbsp;</p>

<h3>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Лара (Анталия)</h3>

<p>&nbsp;</p>

<p>Входит в число престижных пляжей данного региона. Над пляжем развивается голубой флаг, свидетельствующий о признании высшего уровня на международном уровне. Благодаря расположенным вокруг дорогим отелям, территория ухоженная и очень чистая. Добраться до него можно на автобусе из Анталии, курсирующем круглый год.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596035645_9271.jpg" /></p>

<p>&nbsp;</p>

<h3>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Клеопатры</h3>

<p>&nbsp;</p>

<p>Это место отдыха получило свое название после того, как Марк Антоний подарил данную территорию царице Клеопатре. Здесь целых 2 км мелкого песка с золотистым оттенком. Вход в море пологий, а значит здесь комфортно тем, кто приехал на отдых в Турцию с детьми и людям преклонного возраста. За вход на пляж платить не надо, а вот шезлонги и зонтики придется покупать. Кстати, и туалет тут платный. Из Алании до пляжа ходят маршрутные такси, проезд на которых стоит $ 1,2.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596035756_6272.jpg" /></p>

<h3>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Патара (Фетхие)</h3>

<p>&nbsp;</p>

<p>Представитель списка лучших мировых пляжей. Размеры этого лежбища для отдыхающих впечатляют: песчаный берег растянулся на 18 километров. Он считается классическим и идеально подходит для отдыха.</p>

<p>&nbsp;</p>

<p>Главным достоинством этого места называют запрет на строительство любых капитальных многоэтажных строений. Поэтому туристам тут встречаются только дома местных жителей, построенные много лет назад и уютные, небольшого размера, рестораны. На автобусе от города Фатхие надо доехать до остановки Пляж Патара, от которой 5 км до пляжа. Для нежелающих преодолевать это расстояние пешком свои услуги предлагают многочисленные таксисты.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596035823_8606.jpg" /></p>

<h3>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Капуташ (Калкан)</h3>

<p>&nbsp;</p>

<p>Относится к разряду &laquo;диких&raquo;, то есть здесь не найти зонтики, лежанки, кабинки для переодевания. Тем не менее он входит в число лучших в Турции. Его пустынность и чистота обусловлена отдаленностью от городов и присвоением ему статуса государственного заповедника. Рейсовые автобусы отправляются с автовокзала, расположенного в городе Фетхие.</p>

<p>&nbsp;<img src="http://travel-blogg.ru:3003/files/image/0/1596035897_1013.jpg" /></p>

<p>&nbsp;</p>

<p>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Олимпос (Кемер)</p>

<p>&nbsp;</p>

<p>Входит в мировой список чистейших мест, предназначенных для отдыха и купания. Четыре километра берега покрыты песком с галькой, благодаря чему вода исключительно прозрачная и чистая. Платить за вход не надо. Его удаленность от ближайшего города (до Анталии 85 км) объясняет малое количество купающихся. Этот пляж как нельзя лучше подходит под определение семейный, благодаря тишине, спокойствию и свободе. Но за это благоденствие приходится расплачиваться наличием некоторых неудобств &mdash; никаких кафе тут нет.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596036097_3229.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>В завершение скажем, что ввозить в страну и вывозить из нее ювелирные украшения можно, если их стоимость не превышает $ 15 000. Но это при условии, что они были задекларированы. Когда украшения приобретены во время отдыха, то на таможне потребуют предъявить чек. Категорически нельзя ввозить в Турцию наркотические вещества и алкоголь домашнего производства. Запрещен вывоз антиквариата, морских ракушек, семян табака, цветочных луковиц, конопли.</p>

');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (50, 'На отдых в страну четырех морей: Израиль', 1, NULL, NULL, NULL, 'na-otdyh-v-stranu-chetyreh-morei-izrail', '2020-08-25', '2021-01-03', 'Земля обитованная для многих отдыхающих стала еще и местом где можно хорошо отдохнуть. Мы расскажем как это сделать по-возможности не очень дорого, но полноценно.', true, 3, 0, NULL, NULL, NULL, '<p>Израиль называют страной четырех морей &mdash; Мертвого, Галилейского, Красного, Средиземного. Хотя, справедливости ради, надо сказать, что первые два не являются морями, ведь по сути своей это озера. Однако о Галилейском, как о море говорится в Евангелие, а о Мертвом в этом статусе упоминается в трудах ученого из Древней Греции. С тех пор их продолжают называть морями.</p>

<p>&nbsp;</p>

<p>Ехать на отдых в Израиль стоит уже хотя бы для того, чтобы посетить места, по которым ступала нога самого Господа. И потому количество паломников, стремящихся поклониться святым местам неиссякаем. Другой непрерывный поток направляется на курорты Мертвого моря, где лечебными является не только вода и грязь, но и сам воздух. Здесь лечат различные недуги, начиная с кожных заболеваний и заканчивая ревматизмом. Ну и конечно в Израиль едут просто отдохнуть.</p>

<p>&nbsp;</p>

<p>Виза</p>

<p>Россиянам, отправляющимся на отдых в Израиль виза не нужна, если они не планируют находиться в стране больше 90 дней. Работать в это время запрещено. В ом случае, когда человек едет с намерением найти работу или продолжить учебу, то ему следует с пакетом необходимых документов обратиться к работникам консульского отдела израильского посольства в Москве. Подробная информация по этому вопросу находится <a href="https://visaapp.ru/vizy/aziya/rabochaya-viza-v-izrail.html">здесь</a>.</p>

<p>Безвизовый въезд возможен при наличии следующих документов:</p>

<ol>
	<li>Авиабилет на обратную дорогу.</li>
	<li>Подтверждение платежеспособности. Потребуется предъявить наличные или кредитную карту.</li>
	<li>Загранпаспорт, действительные еще на протяжении шести месяцев после отъезда отдыхающего.</li>
	<li>Если едет ребенок, то обязательно наличие свидетельства о рождении. А в том случае, когда его сопровождает только один родитель, то нужен документ, подтверждающий согласие на отъезд второго супруга.</li>
	<li>Документ, отображающий цель поездки. В качестве такового, как правило, выступают приглашения от родственников, ваучер, подтверждающий наличие брони в отеле. В случае приезда на лечение, потребуется справка, подтверждающая готовность клиники принять больного.</li>
	<li>Наличие медицинской страховки не является обязательным, но рекомендуется таковую иметь. Вполне достаточно стандартного полиса, не превышающего &euro;30 000. Без него помощь будет оказана только если отдыхающий попадет в ДТП или станет жертвой террористического акта. За все остальное придется платить немалые деньги.</li>
</ol>

<p>Каждому, отправляющемуся на отдых в Израиль, следует приготовиться к тому, что на пограничном пункте его ждет тщательный досмотр и допрос, занимающий порой очень много времени. Если у проверяющего не возникло вопросов, то от него вы получите въездную визу. В загранпаспорте штамп при этом не ставится.</p>

<p>В тех случаях, когда по какой-то причине появилась потребность продления пребывания в стране больше 90 дней, придется обращаться в МВД. Там потребуют предъявить паспорт, фотографии и обоснование продления в письменном виде. После подтверждения финансовой состоятельности, выдается официальный бланк, который надо заполнить. За услугу взимается плата &mdash; 175 шекелей.</p>

<h2>Валюта</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598354246_9583.jpeg" /></p>

<p>&nbsp;</p>

<p>Национальной валютой государства Израиль является новый израильский шекель (ILS), пришедший на смену одноименной валюте, которая прекратила хождение в 1985 году. В страну можно ввозить не более $10 000 или эквивалент данной суммы в иных денежных единицах. Какую валюту вы возьмете в поездку не имеет значения, так как здесь обращается как американские, так и европейские деньги.</p>

<p>Специалисты считают небезопасным везти большое количество денег наличными, тем более, что в стране нет недостатка в банкоматах, а также пунктах обмена. Еще один аргумент в пользу этой рекомендации &mdash; кредитная, как и дебетовая карта в Израиле принимается практически везде: и в ресторане, и в сувенирной лавке. Правда, в некоторых магазинах установлена минимум, который можно оплатить безналом.</p>

<p>Если есть потребность воспользоваться услугами банка, то надо помнить о режиме работы основной массы банков: 8:30&divide;18:00. Но это расписание верно для вторника, среды, четверга. В остальные дни недели прием клиентов заканчивается в 12:00. По субботам, само собой разумеется, ни одно уважающее себя еврейское финансовое учреждение не работает. На прогулки, экскурсии, шопинг лучше брать шекели, так больше шансов не нарваться на отказ принимать оплату в иностранной валюте.</p>

<h2>Ограничения на ввоз в Израиль</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598354300_4534.jpg" /></p>

<p>Точно не пропустят через таможенный контроль следующее:</p>

<ol>
	<li>Любые виды оружия, и холодное, в том числе (за исключением перочинных и кухонных ножей). Также под запретом перечные и газовые баллончики, нунчаки, кастеты.</li>
	<li>Металлоискатели, с помощью которых отдельные туристы собирают монетки на пляжах.</li>
	<li>Все виды психотропных, наркотических, токсичных веществ. Пропустить их могут если будет предъявлен заверенный у нотариуса перевод рецепта.</li>
	<li>Любые контрафакты.</li>
	<li>Информационные материалы и предметы, не соответствующие общепринятым этическим нормам. Под эту категорию попадают любые виды порнографии, а также материалы, направленные на разжигание межрасовой, межнациональной, межрелигиозной розни.</li>
	<li>Средства мобильной связи, работающие на частоте 900 МГц. Этому &laquo;бородатому&raquo; запрету уже много лет и таможенники про него почти не вспоминают. Но, как говорится, береженного Бог бережет.</li>
	<li>Морепродукты, рыбу, мясо.</li>
	<li>Семена, растения. Под это определение попадают даже клубни свежего картофеля.</li>
	<li>Любые продукты, содержащие молоко.</li>
	<li>Парфюмерию, содержащую алкоголь, если ее будет больше 250 грамм.</li>
</ol>

<p>&nbsp;</p>

<p>В интернете часто можно встретить утверждение, что запрещено ввозить Коран и все, что написано на арабском языке. Так вот, официально такого запрета не существует. Но если что-то подобное обнаружат таможенники, то последует дополнительная тщательная проверка. Материалы будут взяты служивыми на изучение и это может занять несколько часов.</p>

<p>Помимо серьезных ограничений имеется несколько таких, некоторые из которых невольно вызывают улыбку:</p>

<ul>
	<li>В Израиль нельзя ввозить почву.</li>
	<li>Любое пчеловодческое оборудование, побывавшее в употреблении, таможня не пропустит. Это ограничение призвано предотвратить попадание в страну пчелиных болезней.</li>
	<li>Боязнью возникновения болезней у растений, объясняется табу на транспортировку материалов или оборудования, использовавшегося для упаковки семян.</li>
	<li>Все, что может иметь отношение к игровому бизнесу, так как государством наложен заперт на азартные игры.</li>
</ul>

<p>Одному человеку, достигшему возраста совершеннолетия, разрешается ввезти 1 литр крепких спиртосодержащих напитков и 2 литра изделий градусом пониже. Сигарет допускается иметь не больше двухсот штук (10 пачек) или 250 г табачных изделий (развесной табак, сигары, сигариллы).</p>

<h2>Полезная информация</h2>

<p>Суббота &mdash; священный день отдыха для евреев. Ее началом является время заката солнца в пятницу, а концом &mdash; вечер субботнего дня. Такой же порядок соблюдается при праздновании национальных праздников. Если вам понадобиться куда-нибудь доехать, то придется воспользоваться такси, так как общественный транспорт в эти дни не работает.</p>

<p>Если вы решили посетить святыни или прогуляться по районам, где проживают ортодоксальные иудеи, то должны быть покрыты голова, плечи и ноги.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598354745_7691.jpg" /></p>

<p>Любителям азартных игр придется ехать на территорию Палестины, на которой они разрешены. Специально для всех желающих получить дозу адреналина со станции Тель-Авива два раза в день отходит специальный транспорт. Он бесплатно доставляет игроков до казино Оазис, расположенного в Иерихоне.</p>

<p>Для купания выделены специальные места, на которых обязательно присутствуют спасатели. Об уровне безопасности для купающихся сигнализирует флажок, который устанавливается на пляже:</p>

<ul>
	<li>белый &mdash; купание абсолютно безопасно;</li>
	<li>красный &mdash; не запрещает заходить в воду, но предупреждает, что это не безопасно;</li>
	<li>черный &mdash; купаться категорически запрещено.</li>
</ul>

<p>&nbsp;</p>

<p>В воде Мертвого моря не рекомендуется находиться больше двадцати минут за один сеанс. На протяжении дня процедуру можно повторять только 2 раза.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598354904_7841.jpg" /></p>

<p>&nbsp;</p>

<p>Если после купания в соленной воде планируется принятие серной ванны, то необходимо выдержать интервал не менее одного часа между этими процедурами. Завершающим обязательно должно быть принятие душа.</p>

<p>В ресторанах принято оставлять в качестве чаевых не больше 10% от суммарной стоимости заказа. Но это при условии, что они уже не внесены в счет. Посыльный при гостинице вполне довольствуется пятью-десятью шекелями. Гиды берут поденную плату в размере пяти долларов за каждого экскурсанта.</p>

<h2>Как добраться до Израиля</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598354932_6215.jpg" /></p>

<p>Безусловно самый быстрым, а потому востребованным видом транспорта, является самолет. На наш взгляд самые выгодные предложения находятся на веб-ресурсе aviasales.ru. Здесь предлагается несколько вариантов, как прямых рейсов, так и с пересадками.&nbsp; Так на 9 июля можно было приобрести билет за 12 002 рубля, если вы никуда не спешите и за 14 932 рубля &mdash; для ценящих время.</p>

<p>Прямые рейсы осуществляются как Аэрофлотом, так и самолетами израильской компании. На перелет в обоих случаях уходит 4 часа 10 минут. Если лететь из Санкт-Петербурга, то времени придется потратить на 50 минут больше. Обслуживается это направление российским перевозчиком &laquo;Россия&raquo;. Билет на утренний рейс тут можно приобрести за 11 800 рублей.</p>

<p>Можно также полететь на Turkish Airlines, но в этом случае придется делать пересадку в Стамбуле. Естественно это ведет к увеличению времени перелита и цены на билет.</p>

<h2>&nbsp;Условия проживания</h2>

<p>Израиль считается едва ли не самой дорогой страной в мире. Здесь переночевать в приличном отеле не получится дешевле чем за $300. Есть возможность сэкономить если воспользоваться услугами хостелами или гестхаузами. Чаще всего в них довольно уютно и удобно. Кроме того, они расположены так, чтобы турист мог быстро добраться в центр города или к морю.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598355010_9484.jpg" /></p>

<p>&nbsp;</p>

<p>Перед тем как платить за номер, убедитесь, что вас устраивают все выдвигаемые условия. Если повезет, то проживание в приличном месте, зачастую имеющее даже небольшую кухню, обойдется не дороже $100. Безусловно это не представительские апартаменты, но ведь турист здесь только спит и принимает душ, все остальное время наслаждаясь волшебным миром, открывающимся за стенами жилища.</p>

<h2>Пляжи Израиля, на которых надо обязательно побывать</h2>

<p>&nbsp;</p>

<p>Для израильтян пляж &mdash; место, где они не только купаются и загорают, но и встречаются с друзьями, катаются на качелях, занимаются спортом, организовывают вечеринки. Побывать на всех пляжах Израиля не реально, но посетить хотя бы 5 лучших из них имеет смысл.</p>

<h3>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Гордон, Тель-Авив</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598355132_7573.jpg" /></p>

<p>&nbsp;</p>

<p>Это место, где развлечение по душе найдет себе любой турист. Желающим предоставляется возможность поиграть в волейбол или покачаться на качелях. Для предпочитающих спокойную водную гладь предоставляется возможность поплавать в бассейне с соленой водой. Рестораны помогут утолить голод блюдами из свежей рыбы и большим количеством салатов.</p>

<p>После захода солнца активная жизнь пляжа не прекращается. Известные во всем мире DJ организовывают здесь свои дискотеки, а в пляжном баре предлагается продегустировать множество напитков под рев самолетов, низко проходящих над пляжем для совершения посадки в Бен-Гурион.</p>

<h3>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Бора Бора, Тверия</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598356141_2100.jpg" /></p>

<p>Для некоторых туристов, приехавших в Израиль для отдыха на море, взимание платы за посещение пляжа является поводом отказаться от его посещения. И зря. Хозяева пляжа не пожалели денег чтобы здесь все было идеальным. Ни один шекель тут не тратится зря. В барах подаются чрезвычайно вкусные блюда, а в непосредственной близости расположен рыбный ресторан, который признан самым лучшим в Тверии.</p>

<h3>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Пальмахим</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598356189_4221.jpg" /></p>

<p>&nbsp;</p>

<p>Чтобы добраться до этого пляжа из города Бат Ям, достаточно нескольких минут. Несмотря на то, что его нельзя причислить к самым большим и единственным предлагаемым удобством являются туалеты, сюда стоит приехать. Здесь чрезвычайно красиво и много места для занятий спортом или разбивки пикника. На этот тихий пляж люди приезжают семьями, разбивают палатки и живут в них по несколько дней. Лучше мета для семейного отдыха трудно найти.</p>

<h3>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Мош, Эйлат</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598356373_7840.jpg" /></p>

<p>&nbsp;</p>

<p>Это самая жаркая купальная точка Израиля. С пляжа открывается отличный вид на Иорданские горы. Вода настолько чистая, что видно каждую рыбку и любую другую морскую живность, даже если она передвигается по дну. Здесь можно по вполне приемлемым ценам очень вкусно покушать после посещения океанариума.</p>

<h3>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пляж Хилтон, Тель-Авив</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/50/1598356515_2299.jpg" /></p>

<p>Здесь рай для любителей водных видов спорта. Здесь есть где разгуляться опытным спортсменам и научится каякингу и виндсерфингу начинающим.</p>

');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (52, 'Таиланд: описание и рекомендации для туристов', 1, NULL, NULL, NULL, 'tailand-opisanie-i-rekomendacii-dlya-turistov', '2020-08-26', '2021-01-03', 'В Юго-Восточной Азии нет более привлекательной для туристов страны чем Таиланд, притягивающий своими бесконечными песчаными пляжами. Это рай для тех кому интересны дайвинг и бурная ночная жизнь. Не будут здесь скучать и любители необычных традиций.', true, 3, 0, NULL, NULL, NULL, '<p>Название удивительной страны под название Таиланд произошло от слова &laquo;тхай&raquo;, что на языке аборигенов означает свобода. Туристы едут сюда чтобы вновь и вновь полюбоваться экзотическими растениями, буддистскими храмами, коралловыми рифами. Им нравится посещать крокодиловые фермы, получать тайский массаж, отдаваться беспредельному шопингу и с головой окунаться в бурную ночную жизнь.&nbsp;&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413303_4585.jpg" />&nbsp;</p>

<h2>Виза</h2>

<p>Если ваш отпуск не продлится более тридцати дней, и вы россиянин, то никакой визы не требуется. Правительствами РФ и Таиланда подписано двустороннее соглашение, разрешающее гражданам этих стран совершать туристические поездки без визы. Достаточно на пограничном контроле предъявить следующий набор документов:</p>

<ul>
	<li>Загранпаспорт, срок действия которого заканчивается не раньше полугода с момента въезда в страну.</li>
	<li>Паспорт гражданина РФ.</li>
	<li>Две фотографии размером 4х6.</li>
	<li>Копию авиабилета на вылет с Таиланда.</li>
	<li>Миграционную анкету, которую стюарды предлагают заполнить еще до приземления.&nbsp; &nbsp;&nbsp;</li>
</ul>

<p>Пограничники проверяют наличие необходимых документов и ставят штамп, фиксирующий дату въезда в страну. На все это уходит не больше пяти минут. Вторая отметка ставится, когда турист покидает Таиланд. Размер визового сбора &mdash; 2000 бат (около $ 63).</p>

<p><strong>В порядке полезной информации!</strong> Если срок пребывания превысит 30 дней, то за каждый день просрочки с нарушителя возьмут штраф. Его размер равен 500 бат (примерно $ 16) за каждый день просрочки.</p>

<p>Каждый желающий имеет возможность официально продлить срок пребывания в стране. Для этого ему необходимо в иммиграционном офисе королевства оформить соответствующие документы и заплатить 1900 бат (приблизительно $ 61).</p>

<h2>Валюта</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413404_5955.jpg" /></p>

<p>&nbsp;</p>

<p>Официальной валютой Таиланда является тайский бат (฿), который охотно принимают также в Лаосе, Камбодже, Мьянме. Заплатить за покупки или услуги евро, долларами, а тем более рублями не получится &mdash; все расчеты ведутся исключительно в батах. Чтобы их получить достаточно обратиться в обменник, которых здесь много, но лучше это делать в банках. Если существует потребность поменять крупную сумму (от $10&nbsp;000), то рекомендуется обращаться в китайские обменники, так как там предлагается выгодный курс.</p>

<p>Иностранные банковские карты принимаются для расчета только в больших супермаркетах. Как с долларовой, так и с рублевой карточки без проблем снимаются баты, причем сделать это можно даже в банкомате. Перед использованием карты требуется предупредить свой банк, о месте пребывания, чтобы ее не заблокировали.&nbsp; &nbsp;&nbsp;</p>

<h2>Как долететь до Таиланда не очень дорого</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413425_2494.jpg" /></p>

<p>Чтобы долететь из России в Таиланд без пересадок есть только два конечных пункта &mdash; Пхукет и Бангкок. В последний рейсов больше и, как следствие, стоимость билетов на них ниже.&nbsp; Из Москвы в Пхукет летают Nordwind Airlines, Azur Air, Аэрофлот, тогда как в столицу добираются только с помощью THAI Airways и Аэрофлота. Стоимость перелета в одну сторону начинается с 24&nbsp;500 рублей, тогда как в Пхукет не получится улететь дешевле чем за 29&nbsp;000 рублей.&nbsp;</p>

<p>Из Санкт-Петербурга лететь прямым рейсом &mdash; удовольствие не из дешевых:</p>

<ul>
	<li>Бангкок &mdash; 40&nbsp;000 рублей, и это дефицит.</li>
	<li>Пхукет &mdash; 56000 рублей.</li>
</ul>

<p>Если лететь на отдых с пересадками, то не исключена возможность найти вариант значительно дешевле. В таком варианте свои услуги предлагают (в скобках пункт пересадки):</p>

<ul>
	<li>Finnair (Хельсинки, Финляндия).</li>
	<li>Turkish Airlines (Стамбул, Турция).</li>
	<li>China Southern (Гуанчжоу, Китай).</li>
	<li>Flydubai и Emirates (Дубай, ОАЭ).</li>
	<li>Etihad (Абу-Даби, ОАЭ).</li>
	<li>Qatar Airways (Доха, Катар).</li>
</ul>

<p>Правда, при выборе такого варианта надлежит учитывать продолжительность пересадки: может так получится, что цена билета чрезвычайно привлекательная, но в пути придется находиться до сорока часов. Большое количество вариантов вы найдете <a href="http://www.aviasales.ru/?marker=74563.htgtTHAIlink" target="_blank">здесь</a>.</p>

<h3>Полезные советы</h3>

<p>Следуя этим рекомендациям можно существенно сэкономить на перелете в Таиланд:</p>

<ol>
	<li>Билеты лучше начинать искать заранее. Оптимально приобретать их за два, а то и за три месяца до вылета. Чем ближе дата вылета, тем выше стоимость билета.</li>
	<li>Попробуйте экспериментировать с датами отлета. Не исключена возможность, что в один из рядом расположенных дней есть более выгодные предложения.</li>
	<li>Следите за <a href="https://www.aviasales.ru/offers?marker=74563.htgtTHAIoffers" target="_blank">акциями</a>, проводимыми авиакомпаниями.</li>
	<li>Посмотреть какие предложения имеются с других городов. Так, например, авиакомпания S7 летает в Таиланд из многих крупных российских городов.</li>
	<li>Как правило, если покупать билет &laquo;в оба конца&raquo;, то экономия иногда достигает 40%.</li>
	<li>Самыми дорогими являются новогодние билеты.</li>
</ol>

<p>Передвижение по стране обеспечивают Bangkok Airways и Thai airways. Столица каждой провинции, крупные города, туристические центры &mdash; все имеют собственные Аэропорты. Стандартная цена перелета по стране 1000 бат, но если повезет попасть на распродажу, то есть шанс сэкономить бат 700.&nbsp;</p>

<h2>Что разрешается ввозить в Таиланд, а от чего стоит отказаться</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413566_8339.jpg" /></p>

<p>Чтобы избежать неприятностей при въезде в королевство ознакомьтесь с таможенными правилами, действующим на его территории. В Таиланде запрещено использование электронных сигарет и кальянов, а потому пронести их через таможню не получится. Аналогичная ситуация с вейпами. Размер штрафа может достичь пятикратной стоимости изделия.</p>

<p>Не существует ограничений на личные вещи. Что касается специального оборудования (ноутбук, фотоаппарат, планшет), то это тоже разрешается провезти при условии, что суммарная стоимость всего ввозимого оборудования не превышает $10&nbsp;000. Чтобы у таможенников не возникло сомнений о цели ввоза того или иного гаджета, лучше все заблаговременно распечатать и вытащить из упаковки.</p>

<p>Одному человеку дозволено ввезти в страну не больше двухсот сигарет, при этом желательно блоки разложить по разным сумкам. Одному туристу дозволяется провезти до одного литра крепкого алкоголя. Наркотические вещества и содержащие их препараты запрещено ввозить категорически. По местным законам это карается высшей мерой наказания. А за психотропные вещества в местном суде дают до 5 лет.</p>

<p>На таможне застрянет каждый турист, решивший ввезти оружие или боеприпасы. Сделать это можно только тогда, когда на руках имеется разрешение, выданное тайским полицейским департаментом. Чтобы получить подобный документ, необходимо заблаговременно обратиться в консульство этой страны на территории России.</p>

<p>Еще одно табу наложено на все, что подпадает под классификацию порнографических изделий. Это касается не только эротических аксессуаров, но также видео- и печатной продукции.</p>

<h2>Отели</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413592_2183.jpg" /></p>

<p>Разнообразие гостиниц в королевстве позволяет выбрать вариант по любому вкусу и с учетом финансовых возможностей. Здесь имеются и недорогие <a href="https://www.booking.com/guest-house/country/th.ru.html">guest-house</a> (от $25 за одну ночь с человека) до гостиниц, отвечающих требованиям класса люкс (от $83). Отдыхающие, при желании, имеют возможность арендовать прекрасно оборудованную <a href="https://www.tripadvisor.ru/Hotels-g293915-c3-zff22-Thailand-Hotels.html">виллу</a> или <a href="https://tourtrips.ru/asia/tailand/bungalo-v-tailande/">бунгало</a> для любителей романтического отдыха вдали от шумных и суетливых городов. Вне зависимости от того, какого уровня жилье будет выбрано, можно быть уверенным в качественном сервисе, добродушном и приветливом персонаже.</p>

<p>Изменение туристической инфраструктуры напрямую зависит от места где предоставляется услуга. Так вид 4* гостиницы на материке будет отличаться от аналогичного заведения на островах. Если в Пхукете, Патайе, Банкоке это современные многоэтажные здания, то, например, на Самуи отдыхающих размещают в одноэтажных комплексах или бунгало.&nbsp;</p>

<p>Классификация отелей в Таиланде отличается от привычной для европейцев пятизвездочной системы. Объясняется это тем, что здесь используется собственная оценка работы гостиниц. Поэтому при выборе места для ночлега желательно выяснить какие именно услуги предоставляются, и какова их стоимость. Вне зависимости от последней, местный сервис выше всяческих похвал.&nbsp;</p>

<p>Практически в каждой гостинице практикуется взимание с постояльцев залога на случай порчи имущества. Как правило, его размер не превышает $100, но может и изменяться в зависимости престижности отеля. Если за время пребывания у администрации к постояльцу не появилось претензий, то при выезде вся сумма возвращается. Фактически везде имеется Wi-Fi, правда, кое-где он платный.</p>

<h2>Что почем в Таиланде</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413626_9048.jpg" /></p>

<p>Чтобы легче было спланировать свой бюджет на время отдыха в Таиланде приводим некоторые цены:</p>

<ul>
	<li>Недорого поесть можно в макашницах, где блюда стоят от $0,96 до $1,29.</li>
	<li>Полноценный обед в столовой при крупном торговом центре доступен за $6,46&divide;$9,69.</li>
	<li>Позавтракать в курортном ресторане можно за по такой же цене, обед обойдется в $16,15, а ужин &mdash; в $32,30.</li>
	<li>За проезд на тук-туках (небольшие автобусы) попросят $0,32&divide;$12,92.</li>
	<li>Однодневная экскурсия обходится $48,45&divide;$96,90.</li>
	<li>На сувениры достаточно запланировать $100&divide;$150.</li>
</ul>

<p>Мы привели стоимость только тех услуг, с которыми приходится сталкиваться чаще всего во время отдыха в королевстве.</p>

<h2>Запреты и ограничения</h2>

<p>Список ограничений не очень большой и знать его надо обязательно, иначе велика вероятность нажить неприятности:</p>

<ul>
	<li>О Будде и буддизме нельзя говорить неуважительно.</li>
	<li>Неприемлемо негативно отзываться о королевской семье.</li>
	<li>Тайские деньги нельзя рвать, мять, топтать ведь на них изображение короля.</li>
	<li>Запуск квадрокоптеров запрещен.</li>
	<li>В Таиланде курить можно исключительно в специально для этого отведенных местах. За прогулку с сигаретой по улице есть шанс нарваться на крупный штраф.</li>
	<li>При входе в буддийский храм или жилище местных жителей обязательно снимают обувь.</li>
	<li>Тайцы считают голову священной, а ноги грязными. Поэтому трогать чью-то голову, даже в качестве ласки или поощрения, не желательно.</li>
	<li>&nbsp;&nbsp; В общественных местах не рекомендуется не только целоваться или обниматься, но даже держаться за руки.</li>
</ul>

<p>Житель Таиланда, согласно буддийским канонам, уважительно относятся к животным. Поэтому обижать их не стоит &mdash; не поймут.</p>

<h2>Пляжи</h2>

<p>Любой пляж страны &mdash; собственность Короля. Ими безвозмездно дозволено пользоваться и местным жителям, и гостям страны, даже если в непосредственной близости расположился пятизвездочный отель.</p>

<h3>Прананг (Phra Nang)</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413774_3568.jpg" /></p>

<p>Расположился он на полуострове Рейли в окруженной скалами бухте, поэтому попасть сюда можно только со стороны моря, то есть на катерах.&nbsp; Песок на пляже чистый и до такой степени светлый, что его иногда называют белым. Сквозь чистую и прозрачную воду хорошо просматривается песчаное дно.</p>

<p>Никаких объектов инфраструктур здесь нет поэтому, чтобы укрыться от жаркого полуденного солнца рекомендуется брать с собой зонт. Для утоления голода отдыхающих в непосредственной близости от берега всегда стоит несколько макашниц, предлагающих разнообразную еду и напитки.&nbsp; Купаться в бухте можно даже во время отлива.</p>

<h3>Вайт Сенд (White Sand)</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413802_1558.jpg" /></p>

<p>Название пляжу дал красивый белый песок устилающий его. Он растянулся на 3 км и является самым старым и популярным на острове Ко Чанг. С одной стороны пляжа тянется широкая дорога, по обочинам которых расположились отели, рестораны, бары, тату-салоны, обменные пункты, спа-салоны, магазины сувениров и одежды.</p>

<p>Этот пляж по душе отдыхающим с детьми, что обусловлено довольно продолжительной отмелью. Для достижения приемлемой для пловцов глубины придется немного прогуляться по мелководью. Водных аттракционов нет по причине присвоения острову статуса Национального парка, акваторию которого нельзя загрязнять. По этой же причине здесь не строят высотных отелей.</p>

<h3>Ламаи (Lamai)</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/52/1598413825_2947.jpg" /></p>

<p>Пятикилометровый пляж, отличающийся кристальной водой чистым песком и глубиной, условно разделен на три района: северный, южный и центральный. Удобней заходить в воду с последнего из перечисленных участков. Благодаря крутому уклону и глубине он не зависит от отливов и приливов. Однако в декабре/феврале возможно образование довольно высоких и мощных волн.</p>

<p>Мы остановились на описании только трех пляжей Таиланда, которые больше всего понравились нам, но если вас интересуют другие варианты, то посмотреть их есть возможность <a href="https://1001beach.ru/southeast_asia/thailand">здесь</a>. &nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (47, 'Где россиянину лучше всего отдохнуть в Греции', 1, NULL, NULL, NULL, 'gde-rossiyaninu-luchshe-vsego-otdohnut-v-grecii', '2020-07-30', '2021-01-03', 'Удивительная Греция, благодаря своей неповторимой красоте выделяется среди европейских стран. Это безусловный рай на Земле для любителей туризма и полноценного отдыха. Историческое, культурное, археологическое и языковое наследие Грецией возвышает ее даже над более крупными государствами мира.  ', true, 3, 0, NULL, NULL, NULL, '<p><img src="http://travel-blogg.ru:3003/files/image/0/1595952632_1881.jpg" /></p>

<p>&nbsp;</p>

<p>Наступил 2020 год, а значит пришло время задуматься, как и где отдохнуть летом. При этом место желательно выбрать так, чтобы там в полной мере получилось восполнить нехватку солнечного тепла в организме, истощившегося на протяжении длинной зимы. Мы проанализировали большое количество информации, относящейся к туризму и отдыху, чтобы предложить вам 10 самых лучших мест за границей для полноценного проведения отпуска.</p>

<h2>Турция&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595952900_3950.jpg" /></p>

<p>&nbsp;</p>

<p>Возглавляет составленный нами рейтинг солнечная Анталия &mdash; туристский маршрут, неизменно пользующийся популярностью у отдыхающих разных национальностей. Для ее посещения не требуется визы, а теплое, ласковое море дополняется достопримечательностями, возраст которых перевалил за 2000 лет. Торговые центры, ремесленные лавки и пестрые базары приглашают заняться увлекательным шопингом. Многочисленные пляжи, 459 из которых отмечены Голубым флагом, позволяют найти место и для молодежной тусовки, и для спокойного отдыха узким семейным кругом. Поездка на срок меньше 60 дней не требует оформления визы.</p>

<h2>Греция</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953110_9488.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Что в мечтах представляется россиянину, собирающемуся полноценно отдохнуть? Завтрак на балкончике уютного отеля, с которого открывается великолепный вид на гладь моря? Величественные руины храма, много лет назад воздвигнутого древними зодчими высоко в горах, и сегодня оставляющие яркое впечатление? Золотой песок и ласковое море? Если перефразировать слова киногероя, то уверенно кажем, что все это есть в Греции.</p>

<p>Получить удовольствие от общения с древним искусством можно в величественном афинском Акрополе, помнящем времена до нашей эры? Кому интересно увидеть разноцветные пляжи надо обязательно побывать на островах Санторини, ведь здесь они черные или красные, белоснежные и золотистые. Утверждают, что стать мудрее можно, если поехать в Салоники и потереть палец Аристотеля. Побывав в Греции один раз, вы непременно включите эту в список постоянных туристских маршрутов.</p>

<h2>Мальдивы</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953250_3979.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Если спросить некоторое количество людей, зачем ехать на Мальдивы, то полученные ответы могут сильно отличаться. Серфер скажет, что здесь идеальная волна. Дайверы расскажут о плавании с гигантскими черепахами среди причудливых и поражающих красотой кораллов. Влюбленные вспомнят о незабываемых впечатлениях медового месяца, проведенного здесь. Любители экзотики опишут, как жили в подводном отеле, а люди неравнодушные к рекордам &mdash; пересекли экватора у атолла Адду. А суть этих ответов одна &mdash; туристам и отдыхающим обязательно найдется чем заняться на Мальдивах, а потому туда стоит ехать. Тем более что между Россией и Мальдивами принят безвизовый режим.</p>

<h2>Кипр</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953427_3202.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Посетить Кипр &mdash; получить возможность окунуться в античные времена, увидеть своими глазами муфлона, стать обладателем бронзового загара, донырнуть до затонувшего корабля и стать моложе на несколько лет. Здесь, чтобы съесть апельсин не надо идти на рынок, достаточно протянуть руку и сорвать его с одного из деревьев, растущих вокруг. Туриста или отдыхающего обязательно угостят королевским вином, а доброжелательность местных жителей позволит завести много интересных знакомств.</p>

<p>Берег Фигового дерева отлично подходит для отдыха с детьми. На Нисси-Бич в Айя-Напа вольготно любящей потусоваться молодежи, а романтические встречи вдвоем лучше проводить в Голубой лагуне у мыса Греко. Ходят слухи, что для сохранения любви навечно влюбленным надо хотя бы раз искупаться рядом со скалой Афродиты, расположенной недалеко от города Пафос.</p>

<h2>Египет</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953502_7061.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Страна таинственных пирамид и бескрайних пустынь, с хорошо организованными туристскими маршрутами, позволяющими прикоснуться к древней культуре. Посещение трех великих пирамид Гизы, успешно противостоящим разрушительному действию времени уже делает поездку незабываемой. А если еще отдохнуть в городе-курорте Шарм-эш-Шейх, и окунуться в глубины Красного моря, поражающие своей красотой, то такой отдых запомнится надолго. Россиянам виза оформляется в аэропорту прилета в течение нескольких минут.</p>

<h2>Таиланд</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953632_6506.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Таиланд понравится тем, кому по душе жаркое солнце и ласковое море. Отдых в этой стране вечного лета запоминается просторными чистыми пляжами, экзотической растительностью и доброжелательностью аборигенов. Здесь можно найти отели на любой вкус и возможности, но чаще встречаются заведения категории 2 и 3 звезды. Если ваша туристическая поездка не продлится больше 30 дней, то оформлять визу не надо. Жемчужиной тайских курортов считается Пхукет, который славится количеством пляжей, расположенных, в окаймлении удивительной природы, на берегу чистого моря.</p>

<h2>Хорватия</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953760_6292.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Берега Хорватии омываются Адриатическим морем. Эта юго-восточная европейская страна собрала в себе все положительное из стран Европы. Хорватские отели отличаются гостеприимством и ценами, способными приятно удивить. Для въезда можно воспользоваться болгарской визой. Начало купального сезона приходится на середину мая, а конец &mdash; на октябрь. Пляжи чаще покрыты галькой или песком, но есть и такие, где берег покрыт бетоном или скалами.</p>

<h2>Израиль</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595953925_6788.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Израиль многолик. Он может быть шумным, строгим, спокойным, гостеприимным и в то же время осторожным. История этой страны удивительна. Здесь расположены святыни трех религий: христианской, исламской, иудейской. Страна интересна для туристов и отдыхающих святынями и целебными свойствами Мертвого моря. Виза россиянам оформляется служащими аэропорта, при условии, что они не рассчитывают находиться в стране больше 90 дней. Особенность расположения страны такова, что летом купальный сезон открыт в Средиземном море, а зимой желающие купаются в Красном.</p>

<h2>Болгария</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595954000_5155.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>На восточной части Балканского полуострова расположилась Болгария, курорты которой были очень популярны среди россиян в постсоветское время. Своими берегами она выходит к Черному морю. Практически на любом курорте страны можно отдыхать с детьми, так песчаные берега их пляжей полого уходят в воду. Купаться можно с конца мая до середины сентября. Для поездки надо оформлять паспорт туриста.</p>

<h2>Франция</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1595954089_7387.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Закончим наш обзор изысканным, дорогим и роскошным курортом на Лазурном берегу &mdash; Ниццей. Уже в июле прилегающие к нему воды прогреваются до температуры комфортной для купания. В это же время город становится столицей джаза, придающей ему некоторую раскрепощенность. Истинное лицо курорта становится видно, если совершить променад по набережной, окруженной помпезными зданиями. И она, эта помпезность, поневоле передается людям, гулящим по ней. Здесь каждый почувствует себя истинным аристократом, пусть и на время отпуска.</p>

<p>Приведенная нами информация, конечно же, не является исчерпывающей. Если вы нашли полезный для себя материал &mdash; мы очень рады. В дальнейших публикациях на нашем сайте вы сможете получить большое количество полезной для полноценного, качественного и комфортного отдыха информации.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (54, 'Узбекистан — жемчужина Средней Азии', 1, NULL, NULL, NULL, 'na-otdyh-v-uzbekistan', '2020-09-03', '2021-01-03', 'Отдых в Узбекистане, при правильной его организации, способен оставить неизгладимый след в душе любого отдыхающего. Побывавший здесь хоть раз, обязательно приедет сюда еще. Не верите? А вы попробуйте!', true, 3, 0, NULL, NULL, NULL, '<p>Узбекистан &mdash; страна с тысячелетней историей, в которой живут радушные и гостеприимные люди. Через очень небольшой промежуток времени каждый турист начинает чувствовать себя здесь уютно, безопасно и сытно. Гуляя между фантастической красоты мечетями и мавзолеями, представляешь себя героем сказки про Алладина. И только одно огорчает всех приехавших на отдых в Узбекистан &mdash; рано или поздно отсюда приходится уезжать.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146060_3519.jpg" /></p>

<h2>&nbsp;</h2>

<h2>&nbsp;</h2>

<p><strong>&nbsp;</strong></p>

<h2>Что посмотреть?</h2>

<h2>&nbsp;</h2>

<p>&nbsp;</p>

<p>Независимо от того, какой вы составили маршрут, планируя отдых в Узбекистане, начнется он со столицы республики &mdash; Ташкента.</p>

<h3>Ташкент</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146091_4367.jpg" /></p>

<p>&nbsp;</p>

<p>Все места, которые рекомендуется посетить в этом городе условно делят на две группы. Если в первую входят исключительно культурные объекты, то вторая объединяет в себе места отдыха и развлечений.</p>

<p>Для тех, кто приехал на отдых в Узбекистан впервые, начинать знакомство с ним лучше всего с Государственного музея истории. Здесь каждому желающему помогут ознакомиться со всеми наиболее значимыми вехами региона.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146342_5778.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>С высоты Ташкентской телебашни как на ладони открывается захватывающая дух панорама всего города.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146377_4511.jpg" /></p>

<p>&nbsp;</p>

<p>Далее надо отправиться на осмотр самого крупного и одного из наиболее известных памятников Ташкента &mdash; Медресе Кукельдаш. Его возвели не позднее 1569 года по распоряжению министра ташкентских шайбанидских султанов Барак-хана и Дервиш-хана. Найти строение совсем несложно, так как местом его расположения является историческое сердце столицы &mdash; площадь Чорсу.&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146421_8244.jpg" /></p>

<p>&nbsp;</p>

<p>Следующим объектом посещения по логике должен стать ансамбль Шайхантаур. В центре этого архитектурного комплекса расположился мавзолей Шейха Ховенди ат-Тахура, построенный в XIV-XV веке.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146461_8154.jpg" /></p>

<p>&nbsp;</p>

<p>Хорошим завершением культурной части отдыха станет посещение Русского драматического театра. Год его создания &mdash; 1934, что дает ему право претендовать на звание старейшего.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146488_7310.jpg" /></p>

<p>&nbsp;</p>

<p>Развлекательную часть отдыха можно начать с Аквапарка, раскинувшегося на территории в 10 гектаров недалеко от парка развлечений Ташкентленд. Это первый из подобных парков в Ташкенте, открытый в 1997. Водные развлечения утопают в зелени деревьев и газонов.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146526_4076.jpg" /></p>

<p>&nbsp;</p>

<p>Чтобы отдохнуть здесь на протяжении трех часов, за взрослого человека надо заплатить около $ 6. Если есть ребенок в возрасте от трех до двенадцати лет, то плата за него составляет половину этой суммы, а в том случае, когда малышу еще не исполнилось 3 года, то он может наслаждаться горками и аттракционами бесплатно.&nbsp;</p>

<p>Тем, кто приехал на отдых в Узбекистан с детьми, надо непременно посетить зоопарк, занимающий большую площадь, которая буквально утопает в зелени. Благодаря этому, а также развитой системе орошения, общение с самыми разнообразными животными происходит в комфортной атмосфере. Стоимость посещения не превышает $ 1,5.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146668_4692.JPG" /></p>

<p>&nbsp;</p>

<p>Еще одно место, где можно полноценно отдохнуть и к тому же искупаться &mdash; парк имени Гафура Гуляма. В тени огромных деревьев расположилось большое количество аттракционов на любой вкус, способных удовлетворить самых привередливых детей. На территории парка имеется озеро, в котором можно не только искупаться, но и покататься на лодках и катамаранах.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146743_5352.jpg" /></p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146762_6652.jpg" /></p>

<p>&nbsp;</p>

<p>Ну и, конечно же, нельзя побывать в Ташкенте и не пройтись по его базарам, которых в городе великое множество. Главным и самым большим считается Чорсу. Здесь можно купить все, что душе угодно начиная с самых разнообразных специй, овощей, фруктов, мясомолочных продуктов и заканчивая такими поделками местных ремесленников как глиняная посуда, изделия из серебра, прикольные статуэтки, национальная одежда.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599146982_6301.jpg" /></p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147018_1365.jpg" /></p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147044_2696.jpg" /></p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147061_1050.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Продолжительные прогулки на свежем воздухе пробуждают аппетит, утолить который предлагают зазывалы многочисленных кафе, чайханах, пирожковых, встречающихся буквально на каждом шагу. Исходящий из их дверей аромат национальных узбекских блюд притягивает проголодавшихся туристов, как магнит.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147103_8415.jpg" /></p>

<p><em>Плов</em></p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147137_7935.jpg" /></p>

<p><em>Самса</em></p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147177_1373.jpg" /></p>

<p><em>Лепешки</em></p>

<p>&nbsp;</p>

<h3>Самарканд</h3>

<p>Посетить Узбекистан и не побывать в Самарканде &mdash; непростительная ошибка. Молва об этом древнем городе, считающемся жемчужиной восточной архитектуры, разнеслась далеко за пределы Средней Азии. В реестре ЮНЕСКО присутствуют мавзолеи, мечети, площади, дворцы, раскинувшиеся на площади в 11 километров. &nbsp;</p>

<p>Перечисление и описание каждого из них заняло бы слишком много места, а потому мы перечислим только самые знаменитые.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147267_4877.jpg" /></p>

<p>&nbsp;</p>

<p>Гур-Эмир &mdash; этот мавзолей стал последним пристанищем Темерлана (Амира Тимура), его сыновей и внуков. Считается, что именно его использовали в качестве прототипа при строительстве таких мавзолеев, как Хумаюн (Дели) и Тадж-Махал (Агра).</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147306_3597.jpg" /></p>

<p>&nbsp;</p>

<p>Шахи-Зинда входит в число лучших архитектурных ансамблей Средней Азии и список культурного наследия ЮНЕСКО. Формирование ансамбля происходило на протяжении и нескольких столетий. Это комплекс, в котором покоятся великие правители, мыслители, мудрецы, святые и философы.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147347_1632.jpg" /></p>

<p>&nbsp;</p>

<p>Биби-Ханум &mdash; мечеть, являющаяся памятником истории XV века и религиозное святилище, состоящее из комплекса зданий. Как утверждают историки, оно было построено по приказу Амира Тимура после возвращения из победного похода в Индию. Так звали любимую жену завоевателя.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147380_6599.jpg" /></p>

<p>&nbsp;</p>

<p>Регистан &mdash; центральная площадь Самарканда, на которой разместились 3 медресе. Этому месту по праву присвоено звание главной достопримечательности, которую должен увидеть каждый приехавший на отдых в Узбекистан. Возраст этой площади насчитывает более 600 лет, а возведение некоторых построек датируется пятнадцатым веком, и они сохранили свой первозданный вид.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147409_5117.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Обсерватория Улугбека была построена в 1420-х годах внуком Амира Тимура Мирзо Улугбеком. Она считается одной из лучших в исламском мире, а потому здесь работали известнейшие ученые того времени. Открытия, сделанные в астрономической школе Улугбека, оказали значительное влияние на то, как происходило развитие точных наук в странах Востока и Запада, а также Китае и Индии. &nbsp;</p>

<p>&nbsp;</p>

<h3>Бухара</h3>

<p>&nbsp;</p>

<p>В этом городе также множеств памятников, входящие во Всемирное Наследие, которые, безусловно, заинтересуют приехавших на отдых в Узбекистан.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147439_1951.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Крепость Арк &mdash; это монументальное строение, возвышающееся на 20 м над окружающей местностью, датируют I-VI веком. Она служила пристанищем для бухарских эмиров вплоть до начала XX века. &nbsp;Именно здесь писали свои труды Авиценна и Омар Хайям.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147481_7612.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Деньги на постройку медресе Чор-Минор дал богатый туркменский купец Халиф Нияз-кула. У каждого из четырех минаретов своеобразная форма, что должно говорить о религиозно-философском осмыслении четырех главных мировых религий. Если внимательно посмотреть на орнамент, то можно рассмотреть и символическую христианскую рыбу, и молитвенные колеса буддистов. &nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147516_7002.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Чтобы попасть в мемориальный комплекс Чор-Бакр, придется проехать порядка пяти километров от Бухары до села Сумитан. Его принято считать семейной усыпальницей Джуйбаров &mdash; династии шейхов, считавшихся прямыми потомками Муххамеда, а потому обладавших большим влиянием в Бухаре.&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147545_5708.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Ляби-Хауз считается центральной торговой площадью Бухары, отказаться от посещения которой не может ни один турист, приехавший на отдых в Узбекистан. Всех привлекает сюда величие, сочетающееся со спокойствием, которые призваны хранить тайны многих веков. Сегодня это место, как и столетия назад, является центром общественной жизни города.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599147667_8109.jpg" /></p>

<p>&nbsp;</p>

<p>Кош-Медресе &mdash; это ансамбль, включающий в себя 2 медресе XVI в., чьи фасады расположены на одной оси относительно друг друга. В те далекие времена они входили в число наилучших учебных заведений Бухары.&nbsp;</p>

<p>&nbsp;</p>

<h2>Где еще надо обязательно побывать</h2>

<p>Если вы плохо переносите жару и решили приехать на отдых в Узбекистан зимой, то вам непременно надо съездить на туристический горнолыжный комплекс Чимган. При этом необязательно быт фанатом горных лыж, так как здесь можно отдохнуть и по-другому.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599490406_3517.jpg" /></p>

<p><br />
Трассы для скоростного спуска спланированы таким образом, что здесь найдет для себя безопасный маршрут и начинающий лыжник и опытный покоритель снежных склонов.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599490472_7242.jpg" /></p>

<p>&nbsp;</p>

<p>Рядом с лыжными трассами расположилась девятиэтажная гостиница с ресторанами, кафе, бильярдной, диско-барами и сауной. В номерах можно проживать вдвоем или втроем. Если на отдых приехала компания, то ей предлагаются коттеджи, рассчитанные на проживание шести человек. Стоит эта услуга 1200 рублей/сутки. Имеется гибкая система скидок, зависящая от срока аренды. Так, если пользоваться коттеджем 10 суток и более, та за одни сутки плата снижается до 741 рубля.</p>

<p>&nbsp;</p>

<p>Несмотря на отсутствие у Узбекистана выхода к морям или океанам, и здесь имеется возможность вдоволь накупаться и позагорать. Для этого достаточно поехать на Чарвакское водохранилище, расположенное в шестидесяти километрах от Ташкента. Сделать это можно на электричке или комфортабельных маршрутных такси.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599490514_2296.jpg" /></p>

<p>Все вышесказанное имеет отношение к так называемому &laquo;дикому&raquo; времяпрепровождению, тогда как на этом искусственном водохранилище существует и организованный отдых. Чтобы стать его участником, достаточно купить путевку в Пирамиды. Ее название произошло от трех зданий, имеющих пирамидальную форму. В них, помимо 208 номеров разного класса, разместились ресторан, бары, бизнес-центр, дискотека, бассейн, сауны.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599490588_8681.jpg" /></p>

<p>&nbsp;</p>

<p>Стоимость проживания зависит от класса снимаемого помещения и продолжительности аренды.</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="3" style="vertical-align:top">
			<p><strong>Из расчета на двух отдыхающих</strong></p>
			</td>
		</tr>
		<tr>
			<td rowspan="4" style="vertical-align:top">
			<p>На 2 дня</p>
			</td>
			<td style="vertical-align:top">
			<p>Стандарт</p>
			</td>
			<td style="vertical-align:top">
			<p>11854,73 руб.</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Полулюкс</p>
			</td>
			<td style="vertical-align:top">
			<p>14077,49 руб.</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Люкс</p>
			</td>
			<td style="vertical-align:top">
			<p>17411,64 руб.</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>VIP</p>
			</td>
			<td style="vertical-align:top">
			<p>23709,46 руб.</p>
			</td>
		</tr>
		<tr>
			<td rowspan="4" style="vertical-align:top">
			<p>На 5 дней</p>
			</td>
			<td style="vertical-align:top">
			<p>Стандарт</p>
			</td>
			<td style="vertical-align:top">
			<p>25932,22 руб.</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Полулюкс</p>
			</td>
			<td style="vertical-align:top">
			<p>31118,67 руб.</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Люкс</p>
			</td>
			<td style="vertical-align:top">
			<p>37046,03 руб.</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>VIP</p>
			</td>
			<td style="vertical-align:top">
			<p>52605,37 руб.</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<h2>Как добраться до Узбекистана и перемещаться по республике</h2>

<p>С территории России желающим поехать на отдых в Узбекистан можно сделать это несколькими видами транспорта. У каждого из них имеются свои приверженцы и противники.</p>

<h3>Самолетом</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599490781_3083.jpg" /></p>

<p><em>Ночной Ташкент из иллюминатора самолета</em></p>

<p>Этим, самым современным способом передвижения в Ташкент можно добраться из тринадцати российских городов, если лететь без пересадки, и довольно большого количества населенных пунктов с пересадками. В списке приведены самые популярные маршруты с ориентировочной стоимостью:</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Из какого города вылет</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Наличие пересадок</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>С какой суммы начинаются цены &nbsp;&nbsp;</strong>(рубли)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Москва</p>
			</td>
			<td style="vertical-align:top">
			<p>Нет</p>
			</td>
			<td style="vertical-align:top">
			<p>24 0574</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Санкт-Петербург</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>25 953</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Краснодар</p>
			</td>
			<td style="vertical-align:top">
			<p>Нет</p>
			</td>
			<td style="vertical-align:top">
			<p>31 497</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Новосибирск</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>54 983</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Ростов-на-Дону</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>57 510</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Екатеринбург</p>
			</td>
			<td style="vertical-align:top">
			<p>Нет</p>
			</td>
			<td style="vertical-align:top">
			<p>58 329</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Казань</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>58 690</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Иркутск</p>
			</td>
			<td style="vertical-align:top">
			<p>Нет</p>
			</td>
			<td style="vertical-align:top">
			<p>63 775</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Калининград</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>67 551</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Владивосток</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>68 738</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Самара</p>
			</td>
			<td style="vertical-align:top">
			<p>Нет</p>
			</td>
			<td style="vertical-align:top">
			<p>85 295</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Нижний Новгород</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>85 645</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Томск</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>86 880</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Красноярск</p>
			</td>
			<td style="vertical-align:top">
			<p>Нет</p>
			</td>
			<td style="vertical-align:top">
			<p>92 646</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Якутск</p>
			</td>
			<td style="vertical-align:top">
			<p>Одна и более</p>
			</td>
			<td style="vertical-align:top">
			<p>126 616</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Более подробную информацию можно получить <a href="https://www.skyscanner.ru/transport/flights/ru/tas/?adults=2&amp;children=0&amp;adultsv2=2&amp;childrenv2=&amp;infants=0&amp;cabinclass=economy&amp;rtn=1&amp;preferdirects=true&amp;outboundaltsenabled=false&amp;inboundaltsenabled=false&amp;oym=1803&amp;iym=1803&amp;ref=home">здесь</a>. &nbsp;</p>

<h3>Поездом</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599490874_2492.jpg" /></p>

<p>Однако сегодня, как ни странно, многие предпочитают передвигаться по старой доброй железной дороге. Ну, нравится, людям кушать вареную курочку и пить железнодорожный чай из стеклянного стакана с подстаканником, глядя на проплывающие мимо пейзажи под стук колес. Кроме того, этот вид транспорта несомненно бюджетней предыдущего.</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" style="width:569px">
	<tbody>
		<tr>
			<td>
			<p>Москва Поезд&nbsp;006Ф &laquo;УЗБЕКИСТАН&raquo;</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>12&nbsp;114&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>17&nbsp;730&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Люкс</p>
			</td>
			<td>
			<p>27&nbsp;700&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Москва Поезд&nbsp;150Э</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>8&nbsp;981&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>11&nbsp;686&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Люкс</p>
			</td>
			<td>
			<p>18&nbsp;077&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Уфа</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>5 810 руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>7 529 руб.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Новосибирск</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>6&nbsp;376&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>8&nbsp;274&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Оренбург</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>5&nbsp;674&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>9&nbsp;706&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Саратов</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>6&nbsp;261&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>8&nbsp;805&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Екатеринбург</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>6&nbsp;886&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>9&nbsp;145&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Челябинск</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>6&nbsp;810&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>11&nbsp;211&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Волгоград</p>
			</td>
			<td>
			<p>Плацкарт</p>
			</td>
			<td>
			<p>7&nbsp;291&nbsp;руб.</p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<p>Купе</p>
			</td>
			<td>
			<p>11&nbsp;599&nbsp;руб.</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Если вам интересен именно этот вариант передвижения, то перейдите на <a href="https://poezd.ru/nalichie-mest/Moskva/Tashkent/?SearchForm%5bdateTo%5d=15.10.2020">эту </a>страницу и выберете оптимальный вариант.</p>

<h2>Передвижение по республике</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599490917_4868.jpg" /></p>

<p>&nbsp;</p>

<p>Из столицы республики, каждый приехавший на отдых в Узбекистан, может быстро и комфортно добраться до Бухары или Самарканда на скоростном поезде Афросиаб, который до Бухары домчит вас за 4 часа, а до Самарканда за 2 часа 14 минут. Если в первом случае билет стоит от 2&nbsp;495 рублей, то во втором придется заплатить 1&nbsp;768 рублей.</p>

<h2>Валюта</h2>

<p>На территории республики имеет хождение узбекский сумм. Приготовьтесь к тому, что в результате обмена на руках сразу образуется большое количество купюр, поэтому следует заблаговременно заготовить тару.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/54/1599490947_5485.jpg" /></p>

<p>Современные реалии таковы, что обменять практически любую валюту на официально признанную не составляет труда. Но при этом надо учитывать, что за 1 рубль сегодня дают 143 сума, а за один доллар 10 274.17 сума. То есть обменяв $100 вы сразу станете миллионером. Практически во всех банках есть возможность снять деньги с рублевой карты Visa.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (48, 'Отдых в Египте', 1, NULL, NULL, NULL, 'otdyh-v-egipte', '2020-07-30', '2021-01-03', 'Дешевле всего российским туристам, желающим поехать в Африку, обойдется отдых в Египте. Отдыхающие имеют возможность окунуться в экзотическую природу и воды Красного моря, поразиться размерами Египетских пирамид (одного из чудес света) и их бессменного стража - каменного Сфинкса. Оазисы в пустыни, волшебные миражи ждут туристов, пожелавших отдохнуть в этой стране. ', true, 3, 0, NULL, NULL, NULL, '<p>По берегам Средиземного и Красного морей раскинулось страна, в первую очередь ассоциирующаяся с древними пирамидами, &mdash; Египет. Только 5% процентов территории государства расположилось по берегам Нила и двух морей, а остальное &mdash; пустыня. Тем не менее она стала Меккой туристов всего мира. Достичь этого удалось благодаря волшебному сочетанию низкой цены, с теплым морем, хорошей погодой и приятным сервисом.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596124395_2764.jpg" /></p>

<p>&nbsp;</p>

<h2>Как добраться до Египта</h2>

<p>Многие интересуются, когда будут разрешены поездки в Египет. Но ведь никто и не запрещал туда летать. После авиакатастрофы в 2015 году МИДом России были опубликованы рекомендации российским гражданам отказаться от посещений этой страны. То есть запрета, как такового не существует.</p>

<p>В начале февраля от Минтранса поступило предложение ускорить решение вопроса с открытием Египта. Помимо этого, есть информация что очередной российской комиссией вынесено заключение о соответствии египетских аэропортов всем требованиям безопасности. Но все это касается поездок, организуемых турфирмами, а каждый отдельный индивидуум может в любое время полететь в эту страну.</p>

<p>Правда, поездка получится из разряда &laquo;недешевых&raquo;. Даже если взять самый дешевый прямой рейс из Москвы в Каир, то за него придется заплатить 23&nbsp;000 руб. (туда и обратно). Если лететь с пересадкой, то есть возможность уложиться в 16&nbsp;000 руб. Добраться до Шарм-эль-Шейх или Хургады можно за 19&nbsp;000 руб., если лететь через Стамбул. На <a href="https://avia.yandex.ru/search/result/?serp_uuid=&amp;passengers=%7B%27infants%27%3A+0%2C+%27adults%27%3A+1%2C+%27children%27%3A+0%7D&amp;utm_content=title&amp;utm_source=wizard_ru&amp;toId=c11485&amp;fromId=c213&amp;lang=ru&amp;utm_campaign=country&amp;from=aviawizard_pp&amp;fromName=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&amp;when=2020-04-05&amp;infant_seats=0&amp;wizardReqId=1581574648293677-384814452670540782400075-sas3-5180&amp;utm_medium=pp&amp;adult_seats=1&amp;toName=%D0%9A%D0%B0%D0%B8%D1%80&amp;children_seats=0&amp;return_date=#empty">avia.yandex.ru</a> каждый желающий имеет возможность выбрать самый приемлемый для себя вариант.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596124424_9443.jpg" /></p>

<p>&nbsp;</p>

<h2>Виза</h2>

<p>Прилетающему в Египет гражданину России или стран, входящих в СНГ, визу выдают в аэропорту, вклеивая в паспорт специальную марку. За ее получение взимается плата $ 25, возраст при этом не имеет значения. Однако если турист изъявляет желание отдыхать исключительно на Шарм-эль-Шейх (Синайский полуостров) и срок его пребывания не превышает двух недель, то виза выдается бесплатно, по варианту Sinai Only. Подробная информация о визах находится <a href="https://www.tutu.ru/geo/egypt/article/visa/">тут</a>.</p>

<p><strong>В порядке полезной информации</strong>! В визовой анкете не допускается наличие помарок или исправлений. Если таковые обнаружатся, то придется заполнять документ заново.&nbsp;</p>

<h2>Какие требуется приготовить документы</h2>

<p>Как правило представителями турагентств, оформляющих поездку, дают памятку с указанием всех документов, которые надлежит иметь туристу.&nbsp; Но этот момент настолько важен, что мы решили также привести перечень всего необходимого:</p>

<ol>
	<li>Загранпаспорт, срок действия которого заканчивается не раньше, чем через 2 месяца после окончания поездки. В нем также обязательно наличие двух страниц, не имеющих отметок.</li>
	<li>Виза, об условиях получения которой мы писали выше.</li>
	<li>Если с вами едет ребенок, то обязательное условие &mdash; наличие свидетельства о рождении.</li>
	<li>Туристическая путевка с ваучером, подтверждающим наличие бронирования номера в отеле.</li>
	<li>Билеты на обратный рейс.</li>
	<li>Страховка</li>
	<li>Два цветных снимка 4 х 6. Один приклеивается на анкету при оформлении визы, а второй &mdash; про запас.</li>
	<li>Если имеется намерение взять в прокат автомобиль, то обязательно возьмите водительские права.</li>
</ol>

<p>В качестве страховки рекомендуем сделать скан или фотографии со всех документов и отправить их на свою электронную почту. А еще желательно иметь ксерокопии, которые рекомендуется брать с собой покидая отель, чтобы не рисковать подлинными документами.</p>

<h2>Какие ограничения существуют на ввоз и вывоз</h2>

<h2>&nbsp;</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596124556_9783.jpg" /></p>

<p>&nbsp;</p>

<p>В Египте действуют таможенные правила, согласно которым в страну запрещен ввоз:</p>

<ul>
	<li>оружия, взрывчатых веществ, боеприпасов;</li>
	<li>мяса птицы;</li>
	<li>материалов, не совместимые с канонами исламской морали;</li>
	<li>предметов старины без специального пакета документов, доказывающих факт законного приобретения;</li>
	<li>изделий, изготовленные из слоновой кости и кораллы;</li>
	<li>лекарств, в составе которых присутствуют психотропные вещества;</li>
	<li>наркотиков;</li>
	<li>изделий из хлопка, предназначенные для коммерческой деятельности.</li>
</ul>

<p>Законом страны наложен запрет на вывоз морских раковин, кораллов, морских животных. Если что-то из перечисленного обнаружат при досмотре, то нарушителю придется заплатить довольно внушительный штраф ($ 1000), а в некоторых случаях предусмотрено наказание в виде лишения свободы на 2 года.&nbsp;&nbsp;</p>

<p>Кроме того, имеются ограничена на ввоз. Один турист имеет право ввезти в страну:</p>

<ul>
	<li>не больше $ 10&nbsp;000;</li>
	<li>25 сигар либо 200 сигарет;</li>
	<li>1 л напитков, содержащих алкоголь;</li>
	<li>1 л средств парфюмерии;</li>
	<li>одну электробритву одного вида (ввозится в багаже).</li>
</ul>

<p>Выявленные излишки просто на просто изымут в аэропорту.</p>

<h2>Деньги</h2>

<p>Национальной валютой Египта являются фунты, однако везде принимается расчет долларами или евро. Обменять валюту можно как в отеле, так и в городе. В последнем случае за $ 1 дадут &pound;<strong> </strong>15,73, тогда как в гостинице посчитаю по курсу $ 1 / &pound;<strong> </strong>15,33. Обменные пункты и банки не работают в пятницу и субботу, а в остальных клиентов начинают обслуживать в 8.30 и заканчивают в 15.30.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596124712_7219.jpg" /></p>

<p>&nbsp;</p>

<p>Расчет в супермаркетах можно осуществить с помощью пластиковых карт AmericanExpress, MasterCard, Visa. Во всех остальных случаях понадобятся наличные евро или доллары. Вся информация, имеющая отношение к финансовым вопросам находится <a href="https://www.tutu.ru/geo/egypt/article/money/">здесь.</a></p>

<p>Чтобы вы могли приблизительно определить сколько брать с собой денег, приводим стоимость отдельных услуг и товаров:</p>

<ul>
	<li>от $ 15 стоят экскурсии;</li>
	<li>от $ 10 до $ 15 &mdash; аренда квадроцикла;</li>
	<li>$ 5 придется отдать за использование маски для ныряния;</li>
	<li>$ 30 если погружаться с катера, используя акваланг;</li>
	<li>$ 8 &mdash; за ту же услугу, но с берега;</li>
	<li>$ 10 &mdash; аренда ласт;</li>
	<li>$ 5 &mdash; картины, нарисованные на папирусе;</li>
	<li>в $ 10 обойдется ужин на одного человека вне отеля;</li>
	<li>$ 5 стоят футболки и рубашки из хлопка;</li>
	<li>$ 1 &mdash; масла для ухода за кожей;</li>
	<li>от $ 3 практически все сувениры;</li>
	<li>за проезд в маршрутке придется расплачиваться двумя египетскими фунтами ($ 0,12);</li>
	<li>такси довезет от гостиницы до центра за $ 1.</li>
</ul>

<p>Надеемся, что эта выкладка поможет вам хотя бы приблизительно определиться с необходимой для нормального отдыха суммой. От практической части переходим к описанию того, ради чего собственно россияне и едут в Египет &mdash; где отдохнуть, покупаться и позагорать.</p>

<h2>Курорты и пляжи</h2>

<h2>&nbsp;</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596124870_6606.jpg" /></p>

<p>&nbsp;</p>

<p>Самыми известными курортами Египта безусловно являются Шарм-эль-Шейх и Хургада. Добраться до них можно регулярными или чартерными рейсами. Существуют также прямые рейсы до Марса-Алама, расположенного в двухстах километрах от Хургады и являющегося самым молодым курортом. Чтобы узнать все особенности пребывания на курортах Египта, достаточно перейти <a href="https://www.tutu.ru/geo/egypt/article/resort/">сюда.</a></p>

<p>Описание пляжей начнем с райского уголка, окруженного горами под названием Макади-Бей. В этой бухте не бывает некомфортных климатических условий. Летом температура воздуха тут прогревается до +35&deg;С, опускаясь до +20&deg;С зимой. Самая низкая температура воды &mdash; +20&deg;С. В море, отличающееся кристальной чистотой плавно нисходят массивы золотистого песка.</p>

<p>Прибрежную полосу поделили между собой отели, которые постарались оснастить ее всем, что входит в понятие благоустроенный пляж. Помимо зонтиков и шезлонгов отдыхающим предлагается большое количество водных аттракционов. Территория пляжа постоянно очищается не только от мусора, но и от осколков кораллов. Благодаря расположенным в непосредственной близости коралловым рифам, в воде хорошо видно большое количество разнообразной живности, обитающей на них.&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596124917_2174.jpg" /></p>

<p>&nbsp;</p>

<p>Далее идут пляжи Шарм-эль-Шейх, которые сплошь являются коралловыми. Их изюминка &mdash; многоуровневое устройство при небольшой ширине. Кораллы, протяженностью в 20, а местами и 50 метров &mdash; великолепное зрелище, но их наличие существенно затрудняет вхождение в воду.</p>

<p>Когда начинали строиться первые отели на этом курорте, кораллы в их акватории были вырублены, для создания комфортных условий для купающихся, которые сходят в море по пологому песчаному дну. Владельцы гостиниц, возведенных позже уже попали под запрет уничтожения кораллов. Поэтому они вынуждены снабжать отдыхающих специальной обувью, чтобы предотвратить повреждение ног отдыхающих.&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596124950_2553.jpg" /></p>

<p>&nbsp;</p>

<p>Когда говорят о старейшем и <a href="https://www.topkurortov.com/luchshie-kurorty-egipta/">лучшем курорте Египта</a>, подразумевают Хургаду из чего следует, что здешние пляжи входят в число самых роскошных и благоустроенных.&nbsp; Их, полого уходящая в воду, поверхность покрыта безукоризненно чистым песком, а на дне нет кораллов. Для лучших пляжей этого курорта введена классификация.</p>

<p>На самых роскошных, помимо развитой пляжной инфраструктуры, устанавливаются экраны, защищающие загорающего от порывов ветра характерных для здешнего климата. Правда и за вход на такие пляжи придется заплатить больше. Основная масса пляжей распределена между отелями, но есть и городские.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596125016_7789.jpg" /></p>

<p>&nbsp;</p>

<p>Пляжи Марса-Алам расположены в уютной бухте. Туристов привлекает сюда размер песчаных полос и лазурный цвет воды. За чистотой и ухоженность побережья ведется неусыпный контроль. Водные лыжи и мотоциклы &mdash; неполный перечень предоставляемых отдыхающим развлечений. А перевести дух после активного отдыха можно на удобном лежаке под развесистым зонтиком, которые всегда имеются в избытке.</p>

<p>Вода настолько чистая, что для любования морскими обитателями даже не надо нырять. Возникает ощущение, что плаваешь в огромном аквариуме, заполненного кальмарами, зелеными черепашками, морскими коньками и другими животными, и растениями.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596125060_2343.jpg" />&nbsp;</p>

<p>В отличие от основной массы египетских пляжей на Дахабе берег песчано-каменистый. Может по этой причине здесь относительно немного лежаков и зонтиков. Это безусловно привлечет отдыхающих, не любящих бесконечную толчею и гомон большинства пляжей, но обожающих прекрасную природу почти в нетронутом цивилизацией виде.</p>

<p>Еще здесь любят отдыхать серфингисты и почитатели дайвинга. Первых привлекает возможность, как они выражаются &laquo;поймать волну&raquo;, а вторых возможность погружаться прямо с берега, не пользуясь услугами катеров. Как утверждают местные жители здесь, если повезет, можно найти затонувшие сокровища.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596125080_4904.jpg" />&nbsp;</p>

<h2>Отели</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596125268_9810.jpg" /></p>

<p>&nbsp;</p>

<p>Звездный уровень египетских отелей идентичен существующему в Тунисе, Турции и всех других южных странах. Однако относительно европейских стандартов он ниже. Если и не все, то большинство гостинец Египта сконцентрировано в зоне Хургады и Шарм-эль-Шейха.</p>

<p>&nbsp;</p>

<p>Сертификаты &laquo;звездности&raquo; им выдает местное министерство туризма, а потому для определения уровня услуг следует от европейского показателя отнимать одну звезду. То есть египетский пятизвездочный отель соответствует европейскому уровню 4*. Чаще всего встречаются отели с четырьмя звездами и условия проживания в них вполне приемлемы. Когда говорят, что у отеля 3* или 2*, то надо понимать, что это небольшое строение со спартанскими условиями.</p>

<p>Как правило, отели располагаются вдоль береговой линии. Когда это не так, постояльцам для поездки до пляжа предоставляется бесплатный автобус. Территория пляжей разбита на участки, принадлежащий определенному отелю, постояльцам которого зонтики и лежаки выдаются бесплатно.</p>

<p>Туристов египетские отели привлекают разнообразием цен, позволяющим найти вариант, наиболее походящий по условиям проживания и стоимости. Так номер в гостинице Amar Sina Village (Шарм-эль-Шейх) для одного отдыхающего обойдется всего в 634 руб., тогда как Fairmont Nile City (Каир) за эту же услугу придется заплатить 9&nbsp;272 руб. Чтобы получить более подробную информацию, вам достаточно пройти по этой <a href="https://oteli.biletyplus.ru/egipet">ссылке</a>.</p>

<h2>Полезная информация</h2>

<p>Для тех, кто посещает Египет впервые мы подобрали список полезной информации:</p>

<ul>
	<li>Вода в местном водопроводе не пригодна для питья.</li>
	<li>Прокатиться на верблюде можно бесплатно, а вот слезть с него стоит $ 20.</li>
	<li>Для местных водителей не существует правил дорожного движения, а потому переходя дорогу надо быть очень внимательным.</li>
</ul>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596125357_5727.jpg" /></p>

<ul>
	<li>Фрукты, даже если они упакованы, надо непременно мыть.</li>
	<li>Солнце египетское &laquo;злое&raquo;, а потому хождение по улице без головного убора для некоторых туристов заканчивается солнечным ударом.</li>
	<li>Поверхность некоторых из обитающих в море животных и рыбок ядовита и трогать их руками не надо.</li>
	<li>При посещении храмов или святынь обязательным условием является соблюдение мусульманского дресс-кода. Проще говоря туда нельзя заходить в пляжной одежде или обутыми.</li>
	<li>Даже в гостиничных бассейнах нельзя загорать или купаться топлес.</li>
	<li>На представителей местной подводной фауны запрещено охотится с помощью гарпунного ружья. Нарушителя штрафуют и выдворяют из страны.</li>
	<li>В пятницу до 12 часов беспокоить мусульман нельзя &mdash; у них намаз, то есть пятничная молитва. Поэтому не удивляйтесь, что в этот день с утра закрыты многие магазины, кафе, лавки и рестораны. Они начнут работать, как только их хозяева закончат молитву.</li>
	<li>На все территории Египта действует сухой закон, который не распространяется только на отели и рестораны. На протяжении трех дней после прилета разрешается купить алкогольные напитки в Дьюти-фри, но продавец потребует показать паспорт с датой прилета.</li>
	<li>Река Нил настолько сильно загрязнена, что купаться в ней не рекомендуется.</li>
</ul>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1596125499_9099.jpg" /></p>

<p>И в заключение еще один совет. Решив добраться куда-нибудь на такси, рекомендуется сразу договариваться с водителем об окончательной стоимости его услуги. Иначе не исключена вероятность возникновения неприятной ситуации.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (55, 'Абхазия — цветущий субтропический рай на черноморском побережье', 1, NULL, NULL, NULL, 'abhaziya--cvetushchii-subtropicheskii-rai-na-chernomorskom-poberezhe', '2020-09-09', '2021-01-03', 'Страна Души - именно так называют местные жители Абхазию. И действительно в этой здравнице Советского Союза, душа отдыхает. Ласковое море, реликотвые леса и рощи, горы, гостепреимные люди и вкусная еда - что еще надо для полноценного отдыха?  ', true, 3, 0, NULL, NULL, NULL, '<p>На отдых в Абхазию, маленькую, но очень гордую и гостеприимную страну, едут семьями. Еще со времен Советского Союза здесь купались, загорали, принимали процедуры жители этой великой страны. Для этого по всему побережью строились курорты, отели и пансионаты. Сегодня плюс к этому свои двери распахнули многочисленные хостелы, тогда как частный сектор всегда был рад принять туристов.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652065_2747.jpg" /></p>

<p>&nbsp;</p>

<h2>Климатические условия</h2>

<p>Климат в этой стране влажный субтропический. А благодаря удивительному рельефу местности, на которой раскинулась страна, можно при желании побывать и в лете, и в зиме за один заезд. Ведь когда на побережье люди блаженствуют под солнечными лучами, окунаясь в теплую воду, в горах, если подняться до отметки 2700 м над уровнем моря, лежит толстый слой векового снега.</p>

<p>Принимать солнечные ванны на пляжах можно с мая по октябрь. Самым благоприятным временем для приезда на отдых в Абхазию с детьми считается вторая половина лета и сентябрь. В это время жара заметно спадает и солнечные лучи не так безжалостно опаляют светлую кожу россиян. Температура прибрежной воды подымается до уровня присущего парному молоку.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652101_1031.jpg" /></p>

<p>&nbsp;</p>

<p>Много россиян приезжают на отдых в Абхазию зимой, чтобы встретить здесь Новый год. Так делают те, кому надоели трескучие морозы и захотелось попасть в сказку, когда в разгар зимы температура воздуха прогревается до +10&deg;С, а порой и +12&deg;С. Снег бывает иногда и здесь, но, во-первых, это большая редкость, а во-вторых, при первых же лучах солнца он тает. Но рано утром есть возможность полюбоваться такой картинкой.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652122_5421.jpg" /></p>

<p>&nbsp;</p>

<h2>Какими видами транспорта можно доехать до Абхазии</h2>

<p>Начнем с того, что самолетом в Абхазию долететь не получится. Дело в том, что в Сухуми имеется аэропорт, но обслуживает он исключительно внутренние рейсы.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652156_4777.jpg" /></p>

<p>&nbsp;</p>

<p>Поэтому долететь можно до Адлера, от которого до границы с Абхазией всего 12 км. Кроме того, недалеко расположены аэропорты Сочи, Геленджика, Краснодара. Маршруты через эти 3 города пользуются наибольшей популярностью, а потому приведем примерную стоимость перелетов до них.</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td rowspan="3" style="vertical-align:top">
			<p><strong>Пункт отправления</strong></p>
			</td>
			<td colspan="6" style="vertical-align:top">
			<p><strong>Пункт прибытия</strong></p>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="vertical-align:top">
			<p>Сочи</p>
			</td>
			<td colspan="2" style="vertical-align:top">
			<p>Краснодар</p>
			</td>
			<td colspan="2" style="vertical-align:top">
			<p>Геленджик</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Прямые (руб.)</p>
			</td>
			<td style="vertical-align:top">
			<p>С пересадкой (руб.)</p>
			</td>
			<td style="vertical-align:top">
			<p>Прямые (руб.)</p>
			</td>
			<td style="vertical-align:top">
			<p>С пересадкой (руб.)</p>
			</td>
			<td style="vertical-align:top">
			<p>Прямые (руб.)</p>
			</td>
			<td style="vertical-align:top">
			<p>С пересадкой (руб.)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Москва</p>
			</td>
			<td style="vertical-align:top">
			<p>1&nbsp;904</p>
			</td>
			<td style="vertical-align:top">
			<p>1&nbsp;904</p>
			</td>
			<td style="vertical-align:top">
			<p>1 728</p>
			</td>
			<td style="vertical-align:top">
			<p>1 728</p>
			</td>
			<td style="vertical-align:top">
			<p>2 567</p>
			</td>
			<td style="vertical-align:top">
			<p>2 567</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Санкт-Петербург</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;267</p>
			</td>
			<td style="vertical-align:top">
			<p>20267</p>
			</td>
			<td style="vertical-align:top">
			<p>2 344</p>
			</td>
			<td style="vertical-align:top">
			<p>2 344</p>
			</td>
			<td style="vertical-align:top">
			<p>2 749</p>
			</td>
			<td style="vertical-align:top">
			<p>2 749</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Екатеринбург</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;084</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;084</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;897</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;897</p>
			</td>
			<td style="vertical-align:top">
			<p>5 260</p>
			</td>
			<td style="vertical-align:top">
			<p>4 658</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Казань</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;459</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;459</p>
			</td>
			<td style="vertical-align:top">
			<p>2 218</p>
			</td>
			<td style="vertical-align:top">
			<p>2 218</p>
			</td>
			<td style="vertical-align:top">
			<p>5 558</p>
			</td>
			<td style="vertical-align:top">
			<p>4 143</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Уфа</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;267</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;267</p>
			</td>
			<td style="vertical-align:top">
			<p>2 479</p>
			</td>
			<td style="vertical-align:top">
			<p>2 479</p>
			</td>
			<td style="vertical-align:top">
			<p>6 745</p>
			</td>
			<td style="vertical-align:top">
			<p>4 273</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Новосибирск</p>
			</td>
			<td style="vertical-align:top">
			<p>4&nbsp;441</p>
			</td>
			<td style="vertical-align:top">
			<p>4&nbsp;441</p>
			</td>
			<td style="vertical-align:top">
			<p>6 302</p>
			</td>
			<td style="vertical-align:top">
			<p>6 302</p>
			</td>
			<td style="vertical-align:top">
			<p>10 707</p>
			</td>
			<td style="vertical-align:top">
			<p>6 356</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Самара</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;267</p>
			</td>
			<td style="vertical-align:top">
			<p>2&nbsp;267</p>
			</td>
			<td style="vertical-align:top">
			<p>2 318</p>
			</td>
			<td style="vertical-align:top">
			<p>2 318</p>
			</td>
			<td style="vertical-align:top">
			<p>Цена не найдена</p>
			</td>
			<td style="vertical-align:top">
			<p>4 994</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Тюмень</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;541</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;541</p>
			</td>
			<td style="vertical-align:top">
			<p>2 654</p>
			</td>
			<td style="vertical-align:top">
			<p>2 654</p>
			</td>
			<td style="vertical-align:top">
			<p>4 679</p>
			</td>
			<td style="vertical-align:top">
			<p>4 679</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Челябинск</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;541</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;541</p>
			</td>
			<td style="vertical-align:top">
			<p>2 018</p>
			</td>
			<td style="vertical-align:top">
			<p>2 018</p>
			</td>
			<td style="vertical-align:top">
			<p>7 685</p>
			</td>
			<td style="vertical-align:top">
			<p>6 394</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Пермь</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;600</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;600</p>
			</td>
			<td style="vertical-align:top">
			<p>2 879</p>
			</td>
			<td style="vertical-align:top">
			<p>2 879</p>
			</td>
			<td style="vertical-align:top">
			<p>&nbsp;</p>
			</td>
			<td style="vertical-align:top">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Красноярск</p>
			</td>
			<td style="vertical-align:top">
			<p>4&nbsp;441</p>
			</td>
			<td style="vertical-align:top">
			<p>4&nbsp;441</p>
			</td>
			<td style="vertical-align:top">
			<p>8 783</p>
			</td>
			<td style="vertical-align:top">
			<p>5 058</p>
			</td>
			<td style="vertical-align:top">
			<p>7 909</p>
			</td>
			<td style="vertical-align:top">
			<p>6 367</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Нижний Новгород</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;333</p>
			</td>
			<td style="vertical-align:top">
			<p>3&nbsp;333</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>Цена не найдена</p>
			</td>
			<td style="vertical-align:top">
			<p>5 645</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Омск</p>
			</td>
			<td style="vertical-align:top">
			<p>8&nbsp;132</p>
			</td>
			<td style="vertical-align:top">
			<p>4&nbsp;056</p>
			</td>
			<td style="vertical-align:top">
			<p>5 486</p>
			</td>
			<td style="vertical-align:top">
			<p>4 056</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Сыктывкар</p>
			</td>
			<td style="vertical-align:top">
			<p>4&nbsp;344</p>
			</td>
			<td style="vertical-align:top">
			<p>4&nbsp;344</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Сургут</p>
			</td>
			<td style="vertical-align:top">
			<p>11&nbsp;537</p>
			</td>
			<td style="vertical-align:top">
			<p>3 773</p>
			</td>
			<td style="vertical-align:top">
			<p>4 015</p>
			</td>
			<td style="vertical-align:top">
			<p>3 573</p>
			</td>
			<td style="vertical-align:top">
			<p>8 239</p>
			</td>
			<td style="vertical-align:top">
			<p>4 373</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Сочи</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>2 560</p>
			</td>
			<td style="vertical-align:top">
			<p>2 560</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Новый Уренгой</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>Цена не найдена</p>
			</td>
			<td style="vertical-align:top">
			<p>10 417</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Пермь</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>2 879</p>
			</td>
			<td style="vertical-align:top">
			<p>2 879</p>
			</td>
			<td style="vertical-align:top">
			<p>7 091</p>
			</td>
			<td style="vertical-align:top">
			<p>5 358</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Мурманск</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>8 782</p>
			</td>
			<td style="vertical-align:top">
			<p>6 072</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652191_8509.jpg" /></p>

<p>&nbsp;</p>

<p>Тем, кто предпочитает бюджетные варианты, свои услуги представляет железная дорога. Из Москвы на отдых в Абхазию РЖД предлагает доехать двумя поездами. Первый, под номером 306М, каждый день отправляется в 19:50 с Казанского вокзала. Ему достаточно полутора суток, чтобы прибыть в Сухуми.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652270_8954.jpg" /></p>

<p>&nbsp;</p>

<p>Билет в плацкартный вагон стоит около 3 000 рублей. Однако далеко не каждый отправляющийся на отдых человек согласен мирится с неудобствами этого типа вагонов. Тогда ему предложат за 6&nbsp;000 рублей купейный вагон, или за 12&nbsp;000 рублей &mdash; люкс.</p>

<p>Второй поезд, также курсирующий каждый день и имеющий литеру 479А, проходящий. Станция его отправления &mdash; Санкт-Петербург откуда он отходит в 10:40. Проезд в плацкарте этого состава обойдется на 300 рублей дороже, а в купе эта разница возрастает до 500 рублей. Кроме того, до станции назначения он идет больше двух суток (55 часов) и в нем отсутствуют люксовые вагоны. Поэтому основная масса туристов предпочитает ехать на первом варианте.</p>

<p>Еще один прямой поезд (487С) идет из Самары и тратит на преодоление расстояния до Сухум 2 дня и 2 часа. В нем, так же, как и в предыдущем нет вагонов люкс. За проезд в плацкартном вагоне придется заплатить 5&nbsp;566 рублей, а в купе &mdash; 9&nbsp;576 рублей. Кроме того, существует несколько вариантов с пересадками, о которых вся информация находится <a href="https://www.tutu.ru/poezda/station_d.php?nnst=4200300">тут</a>. Большим плюсом поездки на поезде является отсутствие необходимости ехать до конечной остановки, а выйти, например, в Гаграх.&nbsp;</p>

<p>За проезд от Адлера до Гудаута в течение четырех часов, один пассажир заплатит 596 рублей. Поезд ходит два раза в сутки.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652398_1098.jpg" /></p>

<p>&nbsp;</p>

<p>На отдых в Абхазию некоторые россияне едут автобусами, туры на которых организуются из многих регионов России. Как правило, поездка на этом виде транспорта обходится несколько дешевле, чем, скажем, на поезде. Однако надо понимать, что ради экономии придется пожертвовать некоторыми удобствами и временем.</p>

<p>Маршрут заканчивается на пограничном КПП, через который туристы проходят пешком. На стороне Абхазии имеется стоянка маршрутных такси, которые довезут до любого курорта, расположенного на побережье. Их отправка осуществляется каждые 10-15 минут. Стоимость переезда начинается от 100 рублей и находится в прямой зависимости от удаленности места назначения.</p>

<p>Наличие личного автомобиля позволяет избежать проблем, порой возникающих при пользовании всеми перечисленными выше способами передвижения. На отдых в Абхазию российские туристы въезжают через КПП Псоу, расположенное в поселке Веселое, что в Краснодарском крае (<a href="https://nashaplaneta.net/europe/abkhazia/abkhazia-karta?group=others&amp;name=ent1&amp;zoom=13">здесь </a>можно посмотреть карту).&nbsp;</p>

<p>Протяженность популярных у россиян маршрутов до Абхазии:</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p><strong>Пункт отправки</strong></p>
			</td>
			<td style="vertical-align:top">
			<p><strong>Протяженность&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>(км)</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Москва</p>
			</td>
			<td style="vertical-align:top">
			<p>1 655</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Санкт-Петербург</p>
			</td>
			<td style="vertical-align:top">
			<p>2 373</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Казань</p>
			</td>
			<td style="vertical-align:top">
			<p>2 095</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Самара</p>
			</td>
			<td style="vertical-align:top">
			<p>2 000</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Волгоград</p>
			</td>
			<td style="vertical-align:top">
			<p>1 008</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Ростов-на-Дону</p>
			</td>
			<td style="vertical-align:top">
			<p>586</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Краснодар</p>
			</td>
			<td style="vertical-align:top">
			<p>440</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<h2>Курорты</h2>

<p>Местом расположения практически всех курортов Абхазии является морское побережье, протяженностью 210 км. Мы расскажем о пользующихся наибольшей популярностью у россиян.</p>

<h2>Гагры</h2>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652593_5812.jpg" /></p>

<p>Популярность этот курорт приобрел еще в те времена, когда сюда приезжали на отдых граждане Советского Союза. Но и в современных реалиях он остается самым посещаемым и, естественно, дорогим. Окружающие его горы обеспечивают надежное прикрытие от холодного воздуха, а потому местные воздух и вода считаются самыми теплыми на всем абхазском побережье. Красивым обрамлением для здешних пляжей служат пальмы и вечнозеленый олеандр.&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652616_9350.jpg" /></p>

<p>&nbsp;</p>

<p>Свои услуги туристам, приехавшим на отдых в Абхазию, предлагает несколько десятков отелей. Одним из ярчайших продолжателей традиций гостеприимства считается отель Абаата расположенный на территории одноименной средневековой крепости. Сегодня это три корпуса, возведенные в 1903, 1906 и 2009 годах с номерами, соответствующими всем стандартам, применяемым к гостиничному сервису в Европе.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652640_8072.jpg" /></p>

<p>&nbsp;</p>

<p>У отеля имеется собственный пляж, на котором постояльцам бесплатно выдают лежаки, зонтики и полотенца. На прилегающей территории разбит парк засаженный различными субтропическими и тропическими деревьями и кустарниками. Стоимость номеров варьируется в зависимости от их укомплектованности и размера.</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top">
			<p>&nbsp;</p>
			</td>
			<td style="vertical-align:top">
			<p>Стоимость проживания (руб.)</p>
			</td>
			<td style="vertical-align:top">
			<p>Доп. место для ребенка, не достигшего 12 лет</p>
			</td>
			<td style="vertical-align:top">
			<p>Доп. место для взрослого</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Двухместный номер без балкона</p>
			</td>
			<td style="vertical-align:top">
			<p>3 900</p>
			</td>
			<td style="vertical-align:top">
			<p>900</p>
			</td>
			<td style="vertical-align:top">
			<p>1 100</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Двухместный номер с балконом</p>
			</td>
			<td style="vertical-align:top">
			<p>4 300</p>
			</td>
			<td style="vertical-align:top">
			<p>900</p>
			</td>
			<td style="vertical-align:top">
			<p>1 100</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Улучшенный двухместный номер без балкона</p>
			</td>
			<td style="vertical-align:top">
			<p>4 900</p>
			</td>
			<td style="vertical-align:top">
			<p>900</p>
			</td>
			<td style="vertical-align:top">
			<p>1 200</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Улучшенный двухместный номер с балконом</p>
			</td>
			<td style="vertical-align:top">
			<p>5 500</p>
			</td>
			<td style="vertical-align:top">
			<p>900</p>
			</td>
			<td style="vertical-align:top">
			<p>1 400</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Люкс - Двухместный двухкомнатный номер</p>
			</td>
			<td style="vertical-align:top">
			<p>7 300</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>1 400</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Апартаменты студия</p>
			</td>
			<td style="vertical-align:top">
			<p>7 300</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>1 400</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Апартаменты с одной спальней</p>
			</td>
			<td style="vertical-align:top">
			<p>8 700</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>1 400</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top">
			<p>Апартаменты - Четырехместный трехкомнатный номер</p>
			</td>
			<td style="vertical-align:top">
			<p>10 500</p>
			</td>
			<td style="vertical-align:top">
			<p>-</p>
			</td>
			<td style="vertical-align:top">
			<p>&nbsp;1 400</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Для тех, у кого в этом имеется заинтересованность, в городе имеются и более бюджетные варианты съема жилья. Так ночевка во многих частных гостиницах небольшого размера обойдется в полторы или две тысячи рублей. При этом условия в них довольно приличные и имеется общая кухня для приготовления пищи.</p>

<p>Предложения от частного сектора, которые встречаются на каждом шагу, полностью зависят от удаленности от моря. Если в непосредственной близости к побережью сложно найти что-нибудь приличное дешевле 1200 рублей, то на некотором удалении встречаются варианты с платой менее 1000 рублей.</p>

<h2>Пицунда</h2>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652672_8301.jpg" /></p>

<p>Поселок, расположенный рядом с Гагры, позиционируется в качестве наилучшего отдыха, если семья приехала с детьми. Расположен он на небольшом по размеру мысу, выступающем в море. По отзывам туристов, приехавших на отдых в Абхазию, здешняя вода отличается исключительной чистотой.</p>

<p>Кроме того, это место паломничества людей, у которых имеются проблемы с органами дыхания, что объясняется произрастанием в данной местности реликтовых сосен и эвкалиптов. Здесь имеется поселок Лдзаа с одним из немногих пляжей с песчаным покрытием.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652693_6299.jpg" /></p>

<p>&nbsp;</p>

<h2>Сухум</h2>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652713_2776.jpg" /></p>

<p>Столица Абхазии относится к самым древним городам мира. В нем большое количество санаториев размещено в почти двадцатикилометровой пляжной зоне. Помимо этого, здесь есть минеральные источники и великолепный Ботанический сад, в котором произрастает порядка 5&nbsp;000 растений. Каждый желающий имеет возможность посетить древние пещеры.</p>

<h2>Озеро Рица</h2>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652731_6545.jpg" /></p>

<p>Его называют живописной жемчужиной Абхазии. Это природное водохранилище ледниково-тектонического происхождения расположилось на высоте 950 м над уровнем моря. В длину озеро растянулось на два с половиной километра, а ширина его меняется от 270 м до 870 м. Местами его глубина достигает 131 м. сквозь абсолютную прозрачную воду прекрасно видно рыбу, плавающую на дне.</p>

<p>Добраться до достопримечательности можно только по одной дороге, проходящей мимо Голубого озера, сторожевой башни Хасан-Абаа, Юпшарского каньона, водопадов Мужские и Девичьи слезы, развалин Бзыбской крепости, построенной еще в Х веке.</p>

<p>Вокруг озера раскинулась территория Рицевского реликтового парка с шумными ручьями, водопадами, горными реками и отвесными скалами. Здесь любил отдыхать Сталин, для которого была построена специальная дача.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652751_2984.jpg" /></p>

<p>&nbsp;</p>

<p>Сменившему его Хрущеву также нравились эти места, но жить в доме своего предшественника он не захотел и приказал построить себе новый дом.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652769_4086.jpg" /></p>

<p>&nbsp;</p>

<p>С удовольствием приезжал сюда и Брежнев, для которого дачи Сталина и Хрущева были объединены в единый комплекс.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1599652791_5093.jpg" /></p>

<p>&nbsp;</p>

<h2>Что еще надо знать, собираясь на отдых в Абхазию</h2>

<p>Для граждан России не нужна виза для посещения Абхазии. На всей территории страны без ограничений принимают российский рубль. Можно рассчитываться карточками Visa, MasterCard и другими. В некоторых крупных магазинах и кафе принимаются платежи через платежную систему PayPal. Однако запас наличных лучше иметь при себе.</p>

<p>Приблизительный расчет расходов на двух человек, собирающихся на отдых в Абхазию на 10 дней, выглядит следующим образом:</p>

<ul>
	<li>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Транспортные расходы &mdash; 20&nbsp;000 рублей.</li>
	<li>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Оплата гостиницы &mdash; 20&nbsp;000 рублей.</li>
	<li>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Развлечения &mdash; 15&nbsp;000 рублей.</li>
	<li>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Еда &mdash; 15&nbsp;000 рублей.</li>
	<li>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Покупки &mdash; 10&nbsp;000 рублей.</li>
</ul>

<p>Итого за 80&nbsp;000 рублей можно прекрасно отдохнуть на черноморском побережье, наслаждаясь красотами природы, качественным сервисом и гостеприимством местных жителей.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (49, 'Хорватия: все, что требуется знать об отдыхе в этой стране ', 1, NULL, NULL, NULL, 'horvatiya-vse-chto-trebuetsya-znat-ob-otdyhe-v-etoi-strane-', '2020-08-04', '2021-01-03', 'Если вас прельщает отдых в мягком климате в непосредственной близости к термальным источникам, то вам обязательно надо посетить Хорватию. Сочетание соответствия местных оздоровительных центров всем современным требованиям с развитой инфраструктурой туристического бизнеса сделает ваш отдых в этой стране незабываем и полезным.  ', true, 3, 0, NULL, NULL, NULL, '<h1>&nbsp;</h1>

<p>Отличительная черта этой страны &mdash; мягкий климат, развитая инфраструктура. На базе термальных источников здесь созданы оздоровительные центры, отвечающие всем современным требованиям. Это делает их привлекательными для туристов многих стран. Мы расскажем где в Хорватии можно хорошо отдохнуть, что для этого надо и сколько это будет стоить.&nbsp;</p>

<p>&nbsp;<img src="http://travel-blogg.ru:3003/files/image/49/1596708300_6183.jpg" /></p>

<p>&nbsp;</p>

<h2>Как оформить визу для въезда в Хорватию</h2>

<p>Чтобы подать прошение о визе, воспользовавшись услугами московского ВЦ Хорватии, потребуется предварительная запись. Имеется 3 варианта сделать это:</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; переслать запрос на электронную почту: <a href="mailto:croatia@vfshelpline.com">croatia@vfshelpline.com</a>.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; воспользоваться услугами call-центра, принимающего звонки по телефону (499) 70-34-945 с 9.00 до 17.00;</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; выполнить регистрацию онлайн-заявки, зайдя на сайт VFS.Global.</p>

<p>Желающим получить хорватскую визу не требуется прохождения дактилоскопии. За оформление взимается пошлина &euro; 35, если оформление происходит в стандартном режиме, и &euro; 70 &mdash; за срочное оформление. Сделать это можно в любом банке.</p>

<p>Последовательность действий такова:</p>

<p>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Заполняется визовая анкета.</p>

<p>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Заявителем собственноручно приносится заполненный бланк с комплектом документов в ВЦ. Если это сделать невозможно, то сделать это могут выступать супруги, родители, дети, внуки. В этом случае потребуются документы, подтверждающие родство. Подать прошение через доверенных лиц не получится.</p>

<p>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Оплачивается визовая пошлина, сервисный сбор.</p>

<p>Получив оповещение о благоприятном решении вопроса, претендент обязан лично прийти в ВЦ (Москва, Коробейников переулок, 16/10) для получения паспорта. Оригиналы чеков, как и выданный агентский договор, сохраняются.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708434_2429.jpg" /></p>

<p>&nbsp;</p>

<h3>Какие документы потребуются для получения визы</h3>

<p>В комплект документов, подаваемых вместе анкетой-заявлением, входят:</p>

<p>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Заграничный паспорт, выданный не больше 10 лет назад, и имеющий не менее двух пустых страниц. Еще одно обязательное условие &mdash; со дня завершения действия визы до окончания срока действия паспорта допускается промежуток времени не меньше трех месяцев.</p>

<p>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Банковский документ, подтверждающий оплату сервисного сбора, госпошлины.</p>

<p>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Медицинская страховка, покрывающая не менее &euro; 30 тыс. со сроком действия, соответствующим продолжительности визы. В ней предусматривается возвращение туриста домой по медицинским причинам, срочное лечение, перевозка тела в случае летального исхода.&nbsp;&nbsp;</p>

<p>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Фотография.</p>

<p>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Документы, содержащие информацию о цели поездки и бронировании места проживания.</p>

<p>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Подтверждение платежеспособности. В качестве такового принимается либо распечатка из банка о состоянии счета заявителя, либо справка, подтверждающая финансовую состоятельность третьего лица, выступающего гарантом.</p>

<p>От заявителя требуется подтверждение намерения вернуться в Россию. В этом случае речь идет об обратном билете, подтверждении владения недвижимостью, документе, характеризующий семейное положение.</p>

<h3>Срок оформления визы</h3>

<p>Заявка рассматривается на протяжении десяти дней. Если заявитель обращался через ВЦ, то динамику рассмотрения прошения есть возможность отследить, зайдя на сайт VFS.Global. Для этого достаточно ввести регистрационный номер ходатайства и сведения с паспорта. Более подробная информация по визе доступна <a href="https://www.vfsglobal.com/croatia/russia/">здесь.</a></p>

<h2>Как добраться до Хорватии</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708463_9432.jpg" /></p>

<p>Несколько раз на протяжении одной недели компанией Аэрофлот выполняются рейсы из Москвы в Загреб. Стоимость билета 12468 руб. Если же лететь самолетом Brussels Airlines, хватит 4&nbsp;753 рублей, правда, придется сделать одну пересадку. С пересадкой эту услугу предоставляет компания Air Serbia за 6&nbsp;241 рубля.</p>

<p>В туристический сезон организуются чартерные рейсы в Дубровники, Сплит, Пулу. Подробную информацию, привязанную к конкретным датам найдете на этом <a href="https://avia.yandex.ru/search/result/?serp_uuid=&amp;passengers=%7B%27infants%27%3A+0%2C+%27adults%27%3A+1%2C+%27children%27%3A+0%7D&amp;utm_content=title&amp;utm_source=wizard_ru&amp;toId=c10507&amp;fromId=c213&amp;lang=ru&amp;utm_campaign=city&amp;from=aviawizard_pp&amp;fromName=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&amp;when=2020-03-29&amp;infant_seats=0&amp;wizardReqId=1582208184117313-1102544907764083244900067-vla1-0242&amp;utm_medium=pp&amp;adult_seats=1&amp;toName=%D0%97%D0%B0%D0%B3%D1%80%D0%B5%D0%B1&amp;children_seats=0&amp;return_date=#empty">сайте.</a> Из Загреба на самолетах Хорватских авиалиний можно за &euro; 59 долететь до Дубровника. Перелет в Сплит или Пулу обойдется в &euro; 40. &nbsp;</p>

<p>Конечная остановка поезда из Москвы в Сплите. Дорога до Загреба занимает чуть больше двух суток, а еще через 10 часов поезд прибывает на конечную станцию. Проезд в одну сторону, в зависимости от класса вагона стоит от &euro; 87 до &euro; 111. Маршрут пролегает через территорию Австрии, а потому надо запастись транзитной визой.</p>

<p>Чтобы добираться до Загреба на автомобиле, придется проехать через Венгрию, Польшу, Украину, а значит потребуется оформление транзитной визы. Если имеется шенгенская многократная виза, то беспокоится не о чем. А если нет, тогда надо идти в ВЦ каждой страны для получения необходимого документа. Общая протяженность пути, если ехать по трассе, &mdash; 2283 км. Для преодоления такого расстояния понадобится около двух дней.</p>

<h2>Валюта</h2>

<p>Хорватия &mdash; член Евросоюза, тем не менее на 95% оплата услуг и товар принимается исключительно в национальной валюте. Называется она куны (Kn). По курсу на февраль 2020 года за &euro;1 дают 7.47 Kn. Европейская валюта принимается в качестве оплаты за передвижение по автобанам (от &euro;1 до &euro;8) и иногда ее принимают в качестве оплаты за номер в отеле. Там, где имеется кассовый аппарат принимают только хорватскую валюту.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708491_3744.jpg" /></p>

<p>&nbsp;</p>

<p>Обменять валюту можно как в банках, так и в обменных пунктах, большое количество которых расположено в городах и на побережье. Комиссионные сборы колеблются от одного до полутора процентов. Для того, чтобы банк произвел обратный обмен придется предъявлять квитанции, полученные при обмене на национальную валюту.</p>

<p>На заправках, в супермаркетах, отелях и ресторанах без проблем принимаются для расчета валютные карты American Express, MasterCard, Visa. Выезжая за границу не забудьте уведомить об этом банк, в котором они открыты, иначе возможна блокировка карты. Если на таможне предъявить товар и чек, подтверждающий факт его приобретения на территории страны, то предоставляется возможность возврата НДС. Это положение действует, если на покупку потрачено не менее 500 HRK.</p>

<h2>Условия проживания</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708519_2277.jpg" /></p>

<p>В Хорватии не возникает проблем с выбором места для устройства на ночлег:</p>

<ul>
	<li>Ночь в бюджетном гестхаусе (гостевом доме) обойдется в &euro;20. При этом ночевать придется в общем номере.</li>
	<li>За место в апарт-отеле запросят от &euro;40 до &euro;80. Как правило, в эту суму входит завтрак, а обед и ужин без проблем готовится на общей кухне.</li>
	<li>Номер в трехзвездочной гостинице стоит &euro;110.</li>
	<li>Апартаменты в отеле 4* и 5* оцениваются в &euro;220-&euro;300 (на двоих).</li>
</ul>

<p>Все, или почти все <a href="https://www.booking.com/searchresults.ru.html?aid=357027&amp;label=yan104jc-1DCAIoZTgeSCFYA2juAYgBAZgBIbgBF8gBD9gBA-gBAfgBAogCAagCA7gCk-vT8gXAAgE&amp;sid=64b6316a314784b14169e7eef54aa504&amp;sb=1&amp;sb_lp=1&amp;src=country&amp;src_elem=sb&amp;error_url=https%3A%2F%2Fwww.booking.com%2Fcountry%2Fhr.ru.html%3Faid%3D357027%3Blabel%3Dyan104jc-1DCAIoZTgeSCFYA2juAYgBAZgBIbgBF8gBD9gBA-gBAfgBAogCAagCA7gCk-vT8gXAAgE%3Bsid%3D64b6316a314784b14169e7eef54aa504%3Binac%3D0%26%3B&amp;sr_autoscroll=1&amp;ss=%D0%97%D0%B0%D0%B3%D1%80%D0%B5%D0%B1%2C+%D0%97%D0%B0%D0%B3%D1%80%D0%B5%D0%B1%D1%81%D0%BA%D0%B0%D1%8F+%D0%B6%D1%83%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F%2C+%D0%A5%D0%BE%D1%80%D0%B2%D0%B0%D1%82%D0%B8%D1%8F&amp;is_ski_area=0&amp;checkin_year=2020&amp;checkin_month=2&amp;checkin_monthday=25&amp;checkout_year=2020&amp;checkout_month=2&amp;checkout_monthday=26&amp;group_adults=1&amp;group_children=0&amp;no_rooms=1&amp;b_h4u_keep_filters=&amp;from_sf=1&amp;ss_raw=%D0%97%D0%B0%D0%B3%D1%80%D0%B5%D0%B1&amp;ac_position=0&amp;ac_langcode=ru&amp;ac_click_type=b&amp;dest_id=-101579&amp;dest_type=city&amp;iata=ZAG&amp;place_id_lat=45.813152&amp;place_id_lon=15.977308&amp;search_pageview_id=67fb490999ef006e&amp;search_selected=true">отели</a>, места в которых бронируются места россиянам после реставрации. Стоимость проживания в них несколько ниже чем аналогичных среднеевропейских заведений. Самые высокие цены в Дубровнике и Загребе. Скромные хорватские &laquo;трешки&raquo;, предлагающие небольшие и простенькие номера, конечно не сравнить с подобными апартаментами в Турции, но и в них есть своя прелесть.</p>

<h2>Пляжи Хорватии</h2>

<p>Это заслуженная гордость станы. <a href="https://guruturizma.ru/plyazhi-xorvatii/">Пляжи</a> в большом количестве имеются как на материке, так и на островах. Разнообразие их таково, что вариант по вкусу найдет любой, даже самый привередливый, отдыхающий. Здесь есть где отдохнуть любителям песчаных покрытий, но и для тех, кому по вкусу галечное покрытие найдется большое количество предложений. Галька на них имеет очень красивый молочно-жемчужный цвет.</p>

<p>Мы подобрали самые привлекательные, на наш взгляд, пляжи из великого множества предложений, и предлагаем вам познакомиться с ними.</p>

<h3>Вела Плаза</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708613_6491.jpg" /></p>

<p>Протяженность этого самого большого на острове Крк пляжа 1800 м. Расположен он в черте города Башка и я валяется местом сосредоточения кафе, отелей, ресторанов, баров, пансионатов. Местами он покрыт галькой, а местами &mdash; песком, а у воды небесно-голубой оттенок. С 1999 года над пляжем развивается голубой флаг, как знак его безупречной чистоты.</p>

<p>Вдоволь накупавшись и устав загорать, отдыхающие иногда отправляются на прогулки, для которых оборудованы специальные туристические тропы. В непосредственно близости от пляжа расположилось несколько интересных с исторической точки зрения достопримечательностей.&nbsp; Желающим предоставляется возможность совершить морскую прогулку на одной из яхт, в большом количестве стоящих на якоре недалеко от берега.</p>

<h3>Златни-Рат</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708645_8077.jpg" /></p>

<p>Zlatni Rat&nbsp;по праву называют визитной карточкой Хорватии, привлекающей ежегодно миллионы туристов. Это пляж-трансформер, так как его форма постоянно изменяется. Происходит это из-за того, что его часть, напоминающая язык, уходит в море на 600 м. Ее вид непрерывно меняется под воздействием волн и ветра. Причем изменения порой бывают значительными и многие стремятся увидеть это чудо природы.</p>

<p>И над этим пляжем, расположенным на острове Бранч, развивается Голубой флаг. Он весь покрыт мелкой галькой, а вокруг раскинулся сосновый бор. За вход платить не надо, чего нельзя сказать об аренде лежаков и зонтиков. Отдыхающим предлагается большое количество водных развлечений, а для детей имеется специальная площадка. Каждому желающему доступно прохождение курса в школе дайвинга и виндсерфинга.</p>

<h3>Райский пляж</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708666_9913.jpg" /></p>

<p>Расположился этот пляж в северно-восточной части острова Раб, очень популярного у туристов. У моря здесь изменяются оттенки с нежно-голубого до темно синего. По берегу, расположенному в виде полумесяца, приятно ходить, так ка он покрыт очень светлым и мягким песком.&nbsp; Протяженность пляжа &mdash; порядка полутора километров, что позволяет всегда найти свободное место для отдыха.</p>

<p>Всем желающим предлагаются зонтики и шезлонги, однако многим отдыхающим больше нравится лежать прямо на песке, или расстеленном на нем полотенце. Это любимое место отдыха для семей с детьми. Предпочитающим активный отдых, предлагается размяться на теннисных кортах, футбольном и гандбольном полях.</p>

<p>Чтобы восстановить силы, отдыхающие отправляются посидеть в одном из многочисленных баров и кафе. А если задержаться в них до вечера, то есть возможность побывать на дискотеке, получая удовольствие и от танца, и от вида моря.</p>

<h3>Пляж Дубовица</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708691_5528.jpg" /></p>

<p>Место, где расположен этот красивый пляж &mdash; остров Хвар, пользующейся популярностью у туристов. Галечный берег спрятался в лагуне с изумрудно-зеленой водой. Это место идеально подходит для семейного отдыха и занятия дайвингом. Тень здесь имеется не только под зонтиками, но и под кроной деревьев. Горы, окружившие лагуну, защищают ее от ветров, что позволяет воде отлично прогреваться.&nbsp;&nbsp; &nbsp;</p>

<h3>Пунта Рата</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596708715_5836.jpg" /></p>

<p>Этот пляж находится рядом с курортом Брела, и входит в число наилучших по Хорватии. Его протяженность около шести километров. По мелкой галке приятно ходить, а воздух пропитан ароматами можжевелового и соснового леса, дополненного миртовыми зарослями. В тени деревьев отдыхающие также укрываются от солнца. Глубина воды у берега, который полого уходит в море, не велика.</p>

<p>Чистота воды здесь удивительная и из нее, как древнее морское чудовище величественно выступает скала Брела. &nbsp;Для отдыхающих здесь созданы все условия: шезлонги, зонтики, водные аттракционы, детские площадки, прокат спортивного снаряжения. В непосредственной близости расположилось несколько кафе, где помогут утолить нагулянный аппетит.</p>

<h3>Нудистские пляжи</h3>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/49/1596709446_2938.jpg" /></p>

<p>&nbsp;</p>

<p>Подобных мест для отдыха раскрепощенных людей в Хорватии много. К их названию добавляется аббревиатура FFK (Frei K&ouml;rper Kultur, в переводе с немецкого - культура свободного тела).</p>

<p>&nbsp;</p>

<p>Чаще всего это отдельная зона с платным входом, расположенная внутри общепринятого пляжа. Хорватию не зря называют всемирной столицей нудизма, ведь именно на ее территории расположено больше всего нудистских курортов самого высокого уровня. Самыми известными и красивыми пляжами для нудистов считаются следующие:</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nugal &mdash; уютное место, расположенное между Макарска и Тучели.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Koversada &mdash; очень большой курорт для нудистов, расположившийся на Врсаре. На территории в 120 га имеется уютный пляж, множество развлечений и рестораны, в которые посетители допускаются даже в наряде НЮ.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Valalta &mdash; сказочный городок на курорте Ровинь.</p>

<p>Основная масса подобных пляжей предназначена для семейного отдыха у воды. Некоторые семьи уже не первым поколением приезжают сюда отдыхать. И если раньше родители привозили детей, то теперь бабушки и дедушки привозят внуков.</p>

<p>Ответить однозначно на каком из хорватских пляжей или курорте отдыхать лучше всего невозможно. У каждого из них имеются присущие только ему особенности, набор преимуществ и недостатков. Право выбора за вами: мы только постарались вам хоть немного помочь в этом.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (59, 'Пандемия и отдых в Турции', 1, NULL, NULL, NULL, 'pandemiya-i-otdyh-v-turcii', '2020-12-22', '2021-01-03', 'Турция по праву занимает верхние строки в списке стран, выбираемых российскими туристами для отдыха. Сказалась ли на туристский индустрии этой страны пандемия и если да, то каким образом? Ответ вы найдете в нашей статье.', true, 10, 0, NULL, NULL, NULL, '<p>Начнем, пожалуй, с разъяснения посольства РФ в Турции касающегося ограничений для туристов, приехавших на отдых в Турцию. В нем говорится, что дипломаты послали официальный запрос в МИД Турции по поводу распространения на иностранцев введенного в стране режима ограничений.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608647836_9520.jpg" /></p>

<p>&nbsp;</p>

<p>В полученном ответе сказано, что на иностранцев, въехавших в Турцию с туристическими целями на небольшой промежуток времени, ограничения не распространяются. Чтобы без ограничений приобретать туры в туристических агентствах, пользоваться услугами по трансферу и размещению, перемещаться на общественном транспорте достаточно предъявить документ с подтверждением туристического статуса.</p>

<h2>Какие ограничения введены в стране</h2>

<p>В настоящее время не территории Турции по выходным с 20.00 до 10.00 введен комендантский час. Не работают кинотеатры, кафе, спортивные площадки и рестораны.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608647862_7022.jpg" /></p>

<p>&nbsp;</p>

<p>Торговые центры работают с временными ограничениями, которые совсем не коснулись пятизвездочных отелей &laquo;все включено&raquo; если присутствует у них сертификата &laquo;здорового туриста&raquo;. Для его получения необходимо, чтобы 150 различных критериев находились в соответствии с санитарно-гигиеническими нормами. Но даже в отелях такого высокого уровня запрещено проведение любых развлекательных новогодних программ, а также рекламировать праздничные мероприятия.</p>

<p>Нет возможности посещать СПА-салоны, бассейны и хамам при отелях, так как они также закрыты.</p>

<h2>Памятка от российского посольства в Турции для потенциальных туристов</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608647887_1292.jpg" /></p>

<p>&nbsp;</p>

<p>На странице Facebook, принадлежащей российской дипмиссии опубликована памятка для туристов, отправляющихся на отдых в Турцию. В ней содержится перечень мер в плане санитарно-эпидемиологических мероприятий, которым обязаны следовать иностранные граждане, въезжающие в страну.</p>

<p>Выглядит он следующим образом:</p>

<ul>
	<li>Еще находящемуся в полете пассажиру вменяется в обязанность заполнение информационной карты с указанием персональные данные, номер места, по какому адресу планируется пребывание в Турции, контакты. Кроме того, каждый турист обязан ознакомиться с информацией о COVID-19.</li>
	<li>До прохождения паспортного контроля у каждого прилетевшего измеряется температура. При появлении малейшего подозрения на наличие короновируса потенциальный больной помещается на изоляцию в карантин, с последующей отправкой в госпиталь для обследования.</li>
	<li>Если диагноз подтверждается, то выявляются все попутчики, располагавшиеся на двух креслах впереди и сзади, а также по бокам. Всех их помещают на четырнадцати дневный карантин.</li>
	<li>В турецких аэропортах не предусмотрено обязательное проведение тестирования для выявления наличия короновируса. Необязательно и наличие справки о прохождении теста в стране убытия. Однако всем желающим предоставляется возможность сдать анализы в специально для этой цели открытых центрах, которые создали в каждом из крупных туристических аэропортов. За это придется заплатить 15 евро.</li>
</ul>

<p>Российским Посольством рекомендовано всем въезжающим на отдых в Турцию обзавестись медицинскими страховками. В свою очередь, турецкие власти оповестили о разработке линейки социальных страховок, с включенным в них медицинским обслуживанием в случае выявления COVID-19. Для их приобретения достаточно обратиться к туроператорам, реализаторам авиабилетов, а также в аэропортах.</p>

<h2>Мнение россиян, отдыхавших в Турции в период карантина</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608647951_7956.jpg" /></p>

<p>&nbsp;</p>

<p>Подавляющее большинство россиян, посетивших страну после начала пандемии остались недовольны отдыхом в условиях введенных там ограничений и выразили свои впечатления одной фразой: &laquo;если бы знали, как все получится &mdash; не поехали&raquo;. И для этого имелось несколько причин.</p>

<p>Во-первых, льготы распространяются только на так называемых пакетных туристов. А если желающие пережить пандемию на берегу моря оформили туристический ВНЖ, то на них распространялся весь пакет ограничений, как и на местных жителей.</p>

<p>Во-вторых, даже имея разрешение на прогулки в любое время, потребность в них отпадала сама собой. Кому захочется бродить по пустынным улицам и заглядывать в витрины закрытых магазинов и ресторанов?</p>

<p>В-третьих, основная масса отелей в настоящее время закрыта, так как зима является временем самостоятельных отдыхающих, а не пакетных туристов.</p>

<p>Практически все россияне высказали сожаление по поводу принятого им решения провести карантинную зиму, отдыхая в Турции, так как им оказались недоступны развлечения, типичные для этого времени года, тогда как в России их &laquo;хоть пруд пруди&raquo;.</p>

<p>Большую часть времени приходится проводить дома, оплачивая выросшие счета за отопление, которое теперь приходится включать по субботам и воскресеньям. Раньше в этом не было необходимости, так как в эти дни никто не сидел дома практически круглосуточно.</p>

<p>Главная причина недовольства россиян &mdash; запрет на проведение шумного и веселого праздника в честь прихода Нового года.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608647974_9511.jpg" /></p>

<p>&nbsp;</p>

<p>Справедливости ради надо сказать что есть люди, которым такой отдых все-таки понравился. Как правило, это любители уединенного отдыха. Так одна россиянка, прожив 3 дня в столице и погуляв по ее пустынным улицам, уехала в Анталию, где поселилась в отеле с термальными ваннами и открытым бассейном. В номер приносили заказанную еду, по расписанию проводились процедуры, и разрешалось посещение бассейна.</p>

<p>Подумайте, подходит ли вам такой отдых, или лучше выбрать вариант поближе, повеселей и подешевле? Каждый сам вправе выбрать тот вариант, который его устроит больше всего. Хорошего вам отдыха и помните слова классика про то, что ничто не вечно под Луной. А значит и пандемия рано или поздно останется в прошлом и, как говорится, в Библии: &laquo;все вернется на круги своя&raquo;.</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (58, 'Отдых в Египте с учетом карантинных мер против COVID-19', 1, NULL, NULL, NULL, 'otdyh-v-egipte-s-uchetom-karantinnyh-mer-protiv-covid-19', '2020-12-22', '2021-01-03', 'Насколько сегодня безопасен отдых в Египте? Является ли обязательным наличие отрицательного теста у въезжающих в страну? Существуют ли ограничения при нахождении в отелях, местах общего пользования, а также транспорте в рамках защиты от КОВИД? Разрешено ли отдыхающим передвижение по стране? Ответы на все эти вопросы вы найдете в нашей статье.', true, 10, 0, NULL, NULL, NULL, '<p>Приход второй волны эпидемии коронавируса &mdash; реальность для Египта, правда, она не на столько велика, как первая. На середину декабря на миллион граждан приходилось не более 4,7 инфицированных, тогда как в России на тот период показатель был 192,3/миллион.</p>

<p>&nbsp;</p>

<p>Египетским Министерством здравоохранения объявлено о признании безопасной и эффективной китайской вакцины от Sinopharm. Процесс вакцинации в стране уже начат, правда, темп его пока невысок. В Египет поступила, на безвозмездной основе, партия вакцины из Арабских эмиратов.</p>

<h2>Особенности эпидемиологической ситуации в Египте</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608616307_3489.jpg" /></p>

<p>&nbsp;</p>

<p>Истинное положение не известно никому по той простой причине, что египетскими медиками проводится 1,3 теста на одну тысячу граждан, а статистика как таковая не ведется. Не исключена возможность того, что египтянам уже удалось выработать коллективную форму иммунитета и ехать в эту страну можно не опасаясь заболеть коронавирусом. Но возможен и другой вариант: эпидемия там достигла своего пика и отдыхающий перекочует из номера отеля в больничную палату.</p>

<p>Однозначного ответа на этот вопрос не существует. Но если судить по снятию некоторых ограничений, то вполне логично предположить, что в стране проходит плавный переход от второго к первому.</p>

<p>Еще одним положительным аргументом в пользу поездки говорит тот факт, что отдыхающие с Украины и Белоруссии для поездок на египетские курорты пользуются прямыми авиарейсами. Таким же образом они возвращаются отдохнувшими и никаких всплесков заболеваемости при этом не отмечается ни в одной из названных стран. Это позволяет сделать вывод о нормальности эпидемиологической ситуации в Египте.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608616327_7307.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>За время пандемии только 19 подданных других государств умерло от пандемии в этой стране. И данная цифра, без сомнения, является достоверной, так как любые происшествия с иностранцами фиксируются обязательно. При этом количество зараженных не превысило ста семидесяти двух. Все это позволяет назвать отдых в Египте вполне безопасным.</p>

<p>Египтяне официально входят в число наиболее молодых мировых наций, ведь средний возраст египтян 25 лет. А для молодых людей свойственно не замечать вирусных заболеваний.</p>

<h2>Требования к въезжающим в Египет</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608616344_4956.jpg" /></p>

<p>&nbsp;</p>

<p>Для любого прибывающего на отдых в Египет обязательное требование &mdash; наличие ПЦР-теста с отрицательным результатом на КОВИД-19. При этом с момента его получения не должно пройти более семидесяти двух часов (трех суток) до прохождения регистрации на рейс. Для тех, кто прибывает на курорты, существуют особенности, о которых мы расскажем несколько позже.</p>

<p>Перечень правил выглядит следующим образом:</p>

<ul>
	<li>Отправными точками при расчете давности проведения теста берутся даты получения образца и вылета.</li>
</ul>

<p><strong>В порядке полезной информации!</strong> Когда полетом предусмотрена пересадка, тогда в расчет принимается именно дата вылета непосредственно в Египет. Так, например, если смена рейса происходит в Стамбуле, то учитывается день отлета из турецкой столицы.</p>

<ul>
	<li>Дети, которым не исполнилось 6 лет, освобождаются от тестирования.</li>
	<li>Действительным считается только распечатанный результат, на котором присутствует печать проводившей исследования лаборатории. В бумажном документе не допускается наличие каких бы то ни было исправлений или помарок. Не принимаются результаты в электронном виде и присланные посредством SMS-сообщения.</li>
	<li>&nbsp;Для написания текста допустимо использование английского либо арабского языка. Русскоязычный вариант даже не рассматривается.</li>
	<li>Обязательно точное указание даты и времени забора биологического материала и его типа. Принимается только ПЦР.</li>
</ul>

<p>Насколько строго следуют работники египетской миграционной службы этим правилам? Как утверждают люди, недавно побывавшие на отдыхе в Египте, фактически все не настолько строго. На англоязычных форумах, посвященных этому вопросу, даже имеются утверждения, что принимаются и электронные варианты, и слегка просроченные.</p>

<p><strong>Наши рекомендации!</strong> Не стоит уповать на авось. Ведь в любой момент последует грозный окрик из вышестоящих инстанций и именно ваш документ проверят досконально. Лучше сделать все как положено, чтобы не зависеть от настроения власть имущих.</p>

<h2>Особенности для желающих отдохнуть на курортах</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608616568_5295.jpg" /></p>

<p>&nbsp;</p>

<p>Если едущие на курорт прибывают прямым авиарейсом в воздушную гавань Шарм-эль-Шейха, Хургады, Табы, Марса Алам, то для них не обязательна предварительная сдача теста, так как они могут это сделать, не покидая здания аэропорта, заплатив $30. Сдавший тест турист находится на самоизоляции в собственном номере отеля. Это правило начало действовать с 1.09.20 г., а до этого тест на этих курортах не требовался.</p>

<p>Есть ли смысл в сдаче теста после приземления? Вряд ли. Дело в том, что для доставки результатов используются СМС или посыльные отеля. Назвать эти способы совершено надежными сложно. Во-первых, текстовое сообщение может просто не дойти до получателя, если он пользуется роумингом. Во-вторых, сотрудник отеля легко способен забыть о поручении доставить результат испытуемому. К тому же прохождение теста в России обойдется не так дорого.</p>

<p>Правда, по утверждению утверждающих, размещенных на соответствующих форумах, строгая самоизоляция соблюдается далеко не во всех отелях. В таком случае отдыхающий получает возможность купаться и загорать, не дожидаясь результатов тестирования.</p>

<h2>Требования к транзитным пассажирам</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608616593_3514.jpg" /></p>

<p>&nbsp;</p>

<p>С тех из транзитных пассажиров, которые не покидают территории аэропорта в ожидании очередного рейса, не требуют предъявления результатов теста. Если же между рейсами имеется довольно продолжительный отрезок времени и у туриста появилось желание посмотреть местные достопримечательности, то на него распространяются общие требования.</p>

<h2>Каковы требования по ношению защитных масок и соблюдению дистанции</h2>

<p>В Египте с 30.05.20 г. ношение масок правительство сделало обязательным для желающих посетить офис частной компании или государственного учреждения, а также банков и магазинов. Кроме того, их наличие является обязательным условием при пользовании общественным транспортом. Требования по соблюдению социальной дистанции, как и во всем мире, не менее одного метра.</p>

<p>Фактически соблюдение всех перечисленных требований стало личным делом каждого гражданина, поэтому маски носят только те, кто верят в их защитные свойства, а неверующие спокойно обходятся без них. Последних не пугает даже штраф, установленные в размере 4 000 египетских фунтов, потому что правоохранительные органы прибегают к нему только в исключительном случае.</p>

<h2>Правила, установленные в отелях</h2>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608616619_8443.jpg" /></p>

<p>&nbsp;</p>

<p>Неукоснительного ношения защитных масок для постояльцев отелей не существует, за исключением случаев, когда проводятся мероприятия с большим скоплением народа. Для обслуживающего персонала ношение масок обязательно. В ресторанах, с учетом сегодняшних реалий, отказались от &laquo;шведских столов&raquo; в привычной их форме. Блюда находятся за стеклянными витринами, и персонал накладывает в тарелку посетителя то блюдо, которое он хотел бы попробовать.</p>

<h2>Порядок передвижения по стране</h2>

<h2>&nbsp;</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608616642_8584.jpg" /></p>

<p>&nbsp;</p>

<p>Любой пассажир прибывший в аэрогавани Александрии либо Каира и предъявивший в них документ с подтверждением получения отрицательного результата по результатам тестирования на COVID-19 получает возможность беспрепятственно передвигаться по территории Египта. Так никому не возбраняется доехать из этих городов до Хургаду, а также Шарм-эль-Шейха на междугородном автобусе.</p>

<h2>Немного полезной информации</h2>

<p>За визу для поездки на отдых в Египет не придется платить до 30.04.21 г, но это вовсе не означает, что отпадает необходимость в заполнении бланка миграционной карты. Если раньше посещающим Синай ставили одноименный штамп, то теперь ставится общепринятая виза. Познакомиться с достопримечательностями сегодня можно вдвое дешевле, чем раньше, благодаря временному снижению цен. Но на аквапарки и парки развлечения это не распространяется.</p>

');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (67, 'Amirsoy Resort: горнолыжный курорт мирового уровня в Узбекистане', 1, NULL, NULL, NULL, 'amirsoy-resort-gornolyzhnyi-kurort-mirovogo-urovnya-v-uzbekistane', '2021-01-06', '2021-01-14', 'Сегодня для того, чтобы насладиться полноценным зимним отдыхом на мировом уровне необязательно ехать за тридевять земель, ведь в непосредственной близости от Ташкента открылся горнолыжный курорт, обслуживающий посетителей в соответствии с мировыми стандартами. Называется он Amirsoy Resort.', true, 2, 0, NULL, NULL, NULL, '<p>В 65 км от Ташкента расположился Чаткальский хребет, на отрогах которого расположился Amirsoy Resort, занимающий более девятисот гектаров очень красивой и живописной местности.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614460_4750.jpg" /></p>

<p>Расширение и благоустройство курорта еще продолжается, но уже сегодня готовы принять отдыхающих 38 шале, выполненных в стиле, принятом в Альпах.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614498_6998.jpg" /></p>

<p>К услугам отдыхающих SPA-комплекс, медиа-центр, зоны для проведения пикников, многофункциональный современный центр обслуживания, а также подъемники ленточного, гандбольного и кресельного типа. Желающим предоставляется возможность проехать по окрестностям на снегоходе и квадроцикле. Пополнить запас сил после активного отдыха можно в ресторанах, предлагающих европейскую или национальную кухни.</p>

<h2>Как добраться до Amirsoy Resort</h2>

<p>В Ташкент из Москвы осуществляется от трех до пяти рейсов авиакомпаниями, ЮТэйр, Аэрофлот. Из Санкт-Петербурга один раз в день летают самолеты компаний Россия и Узбекистон Хаво Йуллари. Кроме того, имеются периодические прямые рейсы из Уфы, Сочи, Ростова на Дону, Минеральных вод, Краснодара, Казани, Воронежа, Тюмени, Самары, Новосибирска, Красноярска, Калининграда, Екатеринбурга. Информацию о стоимости билетов можно получить <a href="https://www.aviasales.uz/cities/tashkent-tas?from=ru">здесь</a>.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614552_3620.jpg" /></p>

<p>Дешевле, но дольше ехать на поезде, однако для тех, кто не любит летать на самолете &mdash; это оптимальная альтернатива. Прямые маршруты имеются из Москвы, Волгограда, Челябинска, Екатеринбурга, Саратова, Оренбурга, Новосибирска, Уфы. Стоимость билетов в купе изменяется от 7 529 рублей до 17 730 рублей в зависимости от протяженности маршрута и категории поезда. Для получения подробных сведений по этому вопросу достаточно перейти на <a href="https://жд-онлайн.рф/zhd-bilety-na-poezd/tashkent/#/">сайт</a>.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614597_4163.jpg" /></p>

<p>Если от Ташкента до курорта ехать на автомобиле, то надо выехать по направлению в Газалкент. Следуя указателю свернуть в сторону Гальвасая, а затем на Чимган и Бельдерсай.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614638_5608.jpg" /></p>

<p>Еще вариант: доехать на такси до станции метро Buyuk ipak yo&#39;li, там пересесть на маршрутное такси до Газалкента, а затем вновь взять такси. Но можно все сделать и значительно проще: позвонить по телефону +998 (95) 177-55-99 и договорится об плате за трансфер по маршруту Ташкент-курорт Amirsoy Resort.</p>

<h2>Горнолыжные трассы</h2>

<p>План лыжных трасс разработан специалистами из испанских канадских компаний, которые учли особенности рельефа и требования международных стандартов. При перепаде высот в 660 м, протяженность всех трасс равна пятнадцати километрам. Благодаря использованию системы искусственного создания снега, сезон катания продолжается от ста до ста двадцати дней в году, что зависит от того, на какой высоте пролегает трасса.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614682_5119.jpg" /></p>

<p>Alfa, как следует из названия, является самой первой трассой курорта с перепадом высот равным четырехсот семи метрам и протяженностью 1 923 м. 2 869 м Bravo проложено среди зарослей арчи, растущей на скалистом и очень красивом рельефе и эта трасса не каждому по плечу. Charlie, проходящая по хребту, растянулась на 3 353 метра при разнице высот между стартом и финишем в 608 м.</p>

<p>Papa на начальном этапе была единственной технической дорогой, начинающейся на вершине Amirsoy, которая теперь стала самой протяженной трассой курорта (3 550 м). Tango отличается небольшой протяженностью (860 м) и рельефностью, заставляющей двигаться очень энергично.</p>

<p>Зоны из серии Oscar подойдут любителям селфи, на котором отобразятся счастливые лица на фоне основных объектов курорта и великолепных закатов. Их характеристики выглядят следующим образом:</p>

<table cellspacing="0" style="border-collapse:collapse; width:623px">
	<tbody>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:1px solid black; vertical-align:top; width:236px">
			<p><strong>Наименование трассы</strong></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; vertical-align:top; width:198px">
			<p><strong>Перепад высот (м)</strong></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; vertical-align:top; width:189px">
			<p><strong>Протяженность (м)</strong></p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:236px">
			<p>Oscar</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:198px">
			<p>42,0</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:189px">
			<p>913,0</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:236px">
			<p>Oscar Lane A</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; width:198px">
			<p>48,0</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; width:189px">
			<p>1527,0</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:236px">
			<p>Oscar Lane B</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; width:198px">
			<p>42,0</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; width:189px">
			<p>194,0</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:236px">
			<p>Oscar Lane С</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; width:198px">
			<p>48,0</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; width:189px">
			<p>279,0</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:236px">
			<p>Oscar Lane D</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; width:198px">
			<p>46,0</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; width:189px">
			<p>227,0</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Начинающие лыжники и сноубордисты обучаются на учебном склоне под названием Beginners. Благодаря наличию опытных инструкторов у каждого отдыхающего имеется возможность быстро освоить премудрости горнолыжных спусков.</p>

<h2>Скипасс</h2>

<p>Без этого элемента сложно представить современный горнолыжный курорт. Произошло это слово от английских ski &mdash; лыжи и pass &mdash; пропуск. В результате получился электронный, пластиковый или бумажный билет, позволяющий его предъявителю пользоваться подъемником. На Amirsoy Resort используются пластиковые карты, имеющие магнитный чип, который требуется приложить к турникету, установленному на каждом подъемнике.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614752_3839.jpg" /></p>

<p>На карточке содержится информация о том, на протяжении какого времени действует абонемент, за какое количество поездок уплачено, а также данные владельца. Передавать ски пасс построенному человеку запрещено. Чтобы этого не допустить, на турникетах установлена система фотофиксации. За выявленные нарушения происходит блокировка билета. Турникеты работают с 9:00 до 16:30.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614794_4598.jpg" /></p>

<h2>Прогулочные билеты</h2>

<p>Это бумажный документ одноразового пользования со штрихкодом, дающий право совершить прогулочную поездку на гондольном подъемнике. Соответственно его предъявитель едет без снаряжения для спуска с горы. До вершины отдыхающих доставляют за 10 минут. Там, на высоте 2290 м над уровнем моря, имеется возможность подышать чистым воздухом и насладится великолепными видами. Желающим предлагают подкрепиться снеками и напитками.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/67/1610614883_1402.jpg" /></p>

<h2>Канатные дороги</h2>

<p>Лыжников обслуживают подъемники компаний SunKid и Doppelmayr - признанных авторитетов среди компаний, производящих подобную продукцию. Из семи, работающих в настоящее время подъемников, 5 траволаторов (ленточные), по одному кресельному и гондольному.</p>

<p>Ски пассы и прогулочные билеты продаются не позже чем за 30 минут до закрытия транспортеров. Причем время прекращения работы может изменяться в зависимости от погодных условий. При этом администрация не обязуется делать предварительные предупреждения.</p>

<table cellspacing="0" style="border-collapse:collapse">
	<tbody>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:1px solid black; vertical-align:top; width:130px">
			<p><strong>Категория подъемника</strong></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; vertical-align:top; width:143px">
			<p><strong>Протяженность маршрута (м)</strong></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; vertical-align:top; width:134px">
			<p><strong>Количество человек, перевозимых в течение одного часа</strong></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; vertical-align:top; width:113px">
			<p><strong>Режим работы</strong></p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:130px">
			<p>Гондола Прима</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:143px">
			<p>2 139</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:134px">
			<p>2 400</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:113px">
			<p>9:00-17:00</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:130px">
			<p>Кресло Chalet Express</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:143px">
			<p>686</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:134px">
			<p>2400</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:113px">
			<p>9:00-16:00</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:130px">
			<p>Траволатор Beginners</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:143px">
			<p>120</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:134px">
			<p>1 680</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:113px">
			<p>9:00-16:00</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:130px">
			<p>Тюбинг взрослый I</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:143px">
			<p>90</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:134px">
			<p>1 680</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:113px">
			<p>9:00-16:00</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:130px">
			<p>Тюбинг взрослый II</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:143px">
			<p>85</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:134px">
			<p>1 680</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:113px">
			<p>9:00-16:00</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:130px">
			<p>Тюбинг взрослый III</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:143px">
			<p>65</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:134px">
			<p>1 680</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:113px">
			<p>9:00-16:00</p>
			</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; vertical-align:top; width:130px">
			<p>Тюбинг детский</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:143px">
			<p>35</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:134px">
			<p>1 680</p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; vertical-align:top; width:113px">
			<p>9:00-16:00</p>
			</td>
		</tr>
	</tbody>
</table>

<h2>Рестораны</h2>

<p>Отдыхающих, предпочитающих европейскую кухню, ожидают в ресторане Olive Garden, который работает с 8:00 до 23:00. Средний обед на одного человека обойдется здесь в 1 060 рублей.</p>

<p>9</p>

<p>Для любителей быстрого питания всегда открыт ресторан Amirsoy Bistro, за обед в котором попросят заплатить не более шестисот пятидесяти рублей.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (61, 'Тестовый пост от Игоря 56578', 1, NULL, NULL, NULL, 'testovyi-post-ot-igorya', '2020-12-23', '2021-01-13', 'Тестовый пост от Игоря', false, 1, 0, NULL, NULL, NULL, '<p>Some text и тут тоже какой-то текст 6</p>

<p>Опять текст</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/61/1609357179_9148.jpg" /></p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (62, 'Как повлиял коронавирус на работу горнолыжных курортов', 1, NULL, NULL, NULL, 'kak-povliyal-koronavirus-na-rabotu-gornolyzhnyh-kurortov', '2020-12-24', '2021-01-03', 'Какие особенности следует учитывать желающим покататься по заграничным горным склонам зимним сезоном 20220/2021? Продолжится ли работа российских горнолыжных курортов? Какие правила действуют для отдыхающих? Какие применяются меры для защиты от коронавируса? Ответы на все эти вопросы вы найдете в нашей статье.', true, 10, 0, NULL, NULL, NULL, '<p>В текущем году любителем покататься на горных лыжах стало значительно сложней найти куда поехать. Нас не покидает вера в окончание пандемии раньше или позже, а пока мы расскажем куда доступно поехать российским горнолыжникам вкупе со сноубордистами, и какие там применяются меры для защиты от коронавируса.<strong>&nbsp;</strong></p>

<p>&nbsp;</p>

<h2>Состоится ли сезон в условиях пандемии?</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608826558_1349.jpg" /></p>

<p>&nbsp;</p>

<p>Прошлый сезон преждевременно прервал COVID-19 и произошло это в конце марта. Столь раннее завершение активного отдыха заставило лыжников, и сноубордистов с нетерпением ожидать возможности добраться до горных склонов. На сегодняшних реалиях еще не сложилась полная картинка, но то, что горнолыжному сезону быть &mdash; не вызывает сомнения.</p>

<p>Да, для подсчета доступных заграничных курортов хватит пальцев одной руки, и на российских склонах имеются определенные ограничения, которые могут ужесточиться в любой момент. Но для истинных поклонников снежных склонов нет непреодолимых препятствий на пути к любимым трассам.</p>

<h2>Ситуация на заграничных курортах</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608826577_3587.jpg" /></p>

<p>&nbsp;</p>

<p>Немецкие, Финляндские, Французские, Австрийские, Швейцарские, Итальянские горнолыжные курорты россияне сегодня посещать не могут, так как закрыты границы. Мало того, на некоторых из них не допускаются даже граждане этих стран по причине задержки начала нормального функционирования из-за пандемии.</p>

<p>Удастся ли до того, как завершится горнолыжный сезон побывать на курортах Альпийских гор &mdash; открытый вопрос, ответ на который целиком и полностью зависит от эпидемиологической ситуации в этих странах. Только после открытия границ для свободного передвижения это станет возможным.</p>

<p>Сегодня российских любителей спусков по крутым горным склонам готовы принять в Казахстане, Киргизии, Черногории, Турции. Однако и там существуют ограничения, о которых лучше знать заранее.</p>

<h3>Турция</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608826620_9148.jpg" /></p>

<p>&nbsp;</p>

<p>Россиянам не нужна виза на въезд, если они не собираются находиться в стране более шестидесяти дней. На паспортном контроле не требуют справку о прохождении тестирования на предмет обнаружения коронавируса и нет карантина для прилетающих. На туристов не распространяется действие комендантского часа, обязательного для местных жителей.</p>

<p>На территории Турции порядка десяти курортов для занятия горными лыжами, сезон на которых открыт с последней декады декабря до начала апреля. Основная масса склонов рассчитана на лыжников со средним и начальным уровнем, наверно поэтому здесь никогда нет очередей.</p>

<p>Список турецких горнолыжных курортов выглядит следующим образом:</p>

<ul>
	<li><strong>Эрджиес</strong>. Для прокладки его трасс использованы склоны спящего вулкана, возвышающегося на Анатолийском плоскогорье. Простота и однообразие трасс в полной мере компенсируется видами вокруг.</li>
</ul>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165207_8000.jpg" /></p>

<p>&nbsp;</p>

<ul>
	<li><strong>Паландокен</strong> расположился недалеко от Эрзурума. Здесь найдут для себя склоны по душе спортсмены с различным уровнем подготовки. Имеются фрирайдерские (неподготовленные), а также черные трассы.</li>
</ul>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165224_6090.jpg" /></p>

<p>&nbsp;</p>

<ul>
	<li><strong>Улудаг</strong> &mdash; это название переводится как &laquo;огромная гора&raquo;. От курорта со спокойными трассами, проложенными в лесу, до Бурсы 35 км.</li>
</ul>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165251_7870.jpg" /></p>

<p>&nbsp;</p>

<ul>
	<li><strong>Сарыкамыш</strong> &mdash; идеальное место для лыжников, делающих первые шаги и семейного отдыха с детьми.</li>
</ul>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165271_1173.jpg" /></p>

<p>&nbsp;</p>

<h3>Черногория</h3>

<p>И в эту страну жители России въезжают без виз и отрицательных тестов на COVID-19, но лететь придется с пересадками в сербском Белграде или турецком Стамбуле.</p>

<p>Услуги черногорских горнолыжных курортов стоят меньше аналогов, расположенных на альпийских склонах при том же уровне сервиса. Сезон стартует перед самым Новым годом. Рекомендуемые курорты:</p>

<p>&nbsp;</p>

<ul>
	<li><strong>Колашин&ndash;1450</strong></li>
</ul>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165303_5587.jpg" /></p>

<p>&nbsp;</p>

<ul>
	<li><strong>Колашин&ndash;1600</strong></li>
</ul>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165325_4638.jpg" /></p>

<p>&nbsp;</p>

<h3>Республика Казахстан и Киргизская республика</h3>

<p>Как в Казахстане, так и в воздушной гавани Киргизии у прилетевших россиян требуют ПЦР-тест на COVID-19, сделанный за 72 часа до вылета. Особенность горнолыжных курортов этих стран в том, что они расположены среди высоких гор.</p>

<p>Недалеко от Бешкека имеется комплекс под названием <strong>Чункурчак:</strong></p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165413_3006.jpg" /></p>

<p>&nbsp;</p>

<p>На восточной оконечности озера Иссык-куль легко отыскать лыжную базу <strong>Каракол</strong>:</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165449_4571.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Для строительства курорта Чимбулак в Казахстане был выбран хребет Заилийский Алатау. Обслуживание здесь практически на европейском уровне.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/62/1609165480_4655.jpg" /></p>

<p>&nbsp;</p>

<h2>Российские горнолыжные курорты</h2>

<p>В России нет единых для всех антивирусных мер, так как решение об уровне опасности и необходимым мерам принимается региональным правительством с учетом эпидемиологической обстановки. Для всех курортов имеется несколько общих требований, обязательных для выполнения отдыхающими:</p>

<ul>
	<li>Соблюдение определенной дистанции;</li>
	<li>Ношение маски в местах, где имеет место скопление людей, к которым относятся и канатные дороги. В качестве защиты принимается баф либо балаклава, если ими закрывается не только рот, но и нос.</li>
</ul>

<p>Об открытии очередного сезона объявили в <strong>Архызе</strong>, кабардино-балкарском Приэльбрусье, <strong>Домбае</strong>, где отдыхающие имеют возможность покататься как на старых-обкатанных, так и вновь обустроенных трассах. Удобней всего на здешние курорты ехать с пересадкой в Минеральных водах или Ставрополе. В последнем сейчас все приезжие отправляются на двухнедельный карантин, но если прямо на выходе из аэропорта взять такси напрямую до Эльбруса, то пассажир получает статус транзитного и на него эта мера не распространяется.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608827034_1821.jpg" /></p>

<p>&nbsp;</p>

<p>На Урале без строгих ограничений доступно катание в <strong>Банном</strong> и <strong>Абзаково</strong>. В один номер гостиниц сибирского Шерегеша администрация поселит несколько человек, только если они подтвердят наличие родственной связи. При этом необязательно быть женой и мужем. Если вас смущает этот нюанс, то обращайтесь в частный сектор.</p>

<p>Администрация красноярского <strong>Бобрового лога</strong> обязательно предупредят о необходимости обязательного ношения масок. Передвигаться на канатно-кресельном подъемнике, расчитаном на четырех пассажиров, разрешается только двум лыжникам одновременно. Перекусить в кафе можно только до 20:00, а плотно поужинать в ресторане &mdash; не позднее 23:00.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608827052_6150.jpg" /></p>

<p>&nbsp;</p>

<p>Открылся сезон на курортах, расположенных в Красной поляне, в их числе, конечно же, и <strong>Роза Хутор</strong>. При этом администрация сразу предупреждает всех прибывших об отмене массовых новогодних гуляний, исключение не делают даже для детских утренников. В ночь прихода Нового года обслуживание в ресторанах, как и кафе допускается только до 24:00.</p>

<p>В Мурманской области комплекс <strong>Кукисвумчорр</strong> и курорт<strong> Большой Вудъявр</strong> готовы пока обслуживать исключительно местных жителей. Предположительно гостей из всех остальных регионов начнут принимать только во второй половине января следующего года.</p>

<p>На курортах Подмосковья с полной нагрузкой работаю пушки для создания искусственного снега. По достижении снежным покровом необходимой толщины откроются комплекс <strong>Яхрома</strong>, курорты <strong>Сорочаны</strong> и <strong>Волен</strong>, а также все остальные курорты региона.</p>

');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (68, 'Горнолыжные курорты России ', 1, NULL, NULL, NULL, 'gornolyzhnye-kurorty-rossii-', '2021-01-08', '2021-01-14', 'Многие россияне каждую зиму норовят уехать за границу, чтобы отдохнуть и спустится с гор на лыжах. И это в то время, когда в пределах нашей страны имеются собственные курорты ничем не уступающие, а по отдельным параметрам и превосходящие иностранные аналоги. Не верите? Прочтите нашу статью и убедитесь в нашей правоте.', true, 2, 0, NULL, NULL, NULL, '<p>Специально для российских любителей горных лыж Национальным туристическим союзом составлен рейтинг, включающий в себя 5 лучших горных курортов. Их, конечно же, значительно больше, но формат нашей статьи заставляет нас остановиться только на лидирующей пятерке.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619016_1925.jpg" /></p>

<h2>Горный Воздух</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619047_9102.jpg" /></p>

<p>Горнолыжный комплекс торжественно открыли 25 декабря 1960 года при большом скоплении сахалинцев и мастеров Советско-соэзовского горнолыжного спорта. С тех пор на его трассах проходят подготовку российские горнолыжники, а также регулярно проводятся разнообразные соревнования. Ознаменованием нового этапа развития Горного воздуха стал запуск канатной дороги гондольно-кресельного типа.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619110_7629.jpg" /></p>

<p>В составе современного комплекса 14 трасс, имеющих различные уровни сложности. Их общая протяженность превышает 25 км.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619204_9351.jpg" /></p>

<p>Для выдачи оборудования, необходимого лыжникам и сноубордистам оборудованы пункты проката, которые предлагают услугу бесплатного хранения личных вещей на время катания. Узнать тарифы можно, перейдя по <a href="http://www.championsakh.com/">ссылке</a>. Команда квалифицированных инструкторов готова оказать помощь тем, для кого спуск с горы в диковинку, а опытные спасатели придут на помощь попавшим в беду.</p>

<h2>Шерегеш</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619242_8542.jpg" /></p>

<p>Так называется туристический центр, расположенный на расстоянии четырех километров от поселка с идентичным названием, от которого, в свою очередь, 150 км до Новокузнецка. Но добраться до курорта можно также из Горно-Алтайска, Новосибирска, Барнаула, Кемерово, находящихся в пятисоткилометровой зоне. Если ехать на поезде, то выходить надо на станции Таштагол, пересаживаться на такси и, проехав 20 км оказаться на месте.</p>

<p>Между новокузнецком и Новосибирском имеется автобусное сообщение, маршрут которого пролегает через Таштагол, откуда до Шерегеша ходит рейсовый автобус под номером 101.</p>

<p>Курорт очень нравится рейдерам всей России, а профессионалы-экстремалы вообще считают его наилучшим в стране. Огромное количество трасс, схема которых представлена ниже, в состоянии удовлетворить любой вкус, а фрирайдеры почитают Геш как Мекку.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619277_6565.jpg" /></p>

<p>С детей до восьми лет не взимается плата за пользование подъемниками. Взрослые лыжники покупают ски пасс на 4 часа за 1 350 руб., а на 4 дня &mdash; 3 000 руб. Полный объем информации по стоимости услуг найдете на <a href="http://www.sheregesh.su/skhema-gornolyzhnykh-trass-v-sheregeshe">официальном сайте курорта</a>. Для подъема на трассы отдыхающим предоставляется возможность воспользоваться шестью бутельными подъемниками и четырьмя кресельными канатными дорогами.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619320_8719.jpg" /></p>

<p>Ценообразовательная политика определяется наличием здоровой конкуренции во всех сферах начиная с жилья и заканчивая питанием.</p>

<h2>Роза Хутор</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619364_7486.jpg" /></p>

<p>Для расположения горно-туристического комплекса Роза Хутор была выбрана долина реки Мзымта, прилегающая к подножью хребта Псекохо, а точнее &mdash; его южному склону. За этой зоной катания прочно закрепилось первое место по величине. Здесь созданы условия для фристайлеров, фрирайдеров, горнолыжников, биатлонистов и любителей бега на лыжах.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619783_9552.jpg" /></p>

<p>В инфраструктуру, не уступающую по уровню самым знаменитым мировым курортам, входят свыше двадцати гостиниц и отелей, имеющих разную звездность, что позволяет подобрать вариант на любые финансовые возможности. Для семейного отдыха отдыхающим предлагаются высокогорные шале, где очень приятно отдохнуть после активного дня у камина.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619822_8087.jpg" /></p>

<p>Любителей ночных развлечений ожидает порядка пятидесяти ресторанов, караоке-баров, кафе, пекарен, пабов, бургерных. Восстановить силы после активного отдыха можно в банях разного типа, центрах занятия йогой, салонах СПА и тайского массажа. О том какие услуги сколько стоят можно узнать, если перейти по <a href="https://rosakhutor.com/">по ссылке</a>.</p>

<h2>Кукисвумчорр</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619867_5960.jpg" /></p>

<p>В Мурманской области есть город Кировск, окруженный Хибиногорским массивом. Один из районов населенного пункта носит наименование Кукисвумчорр так было решено назвать и комплекс, образованный в нем. Все это находится за пределами полярного круга, а потому горнолыжные трассы используются практически круглый год. Местный горный ландшафт настолько разнообразен, что трассу по вкусу найдут себе и новичок, и опытный фрирайдер.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610619909_8191.jpg" /></p>

<p>Чтобы добраться до курорта, достаточно долететь самолетом из Санкт-Петербурга или Москвы до Мурманска, а там пересесть на автобус или маршрутное такси до Кировска. Либо сесть на борт до города Апатиты, где взять такси и доехать до курорта. Если ехать поездом мурманского направления, то выйти надо будет на станции Апатиты.</p>

<p>6 трасс, имеющихся в комплексе, обслуживается тремя подъемниками бугельного типа и одним учебным. В Кукисвумчорр имеется школа подготовки олимпийского резерва. Чтобы создать отдыхающим комфортные условия, организовано несколько пунктов, занимающихся прокатом инвентаря, а также несколько уютных кафе. Об условиях проживания информация расположена <a href="https://25chorr.ru/?p=hostjelupodnozh">здесь</a>.</p>

<h2>Эльбрус-Азау</h2>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610620046_2514.jpg" /></p>

<p>Как следует из названия курорта, его месторасположения &mdash; южный склон знаменитой горы Эльбрус. В 1969 году был осуществлен запуск в эксплуатацию первой очереди канатной дороги и с этой отметки началась история Эльбрус-Азау. 70% его трасс по уровню сложности относится к категории синих, но имеются также и черные, красные, зеленые спуски. Их суммарная протяженность составляет порядка одиннадцати километров.</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610620114_1429.jpg" /></p>

<p>На поляне Азау действует бугель ВЛ-400, который за 5 минут поднимет на высоту 200 м. Первая очередь маятниковой канатной дороги начинается на Азау (2350м), а заканчивается на Старом Кругозоре (3000м). Вторая очередь берет свое начало на Старом Кругозоре (3000м) и поднимает лыжников до станции Мир (3500м). Третья очередь обслуживается одно-кресельной канатной дорогой, верхняя точка которой Гара-Баши (3780м). Чтобы ознакомиться со стоимостью передвижения на каждом из перечисленных подъемников, достаточно зайти на <a href="https://prielbrusie-ski.ru/ropeways-slopes/elbrus-azau/">сайт</a>.&nbsp;</p>

<p><img alt="" src="http://travel-blogg.ru:3003/files/image/68/1610620159_1326.jpg" /></p>

<p>Отели и гостиницы расположены таким образом, что не надо тратить много времени, чтобы дойти до подъемников и вернуться, так как в середине курорта образовался просторный выкат, позволяющий подъехать на лыжах непосредственно к гостинице.</p>

<p>&nbsp;</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (36, 'Кипр: как, где именно и сколько стоит отдохнуть', 1, NULL, NULL, NULL, 'kipr-kak-gde-imenno-i-skolko-stoit-otdohnut', '2020-07-15', '2021-01-03', 'Кипр - это ласковое море, доброжелательные жители, изысканная кухня и высокий уровень сервиса. Максимально возможное количество информации, относящейся к отдыху в этой стране вы получите, прочитав нашу статью.  ', true, 3, 0, 'Отдых на Кипре', 'Кипр - это ласковое море, доброжелательные жители, изысканная кухня и высокий уровень сервиса. Максимально возможное количество информации, относящейся к отдыху в этой стране вы получите, прочитав нашу статью.  ', 'пляж, кипр, рубль, остров, кипрский, отпуск', '<p>В Средиземноморье сложно найти более солнечный остров чем Кипр. Небесное светило обогревает эту землю 320 дней на протяжении года. На острове не реже одного раза в неделю проходит фестиваль или карнавал. На его берегу, согласно легенде, появилась из морских пучин Афродита &mdash; богиня любви и красоты. Кипрские пляжи, купальный сезон на которых длится 8 месяцев, невозможно не полюбить.</p>

<p>&nbsp;</p>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595826319_7765.jpg" /></p>

<h2>Где хорошо отдыхать на Кипре</h2>

<p>Продолжительность купального сезона на Кипре больше чем в любой морской европейской стране. Пляжи острова с желтым песком располагаются в живописных небольших бухтах. Вход в воду может быть пологим или скалистым, но всегда это очень красиво. На 56 пляжах Кипра установлены Голубые флаги ЕС как подтверждение чистоты и экологичности.</p>

<p>Основная масса кипрских пляжей муниципального подчинения. Поэтому на них не надо платить за вход, а вот аренда пляжного оборудования стоит денег. Сет, состоящий из двух лежаков и зонтика обходится в &euro; 7, если же ограничиться одним предметом, то хватит &euro; 2,5.</p>

<p>ТОП 5 кипрских пляжей выглядит так:</p>

<h3>1. Корал-Бэй</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595826355_9056.jpg" /></p>

<p>Пляж расположен в Пафосе и считается лучшим песчаным пляжем острова. Это берег небольшой тихой бухты, окруженной высокими горами, защищающими ее от сильного ветра. Все это способствует повышению на несколько градусов температуры моря относительно пляжей, расположенных в центральной части.</p>

<p>Покрытие пляжа состоит из мелкого и мягкого песка с желтоватым оттенком без примеси ракушек и гальки, что особенно ценится отдыхающими с детьми. Пляж оборудован душевыми кабинками, раздевалками, фонтанчиками, в которых моют ноги.&nbsp; За безопасностью отдыхающих постоянно наблюдают спасатели. Утолить голод и жажду можно в нескольких ресторанчиках и кафе, расположенных вокруг..</p>

<h3>2. Лара</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595826411_6485.jpg" /></p>

<p>На небольшом расстоянии от Пафоса имеется уютная бухта полуострова Акамас. В ней расположился пляж, называемый местными жителями Черепашьим. На протяжении многих лет сюда приплывают морские черепахи чтобы отложить в песок яйца. Эта зона охраняется властями Кипра, которые присвоили ей статус заповедника.</p>

<p>Здесь нельзя расположиться с зонтиком и шезлонгом, так как это может нанести вред яйцам. Несмотря на то, что добираться до пляжа приходится пешком, многие туристы готовы проделать этот путь, чтобы полюбоваться уникальной красотой природы. Кроме того, отдыхающих привлекает возможность поплавать в чистейшей воде в обществе черепах.</p>

<h3>3. Айя-Текла</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595826438_2518.jpg" /></p>

<p>Название этого красивого пляжа, расположенного в Айя-Напе ассоциируется у многих отдыхающих с бело-голубой часовней Св. Феклы. Это любимое место для тех кому претит отдых среди большого скопления людей и по душе свобода пространства.</p>

<p>Неподалеку есть островок небольшого размера, до которого можно легко дойти вброд и позагорать, представив себя Робинзоном. Песок на берегу светлый и мягкий. На пляже отдыхающим предлагают взять на прокат зонтики и шезлонги, а также воспользоваться раздевалками, расположенными в санитарных зонах. Для желающих подкрепиться имеется несколько небольших кафе.</p>

<h3>4. Пляж в заливе Фигового дерева</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595826464_9313.jpg" /></p>

<p>За этим местом отдыха прочно укрепилась слава самого лучшего пляжа Протараса и самого посещаемого кипрского пляжа. Причем популярностью он пользуется не только у приезжих, но и у местного населения. Объясняется это наличием необычайно светлого и мелкого песка вкупе с мелководьем. Его название пошло от расположившейся недалеко фиговой рощи.</p>

<p>Большой каменистый остров прикрывает морю открытый доступ, благодаря чему вода в бухте удивительно чистая. По этой же причине здесь нет больших волн и вода всегда находится в спокойном состоянии. Благоустроен пляж по возможному максимуму: шезлонги, душ, раздевалки, рестораны, кафе, надувные горки, волейбольная площадка, то есть все, что нужно отдыхающему.</p>

<h3>5. Нисси Бич</h3>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595826483_6792.jpg" /></p>

<p>Безусловно это самый красивый пляж Кипра, песок которого отличается красивым белым цветом. Помимо этого, он обрел популярность благодаря своему необычному строению и шумным вечеринкам. Нисси по праву называют молодежным пляжем. Если утром и в обед здесь отдыхает много семей с детьми, то после обеда сюда начинают стекаться любители вечеринок.</p>

<p>Администрацией пляжа организовывается большое количество развлечений для молодежи в числе которых дискотеки, пенные вечеринки, конкурсы, разучивание танцев и даже уроки аэробики. В пунктах проката имеется транспорт для передвижения по воде. Любителям пощекотать нервы предлагается принять участие в экстремальных аттракционах.</p>

<h2>Какие документы надо подготовить</h2>

<p>Чтобы избежать неприятностей и беспрепятственно попасть на Кипр следует заблаговременно подготовить следующие документы:</p>

<ol>
	<li>Заграничный паспорт, срок действия которого заканчивается не раньше, чем через 3 месяца, после запланированной даты отъезда с острова.</li>
	<li>Виза, подтверждающая разрешение на посещение Республики Кипр. Гражданам России она выдается бесплатно. Подробная информация о получении провизы имеется <a href="https://www.provisa.mfa.gov.cy/russia" target="_blank">на&nbsp;сайте посольства.</a> Она присылается на электронную почту на протяжении одних суток. Также разрешено въезжать по открытой шенгенской визе.</li>
	<li>Страховой медицинский полис.</li>
	<li>Оригинальная доверенность от каждого из родителей, если дети путешествуют самостоятельно. Когда они едут с матерью или отцом, тогда требуется наличие такого же документа от второго супруга. Фотографии детей вклеиваются в паспорт сопровождающего, если на них не выписан индивидуальный документ.</li>
	<li>Ветеринарный сертификат, который соответствующий установленной форме, для тех, кто не желает расстаться с любимыми питомцами. Помимо этого, понадобится документ, подтверждающий чипирование и вакцинацию.</li>
</ol>

<h2>Что разрешается провозить через таможню</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595772164_5999.jpg" /></p>

<p>Для прилетающих на отдых властью Кипра введены некоторые ограничения, касающиеся ввозимых товаров и личных вещей. Если товары были приобретены не в странах, входящих в ЕС, то их разрешается провозить на остров в следующих количествах:</p>

<ol>
	<li>200 шт. сигарет.</li>
	<li>50 шт. сигар.</li>
	<li>250 г табака.</li>
	<li>1 л крепкого алкоголя.</li>
	<li>2 л вина.</li>
	<li>16 л пива.</li>
</ol>

<p>Запрещен ввоз следующих предметов и продуктов:</p>

<ol>
	<li>Оружия, если на него отсутствуют разрешительные документы.</li>
	<li>Объявленных в розыск культурных ценностей.</li>
	<li>Лекарственных средств, когда нет документов, подтверждающих необходимость их приема в лечебных целях.</li>
	<li>Свежих овощей и фруктов.</li>
	<li>&nbsp;Мяса.</li>
	<li>Цветов.</li>
	<li>Молочных продуктов.</li>
</ol>

<p>Если таможенник обнаружит у туриста значительные валютные запасы помимо задекларированных &euro;10&nbsp;000, то придется заплатить штраф. Здесь содержится подробная информация <a href="https://visasam.ru/russia/tamozhnia/tamozhennye-pravila-kipra.html">о таможенных правилах Кипра.</a></p>

<h2>Все о деньгах</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595772420_5836.jpg" /></p>

<p>Официальной валютой Кипра является евро, а на севере помимо этого имеют хождение турецкие лиры. Для обмена валюты достаточно обратиться на ресепшен в отеле проживания. Помимо этого, данную услугу предоставляют банки или обменные пункты. Кипрские финансовые учреждения в понедельник работаю до 17.00, а в остальные дни недели они прекращают обслуживать клиентов после 13.30.</p>

<p>Для проведения обмена обязательно предъявлять паспорт. Комиссионные сборы колеблются от одного до двух процентов. Банкоматы работают круглые сутки, но комиссия в них составит 4%.</p>

<p>В порядке полезной информации! Желательно не брать у киприотов сдачу банкнотами в &euro;100 и &euro;50, так как именно эти купюры подделываются чаще всего.&nbsp;</p>

<p>Практически все расчеты проводятся с помощью пластиковой карточки. Терминалами оснащены даже небольшие сувенирные лавки. Однако после 22.00 в некоторых заведениях просят рассчитываться наличными если покупка не превышает &euro;10.&nbsp;Кроме того мелкие монеты и купюры понадобятся если вы захотите что-то приобрести у мелкого ремесленника или розничного торговца.</p>

<h2>Перелет до Кипра</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595772464_1884.jpg" /></p>

<p>Несколько сотен авиарейсов с туристами ежедневно принимают два кипрских аэропорта, расположенных в Ларнаке и Пафосе. Оптимальный вариант &mdash; лететь до Ларнаки или Пафоса из Москвы. Туристам, отправляющимся отдохнуть под теплым кипрским солнцем из российской столицы, услуги предлагают такие авиакомпании, как:</p>

<ul>
	<li>Аэрофлот (Шереметьево) &mdash; 10&nbsp;529 руб.</li>
	<li>Победа (Внуково) &mdash; 5&nbsp;980 руб.</li>
	<li>URAL AIRLINES (Домодедово) &mdash; 6&nbsp;093 руб.</li>
	<li>Air Baltic (Шереметьево) &mdash; 6&nbsp;564 руб.</li>
	<li>Aegean Air (Домодедово) &mdash; 9&nbsp;704 руб.</li>
</ul>

<p>Чтобы получить весь объем информации по этому вопросу, достаточно пройти по ссылке: <a href="https://www.anywayanyday.com/avia/offers/2902MOWLCAAD1CN0IN0SCE/">anywayanyday.com</a>.</p>

<p>Из Санкт-Петербурга доставить отдыхающих на Кипр предлагает несколько компаний, но все маршруты с пересадками. Список компаний и стоимости их услуг из расчета на одного пассажира выглядит следующим образом:</p>

<ul>
	<li>URAL AIRLINES (с пересадкой в Москве) &mdash; 6&nbsp;546 руб.</li>
	<li>S7 AIRLINES (с пересадкой в Москве) &mdash; 7&nbsp;100 руб.</li>
	<li>URAL AIRLINES (с пересадкой в Москве и Афинах) &mdash; 8&nbsp;847 руб.</li>
	<li>S7 AIRLINES (с пересадкой в Москве и Афинах) &mdash; 10 235 руб.</li>
	<li>Nordwind (c пересадкой в&nbsp;Москве и Белграде) &mdash; 11&nbsp;426 руб.</li>
	<li>Btkfvia (пересадка в&nbsp;Минске) &mdash; 11&nbsp;713 руб.</li>
</ul>

<p>Со всеми предложениями можно ознакомиться здесь: <a href="https://avia.tutu.ru/flights/?passengers=100&amp;from=75&amp;to=294&amp;id=25dee19b-8a59-42c5-8c04-757dd2bd20f9&amp;class=Y&amp;changes=all&amp;route%5b0%5d=75-29022020-294">avia.tutu.ru</a>.&nbsp;</p>

<p>Современные технологии позволяют отследить движение каждого самолета, летящего на Кипр или любую другую точку мира в режиме реального времени. Для этого достаточно зайти на сервис <a href="https://www.flightradar24.com" target="blank">Flightradar24</a>. На открывшейся карте мира передвигается большое количество самолетиков желтого цвета. Если на любой из них навести курсор и выполнить клик, то появится информационное поле, в котором находятся сведения о названии авиарейса, типе самолета и пункте назначения.</p>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595772578_6330.png" /></p>

<p>Чтобы найти нужный рейс, достаточно в строку поиска ввести его номер и активировать изображение самолета. Программа проложит маршрут передвижения, отобразит фактическое место нахождения борта на текущий момент.</p>

<p>Специалистами туристического сервиса Momondo был проведен анализ сведений о стоимости билетов в зависимости от времени вылета. Его результаты позволили сделать вывод, что лучше всего покупать билеты на авиарейсы, вылет которых приходится на временной отрезок с 18:00 до 00:00. Утренние и дневные рейсы стоят дороже. Самый благоприятный, с точки зрения экономии, день для вылета &mdash; вторник. А в субботу летают те, для кого деньги не имеют значения.</p>

<h2>Жилье</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595826641_7930.jpg" /></p>

<p>В разгар туристического сезона за номер на двоих придется заплатить не меньше 2&nbsp;000 рублей в сутки. Основная масса отелей на острове имеют 2 или 3 звезды. Уровень сервиса в них привычный для российского туриста. Выбор жилья настолько велик, что здесь без труда найдет приемлемый для себя вариант и путешественник с ограниченным бюджетом, и отдыхающий, для которого деньги &mdash; второстепенный вопрос.</p>

<p>Кипрские гостиницы &mdash; составляющая часть хорошо индустрии отдыха, которая полна феерических развлечений и славится греческим гостеприимством. На прилегающих территориях многих отелей разбиты фруктовые сады, а потому бананы, гранаты, апельсины подаются свежесобранными.</p>

<p>Отличительной чертой расположенных на острове отелей является то факт, что в них предлагается несколько больше, чем это положено в соответствии со звездами. Здесь в порядке вещей, когда в отеле с двумя звездами обслуживание на уровне трехзвездочного и это справедливо для всех остальных степеней звездности.</p>

<p>Независимо от звездности отелей во всех номерах установлены кондиционеры, и они всегда в рабочем состоянии. Безусловно от уровня гостиницы зависит уровень и количество услуг. Так, например, завтрак в отеле 2* довольно скромный, тогда как в 3* и выше предлагается шведский стол.</p>

<p>За проживание в гостинице четырех- пятизвездочных в сутки придется заплатить от 4000 до 8000 руб. Чтобы сэкономить, некоторые отдыхающие арендуют квартиру, за одни сутки в которой просят от &euro;70 до &euro;200. Стоимость зависит от размера квартиры, места ее расположения, обстановки и сезона.</p>

<h2>О чем нельзя забывать, посещая Кипр</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/36/1595826773_9440.jpg" /></p>

<p>Кипрское солнце, ласковое по первым ощущениям, порой создает серьезные проблемы отдыхающему, злоупотребляющему солнечными ваннами без предварительной подготовки. Некоторые отдыхающие получают серьезные ожоги, которые портят весь отпуск. Чтобы этого избежать лучше на протяжении первых двух-трех дней загорать или ранним утром, или после 16 часов. И помните, что солнечные ожоги не считаются страховым случаем.</p>

<p>Теперь о местных жителях. Они чрезвычайно эмоциональны, а потому говорят очень громко, активно размахивая при этом руками и &laquo;гримасничая&raquo;. Но если вы попробуете вести себя так же, то это может быть воспринято как агрессивный акт, со всеми вытекающими отсюда последствиями.</p>

<p>Для посетителей храмов и мечетей существует определенный дресс-код, причем его требования для представителей разных полов отличаются. Женщине надлежит одеваться так, чтобы у нее были закрыты колени, плечи, зона декольте и покрыта голова. У мужчин голова наоборот обязательно отсутствие головного убора, а вот требование к ногам идентичное, то есть не допускается их открытость.</p>

<p>Для киприотов очень больной темой является все, что касается разделения острова на два государства.&nbsp; Категорически не допускается шутить на эту тему, а лучше ее совсем не касаться, даже вскользь.&nbsp;&nbsp;&nbsp;</p>
');
INSERT INTO public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, is_public, category_id, views, meta_title, meta_description, meta_keyword, description_full) VALUES (60, 'Коронавирус сократил число стран, готовых принять туристов на зимний отдых', 1, NULL, NULL, NULL, 'koronavirus-sokratil-chislo-stran-gotovyh-prinyat-turistov-na-zimnii-otdyh', '2020-12-23', '2021-01-03', 'В условиях пандемии перед правительствами стран всего мира стоит непростой вопрос: как предотвратить распространение короновируса, нанеся при этом минимально возможный вред туриндустрии. Мы расскажем о мерах, предпринимаемых каждым из наиболее часто посещаемых россиянами государств.', true, 10, 0, NULL, NULL, NULL, '<p>Пандемия свела до минимума количество стран, в которых для граждан России доступно проведение зимнего отпуска. Если в прошлом году больше всего отпускников ехало в Таиланд, Турцию, ОАЭ, Вьетнам, Кубу, Доминикану, то сегодня только 3 страны из этого списка по-прежнему готовы принять людей из России: Куба, Турция, ОАЭ. С них и начнем наш обзор плавно перейдя к другим доступным вариантам.<strong> </strong></p>

<p>&nbsp;</p>

<h2>Куба</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608742348_5805.jpg" /></p>

<p>&nbsp;</p>

<p>Рейсы из России совершаются в город Санта-Коара, находящийся в центральной части Кубы, и на остров Кайо-Коко. Отдыхающих готовы принимать отели, расположенные на курортах Кайо-Гильермо и Кайо-Коко, но покинуть их не удастся, так как перемещения между регионами ограничены.</p>

<p>У каждого, прилетевшего, работниками аэропорта измеряется температура и берется ПЦР-тест, за который не взимается плата. Пока не будет получен окончательный результат, а на это уходит до двадцати четырех часов, туристу предоставляется возможность свободно передвигаться по отелю. Но покинуть его он сможет только после получения отрицательного вердикта.&nbsp;</p>

<p>Гражданам России позволено отдыхать на Кубе без визы до девяноста дней. Ориентировочная стоимость для двух взрослых индивидуумов &mdash; 150 000 р., при условии, что он продлится не более десяти дней. Недельное проживание в четырехзвездочном отеле по схеме &laquo;все включено&raquo; для семьи из трех человек обойдется примерно в 267&nbsp;600 р.</p>

<h2>Турция</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608742366_1226.jpg" /></p>

<p>&nbsp;</p>

<p>Наличие прямого сообщения, доступность цен и &laquo;мягкие&raquo; эпидемиологические требования к приехавшим на отдых в Турцию делает это направление довольно востребованным у россиян.</p>

<p>Любители купания в открытом море вряд ли смогут это сделать, ведь купальный сезон заканчивается в октябре. Но окунуться в любимую стихию им все-таки представится возможность, так как многие отели имеют бассейны с подогретой морской водой.</p>

<p>Для въезда не нужен документ, подтверждающий наличие отрицательного теста на COVID-19. Эта информация официально подтверждена российским посольством в Турции. Однако у всех пребывающих в обязательном порядке измеряется температура. Всех подозрительных пассажиров тестируют и при получении положительного результата помещают в карантин. Такая же участь ожидает тех, кто сидел рядом с зараженным в самолете.</p>

<p>Комендантский час, введенный в государстве, на туристов не распространяется. Правда, гулять по безлюдному городу &mdash; это удовольствие не для каждого. Два человека смогут отдохнуть в Турции 10 дней за 100 000 р.</p>

<h2>ОАЭ</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608742386_3635.jpg" /></p>

<p>&nbsp;</p>

<p>В этот, пользующийся у россиян популярностью, эмират летают как регулярные, так и чартерные рейсы. Поселиться можно как в городских, так и в прибрежных отелях. Купаться зимой отважится не каждый, так как температура воды не поднимается выше 20оС.</p>

<p>Если отдыхать планируется не более девяноста дней, то виза для въезда не нужна. На паспортном контроле потребуют предъявить тест на COVID-19 с отрицательным результатом. Причем получить его следует самое позднее за 96 часов до вылета, а если конечный пункт Абу-Даби, то этот срок уменьшается до сорока восьми часов. Но даже если присутствует требуемый документ, каждого прилетевшего попросят бесплатно пройти тест в здании аэропорта.&nbsp;</p>

<p>Туристам предписывается находиться в отеле до момента получения результатов, получить которые можно на протяжении двадцати четырех часов. Тем же, кто летел до Абу-Даби, в любом случае придется пройти четырнадцати дневный карантин. В турагентстве, базирующемся на регулярных авиационных перевозках, стоимость тура на одну неделю для двух человек начинается с двухсот сорока рублей.&nbsp;</p>

<h2>Танзания</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608742410_8364.jpg" /></p>

<p>&nbsp;</p>

<p>До 2020 года основная масса россиян не знала, что на пляжах Танзании возможен прекрасный пляжный отдых. До прихода пандемии здесь чаще всего встречались граждане Италии. На протяжении всего 2019 года здесь отдохнула всего 41&nbsp;000 граждан России. В сегодняшних реалиях около 90% зимующих на Занзибаре &mdash; наши соотечественники.</p>

<p>От пассажиров чартерных рейсов не требуют предъявления результатов отрицательного теста на коронавирус, не проходят его и по прилете. Пассажирам международных рейсов измеряют температуру. Тех, у кого она не соответствует нормам, помещают в карантин.</p>

<p>Виза выдается в аэропорту за $50 и дает право россиянам находиться в стране на протяжении девяноста дней. За каждую, проведенную в Танзании ночь, с человека взимается налог, равный $1. Восьмидневный тур на Занзибар для двух человек, при условии проживания в трехзвездочном отеле, стоит порядка 272 600 р.</p>

<h2>Мальдивы</h2>

<p><img src="http://travel-blogg.ru:3003/files/image/0/1608742696_4490.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Долететь в Мальдивы можно из Москвы, приобретя билет на прямой рейс &laquo;Аэрофлота&raquo;, или с присадками, воспользовавшись услугами компаний Emirates или Turkish Airlines. На срок пребывания до тридцати дней не требуется визы.</p>

<p>Мальдивы отличаются едва ли не самыми жесткими эпидемиологическими правилами. Тест должен быть сдан не раньше девяноста шести часов до времени вылета. На паспортном контроле предъявляется оригинал документа, подтверждающего отрицательный результат теста на коронавирус. Для его написания допускается использование исключительно английского языка.</p>

<p>Не раньше, чем за сутки желающим въехать заполняется декларация о здоровье, в которой перечисляются все государства, посещаемые им на протяжении последних дней. К документу прилагается копия ПЦР-теста. Делается все это в режиме онлайн и подтверждается выдачей QR-кода, который предъявляется на паспортном контроле. Еще одно обязательное условие &mdash; загрузка специального мобильного приложения, позволяющего отслеживать контакт.</p>

<p>10 дней на Мальдивах 2 человека могут провести за 500&nbsp;000 &ndash; 800 000 р. Отдых в бунгало, расположенном на острове Маафуши семье из трех человек обойдется в 337 900 р.</p>

<p>Безусловно предложенные нами варианты не являются исчерпывающими, но они выбраны с учетом предпочтений, отдаваемых российскими туристами. Какой из них выбрать &mdash; решать вам. Счастливого и запоминающегося отпуска!</p>
');


--
-- Data for Name: Rights_blocks; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Rights_blocks" (id, title, slug, "createdAt", "updatedAt") VALUES (1, 'Фирмы', 'firms', NULL, NULL);
INSERT INTO public."Rights_blocks" (id, title, slug, "createdAt", "updatedAt") VALUES (2, 'Файлы', 'files', NULL, NULL);
INSERT INTO public."Rights_blocks" (id, title, slug, "createdAt", "updatedAt") VALUES (4, 'Images', 'images', NULL, NULL);
INSERT INTO public."Rights_blocks" (id, title, slug, "createdAt", "updatedAt") VALUES (5, 'Category', 'categories', NULL, NULL);
INSERT INTO public."Rights_blocks" (id, title, slug, "createdAt", "updatedAt") VALUES (3, 'hash-tag', 'hash-tags', NULL, NULL);


--
-- Data for Name: Roles_rights; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Roles_rights" (id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id, role_id) VALUES (4, true, true, true, true, NULL, NULL, 4, 2);
INSERT INTO public."Roles_rights" (id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id, role_id) VALUES (5, true, true, true, true, NULL, NULL, 2, 2);
INSERT INTO public."Roles_rights" (id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id, role_id) VALUES (6, true, true, true, true, NULL, NULL, 3, 2);
INSERT INTO public."Roles_rights" (id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id, role_id) VALUES (7, true, true, true, true, NULL, NULL, 5, 2);


--
-- Data for Name: Tag_links; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (212, 5, 34, '2020-06-22', '2020-06-22', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1171, 27, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1172, 41, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1173, 42, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1174, 49, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (15, 3, 4, '2019-09-16', '2019-09-16', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (16, 4, 4, '2019-09-16', '2019-09-16', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (41, 6, 31, '2019-11-07', '2019-11-07', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (42, 5, 31, '2019-11-07', '2019-11-07', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (43, 2, 32, '2019-11-07', '2019-11-07', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (44, 5, 32, '2019-11-07', '2019-11-07', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (48, 4, 33, '2019-11-07', '2019-11-07', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (47, 6, 33, '2019-11-07', '2019-11-07', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (58, 2, 7, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (61, 4, 9, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (62, 5, 9, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (66, 4, 10, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (67, 2, 10, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (68, 6, 10, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (84, 4, 11, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (85, 5, 11, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (86, 2, 11, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (87, 4, 8, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (88, 5, 8, '2019-11-17', '2019-11-17', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1176, 62, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1178, 73, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (344, 5, 44, '2020-07-25', '2020-07-25', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1109, 27, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1110, 28, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1179, 75, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1181, 76, 54, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1183, 78, 54, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1185, 80, 54, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1187, 41, 52, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1188, 60, 52, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1189, 82, 52, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1191, 84, 52, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1193, 42, 50, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1195, 60, 50, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1111, 29, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1112, 30, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1114, 32, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1115, 33, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1117, 34, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1125, 41, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1127, 45, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1130, 47, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1132, 119, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1133, 35, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1134, 36, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1135, 41, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1136, 42, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1138, 49, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1139, 51, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1141, 52, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1143, 54, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1197, 86, 50, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (319, 4, 40, '2020-07-22', '2020-07-22', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (320, 4, 41, '2020-07-22', '2020-07-22', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1209, 39, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1212, 60, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1149, 55, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (325, 4, 42, '2020-07-23', '2020-07-23', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1151, 57, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (327, 6, 43, '2020-07-23', '2020-07-23', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1153, 27, 57, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1154, 41, 57, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1155, 42, 57, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1156, 49, 57, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1157, 60, 57, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1159, 62, 57, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1163, 60, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1165, 66, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1167, 68, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1169, 71, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (206, 6, 19, '2020-05-18', '2020-05-18', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (207, 7, 19, '2020-05-18', '2020-05-18', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1213, 87, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1216, 90, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (210, 7, 18, '2020-05-18', '2020-05-18', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (211, 6, 18, '2020-05-18', '2020-05-18', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1217, 91, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1219, 39, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1221, 42, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (516, 2, 51, '2020-08-25', '2020-08-25', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1223, 60, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1225, 83, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1227, 92, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1229, 42, 47, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1231, 94, 47, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1375, 66, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (529, 2, 53, '2020-08-27', '2020-08-27', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1378, 122, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1380, 124, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1383, 30, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1233, 96, 47, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1235, 41, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1236, 42, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1240, 98, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1386, 103, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1389, 127, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1503, 144, 67, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1505, 146, 67, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1506, 147, 67, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1508, 149, 67, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1304, 27, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1305, 39, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1306, 40, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1307, 42, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1309, 55, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1311, 62, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1312, 86, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1314, 87, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1316, 101, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1317, 102, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1329, 90, 36, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1331, 104, 36, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1532, 30, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1534, 125, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1537, 152, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1539, 154, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1552, 30, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1555, 127, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1557, 158, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1572, 31, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1573, 34, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1576, 130, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1577, 131, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1583, 49, 20, '2021-01-15', '2021-01-15', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1586, 108, 20, '2021-01-15', '2021-01-15', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1479, 30, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1481, 135, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1484, 138, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1486, 140, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1502, 31, 67, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1504, 145, 67, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1507, 148, 67, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1372, 27, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1373, 30, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1374, 42, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1376, 65, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1377, 121, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1379, 123, 63, '2021-01-04', '2021-01-04', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1381, 27, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1382, 29, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1384, 31, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1385, 49, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1308, 49, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1310, 60, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1313, 82, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1315, 94, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1318, 103, 45, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1387, 126, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1388, 125, 66, '2021-01-05', '2021-01-05', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1326, 42, 36, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1327, 60, 36, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1328, 89, 36, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1330, 101, 36, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1332, 105, 36, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1531, 27, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1533, 31, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1535, 150, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1536, 151, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1417, 142, 61, '2021-01-13', '2021-01-13', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1175, 60, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1177, 72, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1180, 74, 55, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1182, 77, 54, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1538, 153, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1540, 155, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1541, 156, 69, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1551, 27, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1113, 31, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1116, 35, 62, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1118, 35, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1119, 36, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1120, 37, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1121, 38, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1122, 39, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1123, 40, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1124, 44, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1126, 42, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1128, 48, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1129, 46, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1131, 49, 60, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1137, 45, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1140, 50, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1142, 53, 59, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1144, 27, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1145, 37, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1146, 41, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1147, 49, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1148, 56, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1150, 58, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1152, 59, 58, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1158, 61, 57, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1160, 27, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1161, 41, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1162, 42, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1164, 65, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1166, 67, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1168, 69, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1170, 70, 56, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1184, 79, 54, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1186, 81, 54, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1190, 83, 52, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1192, 85, 52, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1194, 49, 50, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1196, 62, 50, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1553, 49, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1554, 126, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1556, 157, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1558, 159, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1208, 27, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1210, 42, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1211, 49, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1214, 88, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1215, 89, 49, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1218, 27, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1220, 41, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1222, 56, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1224, 62, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1226, 93, 48, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1228, 27, 47, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1230, 51, 47, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1232, 95, 47, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1234, 36, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1237, 60, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1238, 49, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1239, 97, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1241, 99, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1242, 100, 46, '2021-01-03', '2021-01-03', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1559, 160, 64, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1570, 27, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1571, 30, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1574, 83, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1575, 129, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1578, 132, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1579, 134, 68, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1580, 40, 20, '2021-01-15', '2021-01-15', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1581, 41, 20, '2021-01-15', '2021-01-15', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1582, 42, 20, '2021-01-15', '2021-01-15', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1584, 89, 20, '2021-01-15', '2021-01-15', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1585, 106, 20, '2021-01-15', '2021-01-15', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1478, 27, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1480, 31, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1482, 136, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1483, 137, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1485, 139, 70, '2021-01-14', '2021-01-14', 'image');
INSERT INTO public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) VALUES (1487, 141, 70, '2021-01-14', '2021-01-14', 'image');


--
-- Data for Name: Users; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Users" (id, last_name, avatar, sex, password, salt, email, role_id, "createdAt", "updatedAt", name) VALUES (1, 'Shadrin', NULL, 1, '$2a$08$cqxwA8L5FeZRgSmCBoX61eaENK8I4sfeLUCM//mkkMddpbrOjEA0m', '$fg08jh;', 'admin2@gmail.com', 2, NULL, NULL, NULL);


--
-- Data for Name: Users_rights; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Users_rights" (id, user_id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id) VALUES (1, 1, true, true, true, true, NULL, NULL, 1);
INSERT INTO public."Users_rights" (id, user_id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id) VALUES (2, 1, true, true, true, true, NULL, NULL, 2);
INSERT INTO public."Users_rights" (id, user_id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id) VALUES (3, 1, true, true, true, true, NULL, NULL, NULL);


--
-- Data for Name: Users_roles; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Users_roles" (id, name, is_admin, "createdAt", "updatedAt") VALUES (1, 'user', false, NULL, NULL);
INSERT INTO public."Users_roles" (id, name, is_admin, "createdAt", "updatedAt") VALUES (2, 'admin', true, NULL, NULL);


--
-- Data for Name: Views; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (6, '2019-12-22', '2019-12-22', 7, 'image', '127:0:0:1', '2019-12-23');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (107, '2021-01-14', '2020-12-22', 58, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (8, '2020-01-08', '2020-01-08', 14, 'image', '::ffff:82.215.114.152', '2020-01-08');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (10, '2020-01-08', '2020-01-08', 20, 'image', '::ffff:82.215.114.152', '2020-01-08');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (11, '2020-01-08', '2020-01-08', 11, 'image', '::ffff:91.240.84.122', '2020-01-08');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (12, '2020-01-08', '2020-01-08', 11, 'image', '::ffff:89.236.230.243', '2020-01-08');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (13, '2020-01-08', '2020-01-08', 11, 'image', '::ffff:89.146.75.163', '2020-01-08');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (15, '2020-01-16', '2020-01-16', 19, 'image', '::ffff:82.215.114.251', '2020-01-16');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (49, '2020-07-28', '2020-07-28', 20, 'image', '::ffff:89.146.66.160', '2020-07-28');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (14, '2020-01-17', '2020-01-16', 20, 'image', '::ffff:82.215.114.251', '2020-01-18');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (16, '2020-03-22', '2020-03-22', 20, 'image', '::ffff:89.146.95.231', '2020-03-22');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (17, '2020-04-05', '2020-04-05', 20, 'image', '::ffff:89.146.120.82', '2020-04-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (20, '2020-04-20', '2020-04-20', 19, 'image', '::ffff:89.146.69.17', '2020-04-21');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (52, '2020-07-28', '2020-07-28', 45, 'image', '127:0:0:1', '2020-07-29');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (53, '2020-07-29', '2020-07-29', 45, 'image', '::ffff:89.146.66.160', '2020-07-29');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (96, '2021-01-12', '2020-09-14', 56, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (18, '2020-05-11', '2020-04-20', 20, 'image', '::ffff:89.146.69.17', '2020-05-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (55, '2020-07-29', '2020-07-29', 46, 'image', '127:0:0:1', '2020-07-30');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (56, '2020-07-30', '2020-07-30', 46, 'image', '::ffff:89.146.121.134', '2020-07-30');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (87, '2020-09-05', '2020-09-05', 54, 'image', '::ffff:66.249.64.137', '2020-09-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (21, '2020-05-16', '2020-05-12', 20, 'image', '::ffff:89.146.68.52', '2020-05-17');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (22, '2020-05-18', '2020-05-18', 19, 'image', '::ffff:89.146.68.52', '2020-05-18');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (23, '2020-05-18', '2020-05-18', 18, 'image', '::ffff:89.146.68.52', '2020-05-18');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (24, '2020-06-03', '2020-06-03', 20, 'image', '::ffff:89.146.100.107', '2020-06-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (88, '2020-09-05', '2020-09-05', 54, 'image', '::ffff:66.249.64.141', '2020-09-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (89, '2020-09-05', '2020-09-05', 54, 'image', '::ffff:66.249.64.139', '2020-09-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (69, '2020-08-24', '2020-08-24', 46, 'image', '::ffff:89.236.238.71', '2020-08-24');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (70, '2020-08-24', '2020-08-24', 47, 'image', '::ffff:89.236.238.71', '2020-08-24');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (26, '2020-07-04', '2020-06-24', 35, 'image', '127:0:0:1', '2020-07-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (27, '2020-07-10', '2020-07-10', 20, 'image', '::ffff:185.163.27.88', '2020-07-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (28, '2020-07-12', '2020-07-11', 20, 'image', '::ffff:89.146.74.124', '2020-07-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (25, '2020-07-12', '2020-06-22', 34, 'image', '127:0:0:1', '2020-07-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (29, '2020-07-13', '2020-07-12', 20, 'image', '::ffff:89.146.114.78', '2020-07-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (73, '2021-01-12', '2020-08-25', 50, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (60, '2020-07-31', '2020-07-31', 48, 'image', '::ffff:82.215.89.238', '2020-08-01');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (31, '2020-07-15', '2020-07-15', 18, 'image', '::ffff:89.146.114.78', '2020-07-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (32, '2020-07-15', '2020-07-15', 36, 'image', '::ffff:89.146.114.78', '2020-07-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (61, '2020-07-31', '2020-07-31', 48, 'image', '127:0:0:1', '2020-08-01');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (71, '2020-08-25', '2020-08-25', 49, 'image', '::ffff:89.236.238.71', '2020-08-25');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (72, '2020-08-25', '2020-08-25', 50, 'image', '::ffff:89.236.238.71', '2020-08-25');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (74, '2020-08-25', '2020-08-25', 51, 'image', '::ffff:89.146.86.3', '2020-08-25');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (51, '2021-01-10', '2020-07-28', 45, 'image', '::ffff:91.240.84.122', '2021-01-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (34, '2020-07-18', '2020-07-18', 39, 'image', '127:0:0:1', '2020-07-19');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (58, '2020-08-04', '2020-07-30', 48, 'image', '::ffff:62.209.137.219', '2020-08-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (35, '2020-07-20', '2020-07-20', 36, 'image', '::ffff:82.215.91.101', '2020-07-20');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (19, '2021-01-14', '2020-04-20', 20, 'image', '127:0:0:1', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (57, '2020-08-04', '2020-07-30', 47, 'image', '::ffff:62.209.137.219', '2020-08-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (54, '2020-08-04', '2020-07-29', 46, 'image', '::ffff:62.209.137.219', '2020-08-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (37, '2020-07-22', '2020-07-21', 36, 'image', '::ffff:89.146.73.116', '2020-07-22');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (39, '2020-07-22', '2020-07-22', 20, 'image', '::ffff:89.146.73.116', '2020-07-22');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (7, '2021-01-15', '2020-01-06', 20, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (50, '2020-08-04', '2020-07-28', 45, 'image', '::ffff:62.209.137.219', '2020-08-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (30, '2020-08-04', '2020-07-15', 36, 'image', '::ffff:62.209.137.219', '2020-08-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (40, '2020-07-23', '2020-07-23', 42, 'image', '127:0:0:1', '2020-07-24');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (41, '2020-07-23', '2020-07-23', 43, 'image', '127:0:0:1', '2020-07-24');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (38, '2020-08-04', '2020-07-22', 20, 'image', '::ffff:62.209.137.219', '2020-08-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (111, '2021-01-12', '2020-12-22', 59, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (42, '2020-07-25', '2020-07-24', 36, 'image', '::ffff:89.146.75.192', '2020-07-26');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (43, '2020-07-25', '2020-07-25', 20, 'image', '::ffff:89.146.75.192', '2020-07-26');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (44, '2020-07-25', '2020-07-25', 44, 'image', '::ffff:89.146.75.192', '2020-07-26');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (45, '2020-07-25', '2020-07-25', 44, 'image', '127:0:0:1', '2020-07-26');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (46, '2020-07-26', '2020-07-26', 44, 'image', '::ffff:62.209.137.219', '2020-07-26');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (63, '2020-08-04', '2020-08-04', 49, 'image', '::ffff:89.146.115.203', '2020-08-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (47, '2020-07-27', '2020-07-27', 36, 'image', '::ffff:89.146.96.13', '2020-07-27');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (33, '2020-07-27', '2020-07-15', 36, 'image', '127:0:0:1', '2020-07-28');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (48, '2020-07-28', '2020-07-27', 36, 'image', '::ffff:89.146.66.160', '2020-07-28');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (77, '2020-08-26', '2020-08-26', 52, 'image', '::ffff:5.45.207.110', '2020-08-26');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (36, '2021-01-10', '2020-07-21', 36, 'image', '::ffff:91.240.84.122', '2021-01-10');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (64, '2020-08-05', '2020-08-04', 49, 'image', '127:0:0:1', '2020-08-06');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (62, '2020-08-06', '2020-08-04', 49, 'image', '::ffff:62.209.137.219', '2020-08-06');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (59, '2021-01-12', '2020-07-31', 48, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (78, '2020-08-27', '2020-08-27', 53, 'image', '::ffff:89.236.238.71', '2020-08-27');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (79, '2020-08-27', '2020-08-27', 53, 'image', '::ffff:91.240.84.122', '2020-08-27');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (80, '2020-08-27', '2020-08-27', 53, 'image', '::ffff:89.146.71.74', '2020-08-27');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (75, '2020-08-27', '2020-08-26', 52, 'image', '::ffff:89.236.238.71', '2020-08-27');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (66, '2020-08-10', '2020-08-10', 49, 'image', '::ffff:213.180.203.32', '2020-08-10');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (94, '2020-09-14', '2020-09-13', 55, 'image', '::ffff:66.249.79.183', '2020-09-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (95, '2020-09-14', '2020-09-14', 56, 'image', '::ffff:89.236.238.71', '2020-09-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (67, '2020-08-14', '2020-08-14', 49, 'image', '::ffff:66.249.64.201', '2020-08-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (90, '2020-09-08', '2020-09-08', 54, 'image', '::ffff:89.146.98.201', '2020-09-08');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (84, '2020-09-08', '2020-09-03', 54, 'image', '::ffff:89.236.238.71', '2020-09-08');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (9, '2021-01-12', '2020-01-08', 20, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (68, '2020-08-17', '2020-08-17', 49, 'image', '::ffff:5.45.207.113', '2020-08-17');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (81, '2020-08-31', '2020-08-31', 52, 'image', '::ffff:66.249.66.45', '2020-08-31');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (82, '2020-08-31', '2020-08-31', 52, 'image', '::ffff:66.249.66.41', '2020-08-31');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (91, '2020-09-09', '2020-09-09', 55, 'image', '::ffff:89.236.238.71', '2020-09-09');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (83, '2020-09-02', '2020-09-02', 52, 'image', '::ffff:66.249.69.41', '2020-09-02');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (92, '2021-01-11', '2020-09-09', 55, 'image', '::ffff:91.240.84.122', '2021-01-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (86, '2020-09-04', '2020-09-04', 54, 'image', '::ffff:82.215.88.49', '2020-09-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (98, '2020-09-16', '2020-09-16', 56, 'image', '::ffff:46.19.86.144', '2020-09-17');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (93, '2020-09-12', '2020-09-12', 55, 'image', '::ffff:66.249.64.139', '2020-09-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (99, '2020-09-16', '2020-09-16', 56, 'image', '::ffff:66.249.79.205', '2020-09-17');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (76, '2021-01-11', '2020-08-26', 52, 'image', '::ffff:91.240.84.122', '2021-01-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (97, '2020-09-16', '2020-09-16', 56, 'image', '::ffff:66.249.79.201', '2020-09-17');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (100, '2020-09-16', '2020-09-16', 56, 'image', '::ffff:66.249.79.203', '2020-09-17');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (101, '2020-09-17', '2020-09-17', 57, 'image', '::ffff:89.236.238.71', '2020-09-17');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (103, '2020-09-17', '2020-09-17', 57, 'image', '::ffff:66.249.79.203', '2020-09-17');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (104, '2020-09-21', '2020-09-21', 57, 'image', '::ffff:217.30.165.159', '2020-09-21');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (105, '2020-12-21', '2020-12-21', 57, 'image', '::ffff:185.163.26.79', '2020-12-22');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (108, '2021-01-12', '2020-12-22', 58, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (109, '2020-12-22', '2020-12-22', 58, 'image', '::ffff:89.146.84.135', '2020-12-22');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (65, '2021-01-11', '2020-08-05', 49, 'image', '::ffff:91.240.84.122', '2021-01-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (106, '2021-01-15', '2020-12-22', 57, 'image', '::ffff:80.80.221.95', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (85, '2021-01-12', '2020-09-03', 54, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (102, '2021-01-10', '2020-09-17', 57, 'image', '::ffff:91.240.84.122', '2021-01-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (112, '2020-12-22', '2020-12-22', 59, 'image', '::ffff:5.255.231.87', '2020-12-22');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (113, '2020-12-23', '2020-12-23', 59, 'image', '::ffff:89.146.84.135', '2020-12-23');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (114, '2020-12-23', '2020-12-23', 59, 'image', '::ffff:159.220.77.4', '2020-12-23');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (116, '2020-12-23', '2020-12-23', 60, 'image', '::ffff:89.146.84.135', '2020-12-23');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (117, '2020-12-23', '2020-12-23', 49, 'image', '::ffff:89.146.84.135', '2020-12-23');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (119, '2020-12-23', '2020-12-23', 61, 'image', '::ffff:89.146.84.135', '2020-12-23');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (124, '2020-12-24', '2020-12-24', 62, 'image', '::ffff:66.249.64.139', '2020-12-24');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (125, '2020-12-24', '2020-12-24', 62, 'image', '::ffff:66.249.64.141', '2020-12-24');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (118, '2021-01-11', '2020-12-23', 60, 'image', '::ffff:91.240.84.122', '2021-01-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (123, '2020-12-24', '2020-12-24', 62, 'image', '::ffff:66.249.64.137', '2020-12-25');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (206, '2021-01-13', '2021-01-13', 68, 'image', '::ffff:89.146.67.111', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (149, '2021-01-03', '2021-01-02', 61, 'image', '::ffff:89.146.84.70', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (150, '2021-01-03', '2021-01-02', 62, 'image', '::ffff:89.146.84.70', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (137, '2020-12-25', '2020-12-25', 62, 'image', '::ffff:82.215.78.99', '2020-12-25');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (138, '2020-12-25', '2020-12-25', 61, 'image', '::ffff:82.215.78.99', '2020-12-25');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (151, '2021-01-03', '2021-01-03', 60, 'image', '::ffff:89.146.84.70', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (152, '2021-01-03', '2021-01-03', 59, 'image', '::ffff:89.146.84.70', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (140, '2020-12-26', '2020-12-26', 62, 'image', '::ffff:66.249.75.203', '2020-12-26');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (153, '2021-01-03', '2021-01-03', 57, 'image', '::ffff:89.146.84.70', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (210, '2021-01-14', '2021-01-13', 64, 'image', '::ffff:89.146.67.111', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (185, '2021-01-15', '2021-01-08', 69, 'image', '::ffff:80.80.221.95', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (128, '2021-01-03', '2020-12-25', 55, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (129, '2021-01-03', '2020-12-25', 54, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (126, '2021-01-15', '2020-12-25', 20, 'image', '::ffff:80.80.221.95', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (131, '2021-01-03', '2020-12-25', 50, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (132, '2021-01-03', '2020-12-25', 49, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (133, '2021-01-03', '2020-12-25', 48, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (134, '2021-01-03', '2020-12-25', 47, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (142, '2021-01-03', '2020-12-28', 46, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (135, '2021-01-03', '2020-12-25', 45, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (136, '2021-01-03', '2020-12-25', 36, 'image', '::ffff:80.80.221.95', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (154, '2021-01-03', '2021-01-03', 62, 'image', '::ffff:89.146.113.138', '2021-01-03');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (148, '2021-01-03', '2020-12-30', 62, 'image', '127:0:0:1', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (188, '2021-01-11', '2021-01-11', 70, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (195, '2021-01-13', '2021-01-13', 36, 'image', '::ffff:127.0.0.1', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (156, '2021-01-04', '2021-01-04', 58, 'image', '127:0:0:1', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (143, '2020-12-28', '2020-12-28', 62, 'image', '::ffff:66.249.65.71', '2020-12-28');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (144, '2020-12-28', '2020-12-28', 62, 'image', '::ffff:82.215.79.74', '2020-12-28');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (155, '2021-01-04', '2021-01-03', 58, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (157, '2021-01-04', '2021-01-04', 59, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (145, '2020-12-29', '2020-12-29', 62, 'image', '::ffff:66.249.66.76', '2020-12-29');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (158, '2021-01-04', '2021-01-04', 60, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (159, '2021-01-04', '2021-01-04', 54, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (146, '2020-12-30', '2020-12-29', 62, 'image', '::ffff:89.146.103.156', '2020-12-30');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (160, '2021-01-04', '2021-01-04', 50, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (161, '2021-01-04', '2021-01-04', 49, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (162, '2021-01-04', '2021-01-04', 36, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (147, '2020-12-31', '2020-12-30', 61, 'image', '::ffff:89.146.103.156', '2020-12-31');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (141, '2020-12-31', '2020-12-28', 61, 'image', '::ffff:80.80.221.95', '2020-12-31');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (163, '2021-01-04', '2021-01-04', 20, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (189, '2021-01-12', '2021-01-12', 70, 'image', '::ffff:89.146.115.147', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (213, '2021-01-13', '2021-01-13', 20, 'image', '::ffff:66.249.69.75', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (174, '2021-01-12', '2021-01-04', 46, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (180, '2021-01-12', '2021-01-06', 64, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (110, '2021-01-14', '2020-12-22', 59, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (197, '2021-01-13', '2021-01-13', 49, 'image', '::ffff:127.0.0.1', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (179, '2021-01-10', '2021-01-05', 47, 'image', '::ffff:91.240.84.122', '2021-01-10');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (122, '2021-01-12', '2020-12-24', 62, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (199, '2021-01-13', '2021-01-13', 57, 'image', '::ffff:127.0.0.1', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (166, '2021-01-12', '2021-01-04', 63, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (164, '2021-01-04', '2021-01-04', 56, 'image', '::ffff:89.146.113.138', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (167, '2021-01-04', '2021-01-04', 58, 'image', '::ffff:82.215.91.142', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (168, '2021-01-04', '2021-01-04', 20, 'image', '::ffff:82.215.91.142', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (169, '2021-01-04', '2021-01-04', 63, 'image', '::ffff:82.215.91.142', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (170, '2021-01-04', '2021-01-04', 56, 'image', '127:0:0:1', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (171, '2021-01-04', '2021-01-04', 45, 'image', '::ffff:82.215.91.142', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (172, '2021-01-04', '2021-01-04', 63, 'image', '127:0:0:1', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (173, '2021-01-04', '2021-01-04', 57, 'image', '127:0:0:1', '2021-01-04');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (186, '2021-01-12', '2021-01-09', 69, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (182, '2021-01-12', '2021-01-07', 67, 'image', '::ffff:91.240.84.122', '2021-01-12');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (175, '2021-01-05', '2021-01-05', 63, 'image', '::ffff:89.146.115.33', '2021-01-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (176, '2021-01-05', '2021-01-05', 62, 'image', '::ffff:89.146.115.33', '2021-01-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (178, '2021-01-05', '2021-01-05', 66, 'image', '::ffff:80.80.221.95', '2021-01-05');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (127, '2021-01-13', '2020-12-25', 56, 'image', '::ffff:80.80.221.95', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (201, '2021-01-13', '2021-01-13', 55, 'image', '::ffff:127.0.0.1', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (184, '2021-01-11', '2021-01-08', 68, 'image', '::ffff:91.240.84.122', '2021-01-11');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (139, '2021-01-12', '2020-12-25', 61, 'image', '127:0:0:1', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (192, '2021-01-12', '2021-01-12', 20, 'image', '::ffff:66.249.66.75', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (193, '2021-01-12', '2021-01-12', 47, 'image', '::ffff:127.0.0.1', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (194, '2021-01-13', '2021-01-13', 56, 'image', '::ffff:127.0.0.1', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (215, '2021-01-14', '2021-01-14', 69, 'image', '::ffff:127.0.0.1', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (165, '2021-01-14', '2021-01-04', 63, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (181, '2021-01-14', '2021-01-06', 67, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (216, '2021-01-14', '2021-01-14', 60, 'image', '::ffff:89.146.67.111', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (209, '2021-01-13', '2021-01-13', 68, 'image', '127:0:0:1', '2021-01-13');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (115, '2021-01-14', '2020-12-23', 60, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (208, '2021-01-15', '2021-01-13', 70, 'image', '127:0:0:1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (214, '2021-01-13', '2021-01-13', 20, 'image', '::ffff:66.249.69.73', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (183, '2021-01-14', '2021-01-08', 68, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (205, '2021-01-14', '2021-01-13', 68, 'image', '::ffff:127.0.0.1', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (191, '2021-01-15', '2021-01-12', 70, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (187, '2021-01-14', '2021-01-11', 70, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (177, '2021-01-14', '2021-01-05', 64, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (203, '2021-01-14', '2021-01-13', 62, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (204, '2021-01-14', '2021-01-13', 58, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (211, '2021-01-14', '2021-01-13', 59, 'image', '::ffff:89.146.67.111', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (190, '2021-01-14', '2021-01-12', 61, 'image', '::ffff:89.146.67.111', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (207, '2021-01-14', '2021-01-13', 69, 'image', '127:0:0:1', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (120, '2021-01-14', '2020-12-24', 60, 'image', '127:0:0:1', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (200, '2021-01-14', '2021-01-13', 67, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (196, '2021-01-14', '2021-01-13', 64, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (198, '2021-01-14', '2021-01-13', 60, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (202, '2021-01-14', '2021-01-13', 59, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (130, '2021-01-15', '2020-12-25', 52, 'image', '::ffff:80.80.221.95', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (121, '2021-01-14', '2020-12-24', 62, 'image', '::ffff:80.80.221.95', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (219, '2021-01-14', '2021-01-14', 63, 'image', '::ffff:89.146.67.111', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (212, '2021-01-14', '2021-01-13', 20, 'image', '::ffff:89.146.67.111', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (220, '2021-01-14', '2021-01-14', 46, 'image', '::ffff:127.0.0.1', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (221, '2021-01-14', '2021-01-14', 52, 'image', '::ffff:127.0.0.1', '2021-01-14');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (217, '2021-01-14', '2021-01-14', 63, 'image', '::ffff:127.0.0.1', '2021-01-15');
INSERT INTO public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip, date) VALUES (218, '2021-01-15', '2021-01-14', 70, 'image', '::ffff:89.146.67.111', '2021-01-15');


--
-- Data for Name: Views_stats; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (34, 69, 'image', 11, '2021-01-15', '2021-01-08');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (35, 70, 'image', 15, '2021-01-15', '2021-01-11');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (10, 44, 'image', 3, '2020-07-26', '2020-07-25');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (2, 19, 'image', 1, '2020-05-18', '2020-05-18');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (11, 45, 'image', 16, '2021-01-10', '2020-07-28');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (31, 66, 'image', 1, '2021-01-05', '2021-01-05');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (22, 56, 'image', 21, '2021-01-13', '2020-09-14');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (5, 35, 'image', 2, '2020-07-04', '2020-06-24');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (4, 34, 'image', 2, '2020-07-12', '2020-06-22');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (3, 18, 'image', 2, '2020-07-15', '2020-05-18');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (17, 51, 'image', 1, '2020-08-25', '2020-08-25');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (7, 39, 'image', 1, '2020-07-18', '2020-07-18');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (33, 68, 'image', 9, '2021-01-14', '2021-01-08');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (14, 48, 'image', 22, '2021-01-12', '2020-07-30');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (19, 53, 'image', 3, '2020-08-27', '2020-08-27');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (20, 54, 'image', 23, '2021-01-12', '2020-09-03');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (8, 42, 'image', 1, '2020-07-23', '2020-07-23');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (9, 43, 'image', 1, '2020-07-23', '2020-07-23');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (16, 50, 'image', 13, '2021-01-12', '2020-08-25');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (27, 61, 'image', 21, '2021-01-14', '2020-12-23');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (13, 47, 'image', 11, '2021-01-12', '2020-07-30');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (6, 36, 'image', 41, '2021-01-13', '2020-07-15');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (15, 49, 'image', 39, '2021-01-13', '2020-08-04');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (21, 55, 'image', 18, '2021-01-13', '2020-09-09');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (12, 46, 'image', 14, '2021-01-14', '2020-07-29');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (32, 67, 'image', 11, '2021-01-14', '2021-01-06');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (30, 64, 'image', 11, '2021-01-14', '2021-01-05');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (29, 63, 'image', 16, '2021-01-14', '2021-01-04');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (28, 62, 'image', 48, '2021-01-14', '2020-12-24');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (26, 60, 'image', 23, '2021-01-14', '2020-12-23');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (25, 59, 'image', 23, '2021-01-14', '2020-12-22');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (24, 58, 'image', 20, '2021-01-14', '2020-12-22');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (18, 52, 'image', 25, '2021-01-15', '2020-08-26');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (1, 20, 'image', 66, '2021-01-15', '2020-05-05');
INSERT INTO public."Views_stats" (id, product_id, product_type, views, "updatedAt", "createdAt") VALUES (23, 57, 'image', 31, '2021-01-15', '2020-09-17');


--
-- Data for Name: Сities; Type: TABLE DATA; Schema: public; Owner: travel_blogg_user
--

INSERT INTO public."Сities" (id, name, country_id, "createdAt", "updatedAt") VALUES (2, 'Ташкент', 1, NULL, NULL);
INSERT INTO public."Сities" (id, name, country_id, "createdAt", "updatedAt") VALUES (3, 'Самарканд', 1, NULL, NULL);
INSERT INTO public."Сities" (id, name, country_id, "createdAt", "updatedAt") VALUES (4, 'Хива', 1, NULL, NULL);
INSERT INTO public."Сities" (id, name, country_id, "createdAt", "updatedAt") VALUES (5, 'Москва', 2, NULL, NULL);
INSERT INTO public."Сities" (id, name, country_id, "createdAt", "updatedAt") VALUES (7, 'Питер', 2, NULL, NULL);
INSERT INTO public."Сities" (id, name, country_id, "createdAt", "updatedAt") VALUES (8, 'Куалла Лумпур', 3, NULL, NULL);


--
-- Name: Users_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public."Users_roles_id_seq"', 2, true);


--
-- Name: access_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.access_token_id_seq', 62, true);


--
-- Name: categories_id; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.categories_id', 6, true);


--
-- Name: cities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.cities_id_seq', 8, true);


--
-- Name: comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.comment_id_seq', 5, true);


--
-- Name: countries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.countries_id_seq', 3, true);


--
-- Name: emotions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.emotions_id_seq', 6, true);


--
-- Name: files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.files_id_seq', 917, true);


--
-- Name: firm_moderations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.firm_moderations_id_seq', 1, false);


--
-- Name: firm_pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.firm_pages_id_seq', 1, false);


--
-- Name: firm_services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.firm_services_id_seq', 1, false);


--
-- Name: firm_user_pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.firm_user_pages_id_seq', 1, false);


--
-- Name: firm_user_services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.firm_user_services_id_seq', 1, false);


--
-- Name: firms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.firms_id_seq', 7, true);


--
-- Name: firms_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.firms_id_seq1', 1, false);


--
-- Name: hash_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.hash_tag_id_seq', 160, true);


--
-- Name: messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.messages_id_seq', 1, false);


--
-- Name: rights_blocks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.rights_blocks_id_seq', 5, true);


--
-- Name: tag_links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.tag_links_id_seq', 1586, true);


--
-- Name: tours_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.tours_id_seq', 70, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: users_rights_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.users_rights_id_seq', 7, true);


--
-- Name: views_id_seq; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.views_id_seq', 221, true);


--
-- Name: views_stat_id; Type: SEQUENCE SET; Schema: public; Owner: travel_blogg_user
--

SELECT pg_catalog.setval('public.views_stat_id', 35, true);


--
-- Name: Categories Categories_pkey; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Categories"
    ADD CONSTRAINT "Categories_pkey" PRIMARY KEY (id);


--
-- Name: Category_links Category_links_pkey; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Category_links"
    ADD CONSTRAINT "Category_links_pkey" PRIMARY KEY (id);


--
-- Name: Comments Comments_pkey; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Comments"
    ADD CONSTRAINT "Comments_pkey" PRIMARY KEY (id);


--
-- Name: Emotions Emotions_pkey; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Emotions"
    ADD CONSTRAINT "Emotions_pkey" PRIMARY KEY (id);


--
-- Name: Files Files_pkey; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Files"
    ADD CONSTRAINT "Files_pkey" PRIMARY KEY (id);


--
-- Name: Hash_tags Hash_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Hash_tags"
    ADD CONSTRAINT "Hash_tags_pkey" PRIMARY KEY (id);


--
-- Name: Images I_u_slug; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Images"
    ADD CONSTRAINT "I_u_slug" UNIQUE (slug);


--
-- Name: Tag_links Tag_links_pkey; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Tag_links"
    ADD CONSTRAINT "Tag_links_pkey" PRIMARY KEY (id);


--
-- Name: Categories categories_name; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Categories"
    ADD CONSTRAINT categories_name UNIQUE (name);


--
-- Name: Categories categories_slug; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Categories"
    ADD CONSTRAINT categories_slug UNIQUE (slug);


--
-- Name: Firms firms_pk; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Firms"
    ADD CONSTRAINT firms_pk PRIMARY KEY (id);


--
-- Name: Hash_tags hash_tags_name; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Hash_tags"
    ADD CONSTRAINT hash_tags_name UNIQUE (name);


--
-- Name: Hash_tags hash_tags_slug_uniq; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Hash_tags"
    ADD CONSTRAINT hash_tags_slug_uniq UNIQUE (slug);


--
-- Name: Countries pk_c_id; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Countries"
    ADD CONSTRAINT pk_c_id PRIMARY KEY (id);


--
-- Name: Сities pk_cities_id; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Сities"
    ADD CONSTRAINT pk_cities_id PRIMARY KEY (id);


--
-- Name: Access_tokens pk_id; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Access_tokens"
    ADD CONSTRAINT pk_id PRIMARY KEY (id);


--
-- Name: Rights_blocks pk_r_b_id; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Rights_blocks"
    ADD CONSTRAINT pk_r_b_id PRIMARY KEY (id);


--
-- Name: Images pk_tours_id; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Images"
    ADD CONSTRAINT pk_tours_id PRIMARY KEY (id);


--
-- Name: Users_rights pk_u_r_id; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Users_rights"
    ADD CONSTRAINT pk_u_r_id PRIMARY KEY (id);


--
-- Name: Roles_rights pk_u_r_id_2; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Roles_rights"
    ADD CONSTRAINT pk_u_r_id_2 PRIMARY KEY (id);


--
-- Name: Users pk_users_id; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT pk_users_id PRIMARY KEY (id);


--
-- Name: Users_roles users_roles_pk; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Users_roles"
    ADD CONSTRAINT users_roles_pk PRIMARY KEY (id);


--
-- Name: Views views_pkey; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Views"
    ADD CONSTRAINT views_pkey PRIMARY KEY (id);


--
-- Name: Views_stats views_stat_id_pk; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Views_stats"
    ADD CONSTRAINT views_stat_id_pk PRIMARY KEY (id);


--
-- Name: Views_stats views_stat_uniq; Type: CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Views_stats"
    ADD CONSTRAINT views_stat_uniq UNIQUE (product_id, product_type);


--
-- Name: categories_index_slug; Type: INDEX; Schema: public; Owner: travel_blogg_user
--

CREATE UNIQUE INDEX categories_index_slug ON public."Categories" USING btree (slug);


--
-- Name: comment_product_index; Type: INDEX; Schema: public; Owner: travel_blogg_user
--

CREATE INDEX comment_product_index ON public."Comments" USING btree (product_type, product_id);


--
-- Name: fki_category_link_category_id; Type: INDEX; Schema: public; Owner: travel_blogg_user
--

CREATE INDEX fki_category_link_category_id ON public."Category_links" USING btree (category_id);


--
-- Name: fki_comment_user_id; Type: INDEX; Schema: public; Owner: travel_blogg_user
--

CREATE INDEX fki_comment_user_id ON public."Comments" USING btree (user_id);


--
-- Name: fki_comments_parent_id; Type: INDEX; Schema: public; Owner: travel_blogg_user
--

CREATE INDEX fki_comments_parent_id ON public."Comments" USING btree (parent_id);


--
-- Name: fki_country_id_fk; Type: INDEX; Schema: public; Owner: travel_blogg_user
--

CREATE INDEX fki_country_id_fk ON public."Сities" USING btree (country_id);


--
-- Name: Roles_rights users_rights_users_roles__fk; Type: FK CONSTRAINT; Schema: public; Owner: travel_blogg_user
--

ALTER TABLE ONLY public."Roles_rights"
    ADD CONSTRAINT users_rights_users_roles__fk FOREIGN KEY (role_id) REFERENCES public."Users_roles"(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

